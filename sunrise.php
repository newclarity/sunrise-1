<?php
/*
Plugin Name: Sunrise
Description: Software to Extend WordPress for Professional Website Builders
Version: 0.0.0
Author: Sunrise
Author URI: http://www.getsunrise.com
*/

/**
 * Define SUNRISE_DIR, but only if unit tests didn't pre-define.
 */
if ( ! defined( 'SUNRISE_DIR' ) )
	define( 'SUNRISE_DIR', dirname( __FILE__ ) );

require( SUNRISE_DIR . '/modules/core/sunrise-core.php' );
sr_load_module_dependencies();


