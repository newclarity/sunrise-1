#Post Meta Meeting 
Tuesday December 3rd 2013

## What 
- Sunrise

## Where
- Git on BitBucket
	- https://bitbucket.org/newclarity/sunrise-1

- Example code:
	- https://gist.github.com/mikeschinkel/7775461

## Who
- Mike Schinkel
- Micah Wood (wpcholar)
- Andrey (Rarst) Savchenko
- Taras Mankovski

## Why
- We built a company's products
	- Platform for building large lawfirm website
- Client didn't respond well to: 
	_"Well we can't (easily) to do"_

##Architecture
- Focused on programmer productivity
	- Source code solution for version control
	- Sunrise was on 2nd generation code
	- We are now on 5th generation
		- See: https://github.com/newclarity/exowp
		- Exo is an MVC Skeleton for WordPress, w/mixins
		
- Core OOP layer
	- Fields
	- Forms
	- etc.
- Sitebuilder layer
	- `register_form()`
	- `register_field()`
- Themer layer
	- `get_field()`
	- `the_field()`


## Storage Format 
- For Post Meta Key & Value
- See Navicat Query

![](http://screenshots.newclarity.net/skitched-20131203-142648.png)

##Field Features Objects
- Entry
- Label
- Help
- InfoBox
- Message

##get\_field()/the\_field()
- Simple naming
- Filters: 
	- `'pre_get_field'`
	- `'get_field'`
	- `'empty_field'`
	- `'the_field'`
	
- Virtual fields
	- _"nested dolls"_
- Issue: Return values of string/numeric vs. object/array vs. HTML

##Virtual Fields
- Especially useful to output Schema.org valid markup
- Postal Address: 'postal_address'
	- Stored as:
		- 'street'
		- 'city'
		- 'state'
		- 'zip'
		- 'country'
		
##Storage Types
- Meta storage
- Core storage
- Table storage (custom sql)
- Taxonomy (term) storage
- Future/Plugins
	- Pods
	- MongoDB
	- Redis
	- Amazon S3
	- Other…?

##Dynamic Forms within Forms 
- Form Selector Field
- One field could display a form within a field
- Created a 'form_type' meta field to control which form type

##Repeating Fields
- Overuse leads to madness
- Should support simpler use-cases
- Repeating should be in core framework, not it's own field type
- Complex use-cases should use object/post relationships (hint/hint)

##Other Considerations
- Minimize nested arrays when initializing
- Separate Declaration from Fixup
- Multiple `register_field()` vs. one `register_fields()`
- Then there is MVC + Mixins...

##Demo SQL
SELECT * FROM wp\_postmeta WHERE post\_id=3358

##Demo Code
https://gist.github.com/mikeschinkel/7775461

