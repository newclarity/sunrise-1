<?php

/**
 * Auto detect the underlying testing framework.
 *
 * PHPUnit loads up automatically so the 'PHPUnit_FrameWork_TestCase' class won't be
 * present if we are not running PHPUnit from the command line.
 */
define( 'IS_PHPUNIT', class_exists( 'PHPUnit_Framework_TestCase' ) ? true: false );

if( IS_PHPUNIT ) {

	abstract class Sunrise_Abstract_Test_Case extends PHPUnit_Framework_TestCase {}

} else {

	require_once( SIMPLETEST_FILEPATH );

	/**
	 * The assertion methods in this class exist in PHPUnit, but not in SimpleTest.  In order for these existing
	 * assertion methods to be used in SimpleTest, we implement them here.
	 */
	abstract class Sunrise_Abstract_Test_Case extends UnitTestCase {

		/**
		 * Asserts that $actual is empty.
		 *
		 * @param        $actual
		 * @param string $message	The message to be displayed on failure
		 */
		public function assertEmpty( $actual, $message = 'Failed asserting that value is empty' ) {
			$this->assertTrue( empty( $actual ), $message );
		}

		/**
		 * Asserts that $actual is not empty.
		 *
		 * @param        $actual
		 * @param string $message	The message to be displayed on failure
		 */
		public function assertNotEmpty( $actual, $message = 'Failed asserting that value is not empty' ) {
			$this->assertFalse( empty( $actual ), $message );
		}

		/**
		 * Asserts that $actual is equal to $expected.  Uses the assertEqual() method present in SimpleTest.
		 *
		 * @param        $expected
		 * @param        $actual
		 * @param string $message	The message to be displayed on failure
		 */
		public function assertEquals( $expected, $actual, $message = '' ) {
			$this->assertEqual( $expected, $actual, $message );
		}

		/**
		 * Asserts that $needle exists within the $haystack.
		 *
		 * NOTE: Not implemented in the same way as PHPUnit!
		 *
		 * @param        $needle
		 * @param        $haystack
		 * @param string $message	The message to be displayed on failure
		 */
		public function assertContains( $needle, $haystack, $message = '' ) {
			if( empty( $message ) )
				$message = "Failed asserting that $needle is contained in $haystack";
			$result = strpos( $haystack, $needle );
			$conditional = $result === false ? false: true;
			$this->assertTrue( $conditional, $message );
		}

		/**
		 * Asserts that $string matches RegExp $pattern. Uses the assertPattern() method present in SimpleTest.
		 *
		 * @param string $pattern
		 * @param string $string
		 * @param string $message	The message to be displayed on failure
		 */
		public function assertRegExp( $pattern, $string, $message = '') {
			$this->assertPattern( $pattern, $string, $message );
		}

		/**
		 * Asserts that $actual is an object and is an instance of $expected.
		 *
		 * @param string $expected
		 * @param object $actual
		 * @param string $message	The message to be displayed on failure
		 */
		public function assertInstanceOf( $expected, $actual, $message = '' ) {
			if( empty( $message ) )
				$message = "Failed asserting that value is an instance of $expected";
			$this->assertTrue( $actual instanceof WP_Error, $message );
		}

		/**
		 * Asserts that the data type of $actual matches $expected.
		 *
		 * @param string $expected
		 * @param        $actual
		 * @param string $message	The message to be displayed on failure
		 */
		public function assertInternalType( $expected, $actual, $message = '' ) {
			$type = gettype( $actual );
			if( empty( $message ) )
				$message = "Failed asserting that value of type $type is of type $expected";
			$this->assertTrue( $type === $expected, $message );
		}

		/**
		 * Marks a test as incomplete.
		 *
		 * @param string $message	An explanation of why the test is incomplete
		 */
		public function markTestIncomplete( $message = '' ) {
			$backtrace = debug_backtrace();
			$class = $backtrace[1]['class'];
			$function = $backtrace[1]['class'];
			$file = $backtrace[0]['file'];
			echo "<strong>Test Marked Incomplete:</strong> {$class}->{$function} in [$file]<br />";
			if( ! empty( $message ) )
				echo $message . '<br />';
			return;
		}

	}

}

/**
 * All test classes utilizing the Sunrise Test Framework must extent the Sunrise_Test_Case class.
 */
abstract class Sunrise_Test_Case extends Sunrise_Abstract_Test_Case {

	protected $test_framework;

	function setUp() {
		$this->test_framework = IS_PHPUNIT ? 'PHPUnit': 'SimpleTest';
		$this->clean_up_global_scope();
	}

	function clean_up_global_scope() {
		$_GET = array();
		$_POST = array();
		$this->flush_cache();
	}

	function flush_cache() {
		global $wp_object_cache;
		$wp_object_cache->group_ops = array();
		$wp_object_cache->stats = array();
		$wp_object_cache->memcache_debug = array();
		$wp_object_cache->cache = array();
		if ( method_exists( $wp_object_cache, '__remoteset' ) ) {
			$wp_object_cache->__remoteset();
		}
		wp_cache_flush();
	}

	function go_to( $url ) {
		/**
		 * Note: the WP and WP_Query classes like to silently fetch parameters
		 * from all over the place (globals, GET, etc), which makes it tricky
		 * to run them more than once without very carefully clearing everything.
		 */
		$_GET = $_POST = array();
		foreach (array('query_string', 'id', 'postdata', 'authordata', 'day', 'currentmonth', 'page', 'pages', 'multipage', 'more', 'numpages', 'pagenow') as $v) {
			if ( isset( $GLOBALS[$v] ) ) unset( $GLOBALS[$v] );
		}
		$parts = parse_url($url);
		if ( isset($parts['scheme']) && isset( $parts['path'] ) ) {
			$req = $parts['path'];
			if ( isset($parts['query']) ) {
				$req .= '?' . $parts['query'];
				// parse the url query vars into $_GET
				parse_str($parts['query'], $_GET);
			}
		} else {
			$req = $url;
		}
		if ( ! isset( $parts['query'] ) ) {
			$parts['query'] = '';
		}

		$_SERVER['REQUEST_URI'] = $req;
		unset($_SERVER['PATH_INFO']);

		$this->flush_cache();
		unset($GLOBALS['wp_query'], $GLOBALS['wp_the_query']);
		$GLOBALS['wp_the_query'] = new WP_Query();
		$GLOBALS['wp_query'] =& $GLOBALS['wp_the_query'];
		$GLOBALS['wp'] = new WP();

		// clean out globals to stop them polluting wp and wp_query
		foreach ($GLOBALS['wp']->public_query_vars as $v) {
			unset($GLOBALS[$v]);
		}
		foreach ($GLOBALS['wp']->private_query_vars as $v) {
			unset($GLOBALS[$v]);
		}

		$GLOBALS['wp']->main($parts['query']);
	}

	/**
	 * Asserts that $actual is a string.
	 * 
	 * @param        $actual
	 * @param string $message	The message to be displayed on failure
	 */
	public function assertString( $actual, $message = '' ) {
		$this->assertInternalType( 'string', $actual, $message );
	}

	/**
	 * Asserts that $actual is an array. 
	 * 
	 * @param        $actual
	 * @param string $message	The message to be displayed on failure
	 */
	public function assertArray( $actual, $message = '' ) {
		$this->assertInternalType( 'array', $actual, $message );
	}

	/**
	 * Asserts that $actual is an object.
	 * 
	 * @param        $actual
	 * @param string $message	The message to be displayed on failure
	 */
	public function assertObject( $actual, $message = '' ) {
		$this->assertInternalType( 'object', $actual, $message );
	}

	/**
	 * Asserts that $actual is an instance of WP_Error
	 *
	 * @param object $actual
	 * @param string $message	The message to be displayed on failure
	 */
	public function assertWPError( $actual, $message = '' ) {
		$this->assertInstanceOf( 'WP_Error', $actual, $message );
	}

	/**
	 * Asserts that a string contains HTML
	 *
	 * @param string $actual
	 * @param string $message	The message to be displayed on failure
	 */
	public function assertHTML( $actual, $message = 'Failed asserting that value contains HTML' ) {
		$isHTML = (boolean) preg_match( '#<[^>]*>#', $actual );
		$this->assertTrue( $isHTML, $message );
	}

	/**
	 * Asserts that a string does not contain HTML.
	 *
	 * @param string $actual
	 * @param string $message	The message to be displayed on failure
	 */
	public function assertNotHTML( $actual, $message = 'Failed asserting that value is not HTML' ) {
		$isHTML = (boolean) preg_match( '#<[^>]*>#', $actual );
		$this->assertFalse( $isHTML, $message );
	}

	/**
	 * Asserts that $actual contains an enclosing HTML element with the name $tag. ( e.g. div )
	 *
	 * Example: $this->assertEnclosingElement( 'div', '<div><p>Text Content</p></div>' );
	 *
	 * @param string $tag		The tag name of the HTML element for which to check ( e.g. div )
	 * @param string $actual	The actual value to check against
	 * @param string $message	The message to be displayed on failure
	 */
	public function assertEnclosingElement( $tag, $actual, $message = '' ) {
		if( empty( $message ) )
			$message = "Failed asserting that <$tag> element was present";
		$pattern = '#<'.$tag.'[^>]*>.*</'.$tag.'>#s';
		$this->assertTrue( (boolean) preg_match( $pattern, $actual ), $message );
	}

	/**
	 * Asserts that $actual contains an empty HTML element with the name $tag. ( e.g. img )
	 *
	 * Example: $this->assertEmptyElement( 'br', '<p>This is really, really<br /> long content.</p>' );
	 *
	 * @param string $tag		The tag name of the HTML element for which to check ( e.g. img )
	 * @param string $actual	The actual value to check against
	 * @param string $message	The message to be displayed on failure
	 */
	public function assertEmptyElement( $tag, $actual, $message = '' ) {
		if( empty( $message ) )
			$message = "Failed asserting that <$tag /> element was present";
		$pattern = '#<'.$tag.'[^>]*\/>#';
		$this->assertTrue( (boolean) preg_match( $pattern, $actual ), $message );
	}

	/**
	 * Asserts that $actual contains an enclosing HTML element with the name $tag that contains $content.
	 *
	 * Example: $this->assertElementContains( 'a', 'Anchor Text', '<a href="#">Anchor Text</a>' );
	 *
	 * @param string $tag		The tag name of the HTML element ( e.g. div )
	 * @param string $content	The content expected to be inside the HTML element
	 * @param string $actual	The actual value to check against
	 * @param string $message	The message to be displayed on failure
	 */
	public function assertElementContains( $tag, $content, $actual, $message = '' ) {
		if( empty( $message ) )
			$message = "Failed asserting that <$tag> element contained '$content'";
		$pattern = '#<'.$tag.'[^>]*>'.$content.'</'.$tag.'>#s';
		$this->assertTrue( (boolean) preg_match( $pattern, $actual ), $message );
	}

	/**
	 * Asserts that $actual contains an HTML element with the name $tag that has a specific $attribute.
	 *
	 * Example: $this->assertElementHasAttribute( 'a', 'href', '<a href="#">Anchor Text<a>' );
	 *
	 * @param string $tag		The tag name of the HTML element ( e.g. a )
	 * @param string $attribute	The name of the HTML attribute for which to check ( e.g. href )
	 * @param string $actual	The actual value to check against
	 * @param string $message	The message to be displayed on failure
	 */
	public function assertElementHasAttribute( $tag, $attribute, $actual, $message = '' ) {
		if( empty( $message ) )
			$message = "Failed asserting that <$tag> element has $attribute attribute";
		$pattern = '#<'.$tag.'[^>]*'.$attribute.'[^>]*>#';
		$this->assertTrue( (boolean) preg_match( $pattern, $actual ), $message );
	}

	/**
	 * Asserts that $actual contains an HTML element with the name $tag that has an $attribute with the expected $value.
	 *
	 * Example: $this->assertElementAttributeEquals( 'a', 'href', '#', '<a href="#">Anchor Text</a>' );
	 *
	 * @param string $tag		The tag name of the HTML element ( e.g. a )
	 * @param string $attribute	The name of the HTML attribute to compare ( e.g. href )
	 * @param string $value		The expected value of the HTML attribute
	 * @param string $actual	The actual value to check against
	 * @param string $message	The message to be displayed on failure
	 */
	public function assertElementAttributeEquals( $tag, $attribute, $value, $actual, $message = '' ) {
		if( empty( $message ) )
			$message = "Failed asserting that <$tag> element has $attribute attribute equal to $value";
		$pattern = '#<'.$tag.'[^>]*'.$attribute.'=[\'"]'.$value.'[\'"][^>]*>#';
		$this->assertTrue( (boolean) preg_match( $pattern, $actual ), $message );
	}

	/**
	 * Asserts that $actual contains an HTML element with a specific $start tag and $end tag.
	 *
	 * Example: $this->assertElementTags( '<h2>', '</h2>', '<h2><a name="link">Heading</a></h2>' );
	 *
	 * @param string $start		The start tag of the HTML element
	 * @param string $end		The end tag of the HTML element
	 * @param string $actual	The actual value to check against
	 * @param string $message	The message to be displayed on failure
	 */
	public function assertElementTags( $start, $end, $actual, $message ='' ) {
		if( empty( $message ) )
			$message = "Failed asserting that a $start$end element exists";
		$start = preg_replace( '/#/', '\#', $start );
		$pattern = empty( $end ) ? '#'.$start.'#' : '#'.$start.'.*'.$end.'#s';
		$match = preg_match( $pattern, $actual );
		$this->assertTrue( (boolean) $match, $message );
	}

	/**
	 * Asserts that the number of HTML elements within $actual that have a specific $tag
	 * will $compare to the expected $value.
	 *
	 * Example: $this->assertElementCount( '</li>', 3, '<ul><li>Item 1</li><li>Item 2</li><li>Item 3</li></ul>' );
	 *
	 * @param string $tag		The HTML tag for which to look ( e.g. '</a>' )
	 * @param string $value		The number of times the HTML tag is expected to appear
	 * @param string $actual	The actual value to inspect
	 * @param string $compare	The comparison to use, defaults to '=='
	 * @param string $message	The message to be displayed on failure
	 */
	public function assertElementCount( $tag, $value, $actual, $compare = '==', $message = '' ) {
		if( empty( $message ) )
			$message = "Failed asserting that <$tag> element count is $compare $value";
		$count = $this->get_element_count( $tag, $actual );
		$result = $count && version_compare( $value, $actual, $compare );
		$this->assertTrue( $result, $message );
	}

	/**
	 * Asserts that HTML element $tag1 occurs the same number of times as HTML element $tag2 within $actual.
	 *
	 * Example: $this->assertEqualElementCount( '</li>', '</a>', '<li><a>Link 1</a></li><li><a>Link 2</a></li>' );
	 *
	 * @param string $tag1		The HTML tag of the first element ( e.g. '</li>' )
	 * @param string $tag2		The HTML tag of the second element ( e.g. '</a>' )
	 * @param string $actual	The actual value to check against
	 * @param string $message	The message to be displayed on failure
	 */
	public function assertEqualElementCount( $tag1, $tag2, $actual, $message = '' ) {
		if( empty( $message ) )
			$message = "Failed asserting that the number of $tag1 element tags is equal to the number of $tag2 element tags";
		$element1_count = $this->get_element_count( $tag1, $actual );
		$element2_count = $this->get_element_count( $tag2, $actual );
		$this->assertTrue( $element1_count == $element2_count, $message );
	}

	/**
	 * A helper function that counts the occurrences of an HTML $tag within a specific $value.
	 *
	 * Example: $this->get_element_count( '</div>', '<div id="wrap"><div class="inner">Content</div></div>' );
	 * Result: 2
	 *
	 * @param string $tag	The HTML tag for which to check ( e.g. </div> )
	 * @param string $value	The text string that is expected to contain a specific element
	 *
	 * @return int			Returns the number of times that the HTML tag was found
	 */
	public function get_element_count( $tag, $value ) {
		$count = preg_match_all( '#'.$tag.'#', $value, $matches );
		return $count ? $count : 0;
	}

	/**
	 * A helper function that fetches the content of a specific HTML element by $tag name within the $subject.
	 *
	 * Example: $this->get_element_content( 'div', '<div><p>Text Content</p></div>' );
	 *
	 * NOTE: This will not work properly when more than one element with the same HTML tag name exist.
	 *
	 * @param string $tag		The HTML tag name from which to pull the contents ( e.g. div )
	 * @param string $subject	The text string that is expected to contain a specific element
	 *
	 * @return mixed			Returns the contents of the HTML element, or false on failure
	 */
	public function get_element_contents( $tag, $subject ) {
		$pattern = '#<'.$tag.'[^>]*>(.*)</'.$tag.'>#';
		preg_match( $pattern, $subject, $matches );
		return isset( $matches[1] ) ? $matches[1] : false;
	}

	/**
	 * A helper function that fetches the content of a specific HTML within the $subject based
	 * on specific HTML $start and $end tags.
	 *
	 * Example: $this->get_element_contents_by_tags( '<p class="text">', '</p>', '<div class="wrap"><p class="text">Text content</p></div>' );
	 *
	 * NOTE: This will not work when there are nested elements with the same start or end tags.
	 *
	 * @param string $start		The starting HTML tag ( e.g. <div> )
	 * @param string $end		The ending HTML tag ( e.g. </div> )
	 * @param string $subject	The text string that is expected to contain a specific element
	 *
	 * @return mixed			Returns the contents of the HTML element, or false on failure
	 */
	public function get_element_contents_by_tags( $start, $end, $subject ) {
		$pattern = '#'.$start.'(.*)'.$end.'#';
		preg_match( $pattern, $subject, $matches );
		return isset( $matches[1] ) ? $matches[1] : false;
	}

	/**
	 * A helper function that fetches the value of an $attribute for an HTML element with a specific
	 * $tag name within the $subject.
	 *
	 * Example: $this->get_attribute_value( 'div', 'class', '<div class="wrap"><p class="text">Text content</p></div>' );
	 *
	 * NOTE: This will not work properly when two elements by the same name have the same attribute.
	 *
	 * @param string $tag		The name of the HTML tag ( e.g. div )
	 * @param string $attribute	The name of the HTML attribute for which to fetch the value ( e.g. class )
	 * @param string $subject	The text string that is expected to contain a specific element
	 *
	 * @return mixed			Returns the contents of the HTML attribute, or false on failure
	 */
	public function get_attribute_value( $tag, $attribute, $subject ) {
		$pattern = '#<'.$tag.'[^>]*'.$attribute.'=[\'"]([^\'"]*)[\'"][^>]*>#';
		preg_match( $pattern, $subject, $matches );
		return isset( $matches[1] ) ? $matches[1] : false;
	}

}