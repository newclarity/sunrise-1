<?php

/**
 * WP_TESTS_DOMAIN is ONLY needed for running tests outside of the browser, i.e. PHPUnit
 */
// define( 'WP_TESTS_DOMAIN', 'mydomain.dev' );

/**
 * If different from the values in your wp-config.php file, define the database info to be used for your tests.
 */
//define( 'DB_NAME', '' );
//define( 'DB_USER', '' );
//define( 'DB_PASSWORD', '' );