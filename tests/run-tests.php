<?php

/**
 * Bootstrap file to load up the WordPress test environment
 */

/**
 * Set the ABSPATH constant
 */
if( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', strstr( __DIR__, '/wp-content', true ) . '/' );

/**
 * Detect if the run-tests.php file was called directly or included as part of a regular test run.
 * In the case of a direct load, we need to determine what tests to actually run.
 */
$direct_load_path = isset( $direct_load_path ) ? $direct_load_path : false;
$test_file = isset( $test_file ) ? $test_file : false;

if( strpos( __FILE__, $_SERVER['PHP_SELF'] ) !== false ) {
	$file = filter_input( INPUT_GET, 'file', FILTER_SANITIZE_URL );
	$test_file = $file && file_exists( __DIR__ . $file ) ? __DIR__ . $file : false;
	$direct_load_path = __DIR__;
}

/**
 * Locate our sunrise-tests-config.php file, if it exists
 */
$config_filepath = isset( $config_filepath ) ? $config_filepath : false;
$have_config_file = $config_filepath;
$config_filename = 'sunrise-tests-config.php';
if( ! $config_filepath ) {
	if( file_exists( ABSPATH . $config_filename ) ) {
		$config_filepath = ABSPATH . $config_filename;
	} else if( file_exists( dirname( ABSPATH ) . '/' . $config_filename ) ) {
		$config_filepath = dirname( ABSPATH ) . '/' . $config_filename;
	}
}

/**
 * If we have a new config file, make sure we can read it, then require it
 */
if( ! $have_config_file && $config_filepath ) {
	if( is_readable( $config_filepath ) ) {
		require( $config_filepath );
	} else {
		$error = <<<HTML
<p>
	<strong>ERROR:</strong> <code>$config_filepath</code> is not readable!  Please check the file permissions and owner.
</p>
HTML;
		die( $error );
	}
}

/**
 * If no testing config file has been setup, use the default wp-config.php file
 */
if( ! $config_filepath ) {
	if( file_exists( ABSPATH . 'wp-tests-config.php' ) ) {
		$config_filename = 'wp-tests-config.php';
		require( ABSPATH . $config_filename );
	} else {
		$config_filename = 'wp-config.php';
		require( ABSPATH . $config_filename );
	}
}

/**
 * Enable the display of ALL errors
 */
error_reporting( -1 );
ini_set( 'display_errors', 1 );

if( ! defined( 'WP_DEBUG' ) )
	define( 'WP_DEBUG', true );

/**
 * Remove any memory limits, since tests take up much more than normal
 */
ini_set( 'memory_limit', -1 );

if( ! defined( 'WP_MEMORY_LIMIT' ) )
	define( 'WP_MEMORY_LIMIT', -1 );

if( ! defined( 'WP_MAX_MEMORY_LIMIT' ) )
	define( 'WP_MAX_MEMORY_LIMIT', -1 );

/**
 * Make sure we know where SimpleTest is installed
 */
if( ! defined( 'SIMPLETEST_FILEPATH' ) ) {
	define( 'SIMPLETEST_FILEPATH', ABSPATH . '/wp-content/libraries/simpletest/autorun.php' );
	if( ! file_exists( SIMPLETEST_FILEPATH ) ) {
		$error = <<<HTML
<p>
	<strong>ERROR:</strong> SimpleTest may not be installed or may be in a custom location.  Please define the <code>SIMPLETEST_FILEPATH</code> constant in <code>$config_filename</code>
</p>
HTML;
		die( $error );
	}
}

/*
 * Globalize some WordPress variables, because PHPUnit loads this file inside a function
 * See: https://github.com/sebastianbergmann/phpunit/issues/325
 *
 * These are not needed for WordPress 3.3+, only for older versions
 */
global $table_prefix, $wp_embed, $wp_locale, $_wp_deprecated_widgets_callbacks, $wp_widget_factory;

/**
 * These are still needed
 */
global $wpdb, $current_site, $current_blog, $wp_rewrite, $shortcode_tags, $wp;

/**
 * Load the Sunrise test framework
 */
require( __DIR__ . '/sunrise-test-framework.php' );

/**
 * Cron tries to make an HTTP request to the blog, which always fails when tests are run in CLI mode
 */
if( IS_PHPUNIT && ! defined( 'DISABLE_WP_CRON' ) )
	define( 'DISABLE_WP_CRON', true );

/**
 * Ensure that we have a domain from which tests are run
 */
if( ! defined( 'WP_TESTS_DOMAIN' ) && isset( $_SERVER['HTTP_HOST'] ) )
	define( 'WP_TESTS_DOMAIN', $_SERVER['HTTP_HOST'] );

if( ! defined( 'WP_TESTS_DOMAIN' ) ) {
	$error = <<<HTML
<p>
	<strong>ERROR:</strong> The <code>WP_TESTS_DOMAIN</code> constant is not defined in <code>$config_filename</code>
</p>
HTML;
	die( $error );
}

/**
 * Set common defaults so that we don't have to have these in our test configuration
 */
if( empty( $table_prefix ) )
	$table_prefix  = 'wp_';
if( ! defined( 'DB_HOST' ) )
	define( 'DB_HOST', 'localhost' );
if( ! defined( 'DB_CHARSET' ) )
	define( 'DB_CHARSET', 'utf8' );
if( ! defined( 'DB_COLLATE' ) )
	define( 'DB_COLLATE', '' );
if( ! defined( 'WPLANG' ) )
	define( 'WPLANG', '' );

/**
 * Set the location of WordPress
 */
if( ! defined( 'WP_HOME' ) )
	define( 'WP_HOME', 'http://' . WP_TESTS_DOMAIN );
if( ! defined( 'WP_SITEURL' ) )
	define( 'WP_SITEURL', 'http://' . WP_TESTS_DOMAIN );

/**
 * Setup the environment
 */
$_SERVER['SERVER_PROTOCOL'] = 'HTTP/1.1';
$_SERVER['HTTP_HOST'] = WP_TESTS_DOMAIN;
$_SERVER['SERVER_NAME'] = $_SERVER['HTTP_HOST'];
$_SERVER['SERVER_PORT'] = 80;
$PHP_SELF = $GLOBALS['PHP_SELF'] = $_SERVER['PHP_SELF'] = '/index.php';

/**
 * Load WordPress
 */
require_once( ABSPATH . 'wp-settings.php' );

/**
 * Run specific tests, if file was loaded directly and we aren't using PHPUnit.
 */
if( ! IS_PHPUNIT && $direct_load_path && strpos( $direct_load_path, 'sunrise' ) !== false ) {
	if( $test_file )
		require( $test_file );
	/**
	 * If no specific test was required, load them all
	 */
	else {
		/**
		 * Create a new SimpleTest test suite
		 */
		$test_suite = new TestSuite( 'All Tests' );
		/**
		 * Cycle through all the files that end with 'Test.php' and add to our test suite
		 */
		$Directory = new RecursiveDirectoryIterator( $direct_load_path );
		$It = new RecursiveIteratorIterator( $Directory );
		$Regex = new RegexIterator( $It, '#.*Test\.php$#', RecursiveRegexIterator::GET_MATCH );
		foreach( $Regex as $test_case ){
			if( is_readable( $test_case[0] ) )
				$test_suite->addFile( $test_case[0] );
		}
		/**
		 * Run our test suite
		 */
		$test_suite->run( new SimpleReporter() );
	}
}
