<?php

require_once( strstr( __DIR__, '/tests', true ) . '/tests/run-tests.php' );

class Sunrise_Test extends Sunrise_Test_Case {

	function setUp() {
		parent::setUp();
		$this->go_to( home_url() );
	}

	function test_runs_successfully() {
		$this->assertTrue( true );
	}

	function test_wp_tests_config_file_is_loaded() {
		$this->assertTrue( defined('DB_NAME'), "The wp-tests-config.php file isn't loaded." );
	}

	function test_wordpress_is_loaded() {
		$this->assertTrue( function_exists('is_admin'), "WordPress isn't loaded." );
	}

	function test_sunrise_is_loaded() {
		$this->assertTrue( defined('SUNRISE_DIR'), "Sunrise isn't loaded." );
	}

	function test_sunrise_fields_is_loaded() {
		$this->assertTrue( defined('SUNRISE_FIELDS_DIR'), "The Sunrise fields module isn't loaded." );
	}

	function test_sr_forms_global_is_not_null() {
		global $sr_forms;
		$this->assertTrue( ! is_null( $sr_forms ), 'The $sr_forms global is null.' );
	}

	function test_sr_fields_global_is_not_null() {
		global $sr_fields;
		$this->assertTrue( ! is_null( $sr_fields ), 'The $sr_fields global is null.' );
	}

}