/*
	jQuery Hooks for WordPress and other core stuff

	Examples:

	// Add three different test actions
	$.wp.addAction( 'test', function() { alert('Foo!'); } );
	$.wp.addAction( 'test', function() { alert('Bar!'); } );
	$.wp.addAction( 'test', function() { alert('Baz!'); } );

	// Remove the first one
	$.wp.removeAction( 'test', 'test_1' );

	// Do the remaining test actions
	$.wp.doAction( 'test' );


	// Add a filter somewhere
	$.wp.addFilter('filterOptions',function(options) {
		// Do stuff here to modify variable options
		return options;
	} );

	// Use the filter here
	options = $.wp.applyFilters('filterOptions',options);

*/
jQuery(document).ready(function($) {
	$.sunrise = {
		name: 'sunrise',
		getViewportDimensions: function() {
			return {width:$(window).width(),height:$(window).height()};
		},
		getInstance: function($object,parentClass) {
			switch (typeof $object) {
				case "string":
					$object = $($object);
					break;
				case "object":
					$object = undefined==$object.jquery?$($object):$object;
					break;
				default:
					$object = $("");
					break;
			}
			if (!$object.hasClass(parentClass)) {
				$object = $object.parents("."+parentClass);
			}
			return {
				namespace: $.sunrise,
				parent: null,
				objectClass: parentClass,
				$object: $object
			}
		},
		getObjectType: function() {
			return $('#object_type').val();
		},
		getObjectSubType: function() {
			return $('#object_sub_type').val();
		},
		getObjectId: function() {
			return $('#object_id').val();
		},
		ajaxPost: function(data, onSuccess) {
			$.post(ajaxurl,data,function(response) {
				if (undefined!=onSuccess) {
					onSuccess(response,data);
				}
			});
		},
		/**
		 * Implement a WordPress-link Hook System for Javascript Below
		 * TODO (mikes 2012-01-27): Change 'tag' to 'args', allow number (priority), string (tag), object (priority+tag)
		 */
		hooks: { action: {}, filter: {} },
		addAction: function( action, callable, tag ) {
			jQuery.sunrise.addHook( 'action', action, callable, tag );
		},
		addFilter: function( action, callable, tag ) {
			jQuery.sunrise.addHook( 'filter', action, callable, tag );
		},
		doAction: function( action, args ) {
			jQuery.sunrise.doHook( 'action', action, null, args );
		},
		applyFilters: function( action, value, args ) {
			return jQuery.sunrise.doHook( 'filter', action, value, args );
		},
		removeAction: function( action, tag ) {
			jQuery.sunrise.removeHook( 'action', action, tag );
		},
		removeFilter: function( action, tag ) {
			jQuery.sunrise.removeHook( 'filter', action, tag );
		},
		addHook: function( hookType, action, callable, tag ) {
			if ( undefined == jQuery.sunrise.hooks[hookType][action] ) {
				jQuery.sunrise.hooks[hookType][action] = [];
			}
			var hooks = jQuery.sunrise.hooks[hookType][action];
			if ( undefined == tag ) {
				tag = action + '_' + hooks.length;
			}
			jQuery.sunrise.hooks[hookType][action].push( { tag:tag, callable:callable } );
		},
		doHook: function( hookType, action, value, args ) {
			if ( undefined != jQuery.sunrise.hooks[hookType][action] ) {
				var hooks = jQuery.sunrise.hooks[hookType][action];
				for( var i=0; i<hooks.length; i++) {
					if ( 'action'==hookType ) {
						hooks[i].callable(args);
					} else {
						value = hooks[i].callable(value, args);
					}
				}
			}
			if ( 'filter'==hookType ) {
				return value;
			}
		},
		removeHook: function( hookType, action, tag ) {
			if ( undefined != jQuery.sunrise.hooks[hookType][action] ) {
				var hooks = jQuery.sunrise.hooks[hookType][action];
				for( var i=hooks.length-1; i>=0; i--) {
					if (undefined==tag||tag==hooks[i].tag)
						hooks.splice(i,1);
					}
				}
			}
	}
});
