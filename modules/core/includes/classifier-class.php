<?php
/**
 * TODO (mikes 2012-06-16): This it to be used when we refactor to remove 'object_type' and 'object_sub_type'
 */

class Sunrise_Classifier implements ArrayAccess {
  var $type;
  var $subtype;

  /**
   *  Constructs a new object type.  Can be called one of the following ways:
   *
   *  $classifer = new Sunrise_Classifier( "post/person" );
   *  $classifer = new Sunrise_Classifier( array( 'post', 'person' ) );
   *  $classifer = new Sunrise_Classifier( 'post', 'person' );
   *
   */
  function __construct() {
    if ( func_num_args() ) {
      $this->set( func_get_args() );
    }
  }

  /**
   *  Initializes a newly created object type. Can be called one of the following ways:
   *
   *  $classifer->set( "post/person" );
   *  $classifer->set( array( 'post', 'person' ) );
   *  $classifer->set( 'post', 'person' );
   *
   * @param string|array $arg1 An object classifier or an object type
   * @param string|boolean $arg2 An object subtype
   */
  function set( $arg1, $arg2 = false ) {
    if ( ! $arg2 ) {
      if ( is_string( $arg1 ) ) {
        list($this->type, $this->subtype ) = explode( '/', "{$arg1}/" );
      } else if ( is_array( $arg1 ) && $arg2 ) {
        list( $this->type, $this->subtype ) = $arg1;
      }
    } else if ( $arg2 ) {
      list( $this->type, $this->subtype ) = array( $arg1, $arg2 );
    }
    if ( empty( $this->subtype ) ) {
      $this->subtype = null;
    }
  }
  function offsetUnset( $key ) {
    if ( $this->offsetExists( $key ) )
      $this->$key = null;
  }

  function offsetSet( $key, $value ) {
    if ( ! $this->offsetExists( $key ) )
      throw new Exception( sprintf( __( "Offset for ['%s'] doesn't exist on assignment", 'sunrise' ), $key ) );
    $this->$key = $value;
  }

  function offsetGet( $key ) {
    if ( ! $this->offsetExists( $key ) )
      throw new Exception( sprintf( __( "Offset for ['%s'] doesn't exist on access.", 'sunrise' ), $key ) );
    return $this->$key;
  }

  function offsetExists( $key ) {
    return property_exists( __CLASS__, $key );
  }

  function __toString() {
    if ( $this->type && $this->subtype )
      $as_string = "{$this->type}/{$this->subtype}";
    else if ( $this->type )
      $as_string = $this->type;
    else
      $as_string = null;
    return $as_string;
  }
}
