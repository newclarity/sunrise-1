<?php
/**
 * Base class to offer common functionality to all Sunrise Instance (vs. Static) classes
 *
 *  @method apply_filters
 */
if ( ! class_exists( 'Sunrise_Base' ) ) {
	/**
	 * @property Sunrise_Base $parent
	 * @property string $module_name
	 * @property string $description
	 */
	class Sunrise_Base {
		var $extra        = array();  // Used to capture $args passed in for which there are no properties
		var $last_error   = false;    // An exception set based on the last try {} catch ($e) {} error
		var $args         = array();  // Use to store original state of args passed in to a __construct()
		var $msg_stack    = array();
		/**
		 * @var bool|Sunrise_Base
		 */
		protected $_parent      = false;    // Used when appropriate;  TODO: Make all other classes use this one.
		protected $_module_name = false;    // Used when appropriate;  TODO: Make all other classes use this one.

		/**
		 * $virtuals are not used yet (much?) but we plan to.
		 *
		 * @var array
		 */
		static $virtuals = array(
			'__get' => false,
			'__set' => false,
			'__call' => false,
			);
		function get_parent() {
			return $this->_parent;
		}
		function set_parent( $parent ) {
			$this->_parent = $parent;
		}
		/**
		 * @param array $args
		 * @return array
		 */
		function expand_args( $args ) {
			return Sunrise::expand_args( $args );
		}
		/**
		 * @param array $args
		 * @return array
		 *
		 * TODO (mikes): Much of this code (label, singular_label) should be merged into a central Sunrise function.
		 *
		 */
		static function normalize_labels( $args ) {

			if ( ! isset( $args['_labels_transformed'] ) ) {

				/**
				 * If 'singular_label' not set uppercase the first word in each of the post type's name,
				 * after converting dashes and underscores to spaces.
				 */
				if ( ! isset( $args['singular_label'] ) ) {
					$post_type = isset( $args['post_type'] ) ? $args['post_type'] : false;
					$args['singular_label'] = ucwords( str_replace( array( '-', '_' ), ' ', $post_type ) );
				}

				/**
				 * If 'label' not set add an 's' to the 'singular_label'.
				 * TODO: Need to consider how to localize this
				 */
				if ( ! isset( $args['label'] ) )
					$args['label'] = "{$args['singular_label']}s";

				$args['_labels_transformed'] = true;
			}
			return $args;
		}
		/*
		 * Construct object from an $args array
		 *
		 * @param array $args An array of name/value pairs that can be used to initialize an object's properties.
		 * @return Sunrise_Base The current object
		 */
		function construct_from_args( $args ) {
			$this_class = get_class( $this );
			if ( property_exists( $this_class, 'args' ) ) {
				$this->args = $args;  // Capture the initial state of the args
			}
			foreach( $args as $name => $value ) {
				if ( property_exists( $this_class, $name ) ) {
					$this->{$name} = $value;
				} else if ( property_exists( $this_class, $property_name = "_{$name}" ) ) {
					$this->{$property_name} = $value;
				} else if ( method_exists( $this, $method_name = "set_{$name}" ) ) {
					call_user_func( array( &$this, $method_name), $value );
				} else if ( '_args_normalized' != $name ) {
					$this->extra[$name] = $value;
				}
			}
			return $this;
		}
		/**
		 * TODO (mikes 2011-12-14): Change this to call with a variable number of arguments.
		 */
		function apply_filters( $filter_name, $value, $args = array() ) {
			$filter_method = "filter_{$filter_name}";
			if ( method_exists( $this, $filter_method ) ) {
				$value = call_user_func( array( &$this, $filter_method), $value, $args );
			}
			if ( is_object( $this->_parent ) ) {
				$filter_method = "filter_child_{$filter_name}";
				$parent = $this->_parent;
				if ( method_exists( $parent, $filter_method ) ) {
					$args['child'] = &$this;
					$value = call_user_func( array( &$this->parent, $filter_method), $value, $args, $this );
				}
			}
			/**
			 * TODO (mikes 2012-06-13): Consider if this is the better way to go?
			 */

			// MHS QUARLES-46
			// Note: (MHS 2013-11-06) Not sure why/when the following line was added/disabled, commented out, or if it was ever in use, but at first glance it seems both class-based filters and global WP filters are ideal.
			// Ideally we want to be able to hook into this entire system from the Client Theme; or at the very least hook into the Fields module.
			// .. e.g., We want to be able to inject HTML into an admin field, override admin dropdown field options, etc., all while being aware of the actual field value (which I believe is currently not possible with the commonly-used register_field_args filter.)
			// .. This seems to be the ideal location to trigger such a filter, however this may or may not violate the design pattern(s) at play in Sunrise.
			// TODO:(MHS 2013-11-06) consider the potential overhead of this filter. Seems to be only a few milliseconds on most pages.
			// TODO:(MHS 2013-11-06) investigate whether this filtering was ever active or in use. If so, we may need to prefix the name of the filter, so as to not inadvertently re-activate/re-enable any old/deprecated usage of this filter which may be lingering in other sites.
			// TODO:(MHS 2013-11-06) possibly make this an optional/configurable step?, e.g., named constant "CLIENT_ENABLE_SUNRISE_WP_APPLY_FILTERS"

			$value = apply_filters( get_class( $this ) . "-{$filter_name}", $value, $args, $this ); // Originally commented out, per mikes comment above.
			
			return $value;
		}
		function do_action( $action_name, $args = array() ) {
			$do_method = "do_{$action_name}";
			if ( method_exists( $this, $do_method ) ) {
				call_user_func( array( &$this, $do_method), $args );
			}
			$parent = $this->parent;
			if ( is_object( $parent ) ) {
				$do_method = "do_child_{$do_method}";
				if ( method_exists( $parent, $do_method ) ) {
					$args['child'] = &$this;
					$call_user_func( array( &$parent, $do_method), $args, $this );
				}
			}
			/**
			 * TODO (mikes 2012-06-13): Consider if this is the better way to go?
			 */
//			do_action( get_class( $this ) . "-{$action_name}", $args, $this );
		}
		/*
		 * Returns array of the property value for this class to be use as arguments to functions that expect array arguments
		 * @return array Property values as an array
		 */
		function as_args() {
			$args = Sunrise::as_args( $this );
			return $args;
		}
		/*
		 * Initializes this instance given an instance of same class or of parent class
		 */
		function initialize_from( $instance ) {

			if ( WP_DEBUG && ! is_a( $this, get_class( $instance ) ) )
				Sunrise::_die( 'Cannot initialize from this object; class [%s] is not a child of [%s].', get_class( $this ), get_class( $instance ) );

			$this->construct_from_args( Sunrise::as_args( $instance ) );

			return $this;  // Enable this method to be chained.
		}
		/**
		 * Returns Sunrise 'Module' name.
		 *
		 * TODO: Must be refactored to be more robust. Currently looks at filename that defined class. Not very robust!
		 *
		 * @return bool|string
		 */
		function get_module_name() {
			static $_classes;
			if ( ! $this->_module_name ) {
				if ( ! isset( $_classes[$class = get_class( $this )] ) ) {
					$reflector = new ReflectionClass( $class );
					$filename = $reflector->getFileName();
					if ( preg_match( '#/sunrise/modules/([^/]+)/#', $filename, $match ) ) {
						$_classes[$class] = $match[1];
					}
				}
				$this->_module_name = $_classes[$class];
			}
			return $this->_module_name;
		}
		function get_ID() {
			sr_trigger_error( __( 'The get_ID() method must be defined in the %s class or one of it\'s parent classes' ), get_class( $this ) );
		}
		/**
		 * @param array|string $properties
 		 * @return string
		 */
		function get_description( $properties ) {
			if ( is_array( $properties ) ) {
				foreach( $properties as $name => $value )
					$properties[$name] = "{$name}= {$value}";
				$properties = implode( ', ', $properties );
			}
			return get_class( $this ) . "[{$properties}]" . ( $this->parent ? ", child of {$this->parent->description}" : '' );
		}
		function _not_assigned_error( $property_name, $fail = false ) {
			if ( WP_DEBUG || $fail  )
				sr_die(  __( 'Property %s->%s not yet assigned a value.' ), get_class( $this ), $property_name );
		}
		function _cannot_get_error( $property_name, $fail = false ) {
			if ( WP_DEBUG || $fail  )
				sr_die(  __( 'Cannot get %s->%s.' ), get_class( $this ), $property_name );
		}
		function _cannot_set_error( $property_name, $value, $fail = false ) {
			if ( WP_DEBUG || $fail ) {
				if ( is_object( $value ) ) {
					$value = '(object)';
				}
				sr_die(  __( 'Cannot set <strong><code>%s->%s</code></strong>. Attempted to set with <strong><code>%s</code></strong>.' ), get_class( $this ), $property_name, (string)$value );
			}
		}
		private function _get_set_not_configured_error( $property_name, $method ) {
			$error_msg = __( 'The class <strong><code>%s</code></strong> does not yet have a virtual method <strong><code>%s()</code></strong> for property <strong><code>%s</code></strong>' );
			sr_die(  $error_msg, get_class( $this ), $method, $property_name );
		}
		function __set( $property_name, $value ) {
			switch ( $property_name ) {
				case 'parent':
					call_user_func( array( &$this, "set_{$property_name}" ), $value );
					break;

				case 'ID':
				case 'module_name':
				case 'description':
				case 'as_args':
					$this->_cannot_set_error( $property_name, $value );
					break;

				default:
					$this->_get_set_not_configured_error( $property_name, __FUNCTION__ );
					break;
			}
		}
		function __get( $property_name ) {
			$value = false;
			switch ( $property_name ) {
				case 'ID':
				case 'parent':
				case 'module_name':
				case 'description':
					$value = call_user_func( array( &$this, "get_{$property_name}" ) );
					break;

				case 'as_args':
					$value = $this->as_args();
					break;

				default:
					$this->_get_set_not_configured_error( $property_name, __FUNCTION__ );
					break;
			}
			return $value;
		}
	}
}
