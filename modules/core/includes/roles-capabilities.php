<?php

function sr_register_roles_capabilities( $args = array() ) {
	$value = false;
	extract( $args );
	if ( isset( $args['roles'] ) ) {
		Sunrise_Roles_Capabilities::register_roles( $roles, $capabilities, $non_capabilities );
	} else 	if ( isset( $args['label'] ) ) {
		Sunrise_Roles_Capabilities::register_role( $name, $label, $capabilities, $non_capabilities );
	} else 	if ( isset( $args['name'] ) ) {
		Sunrise_Roles_Capabilities::register_role_capabilities( $name, $capabilities, $non_capabilities );
	} else {
		Sunrise_Roles_Capabilities::register_capabilities( $capabilities, $non_capabilities );
	}
	return $value;
}
function sr_register_role( $role_name, $role_label, $capabilities = array(), $non_capabilities = array() ) {
	Sunrise_Roles_Capabilities::register_role(  $role_name, $role_label, $capabilities, $non_capabilities );
}
function sr_register_capabilities( $capabilities = array(), $non_capabilities = array() ) {
	Sunrise_Roles_Capabilities::register_capabilities( $capabilities, $non_capabilities );
}
function sr_register_role_capabilities( $role_name, $capabilities = array(), $non_capabilities = array() ) {
	Sunrise_Roles_Capabilities::register_role_capabilities( $role_name, $capabilities, $non_capabilities );
}
function sr_register_roles( $roles, $capabilities = array(), $non_capabilities = array() ) {
	Sunrise_Roles_Capabilities::register_roles( $roles, $capabilities, $non_capabilities );
}

/**
 *
 *
 */
class Sunrise_Roles_Capabilities extends Sunrise_Static_Base {
	/**
	 * @var WP_Roles $_role_object
	 */
	protected static $_roles_object;
	protected static $_capabilities = array();
	protected static $_roles = array();
	protected static $_role_capabilities = array();

	static function on_load() {
		self::$_roles_object = new WP_Roles();
		self::$_roles_object->use_db = false;
		sr_add_action( __CLASS__, 'init', 100 );
	}
	/**
	 * Ensure all the roles and capabilities expected are added after all other inits are run.
	 */
	static function init() {
		/**
		 * TODO (mikes 2012-01-30): Need to implement an 'initialization' mode and
		 * TODO: online run these during initialization.
		 */
		$debug = array(
			self::$_capabilities,
			self::$_roles,
			self::$_role_capabilities,
		);
		self::_add_roles_capabilities( self::$_capabilities );

		foreach( self::$_roles as $role_name => $role_args )
			self::_add_role( $role_name, $role_args['label'], $role_args['capabilities'] );

		foreach( self::$_role_capabilities as $role_name => $capabilities )
			self::_add_role_capabilities( $role_name, $capabilities );

		/**
		 * Write the changes to the database
		 */
		update_option( self::$_roles_object->role_key, self::$_roles_object->roles );

		/**
		 * Clear the memory, don't need anymore
		 */
		self::$_roles = false;
		self::$_capabilities = false;
		self::$_role_capabilities = false;

	}
	protected static function _add_roles_capabilities( $capabilities ) {
		foreach( self::$_roles_object->role_objects as $role )
			self::_add_role_capabilities( $role->name, $capabilities );
	}
	protected static function _add_role_capabilities( $role_name, $capabilities ) {
//		/**
//		 * This is the correct way, but it doesn't work without use_db=true.
//		 */
//		/**
//		 * @var WP_Role $role
//		 */
//		$role = self::$_roles_object->get_role( $role_name );
//		foreach ( $capabilities as $capability => $value ) {
//			if ( $value != $role->has_cap( $capability ) ) {
//				$role->add_cap( $capability, $value );
//			}
		self::$_roles_object->roles[$role_name]['capabilities'] = array_merge( self::$_roles_object->roles[$role_name]['capabilities'], $capabilities );
	}
	protected static function _add_role( $role_name, $role_label, $capabilities ) {
//		/**
//		 * This is the correct way, but it doesn't work without use_db=true.
//		 */
//		if ( ! isset( self::$_roles_object->role_objects[ $role_name ] ) ) {
//			self::$_roles_object->add_role( $role_name, $role_label, $capabilities );
//		} else {
//			self::_add_role_capabilities( $role_name, $capabilities );
//		}
		if ( ! isset( self::$_roles_object->role_objects[ $role_name ] ) )
			self::$_roles_object->add_role( $role_name, $role_label, array() );
		self::_add_role_capabilities( $role_name, $capabilities );
	}
	/**
	 * Register a user role to be added if it doesn't exist.
	 *
	 * Register a user role to be added if it doesn't exist. Accepts  (lowercase identifier) name,
	 * human-readable label, array of capabilities and optional array of non-capabilities (if
	 * numerically indexed), or just an array of capabilities (if associative.)
	 *
	 * @param string $role_name
	 * @param array $capabilities
	 * @param array $non_capabilities
	 * @return array
	 */
	static function register_role_capabilities( $role_name, $capabilities = array(), $non_capabilities = array() ) {
		return self::$_role_capabilities[$role_name] = self::_transform_capabilities( $capabilities, $non_capabilities );
	}
	/**
	 * Register a user role to be added if it doesn't exist.
	 *
	 * Register a user role to be added if it doesn't exist. Accepts  (lowercase identifier) name,
	 * human-readable label, array of capabilities and optional array of non-capabilities (if
	 * numerically indexed), or just an array of capabilities (if associative.)
	 *
	 * @param string $role_name
	 * @param string $role_label
	 * @param array $capabilities
	 * @param array $non_capabilities
	 * @return array
	 */
	static function register_role( $role_name, $role_label, $capabilities = array(), $non_capabilities = array() ) {
		return self::$_roles[$role_name] = array(
			'label' => $role_label,
			'capabilities' => self::_transform_capabilities( $capabilities, $non_capabilities ),
		);
	}
	/**
	 * Register a list of capabilities to be added for all user roles.
	 *
	 * Register a list of capabilities to be added for all user roles. Accepts array of capabilities
	 * and optional array of non-capabilities (if numerically indexed), or just an array of
	 * capabilities (if associative.)
	 *
	 * @param array $capabilities
	 * @param array $non_capabilities
	 * @return array
	 */
	static function register_capabilities( $capabilities = array(), $non_capabilities = array() ) {
		/**
		 * Transform the capabilities, and the non-capabilities if any.
		 * Result will be associative array even if input arrays are numerically indexed.
		 */
		$capabilities = self::_transform_capabilities( $capabilities, $non_capabilities );

		if ( 0 == count( self::$_capabilities ) ) {
			self::$_capabilities = $capabilities;
		} else {
			self::$_capabilities = self::_transform_capabilities( $capabilities, self::$_capabilities );
		}

		return self::$_capabilities;
	}
	static function register_roles( $roles, $capabilities = array(), $non_capabilities = array() ) {
		foreach( $roles as $name => $label )
			self::register_role( $name, $label, $capabilities, $non_capabilities );
	}
	/**
	 * Merge two sets of capabilities.
	 *
	 * Merge two sets of capabilities. Accepts two arrays, if numerically indexed arrays they are assumed to be
	 * $capabilities and $non_capabilities, respectively. If associative arrays, values assumed correct.
	 * If both arrays have same keys, first array wins.
	 *
	 * @param array $capabilities1 - If numerically index, will assume $capabilities (i.e. true)
	 * @param array $capabilities2 - If numerically index, will assume $non_capabilities (i.e. false)
	 * @return array
	 */
	private static function _transform_capabilities( $capabilities1, $capabilities2 ) {
		if ( ! is_array( $capabilities1 ) )
			$capabilities1 = array( $capabilities1 );
		if ( ! is_array( $capabilities2 ) )
			$capabilities2 = array( $capabilities2 );
		$new_capabilities = array();
		foreach( $capabilities1 as $capability => $value ) {
			if ( is_numeric( $capability ) ) {
				$capability = $value;
				$value = true;
			}
			$new_capabilities[$capability] = $value;
		}
		foreach( $capabilities2 as $capability => $value ) {
			if ( is_numeric( $capability ) ) {
				$capability = $value;
				$value = false;
			}
			/**
			 * Only set a capability false if not already set to true.
			 */
			if ( ! isset( $new_capabilities[$capability] ) )
				$new_capabilities[$capability] = $value;
		}
		return $new_capabilities;
	}
}
Sunrise_Roles_Capabilities::on_load();

