<?php

require( sr_admin_path( 'includes/file.php' ) );

if ( ! class_exists( 'Sunrise' ) ) {
	class Sunrise extends Sunrise_Static_Base {

		static $methods = array();
		private static $_valid_types = false;
		private static $_valid_classes = false;
		private static $_context = false;
		private static $_msg_stack = array();
		private static $_ajax_handlers = array();

		static function on_load () {
			self::$_context = new stdClass();
			self::$_context->_initialized = false;
			sr_add_action( __CLASS__, 'init' );
		}

		static function init () {
			$filepath = '/js/sunrise-core.js';
			$src = dirname( plugins_url( '', __FILE__ ) ) . $filepath;
			sr_enqueue_script(
				'sunrise-core',
				$src,
				array(
					'file'         => dirname( dirname( __FILE__ ) ) . $filepath,
					'dependencies' => array( 'jquery' ),
				)
			);
		}

		function __construct () {
			sr_die( __( 'The class [Sunrise] is not meant to be instantiated with the \'new\' keyword.' ) );
		}

		static function add_ajax_handler ( $class, $action ) {
			self::$_ajax_handlers[$action] = $class;
			add_action( "wp_ajax_{$action}", array( __CLASS__, '_ajax_action_handler' ) );
		}

		static function _ajax_action_handler () {
			$class = self::$_ajax_handlers[$_POST['action']];
			$result = false;
			$context = Sunrise::get_context();
			if ( WP_DEBUG && ! isset( $context->sub_action ) ) {
				$result = array(
					'status' => 'error',
					'html'   => sprintf( __( "No 'sub_action' passed via AJAX." ) ),
				);
			}
			if ( ! $result ) {
				$method_name = "sr_ajax_{$context->sub_action}";
				if ( method_exists( $class, $method_name ) ) {
					$result = call_user_func( array( $class, $method_name ), $context );
					$result = array_merge( array( 'status' => 'success' ), $result );
				} else {
					$result = array(
						'status' => 'error',
						'html'   => sprintf( __( "No AJAX Subaction %s." ), $method_name ),
					);
				}
			}
			header( 'Content-type:application/json' );
			echo json_encode( $result );
			die();
		}

		/**
		 * Accept a mixed array: numeric keys and string values or string keys and array values and convert everything to
		 * string keys and array values. For example:
		 *    $input = array( 'foo' => array( 'a'=>1,'b'=>2 ), 'bar', 'baz' );
		 *    $output = $this->expand_args( $args );  => array( 'foo' => array( 'a'=>1,'b'=>2 ), 'bar' => array(), 'baz' => array() );
		 *
		 * @param array $args
		 * @return array
		 */
		static function expand_args ( $args ) {
			$new_args = array();
			foreach ( $args as $key => $arg ) {
				if ( is_numeric( $key ) && is_string( $arg ) ) {
					$key = $arg;
					$arg = array();
				}
				$new_args[$key] = $arg;
			}
			return $new_args;
		}

		/**
		 * @param object $object
		 * @param string $property_name
		 * @param mixed  $value
		 * @param bool   $trigger_error
		 * @return bool|mixed|null
		 */
		static function delegate_set ( $object, $property_name, $value, $trigger_error = false ) {
			if ( is_object( $object ) ) {
				$result = self::delegate( $object, '__set', $property_name, $value );
			} else if ( $trigger_error ) {
				sr_trigger_error( __( 'Not an object when attempting to __set property [%s].' ), $property_name );
			} else {
				$result = null;
			}
		}

		/**
		 * @param object $object
		 * @param string $property_name
		 * @param bool   $trigger_error
		 * @return bool|mixed|null
		 */
		static function delegate_get ( $object, $property_name, $trigger_error = false ) {
			if ( is_object( $object ) ) {
				$result = self::delegate( $object, '__get', $property_name );
			} else if ( $trigger_error ) {
				$result = sr_trigger_error( __( 'Not an object when attempting to __get property [%s].' ), $property_name );
			} else {
				$result = null;
			}
		}

		/**
		 * @param Sunrise_Base $object
		 * @param string       $method
		 * @param string       $property_name
		 * @return mixed|null - MUST return null vs. false because false might be a valid return value; null never is.
		 */
		static private function delegate ( $object, $method, $property_name ) {
			try {
				/**
				 * If our class has set the appropriate virtual list (i.e. if it is an array)
				 * check to see if we should even try. This allows us to limit the virtual methods
				 * available for external calls.
				 */
				$try = true;

				if ( isset( $object->virtuals[$method] ) ) {
					if ( is_array( $object->virtuals[$method] ) ) {
						$try = isset( $object->virtuals[$method][$property_name] );
					}
				}
				if ( $try ) {
					$result = call_user_func_array( array( $object, $method ), array_slice( func_get_args(), 2 ) );
				}
			}
			catch ( Exception $e ) {
				$result = null;
			}
			return $result;
		}

		static function push_message ( $message ) {
			array_push( self::$_msg_stack, $message );
		}

		static function is_valid_type ( $slug_file_group, $type ) {
			return false !== self::$_valid_types[$slug_file_group][$type];
		}

		/**
		 * $valid_classes = array(
		 *  'admin_post' => 'Sunrise_Admin_Post_Form',
		 *  'basic'      => 'Sunrise_Basic_Form',
		 *  )
		 *
		 * @param bool|string $slug_file_group In the format of '{sunrise_plugin}/{sunrise_instance_type}', i.e. 'fields/form'
		 * @return array Contains instance type keys and class name values.
		 */
		static function get_valid_classes ( $slug_file_group = false ) {
			if ( ! $slug_file_group ) {
				$result = self::$_valid_classes;
			} else {
				if ( ! isset( self::$_valid_classes[$slug_file_group] ) ) {
					self::$_valid_classes[$slug_file_group] = array_flip( self::get_valid_types( $slug_file_group ) );
				}
				$result = self::$_valid_classes[$slug_file_group];
			}
			return $result;
		}

		static function get_all_valid_types () {
			return self::$_valid_types;
		}

		/**
		 * $valid_types = array(
		 *  'Sunrise_Admin_Post_Form' => 'admin_post',
		 *  'Sunrise_Basic_Form'      => 'basic',
		 *  )
		 *
		 * @param bool|string $slug_file_group In the format of '{sunrise_plugin}/{sunrise_instance_type}', i.e. 'fields/form'
		 * @return array Contains class name keys and instance type values.
		 * @requires sr_admin_path( 'includes/file.php' )
		 */
		static function get_valid_types ( $slug_file_group = false ) {
			if ( ! $slug_file_group ) {
				$result = self::$_valid_types;
			} else {
				if ( ! isset( self::$_valid_types[$slug_file_group] ) ) {
					if ( ! self::$_valid_types ) {
						self::$_valid_types = array( $slug_file_group => array() );
					} else {
						self::$_valid_types[$slug_file_group] = array();
					}
					$file_group = explode( '/', $slug_file_group );
					$file_group = array_pop( $file_group );
					$ucfirst_file_group = ucfirst( $file_group );
					$fulldir = dirname( dirname( dirname( __FILE__ ) ) ) . "/{$slug_file_group}-types";
					$files = list_files( $fulldir );
					foreach ( $files as $file ) {
						if ( preg_match( "#/([^/]*)-{$file_group}.php$#", $file, $match ) ) {
							$type = sr_underscorize( $match[1] );
							$class_name = explode( '_', sr_underscorize( "Sunrise_{$type}_{$ucfirst_file_group}" ) );
							foreach ( $class_name as $key => $part )
								$class_name[$key] = ucfirst( $part );
							self::$_valid_types[$slug_file_group][implode( '_', $class_name )] = $type;
						}
					}
				}
				$result = self::$_valid_types[$slug_file_group];
			}
			return $result;
		}

		static function _die ( $error_msg ) {
			$args = func_get_args();
			unset( $args[0] );
			$error_msg = vsprintf( __( $error_msg ), $args );
			sr_die( $error_msg );
		}

		static function renormalize_args ( $args, $short_hands ) {
			unset( $args['_args_normalized'] );
			return self::normalize_args( $args, $short_hands );
		}

		static function normalize_args ( $args, $short_hands ) {
			if ( ! isset( $args['_args_normalized'] ) ) {
				foreach ( $short_hands as $short_hand => $long_hand ) {
					if ( ! isset( $args[$long_hand] ) || is_null( $args[$long_hand] ) ) {
						$args[$long_hand] = isset( $args[$short_hand] ) ? $args[$short_hand] : null;
						unset( $args[$short_hand] );
					}
					if ( isset( $args[$short_hand] ) && $args[$short_hand] == $args[$long_hand] ) {
						unset( $args[$short_hand] );
					}
				}
				$args['_args_normalized'] = true;
			}
			return $args;
		}

		static function set_context_vars ( $additional_context, $overwrite = true ) {
			self::$_context = sr_set_object_vars( self::$_context, $additional_context, $overwrite );
		}

		/**
		 * Return a context object that can be used throughout a site to determine current context.
		 * TODO: Lots of work to do on this. Add to it as needed.
		 *
		 * @return SR_DOCS_Context
		 */
		static function get_context () {
			if ( ! self::$_context->_initialized ) {
				global $pagenow, $typenow;
				/**
				 * Set up a content object with a default value of false for every 'standard' context property
				 *
				 * @var SR_DOCS_Context $context
				 */
				$context = (array) sr_set_object_vars(
					self::$_context,
					array(
						'pagenow'         => $pagenow,
						'typenow'         => $typenow,
						'page'            => false,
						'action'          => false,
						'post_id'         => false,
						'post'            => false,
						'post_type'       => $typenow,
						'page_type'       => get_page_type(),
						'parent'          => false,
						'post_parent'     => false,
						'parent_type'     => false,
						'object'          => false,
						'object_id'       => false,
						'object_type'     => false,
						'object_sub_type' => false,
						'is_wp_ajax'      => false,
						'wp_ajax_action'  => false,
					),
					false
				);

				if ( is_admin() ) {
					global $pagenow;
					switch ( $pagenow ) {
						case 'admin-ajax.php':
							$context['is_wp_ajax'] = DOING_AJAX;
							$context['wp_ajax_action'] = sr_POST( 'action' );
							$context = array_merge( $context, $_REQUEST );
							break;

						case 'edit.php':
						case 'post.php':
							if ( isset( $_REQUEST['action'] ) ) {
								$context['action'] = $_REQUEST['action'];
							}

							if ( isset( $_REQUEST['post_ID'] ) ) // Will be true for at least 'editpost'==$_POST['action']
							{
								$context['post_id'] = $_REQUEST['post_ID'];
							} else if ( isset( $_GET['post'] ) ) {
								$context['post_id'] = $_GET['post'];
							}

							if ( isset( $_GET['page'] ) ) {
								$context['page'] = $_GET['page'];
							}

							if ( $context['post_id'] ) {
								$context['post'] = get_post( $context['post_id'] );
							}

							if ( $context['post'] ) {
								$context['post_type'] = $context['post']->post_type;
							}

							/**
							 * TODO (mikes): This does not belong here. Core should have no knowlege of other modules.
							 * TODO: This belongs in the Microsite module in the 'sr_get_context' hook. I need to know
							 * TODO: when and why this is needed so I can move it to where it is supposed to be.
							 */
							// Set microsite context
							//							if( 'microsite-page' == $context['post_type'] ){
							//								$context['is_microsite'] = 1;
							//								$context['microsite_page_post_id'] = $context['post']->ID;
							//								$context['microsite_page_post_name'] = $context['post']->post_name;
							//								$context['microsite_post_id'] = $context['post']->ID;
							//								$context['microsite_post_name'] = $context['post']->post_name;
							//								$context['microsite_post_type'] = get_post_type( $context['post']->post_parent );
							//								$context['microsite_page_key'] = Sunrise_Microsites::get_page_key( $context['post']->ID );
							//								$parsed = Sunrise_Microsites::parse_page_key( $context['microsite_page_key'] );
							//								$context['microsite_page_type'] = $parsed['microsite_page_type'];
							//								$context['microsite_page_template'] = $parsed['microsite_page_template'];
							//							}

							$context['object_type'] = 'post';
							$context['object_sub_type'] = $context['post_type'];
							$context['object_id'] = $context['post_id'];
							$context['object'] = $context['post'];

							break;

						case 'post-new.php':
							$context['action'] = 'add-new';
							$context['post_id'] = 0;

							if ( isset( $_GET['post_type'] ) ) {
								$context['post_type'] = $_GET['post_type'];
							}

							$context['object_type'] = 'post';
							$context['object_sub_type'] = $context['post_type'];
							$context['object_id'] = $context['post_id'];
							$context['object'] = $context['post'];
							break;

						default:
							break;

					}
					$context = (object) $context;
					if ( $context->post_id ) {
						if ( ! $context->post ) {
							$context->post = get_post( $context->post_id );
						}
					}
					if ( false !== $context->post_id ) {
						if ( ! $context->object_id ) {
							$context->object_id = $context->post_id;
						}
					}
					if ( $context->post ) {
						if ( ! $context->post_type ) {
							$context->post_type = $context->post->post_type;
						}
						if ( ! $context->object ) {
							$context->object = $context->post;
						}
					}
					if ( $context->post_type ) {
						if ( ! $context->typenow ) {
							$context->typenow = $context->post_type;
						}
						if ( ! $context->object_type ) {
							$context->object_type = 'post';
						}
						if ( ! $context->object_sub_type ) {
							$context->object_sub_type = $context->post_type;
						}
					}
					if ( ! $context->post_parent ) {
						if ( is_object( $context->post ) ) {
							$context->post_parent = $context->post->post_parent;
						}
					}
					if ( $context->post_parent ) {
						if ( ! $context->parent ) {
							$context->parent = get_post( $context->post_parent );
						}
						if ( ! $context->parent_type ) {
							$context->parent_type = $context->parent->post_type;
						}
					}
				} else {
					global $post, $typenow;
					if ( is_object( $post ) && ! is_post_type_archive() ) {
						$context['post_id'] = $context['object_id'] = $post->ID;
						$context['post'] = $context['object'] = $post;
						$context['object_type'] = 'post';
						$context['typenow'] = $context['object_sub_type'] = $context['post_type'] = $post->post_type;
						$context['post_parent'] = $post->post_parent;
						$context['parent'] = @get_post( $post->post_parent );
						$context['parent_type'] = @$context['parent']->post_type;
					}
				}
				$context = apply_filters( 'sr_get_context', (object) $context );

				self::set_context_vars( (array) $context, true );

				//				if ( $context->is_wp_ajax )
				//					$context->post = $context->object = $context->parent = false;

				self::$_context->_initialized = true;

			}
			return self::$_context;
		}

		static function get_options () {
			return get_option( 'sunrise-options' );
		}

		/*
		 * Enable passing of class name, or "{plugin_slug}/{$class_type}/{$instance_type}"
		 */
		static function parse_class ( $class ) {
			if ( ! class_exists( $class ) ) {
				list( $plugin_slug, $class_type, $instance_type ) = explode( '/', $class );
				$class = Sunrise::get_class_for( "{$plugin_slug}/{$class_type}", $instance_type );
			}
			return $class;
		}

		static function call_method ( $class, $method_name, $args ) {
			$value = null;
			$class = self::parse_class( $class );
			if ( class_exists( $class ) ) {
				$value = Sunrise::call_instance_method( $class, $method_name, $instance, $args );
			}
			return $value;
		}

		static function call_instance_method ( $class, $method_name, $instance /* [$value], [$args] */ ) {
			$value = null;
			$class = self::parse_class( $class );
			if ( class_exists( $class ) ) {
				$args = array_slice( func_get_args(), 2 ); // Strip off $class and $method_name.
				// Call instance method but pass ($instance,$args) or ($instance,$value,$args)
				$value = call_user_func_array( array( $class, $method_name ), $args );
			}
			return $value;
		}

		static function apply_class_filters ( $class, $filter_name, $value, $args = array() ) {
			$class = self::parse_class( $class );
			$filter_method = "filter_{$filter_name}";
			if ( method_exists( $class, $filter_method ) ) {
				$value = call_user_func( array( $class, $filter_name ), $value, $args );
			}
			return $value;
		}

		static function do_class_action ( $class, $action_method, $args = array() ) {
			$class = self::parse_class( $class );
			if ( method_exists( $class, $action_method ) ) {
				call_user_func( array( $class, $action_method ), $args );
			}
		}

		static function apply_instance_filters ( $instance, $filter_name, $value, $args = array() ) {
			$filter_method = "filter_{$filter_name}";
			if ( method_exists( $instance, $filter_method ) ) {
				$value = call_user_func( array( &$instance, $filter_method ), $value, $args );
			}
			return $value;
		}

		static function do_instance_action ( $instance, $action_method, $args = array() ) {
			if ( method_exists( $instance, $action_method ) ) {
				call_user_func( array( &$instance, $action_method ), $args );
			}
		}

		/*
		 * Pass an array of element names and convert to an $args array
		 * TODO: Should we move this to be a instance method of Sunrise_Base?
		 */
		static function args_from_object ( $instance, $element_names ) {
			$args = array();
			foreach ( $element_names as $element_name )
				$args[$element_name] = $instance->$element_name;
			return $args;
		}

		static function get_class_for ( $slug_file_group, $instance_type ) {
			$valid_classes = Sunrise::get_valid_classes( $slug_file_group );
			if ( ! isset( $valid_classes[$instance_type] ) ) {
				sr_die( "Invalid class instance type [%s].", $instance_type );
			}
			$class = $valid_classes[$instance_type];
			if ( ! class_exists( $class ) ) {
				$class = false;
			}
			return Sunrise::apply_class_filters( __CLASS__, 'get_class_for', $class );
		}

		static function get_instance_for ( $class_type, $instance_type, $args = array() ) {
			$instance = false;
			$class = self::get_class_for( $class_type, $instance_type );
			if ( class_exists( $class ) ) {
				$args['type'] = $instance_type;
				$instance = new $class( $args );
			}
			return Sunrise::apply_class_filters( $class, 'get_instance_for', $instance );
		}

		static function instance_has_property ( $instance, $property_name ) {
			return ( is_object( $instance ) && property_exists( $instance, $property_name ) );
		}

		static function element_count ( $array ) {
			$count = false;
			if ( is_array( $array ) ) {
				$count = count( $array );
			}
			return $count;
		}

		static function as_args ( $instance ) {
			$reflector = new ReflectionObject( $instance );
			$properties = $reflector->getProperties( ReflectionProperty::IS_PUBLIC );
			$args = array();
			foreach ( $properties as $property ) {
				false && $property = new ReflectionProperty();
				$property_name = ltrim( $property->getName(), '_' );
				$args[$property_name] = $property->getValue( $instance );
			}
			return $args;
		}

		static function _as_args_old ( $instance ) {
			$args = array();
			$class = get_class( $instance );
			foreach ( get_class_vars( $class ) as $property => $value ) {
				$args[$property] = $instance->$property;
			}
			$args['this'] = $instance;
			return $args;
		}

		/**
		 * Format response message via Ajax
		 *
		 * @param string $message Raw error message if error, HTML string if success (this allows Image_Field to return custom HTML markup)
		 * @param string $status  Ajax status, should be 'error' or 'success'
		 * @return JSON object {status: $status, html: $html}
		 * TODO (mikes): Should this function have 3 parameters? $html, $status and $message?
		 * TODO (mikes): Or show it be a status and an $args array? (Probably better.)
		 */
		static function format_ajax_response ( $message, $status = 'error' ) {
			$error_html = ob_get_clean(); // This mates with ob_start() in Sunrise_Static_Base.
			/*
			 * Remove field information and additional classes
			 * We can always reference to them in hidden fields
			 */
			if ( 'error' == $status ) {
				$message = __( preg_replace( '#^(ERROR: )?(.*)$#', 'ERROR: $2', $message ) );

				$html = "<span class=\"message error\">{$message}</span>";
			} else {
				$html = $message;
				$message = false;
			}

			/*
			 * Use single quotes in HTML attributes to make jQuery get the correct content of iframe (added when upload)
			 * TODO (Anh): Modify jQuery upload plugin to make it works with double quotes as well
			 */
			$html = str_replace( array( '\"', '"' ), "'", $html );

			/*
			 * Generate JSON object
			 * Using esc_html to escape <, >, quotes in Javascript to prevent parsing error when pass as JSON text
			 */
			$json = array(
				'message' => $message,
				'status'  => $status,
				'html'    => esc_html( $html )
			);

			return json_encode( $json );
		}

		/* THIS IS ONLY HERE AS A TEMPLATE FOR FUTURE SUNRISE CLASSES TO USE. COPY AND UNCOMMENT

				function __set( $property_name, $value ) {
					switch ( $property_name ) {
						case 'foo':
							call_user_func( array( &$this, "set_{$property_name}" ), $value );
							break;

						default:
							parent::__set( $property_name, $value );
							break;
					}
				}
				function __get( $property_name ) {
					$value = false;
					switch ( $property_name ) {
						case 'foo':
							$value = call_user_func( array( &$this, "get_{$property_name}" ) );
							break;
						default:
							$value = parent::__get( $property_name );
							break;
					}
					return $value;
				}
		*/
	}

	Sunrise::on_load();
}

