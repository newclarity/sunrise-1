<?php
/**
 * Object Type functions
 *
 * @package Sunrise Core
 * @subpackage Object Types
 */

/**
 * Creates the list of object types when the 'init' action is fired.
 *
 */
function sr_create_object_types() {
	global $sr_object_types;
	$sr_object_types = array(
		'post' => array( 'name' => __( 'Post' ) ),   // General case of a post type
		'page' => array( 'name' => __( 'Page' ) ),   // Special case of $post->post_type == 'page'
		'user' => array( 'name' => __( 'User' ) ),
		'term' => array( 'name' => __( 'Term' ) ),
		'term_taxonomy' => array( 'name' => __( 'Taxonomy Term' ) ),
		'term_relationship' => array( 'name' => __( 'Taxonomy Term Object' ) ),
		'link' => array( 'name' => __( 'Link' ) ),
		'comment' => array( 'name' => __( 'Comment' ) ),
		'option' => array( 'name' => __( 'Option' ) ),
		'postmeta' => array( 'name' => __( 'Post Metadata' ) ),
		'usermeta' => array( 'name' => __( 'User Metadata' ) ),
		'commentmeta' => array( 'name' => __( 'Comment Metadata' ) ),
		'site' => array( 'name' => __( 'Multisite Network' ) ),
		'blog' => array( 'name' => __( 'Multisite Site/Blog' ) ),
		'relationship' => array( 'name' => __( 'Object Relationship' ) ),
	);
	return $sr_object_types;
}
add_action( 'init', 'sr_create_object_types', 0 );  // 0 is highest priority

/**
 * Return the list of object types
 *
 */
function sr_get_object_types() {
	global $sr_object_types;
	return apply_filters( 'sr_get_object_types', $sr_object_types );
}

/**
 * Adds an object type.
 *
 */
function sr_add_object_type( $object_type, $args = array() ) {
	global $sr_object_types;
	$sr_object_types[ $object_type ] = $args;
	return $sr_object_types;
}

/**
 * Removes an object type.
 *
 * @since 3.3
 */
function sr_remove_object_type( $object_type ) {
	global $sr_object_types;
	unset( $sr_object_types[ $object_type ] );
}



