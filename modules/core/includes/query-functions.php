<?php
/**
 * This really should go into a Sunrise Query module
 */
/**
 *
 * This function is a better version of WordPress' get_post_field()
 *
 * @param string $field_name
 * @param int $post_id
 * @return mixed
 */
function sr_get_post_object_field( $field_name, $post_id ) {
	/**
	 * @var wpdb $wpdb
	 */
	global $wpdb;

	$post_id = (int) $post_id;
	if ( $post = wp_cache_get( $post_id, 'posts' ) ) {
		$post_field = isset( $post->{$field_name} ) ? $post->{$field_name} : false;
	} else if ( ! $post_field = wp_cache_get( $post_id, $cache_key = "sr_post_fields[{$field_name}]" ) ) {
		$sql = "SELECT {$field_name} FROM {$wpdb->posts} WHERE ID = %d LIMIT 1";
		$post_field = $wpdb->get_var( $wpdb->prepare( $sql, $post_id ) );
		if ( $post_field )
			wp_cache_add( $post_id, $post_field, $cache_key );
	} else {
		$post_field = false;
	}

	return $post_field;
}

function sr_get_post_type_field( $post_type, $field_name ) {
	$post_type_object = get_post_type_object( $post_type );
	if ( ! $post_type_object )
		return false;
	return $post_type_object->{$field_name};
}
