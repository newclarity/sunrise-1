<?php
/*
 * Base class to offer common functionality to all Sunrise Instance (vs. Static) classes
 *
 */
if ( ! class_exists( 'Sunrise_Static_Base' ) ) {
	class Sunrise_Static_Base {
		protected static $_module_dirs = array();

		protected static $_test;

		static function on_load() {
			if ( defined( 'DOING_AJAX' ) ) {
				sr_add_filter( __CLASS__, 'query' );
			}

			sr_add_action( __CLASS__, 'init', 1 );	// Load the capabilities before others
			sr_add_action( __CLASS__, 'muplugins_loaded' );

			if ( defined( 'DOING_AJAX' ) ) {
				sr_add_action( __CLASS__, array( 'admin_init', 'core_sunrise_init' ), 0 ); // Do before
			} else if ( is_admin() ) {
				sr_add_action( __CLASS__, array( 'admin_menu', 'core_sunrise_init' ), 0 ); // Need to do this before other things are done.
			} else {
				sr_add_action( __CLASS__, array( 'wp_loaded', 'core_sunrise_init' ) );
			}

			if ( is_admin() ) {
				sr_add_action( __CLASS__, array( 'admin_init', 'sunrise_loaded' ), 11 ); // Do after
			} else {
				sr_add_action( __CLASS__, array( 'wp',  'sunrise_loaded' ), 11 );
			}

			$modules_dir = dirname( dirname( dirname( __FILE__ ) ) );
			if ( $dir_handle = opendir( $modules_dir ) ) {
				while ( ( $dir_entry = readdir( $dir_handle ) ) !== false )
					if ( is_dir( $module_dir = "{$modules_dir}/{$dir_entry}" ) )
						self::$_module_dirs[] = $module_dir;
				closedir( $dir_handle );
			}

		}
		static function get_module_dirs() {
			return self::$_module_dirs;
		}
		static function init() {
			self::load_roles_capabilities();
		}
		static function muplugins_loaded() {
			do_action( 'sr_modules_init' );
		}
		static function core_sunrise_init() {
			do_action( 'sunrise_init' );
		}
		static function sunrise_loaded() {
			do_action( 'sr_loaded' );
		}
		static function query( $query ) {
			remove_filter( 'query', array( __CLASS__, 'query' ) );
			ob_start();	// TODO (mikes): Document why this is needed.
			return $query;
		}

		/**
		 *
		 * @return void
		 *
		 * TODO (mikes 2012-01-30): Make this more performant.
		 */
		static function load_roles_capabilities() {
			foreach( self::$_module_dirs as $module_dir )
				if ( file_exists( $roles_capabilities_file = "{$module_dir}/roles-capabilities.php" ) )
					require( $roles_capabilities_file );
		}
	}
	Sunrise_Static_Base::on_load();
}
