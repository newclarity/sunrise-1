<?php
/*
Include File: Yes
Description: Functions supporting Sunrise
Author: The Sunrise Team
Author URL: http://getsunrise.com
Version: 0.1
Notes:
*/


if ( ! function_exists( 'sr_default' ) ) {
	/**
	 * @param array|object $instance
	 * @param string $property
	 * @param mixed $default
	 * @param bool $allow_false
	 * @return mixed
	 */
	function sr_default( &$instance, $property, $default, $allow_false = false ) {
		$result = false;
		if ( is_array( $instance ) ) {
			if ( ! isset($instance[$property]) ||
					is_null( $instance[$property] ) ||
					(! $allow_false && ! $instance[$property])
			) {
				$result = $default;
			} else {
				$result = $instance[$property];
			}
			$instance[$property] = $result;
		} else if ( is_object( $instance ) ) {
			if ( ! property_exists( $instance, 'property' ) ||
					is_null( $instance->$property ) ||
					(! $allow_false && ! $instance->$property)
			) {
				$result = $default;
			} else {
				$result = $instance->$property;
			}
			$instance->$property = $result;
		}
		return $result;
	}
}

if ( ! function_exists( 'sr_remove_all_metaboxes' ) ) {
	/**
	 * Removes all admin metaboxes.
	 *
	 * @return void
	 *
	 */
	function sr_remove_all_metaboxes() {
		global $wp_meta_boxes;
		$wp_meta_boxes = array();
	}
}

if ( ! function_exists( 'sr_plain_text' ) ) {
	/**
	 * Used for ad-hoc debugging
	 *
	 * @param bool|string $to_output
	 * @return void
	 */
	function sr_plain_text( $to_output = false ) {
		header( 'Content-type:text/plain' );
		echo $to_output;
	}
}
if ( ! function_exists( 'sr_get_context' ) ) {
	function sr_get_context() {
		return Sunrise::get_context();
	}
}
if( ! function_exists( 'sr_context_var' ) ) {
    function sr_context_var( $name ) {
        if( ( $context = sr_get_context() ) && is_object( $context ) && property_exists( $context, $name ) ) {
            return $context->$name;
        }
        return false;
    }
}
if ( ! function_exists( 'sr_push_environment' ) ) {
	function sr_push_environment() {
		return Sunrise_Environment::push();
	}
}
if ( ! function_exists( 'sr_pop_environment' ) ) {
	function sr_pop_environment() {
		return Sunrise_Environment::pop();
	}
}
if ( ! function_exists( 'sr_note' ) ) {
	function sr_note( $message ) {
	}
}
if ( ! function_exists( 'sr_warn' ) ) {
	function sr_warn( $message ) {
	}
}
if ( ! function_exists( 'sr_fail' ) ) {
	function sr_fail( $message ) {
	}
}

if ( ! function_exists( 'sr_set_object_vars' ) ) {
	function sr_set_object_vars( $object, $vars, $overwrite = true ) {
		foreach ( (array) $vars as $key => $value ) {
			if ( $overwrite || empty($object->{$key}) )
				$object->{$key} = $value;
		}
		return $object;
	}
}

if ( ! function_exists( 'sr_add_action' ) ) {
	/**
	 * @param string|object $context Class name for static methods or object for instance methods.
	 * @param string|array $action
	 * @param int $priority
	 * @param int $accepted_args
	 * @return void
	 */
	function sr_add_action( $context, $action, $priority = 10, $accepted_args = 1 ) {
		if ( is_array( $action ) ) {
			$method = $action[1];
			$action = $action[0];
		} else {
			$method = sr_underscorize( $action );
		}
		add_action( $action, array( $context, $method ), $priority, $accepted_args );
	}
}
if ( ! function_exists( 'sr_remove_action' ) ) {
	function sr_remove_action( $class, $action ) {
		if ( is_array( $action ) ) {
			$method = $action[1];
			$action = $action[0];
		} else {
			$method = sr_underscorize( $action );
		}
		remove_action( $action, array( $class, $method ) );
	}
}
if ( ! function_exists( 'sr_add_filter' ) ) {
	/**
	 * @param string|object $context Class name for static methods or object for instance methods.
	 * @param string|array $filter
	 * @param int $priority
	 * @param int $accepted_args
	 * @return void
	 */
	function sr_add_filter( $context, $filter, $priority = 10, $accepted_args = 1 ) {
		if ( is_array( $filter ) ) {
			$method = $filter[1];
			$filter = $filter[0];
		} else {
			$method = sr_underscorize( $filter );
		}
		add_filter( $filter, array( $context, $method ), $priority, $accepted_args );
	}
}
if ( ! function_exists( 'sr_POST' ) ) {
	function sr_POST( $key, $default = null ) {
		return isset($_POST[$key]) ? $_POST[$key] : $default;
	}
}
if ( ! function_exists( 'sr_GET' ) ) {
	function sr_GET( $key, $default = null ) {
		return isset($_GET[$key]) ? $_GET[$key] : $default;
	}
}
if ( ! function_exists( 'sr_REQUEST' ) ) {
	function sr_REQUEST( $key, $default = null ) {
		return isset($_REQUEST[$key]) ? $_REQUEST[$key] : $default;
	}
}
if ( ! function_exists( 'sr_add_ajax_handler' ) ) {
	function sr_add_ajax_handler( $class, $action ) {
		Sunrise::add_ajax_handler( $class, $action );
	}
}
if ( ! function_exists( 'sr_remove_post_type_support' ) ) {
	/**
	 * Same as WordPress' remove_post_type_support(), but supports passing an array of
	 * supports identifiers vs. just a single supports identifier.
	 *
	 * @param string $post_type
	 * @param array|string $supports
	 * @return void
	 *
	 */
	function sr_remove_post_type_support( $post_type, $supports ) {
		if ( is_array( $supports ) )
			foreach ( $supports as $supports_id )
			{
				remove_post_type_support( $post_type, $supports_id );
			}
		else
			remove_post_type_support( $post_type, $supports );
	}
}

if ( ! function_exists( 'sr_array_extract' ) ) {
	/**
	 * Extracts the array elements indicated by the keys.
	 *
	 * @example
	 *
	 *	 $args = sr_array_extract( array( 'a' => 1, 'b' => 2, 'c' => 3 ), array( 'b' ) );
	 *	print_r( $args ); // Prints: Array ( [b] => 2 )
	 *
	 * @param array $keys
	 * @param array $array
	 * @return array
	 */
	function sr_array_extract( $array, $keys ) {
		return array_intersect_key( $array, array_flip( $keys ) );
	}
}
if ( ! function_exists( 'sr_array_rename_keys' ) ) {
	/**
	 * Extracts the array elements indicated by the keys.
	 *
	 * @example
	 *
	 *	 $args = sr_array_rename_keys( array( 'a' => 1, 'b' => 2, 'c' => 3 ), array( 'b' => 'XXX' ) );
	 *	print_r( $args ); // Prints: Array ( [a] => 1 [XXX] => 2 [c] => 3 )
	 *
	 * @param array $keys
	 * @param array $array
	 * @return array
	 *
	 */
	function sr_array_rename_keys( $array, $keys ) {
		$new_array = array();
		foreach ( $array as $key => $value )
		{
			$new_array[isset($keys[$key]) ? $keys[$key] : $key] = $value;
		}
		return $new_array;
	}
}
if ( ! function_exists( 'sr_array_merge' ) ) {
	/**
	 * Merges arrays just like array_merge(), but if the value in array2 is null then uses the value from array1.
	 *
	 * @param array $array1
	 * @param array $array2
	 * @return array
	 */
	function sr_array_merge( $array1, $array2 ) {
		$new_array = $array2;
		foreach ( $array1 as $key => $value )
		{
			if ( ! isset($new_array[$key]) || is_null( $new_array[$key] ) )
				$new_array[$key] = $value;
		}
		return $new_array;
	}
}
/**
 * Insert an array element into an array with numeric indexe. DOES NOT retain indexes.
 *
 * @param array	$array
 * @param mixed	$value_to_insert
 * @param string $value_to_find
 * @param string $where_to_insert
 * @return array
 *$array, , , $where
 *
 * @examples
 *
 *	$fields = array(
 *		1 => 'post_title',
 *		3 => 'description',
 *	);
 *	$fields = sr_array_insert_after( $array, 'headline', 'post_title', 'after' );
 *	print_r( $options );
 *
 *		Result:
 *			array(
 *				0 => 'post_title',
 *				1 => 'headline',
 *				2 => 'description',
 *			);
 *
 */
if ( ! function_exists( 'sr_array_insert_after' ) ) {
	function sr_array_insert_after( &$array, $value_to_insert, $value_to_find, $where_to_insert = 'before' ) {
		$array = array_values( $array );
		$index = array_search( $value_to_find, $array );
		if ( 'after' == $where_to_insert ) {
			$index++;
		}
		array_splice( $array, $index, 0, array( $value_to_insert ) );
		return $array;
	}
}

if ( ! function_exists( 'sr_array_insert_key' ) ) {
	/**
	 * Insert an array element by key order without disturbing key order
	 *
	 * @param array	$array
	 * @param string $insert_key
	 * @param mixed	$insert_value
	 * @param string $find_key
	 * @param string $where
	 * @return array
	 *
	 *
	 * @examples
	 *
	 *	$options = array(
	 *		'abc' => 1,
	 *		'xyz' => 2,
	 *	);
	 *	$options = sr_array_insert_key( $options, 'mno', 3, 'abc', 'before' );
	 *	print_r( $options );
	 *
	 *		Result:
	 *			array(
	 *				'abc' => 1,
	 *				'mno' => 3,
	 *				'xyz' => 2,
	 *			);
	 *
	 */
	function sr_array_insert_key( &$array, $insert_key, $insert_value, $find_key, $where = 'before' ) {
		$new_array = array();
		if ( 'before' == $where ) {
			foreach ( $array as $element_key => $element_value ) {
				if ( $find_key == $element_key )
					$new_array[$insert_key] = $insert_value;
				$new_array[$element_key] = $element_value;
			}
		} else if ( 'after' == $where ) {
			foreach ( $array as $element_key => $element_value ) {
				$new_array[$element_key] = $element_value;
				if ( $find_key == $element_key )
					$new_array[$insert_key] = $insert_value;
			}
		}
		return $new_array;
	}
}
if ( ! function_exists( 'sr_expand_args' ) ) {
	/**
	 * @param array $args
	 * @return array
	 */
	function sr_expand_args( $args ) {
		return Sunrise::expand_args( $args );
	}
}
if ( ! function_exists( 'sr_normalize_args' ) ) {
	/**
	 * @param array $args
	 * @param array $short_hands
	 * @return array
	 */
	function sr_normalize_args( $args, $short_hands ) {
		return Sunrise::normalize_args( $args, $short_hands );
	}
}
if ( ! function_exists( 'sr_load_plugin_files' ) ) {
	/**
	 * @param string $plugin_slug
	 * @param array $file_groups
	 * @return void
	 *
	 * TODO (mikes): Come up with a better name than 'file_groups'
	 */
	function sr_load_plugin_files( $plugin_slug, $file_groups ) {
		/**
		 * @var string $modules_dir - Is there a better way to do this?
		 * TODO: Enable this to support 3rd party modules
		 */
		$modules_dir = dirname( dirname( dirname( __FILE__ ) ) );
		foreach ( $file_groups as $file_group ) {
			$slug_file_group = "{$plugin_slug}/{$file_group}";
			foreach ( Sunrise::get_valid_types( $slug_file_group ) as $class_name => $instance_type ) {
				$class_file = $modules_dir . sr_dashize( "/{$slug_file_group}-types/{$instance_type}-{$file_group}.php" );
				if ( ! class_exists( Sunrise::get_class_for( $slug_file_group, $instance_type ) ) && file_exists(
					$class_file
				)
				) {
					require($class_file);
				}
			}
		}
		return;
	}
}
if ( ! function_exists( 'sr_load_demos' ) ) {
	/**
	 * @param string $directory - Absolute local path to plugin that contains a 'demos' subdir with files named '{base_name}-demo.php'
	 * @return void
	 */
	function sr_load_demos( $directory ) {
		if ( defined( 'SUNRISE_NO_DEMOS' ) )
			return;

		$options = Sunrise::get_options();
		if ( ! isset($options['demo_mode']) ) {
			$options['demo_mode'] = 'yes';
			update_option( 'sunrise-fields-options', $options );
		}
		if ( $options['demo_mode'] != 'no' ) {
			$demo_dir = "{$directory}/demos";
			require_once(sr_admin_path( 'includes/file.php' ));
			$demo_files = list_files( $demo_dir );
			foreach ( $demo_files as $demo_file )
			{
				if ( preg_match( '#/([^/]*)-demo.php$#', $demo_file ) ) {
					require($demo_file);
				}
			}
		}
	}
}
if ( ! function_exists( 'sr_die' ) ) {
	function sr_die( $message ) {
		$args = func_get_args();
		$message = array_shift( $args ); // Remove $message
		$message = vsprintf( $message, $args );
		$push_message = sr_get_stack_trace( __( 'ERROR: ', 'sunrise-core' ) . $message, 2 );
		if ( defined( 'DOING_AJAX' ) ) {
			$response = array(
				'status' => 'error',
				'message' => strip_tags( $push_message ),
			);
			echo json_encode( $response );
			die();
		}
		echo $push_message;
		wp_die( '' );
	}
}
if ( ! function_exists( 'sr_trigger_error' ) ) {
	function sr_trigger_error( $message ) {
		$args = func_get_args();
		$message = array_shift( $args ); // Remove $message
		$message = vsprintf( $message, $args );
		$push_message = sr_get_stack_trace( $message, 2 );
		Sunrise::push_message( $push_message );
		trigger_error( $message );
	}
}
if ( ! function_exists( 'sr_get_stack_trace' ) ) {
	function sr_get_stack_trace( $message, $start = 1, $html = true ) {
		$call_stack = array();
		$debug_backtrace = array_slice( debug_backtrace( 1 ), $start );
		$prior_callable = '';
		foreach ( $debug_backtrace as $stack_item ) {
			$callable = "{$stack_item['function']}()";
			if ( preg_match( '#^(__get|__set|__call|do_action|apply_filters)\(\)$#', $callable ) ) {
				$callable = str_replace( '()', "('{$stack_item['args'][0]}')", $callable );
			} else if ( preg_match( '#^(call_user_func(_array)?)\(\)$#', $callable ) ) {
				$replace = '';
				$arg_0 = array_shift( $stack_item['args'] );
				if ( is_array( $arg_0 ) && 2 == count( $arg_0 ) && is_string( $arg_0[1] ) ) {
					if ( is_object( $arg_0[0] ) )
						$replace = get_class( $arg_0[0] ) . "->{$arg_0[1]}";
					else if ( is_string( $arg_0[0] ) )
						$replace = "{$arg_0[0]}::{$arg_0[1]}";
				}
				if ( count( $stack_item['args'] ) ) {
					foreach ( $stack_item['args'] as $key => $value ) {
						if ( is_object( $value ) ) {
							if ( isset($value->ID) && isset($value->post_type) ) {
								$value = "\$post[ID={$value->ID}&amp;type={$value->post_type}]";
							} else {
								$value = '(object)';
							}
						} else if ( is_array( $value ) ) {
							$value = '(array)';
						}
						$stack_item['args'][$key] = (string) $value;
					}
					$replace .= ',' . implode( ',', $stack_item['args'] );
				}
				$callable = str_replace( '()', "({$replace})", $callable );
			}
			if ( isset($stack_item['class']) )
				$callable = "{$stack_item['class']}{$stack_item['type']}{$callable}";
			if ( $html )
				$callable = "<strong><code>{$callable}</code></strong>";
			//			if ( preg_match( '#>call_user_func\(([a-z_A-Z]+)(->|::)(get|set)_([a-z_A-Z]+)\)<#', $callable, $match ) ) {
			$regex = "([a-z_A-Z]+)(->|::)__(get|set)\('([^']+)'\)";
			$regex = $html ? "#>{$regex}<#" : "#^{$regex}$#";
			if ( preg_match( $regex, $callable, $match ) ) {
				$check_this = "call_user_func({$match[1]}{$match[2]}{$match[3]}_{$match[4]})";
				if ( false !== strpos( $prior_callable, $check_this ) ) {
					//array_pop( $call_stack );
					//continue;
				}
			}
			$file = '';
			if ( isset($stack_item['file']) ) {
				$file = str_replace( WP_CONTENT_DIR, '', $stack_item['file'] );
				if ( $html )
					$file = "<strong><code>{$file}</code></strong>";
				$file = " in {$file}";
			}
			if ( isset($stack_item['line']) ) {
				$line = $stack_item['line'];
				if ( $html )
					$line = "<strong><code>{$stack_item['line']}</code></strong>";
				$line = " on line <strong><code>{$line}</code></strong>";
			}
			if ( ! empty($file) )
				$call_stack[] = $prior_callable = <<<STACK_ITEM
When calling {$callable}{$file}{$line}
STACK_ITEM;
		}
		$call_stack = implode( $html ? ",</li>\n<li>" : ",\n", $call_stack );
		if ( $html )
			$call_stack = '<ol id="callstack"><li>' . "{$call_stack}</li></ol>";
		if ( $html )
			$result = "<p class=\"error message\">{$message}:</p>\n{$call_stack}<br/>\n";
		else
			$result = "{$message}:\n{$call_stack}\n";
		return $result;
	}
}
if ( ! function_exists( 'sr_parse_args' ) ) {
	/**
	 * Same as wp_parse_args() (which is like array_merge() but with params in opposite order)
	 * but allows for URL-encoded strings as well, i.e. 'image_type=headshot&image_size=small'
	 *
	 * @param array $args
	 * @param array|bool $defaults
	 * @return array
	 */
	function sr_parse_args( $args, $defaults = false ) {
		if ( is_string( $args ) )
			parse_str( $args, $args );
		if ( $defaults ) {
			if ( is_string( $defaults ) ) parse_str( $defaults, $defaults );
			$args = wp_parse_args( $args, $defaults );
		}
		return $args;
	}
}
if ( ! function_exists( 'sr_admin_path' ) ) {
	function sr_admin_path( $relative_path = '' ) {
		return ABSPATH . "wp-admin/{$relative_path}";
	}
}
if ( ! function_exists( 'sr_get_from_POST' ) ) {
	/*
	 * Convenience function to a post variable w/o having to first test to see if it has been set.
	 * Allows specifying a default value which when unspecified will be an empty string ('').
	 */
	function sr_get_from_POST( $var_name, $default = '' ) {
		return isset($_POST[$var_name]) ? $_POST[$var_name] : $default;
	}
}
if ( ! function_exists( 'sr_dashize' ) ) {
	/*
	 * Used for turning variable names with underscores into html feature IDs with dashes
	 */
	function sr_dashize( $string ) {
		return str_replace( '_', '-', $string );
	}
}
if ( ! function_exists( 'sr_underscorize' ) ) {
	function sr_underscorize( $string ) {
		return str_replace( '-', '_', $string );
	}
}
if ( ! function_exists( 'sr_spacize' ) ) {
	function sr_spacize( $string ) {
		return str_replace( array( '-', '_' ), ' ', $string );
	}
}
if ( ! function_exists( 'sr_enqueue_style' ) ) {
	function sr_enqueue_style( $handle, $src, $args = array() ) {
		__sr_enqueue_resource( 'style', $handle, $src, $args );
	}
}
if ( ! function_exists( 'sr_enqueue_script' ) ) {
	function sr_enqueue_script( $handle, $src, $args = array() ) {
		__sr_enqueue_resource( 'script', $handle, $src, $args );
	}
}
if ( ! function_exists( '__sr_enqueue_resource' ) ) {
	/**
	 * @param $resource_type
	 * @param $handle
	 * @param $src
	 * @param $args
	 * @return void
	 *
	 * TODO (mikes): Consolidate this and Sunrise_Fields::_enqueue_via_http()
	 *
	 */
	function __sr_enqueue_resource( $resource_type, $handle, $src, $args ) {
		$args = wp_parse_args(
			$args, array(
				'file' => false,
				'dependencies' => array(),
				'version' => false,
				'in_footer' => false,
				'media' => 'all',
			)
		);
		extract( $args );

		if ( ! $version ) {
			if ( ! $file ) {
				if ( $src[0] == '/' ) {
					$file = ABSPATH . substr( $src, 1 );
				} else { // $src start with 'http'
					$file = ABSPATH . substr( $src, strlen( site_url() ) + 1 );
				}
			}
			$version = file_exists( $file ) ? (string) filemtime( $file ) : false;
		}
		switch ( $resource_type ) {
			case 'js':
			case 'script':
				wp_enqueue_script( $handle, $src, $dependencies, $version, $in_footer );
				break;
			case 'css':
			case 'style':
				wp_enqueue_style( $handle, $src, $dependencies, $version, $media );
				break;
		}
	}
}
if ( ! function_exists( 'sr_format_bytes' ) ) {
	/*
	 * Accepts numeric bytes and converts to formats like the following (assuming $decimals==2):
	 *
	 *    100 => 100b
	 *    2048 => 2kb
	 *    3000 => 2.93kb
	 *    10000000 => 9.54mb
	 *    pow( 1024,2 ) => 1mb
	 *    pow( 1024,3 ) => 1gb
	 *    pow( 2500,3 ) => 14.55gb
	 *
	 * @param int $bytes Numeric value of bytes to format
	 * @returns int $decimals Number of decimal values to display using rounding.
	 * @returns string Formatted byte value
	 */
	function sr_format_bytes( $bytes, $decimals = 2 ) {
		preg_match( '#(\d+)(\.\d+)?(B|kB|MB|GB|TB)#i', wp_convert_bytes_to_hr( $bytes ), $m );
		$decimal_value = substr( round( (float) "0{$m[2]}", $decimals ), 1 );
		return $m[1] . $decimal_value . ('B' == $m[3] ? ' bytes' : strtolower( $m[3] ));
	}
}
if ( ! function_exists( 'sr_file_changed' ) ) {
	/**
	 *	Used where you might want to use an activation function but where it's not convenient or possible (like in a theme)
	 *	Accepts __FILE__ for the a unique key to identify the file.
	 *	TODO: Need to properly document this
	 *
	 * @param string $file
	 * @return bool
	 *
	 */
	function sr_file_changed( $file ) {
		$file_changed = false;

		$files_changed = get_option( '_sr_files_last_changed' );
		if ( ! is_array( $files_changed ) )
			$files_changed = array();

		if ( isset($files_changed[$file]) ) {
			$this_time = filemtime( $file );
			$file_changed = ($this_time > $files_changed[$file]);
			$last_changed = $this_time;
		} else {
			$last_changed = filemtime( $file );
			$file_changed = true;
		}
		if ( $file_changed ) {
			$files_changed[$file] = $last_changed;
			update_option( '_sr_files_last_changed', $files_changed );
		}
		return $file_changed;
	}
}
if ( ! function_exists( 'is_post_index' ) ) {
	/*
	 * See: http://lists.automattic.com/pipermail/wp-hackers/2011-April/039106.html
	 * TODO: Add post-index.php to have higher priority than home.php
	 */
	function is_post_index() {
		return is_home();
	}
}
if ( ! function_exists( 'get_post_type_archive_link' ) ) {
	// NOT TESTED YET - This is not needed in wp31
	function get_post_type_archive_link( $post_type ) {
		global $wp_rewrite;
		if ( ! $post_type_obj = get_post_type_object( $post_type ) )
			return false;

		if ( get_option( 'permalink_structure' ) && is_array( $post_type_obj->rewrite ) ) {
			$struct = $post_type_obj->rewrite['slug'];
			if ( $post_type_obj->rewrite['with_front'] )
				$struct = $wp_rewrite->front . $struct;
			$link = home_url( user_trailingslashit( $struct, 'post_type_archive' ) );
		} else {
			$link = home_url( '?post_type=' . $post_type );
		}

		return apply_filters( 'post_type_archive_link', $link, $post_type );
	}
}

//function sr_unflatten_array( $values ) {
//	$new_values = $values;
//	foreach( $new_values as $key => $value ) {
//		if ( preg_match( '#^([^[]+)(.*)$#', $key, $match ) ) {
//			if ( ! empty( $match[2] ) ) {
//				if ( ! preg_match_all( '#\[([^]]+)\]#', $match[2], $sub_keys ) ) {
//					if ( WP_DEBUG )
//						sr_die( "{$key} does not contain valid subscripts; should be like 'example[a][b][c]'.");
//				} else {
//					$new_key = $match[1];
//					if ( ! isset( $new_values[$new_key] ) )
//						$new_values[$new_key] = array();
//					$reference = &$new_values[$new_key];
//					for( $i = 0; $i < count( $sub_keys ) - 1; $i++ ) {
//						$sub_key = $sub_keys[1][$i];
//						if ( ! isset( $reference[$sub_key] ) )
//							$reference[$sub_key] = array();
//						$reference = &$reference[$sub_key];
//					}
//					$reference = $value;
//				}
//				unset( $new_values[$key] );
//			}
//		}
//	}
//	return $new_values;
//}
//function _sr_set_nested_value( &$values, $indexes, $value ) {
//  $these_values = &$values;
//	/*
//	 * Drill down through the array indexes.
//	 */
//  foreach( $indexes as $index ) {
//    /*
//     * Make sure $these_values is an array.
//     */
//    if ( ! is_array( $these_values ) ) {
//      $these_values = array( $these_values );
//    }
//    /*
//     * Make sure $these_values is an array.
//     */
//    if ( ! isset( $these_values[$index] ) ) {
//      $these_values[$index] = array();
//    }
//    /*
//     * Swap parent with child.
//     */
//    $these_values = &$these_values[$index];
//  }
//	/*
//	 * Finally, assign the value to the node.
//	 */
//  $these_values = $value;
//}

// TODO might make more sense in microsite module, but what it calls is context which is in core
if ( ! function_exists( 'is_microsite' ) ) {

	function is_microsite() {

		$context = Sunrise::get_context();

		if ( $context->is_microsite )
			return true;

		return false;
	}
}

if ( ! function_exists( 'sr_globals' ) ) {
	function sr_globals( $starts_with = '' ) {
		$filtered_globals = array();
		$starts_with = "sr_{$starts_with}";
		foreach( $GLOBALS as $key => $value ) {
			if ( $starts_with == substr( $key, 0, strlen( $starts_with ) ) ) {
				$filtered_globals[$key] = $GLOBALS[$key];
			}
		}
		return $filtered_globals;
	}
}

