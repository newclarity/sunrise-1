<?php
/*
Include File: Yes
Description: Functions to support debugging for Sunrise
Author: The Sunrise Team
Author URL: http://getsunrise.com
Version: 0.0.0
Notes:
*/

/*
 * Used for Debugging, specially in an IDE like PhpStorm
 */
function sr_get_globals( $regex = false ) {
	if ( ! $regex )
		$regex = '^sr_(.+)$';
	$globals = array();
	foreach( $GLOBALS as $name => $value ) {
		if (preg_match("#{$regex}#",$name))
			$globals[$name] = $value;
	}
	return $globals;
}
