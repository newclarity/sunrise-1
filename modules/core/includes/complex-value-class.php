<?php

if ( ! class_exists( 'Sunrise_Complex_Value' ) ) {
	class Sunrise_Complex_Value extends Sunrise_Base {
		public $value;
		public $sub_values = array();
		function __construct( $value, $sub_values = array() ) {
			$this->value  = $value;
			$this->sub_values = $sub_values;
			return $this;
		}
		function __toString() {
			return (string)$this->value;
		}
		function __get( $property_name ) {
			if ( isset( $this->sub_values[$property_name] ) )
				return $this->sub_values[$property_name];
			else
				return false;
		}
	}
}
