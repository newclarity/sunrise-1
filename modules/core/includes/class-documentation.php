<?php
/**
 * This is only used for documentation to allow PhpStorm to recognize objects in WordPress
 * that are not are defined classes like wpdb and $post
 */

/**
 * @property object $labels
 * @property string $description
 * @property bool $publicly_queryable
 * @property bool $exclude_from_search
 * @property string $capability_type
 * @property bool $map_meta_cap
 * @property bool $_builtin
 * @property string $_edit_link
 * @property bool $hierarchical
 * @property bool $public
 * @property array $rewrite
 * @property bool $has_archive
 * @property string $query_var
 * @property mixed $register_meta_box_cb
 * @property array $taxonomies
 * @property bool $show_ui
 * @property mixed $menu_position
 * @property mixed $menu_icon
 * @property int $permalink_epmask
 * @property bool $can_export
 * @property bool $show_in_nav_menus
 * @property bool $show_in_menu
 * @property bool $show_in_admin_bar
 * @property string $label
 * @property string $name
 * @property object $cap
 * @property array $sunrise_args
 */
class SR_DOCS_Post_Type_Object {
}
/**
	* @property int $ID
	* @property string $post_type
  * @property string $post_status
  * @property string $post_name
  * @property string $post_title
  * @property string $post_parent
	*/
class SR_DOCS_Post {
}

/**
 * Documents stdClass object returned by Sunrise::get_context()
 *
 * @property bool $is_wp_ajax
 * @property string $pagenow
 * @property string $typenow
 * @property string $page
 * @property string $action
 * @property int $post_id
 * @property int $object_id
 * @property int $post_parent
 * @property string $post_type
 * @property string $object_type
 * @property string $object_sub_type
 * @property string $parent_type
 * @property SR_DOCS_Post $post
 * @property SR_DOCS_Post $object
 * @property SR_DOCS_Post $parent
 *
 */
class SR_DOCS_Context {
}

function generate_PHPDOC( $object ) {
	foreach( (array)$object as $property => $value ) {
		$type = gettype( $value );
		switch ( $type ) {
			case 'integer': $type = 'int';
				break;
			case 'boolean': $type = 'bool';
				break;
			case 'NULL': $type = 'mixed';
				break;
		}
		echo "\n\t* @property {$type} \${$property}";
	}
	exit;
}
