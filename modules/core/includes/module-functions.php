<?php

function sr_register_module( $module_name, $args = array() ) {
	Sunrise_Modules::register( $module_name, $args = array() );
}

/**
 * Performs a 'require()' for all Sunrise Modules specified and all their dependencies.
 *
 * The function should be called from a 'muplugins_loaded' hook in a theme or plugin to allow the
 * themer/plugin developer to specify which Sunrise modules to enable. It handles all dependencies
 * as specified in the dependency.php files.
 *
 * @example
 *
 *    // Put this in theme's functions.php file, or a plugin's file.
 *    add_action( 'muplugins_loaded',  'yourtheme_muplugins_loaded' );
 *    function yourtheme_muplugins_loaded() {
 *      sr_enable_modules( array( 'fields', 'microsites', 'admin-skins' ) );
 *    }
 *
 * @param array $modules - List of module names.
 * @param int $depth - Not passed; used internally.
 * @return array - List of modules found to include; return value is used internally.
 *
 */
function sr_enable_modules( $modules, $depth = 0 ) {
	global $sr_dependencies;
	$index = 0;
	while( $index < count( $modules ) ) {
		if ( ! isset( $modules[$index] ) )
			break;

		$module_name = $modules[$index];

		if ( ! isset( $sr_dependencies[$module_name] ) ) {
			$module_name = "sunrise-{$module_name}";
		  if ( ! isset( $sr_dependencies[$module_name] ) ) {
		    $index++;
		    continue;
		  } else {
				$modules[$index++] = $module_name;
		  }
		} else {
			$index++;
		}

		if ( 1 == count( $sr_dependencies[$module_name] ) && 'sunrise-core' == $sr_dependencies[$module_name][0] )
			continue;

		if ( 0 == count( $sr_dependencies[$module_name] ) )
			continue;

		$diff = array_diff(  $sr_dependencies[$module_name], $modules );
		if ( count( $diff ) ) {
			$before = 1 < $index ? array_slice( $modules, 0, -1 + $index ) : array();
			$after = array_slice( $modules, $index );
			$modules = array_merge( $before, sr_enable_modules( $diff, $depth+1 ), array( $module_name), $after );
			$index += count( $diff );
		}
	}
	if ( 0 == $depth ) {
		foreach( $modules as $module_name ) {
			if ( 'sunrise-core' == $module_name )
				continue;
			$module_file = sr_module_main_file( $module_name );
			require( $module_file );
		}
	}
	return $modules;
}

/**
 * Function that generates the main file name for a Sunrise module from it's module name.
 *
 * @param string $module_name - Must start with 'sunrise-'
 * @return string
 *
 * TODO: Enable support of 3rd party modules
 *
 */
function sr_module_main_file( $module_name ) {
	$short_name = str_replace( 'sunrise-', '', $module_name );
	$modules_directory = dirname( dirname( dirname( __FILE__ ) ) );
	return "{$modules_directory}/{$short_name}/{$module_name}.php";
}
function sr_module_dirs() {
	return Sunrise_Static_Base::get_module_dirs();
}
/**
 * Function to be called from the main Sunrise loader file (/sunrise/sunrise.php) that loads
 * the Sunrise module dependency.php file for ecah module so that a call to sr_enable_modules()
 * from a 'muplugins_loaded' hook can 'require()' all the module specified in the hook.
 *
 * @return void
 *
 * TODO (mikes 2012-01-30): Make this more performant.
 */
function sr_load_module_dependencies() {
	foreach( sr_module_dirs() as $module_dir )
		if ( file_exists( $dependency_file = "{$module_dir}/dependencies.php" ) )
			require( $dependency_file );
}
/**
 * To be used in each modules dependencies.php file to register dependencies for a Sunrise Module.
 *
 * @example
 * This example is the entire file for Sunrise Fields (at the time this documentation was written.)
 *
 *    File: /wp-content/mu-plugins/sunrise/modules/fields/dependencies.php
 *
 *    <?php
 *    sr_enqueue_dependencies( __FILE__, array(
 *    	'sunrise-files',
 *    	'sunrise-controls',
 *    	'sunrise-images',
 *    	'sunrise-relationships',
 *    ));
 *
 * @param string $file - Always just specify __FILE__
 * @param array $dependencies - Array valid of Sunrise Module Names, in order they should be loaded.
 * @return void
 *
 */
function sr_enqueue_dependencies( $file, $dependencies = array() ) {
	global $sr_dependencies;
	if ( ! isset( $sr_dependencies ) )
		$sr_dependencies = array();
	if ( ! is_array( $dependencies ) )
		$dependencies = array( $dependencies );
	$module = preg_replace( '#(^.*)[\\\/]([^/]+)[\\\/]dependencies\.php$#', 'sunrise-$2', $file );
	$sr_dependencies[$module] = $dependencies;

	if ( preg_match( '#^sunrise-#', $module ) && 'sunrise-core' != $module )
		array_unshift( $sr_dependencies[$module], 'sunrise-core' );
}

