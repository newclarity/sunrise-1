<?php
/**
 * Is environment the right name?  Is context better?  Or something else?
 */
if ( ! class_exists( 'Sunrise_Environment' ) ) {
	class Sunrise_Environment {
		static $environment = array();
		static function push() {
			$item = array(
				'post' => $GLOBALS['post'],
			);
			self::$environment[] = $item;
			return $item;
		}
		static function pop() {
			$item = array_pop( self::$environment );
			$GLOBALS['post'] = $item['post'];
			return $item;
		}
	}
}
