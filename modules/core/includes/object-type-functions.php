<?php
/**
 * Object Type functions
 *
 * @package Sunrise Core
 * @subpackage Object Types
 */

/*
 * Returns an Object Classifier object.
 *
 * Returns an Object Classifier object.	Object Classifiers have 'type' and 'subtype' attributes, i.e. 'post' & 'person' for example.
 *
 * Examples:
 *
 *	$object_classifier = sr_get_classifier( "post/person" );
 *	$object_classifier = sr_get_classifier( array( 'post', 'person' );
 *	$object_classifier = sr_get_classifier( 'post', 'person' );
 *	$object_classifier = sr_get_classifier( $object_classifier ); 	// This just a passthru
 *  echo $object_classifier->type; 			// Prints 'post'
 *  echo $object_classifier->subtype; 		// Prints 'person'
 *  echo $object_classifier['type']; 		// Prints 'post'
 *  echo $object_classifier['subtype']; 	// Prints 'person'
 *  echo strpos( $object_classifier, 'post/' );
 *
 * @param string|array|Sunrise_Classifier $arg1 An object classifier or an object type
 * @param string|boolean $arg2 An object subtype
 *
 */
function sr_get_classifier( $arg1, $arg2 = false ) {
	if ( ! $arg2 && $arg1 instanceof Sunrise_Classifier ) {
		$object_classifier = $arg1;
	} else {
		$object_classifier = new Sunrise_Classifier();
		call_user_func_array( array( $object_classifier, 'set' ), func_get_args() );
	}
	return $object_classifier;
}

/**
 * Creates the list of object types when the 'init' action is fired.
 *
 */
function sr_create_object_types() {
	global $sr_object_types;
	$sr_object_types = array(
		'post' => array( 'name' => __( 'Post' ) ),   // General case of a post type
		'page' => array( 'name' => __( 'Page' ) ),   // Special case of $post->post_type == 'page'
		'user' => array( 'name' => __( 'User' ) ),
		'term' => array( 'name' => __( 'Term' ) ),
		'term_taxonomy' => array( 'name' => __( 'Taxonomy Term' ) ),
		'term_relationship' => array( 'name' => __( 'Taxonomy Term Object' ) ),
		'link' => array( 'name' => __( 'Link' ) ),
		'comment' => array( 'name' => __( 'Comment' ) ),
		'option' => array( 'name' => __( 'Option' ) ),
		'postmeta' => array( 'name' => __( 'Post Metadata' ) ),
		'usermeta' => array( 'name' => __( 'User Metadata' ) ),
		'commentmeta' => array( 'name' => __( 'Comment Metadata' ) ),
		'site' => array( 'name' => __( 'Multisite Network' ) ),
		'blog' => array( 'name' => __( 'Multisite Site/Blog' ) ),
		'relationship' => array( 'name' => __( 'Object Relationship' ) ),
	);
	return $sr_object_types;
}
add_action( 'init', 'sr_create_object_types', 0 );  // 0 is highest priority

/**
 * Return the list of object types
 *
 */
function sr_get_object_types() {
	global $sr_object_types;
	return apply_filters( 'sr_get_object_types', $sr_object_types );
}

/**
 * Adds an object type.
 *
 */
function sr_add_object_type( $object_type, $args = array() ) {
	global $sr_object_types;
	$sr_object_types[ $object_type ] = $args;
	return $sr_object_types;
}

/**
 * Removes an object type.
 *
 * @since 3.3
 */
function sr_remove_object_type( $object_type ) {
	global $sr_object_types;
	unset( $sr_object_types[ $object_type ] );
}



