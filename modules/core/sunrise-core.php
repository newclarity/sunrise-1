<?php
/*
Plugin Name: Sunrise Core
Plugin URL: http://docs.getsunrise.org/core/
File Name: sunrise-core.php
Author: The Sunrise Team
Author URL: http://getsunrise.org
Version: 0.0.0
Description: Core functionality for Sunrise
Globals Exposed:
Hooks Exposed:
*/

define( 'SUNRISE_CORE_DIR', dirname( __FILE__ ) );
require( SUNRISE_CORE_DIR . '/includes/module-functions.php');
require( SUNRISE_CORE_DIR . '/includes/functions.php');
require( SUNRISE_CORE_DIR . '/includes/debugging.php');
require( SUNRISE_CORE_DIR . '/includes/static-base-class.php');
require( SUNRISE_CORE_DIR . '/includes/base-class.php');
require( SUNRISE_CORE_DIR . '/includes/class.php');
require( SUNRISE_CORE_DIR . '/includes/object-classifier-class.php');
require( SUNRISE_CORE_DIR . '/includes/object-type-functions.php');
require( SUNRISE_CORE_DIR . '/includes/query-functions.php');
require( SUNRISE_CORE_DIR . '/includes/class-documentation.php');
require( SUNRISE_CORE_DIR . '/includes/environment-class.php');
require( SUNRISE_CORE_DIR . '/includes/roles-capabilities.php');

/**
 *  This is done at the bottom so we can include the file that defines
 *  sr_enqueue_dependencies(). The rest of the Sunrise modules put this in /dependencies.php.
 */;
sr_enqueue_dependencies( __FILE__ );

sr_load_demos( SUNRISE_CORE_DIR );
