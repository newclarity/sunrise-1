<?php
/*
Plugin Name: Sunrise Fields
Plugin URL: http://docs.getsunrise.org/fields/
File Name: sunrise-fields.php
Author: The Sunrise Team
Author URL: http://getsunrise.org
Version: 0.0.0
Description: Adds Field and Form support for Posts, Users, Taxonomies, Terms, Blogs, Sites, Options and a lot more.
Plugins Required: sunrise-core, sunrise-relationships, sunrise-images
Globals Exposed:
Hooks Exposed:
*/

define( 'SUNRISE_FIELDS_DIR', dirname( __FILE__ ) );
define( 'SUNRISE_FIELDS_URL', plugins_url( '/', __FILE__ ) );
require( SUNRISE_FIELDS_DIR . '/includes/query-functions.php');
require( SUNRISE_FIELDS_DIR . '/includes/field-functions.php');
require( SUNRISE_FIELDS_DIR . '/includes/field-feature-class.php');
require( SUNRISE_FIELDS_DIR . '/includes/field-class.php');
require( SUNRISE_FIELDS_DIR . '/includes/fields-class.php');
require( SUNRISE_FIELDS_DIR . '/includes/form-functions.php');
require( SUNRISE_FIELDS_DIR . '/includes/form-class.php');
require( SUNRISE_FIELDS_DIR . '/includes/forms-class.php');
require( SUNRISE_FIELDS_DIR . '/includes/object-class.php');
require( SUNRISE_FIELDS_DIR . '/includes/object-interface.php');
require( SUNRISE_FIELDS_DIR . '/includes/object-functions.php');
require( SUNRISE_FIELDS_DIR . '/includes/objects-class.php');
require( SUNRISE_FIELDS_DIR . '/includes/storage-class.php');
require( SUNRISE_FIELDS_DIR . '/includes/post-type-functions.php');
require( SUNRISE_FIELDS_DIR . '/includes/post-types-class.php');
require( SUNRISE_FIELDS_DIR . '/includes/column-class.php');

sr_load_plugin_files( 'fields', array( 'form', 'object', 'storage', 'field-feature', 'field' ) );
sr_load_demos( SUNRISE_FIELDS_DIR );


