<?php
if (!class_exists('Sunrise_Post_Object')) {
	/**
	 * @property Sunrise_Form $parent_form
	 */
	class Sunrise_Post_Object extends Sunrise_Object implements Sunrise_Object_Interface {
		static function on_load() {
			sr_add_action( __CLASS__, 'add_meta_boxes', 10, 2 );
			sr_add_filter( __CLASS__, 'default_title', 10, 2 );
			sr_add_action( __CLASS__, 'wp_insert_post_data', 10, 2 );
			sr_add_action( __CLASS__, 'save_post', 10, 2 );
			sr_add_action( __CLASS__, 'pre_post_title' );
		}
		function get_object_id() {
			if ( ! $this->_object_id && isset( $this->_object->ID ) ) {
				$this->_object_id = intval( $this->_object->ID );
			}
			return $this->_object_id;
		}
		function get_object_sub_type() {
			if ( ! $this->_object_sub_type && isset( $this->_object->ID ) ) {
				$this->_object_sub_type = $this->_object->post_type;
			}
			return $this->_object_sub_type;
		}
		function load_object( $object_id = null ) {
			if ( ! $object_id ) {
				$object_id = $this->get_object_id();
			}
			$this->object = get_post( $object_id );
			return $this->object;
		}
		/**
		 * @static
		 * @param $post_type
		 * @param $post
		 * @return void
		 *
		 * TODO (mikes 2012-12-13): Move this to /form-types/admin-post-form.php
		 */
		static function add_meta_boxes( $post_type, $post ) {
			if ( sr_has_post_forms( $post_type, array(
				'post' => $post,
				'object' => $post
			))) {
				/**
				 * @var Sunrise_Admin_Post_Form $post_form
				 */
				$post_forms = sr_get_post_forms( $post_type, array( 'object' => $post ) );
				foreach( $post_forms as $form_name => $post_form ) {
					if ( ! $post_form->hidden ) {
						add_meta_box(
							$metabox_name = sr_dashize("{$post_type}-{$form_name}-metabox"), // TODO: Get a better name than sr_dashize()?
							$post_form->title,
							array( __CLASS__, 'the_post_fields_metabox' ),
							$post_type,
							$post_form->context,
							$post_form->priority,
							array(
							 'form_name' => $form_name,
							)
						);
						self::set_meta_box_view_state( $post_form->view_state, $metabox_name, $post_type );
					}
				}
			}
		}

		/**
		 * View state determines whether a meta box is collapsed or not when a user visits an edit screen.
		 * An array of closed metaboxes is stored by WordPress when a user minimizes a meta box.
		 *
		 * @static
		 * @param $view_state
		 * @param $metabox_name
		 * @param $post_type
		 */
		static function set_meta_box_view_state( $view_state, $metabox_name, $post_type ) {
			$user_id = get_current_user_id();
			$closed_metaboxes = get_user_option( "closedpostboxes_$post_type", $user_id );
			switch( $view_state ) {
				case 'initially_closed':
					if( ! is_array($closed_metaboxes) ){
						$closed_metaboxes = array( $metabox_name );
					}
					break;
				case 'always_open':
					if( is_array($closed_metaboxes) ) {
						$key = array_search( $metabox_name, $closed_metaboxes );
						if( $key !== false ) {
							unset( $closed_metaboxes[$key] );
						}
					}
					break;
				case 'always_closed':
					if( ! is_array($closed_metaboxes) ){
						$closed_metaboxes = array( $metabox_name );
					} else {
						$closed_metaboxes[] = $metabox_name;
					}
					break;
				default:
					// 'initially_open' is the WordPress default.  Do nothing.
			}
			if ( is_array( $closed_metaboxes ) ) {
				update_user_option( $user_id, "closedpostboxes_$post_type", array_unique( $closed_metaboxes ), true );
			}
		}

		/**
		 * @static
		 * @param $post
		 * @param $metabox
		 * @return void
		 *
		 * TODO (mikes 2012-12-13): Move this to /form-types/admin-post-form.php
		 */
		static function the_post_fields_metabox( $post, $metabox ) {
			$form_name = $metabox['args']['form_name'];
			/**
			 * @var Sunrise_Admin_Post_Form $form
			 */
			$form = Sunrise_Forms::get_form( $form_name, array(
				'object_type'     => 'post',
				'object_sub_type' => $post->post_type,
			));
			$form->object = $post;
			$form->metabox = $metabox;
			$form->wrapper_id = preg_replace( '#^(.*)-metabox$#', '$1', $metabox['id'] ) . '-wrapper';
			echo $form->get_html();
			echo $form->get_object_args_html();
		}
		/*
		 * This could potentially be used give post_title a default value
		 * See: http://core.trac.wordpress.org/ticket/18713
		 * TODO: Decide if we should use this or remove it.
		 */
		static function pre_post_title( $value ) {
//			$post_id = isset( $_POST['post_ID'] ) ? $_POST['post_ID'] : '';
			return $value;
		}
		static function default_title( $post_title, $post ) {
			// TODO: Determine if these are the right defaults we should have?
			if ( empty( $post_title ) ) {
				$post_title = 'Auto Draft';
				if ( isset( $post->post_type ) ) {
					$post_type_object = get_post_type_object( $post->post_type );
					$post_title .= ': ' . $post_type_object->labels->singular_name;
				}
			}
			return $post_title;
		}
		static function wp_insert_post_data( $data, $postarr ) {
			if ( isset( $_POST['wp-preview'] ) && 'dopreview' == $_POST['wp-preview'] ) {
				/**
				 * TODO (mikes 2011-12-20): Fix this short term HACK to handle the Preview Button on the Post Edit Screen.
				 */
				return $data;
			}
			/**
			 * We currently don't saving values for revisions.
			 * TODO (mikes 2011-12-21): Added revision support.
			 */
			if ( 'revision' == $postarr['post_type'] )
				return $data;

			/*
			 * For each fields for this post type that has a get_value callback,
			 * call the callback to get it's value and store its value in $data.
			 *
			 */
			if ( Sunrise_Forms::get_mode() == 'edit' ) {
				$form_args = Sunrise_Objects::get_args_from_POST( $postarr ) ;
				$forms = sr_get_post_forms( $data['post_type'], $form_args );
				$form_args = array(
					'object_type'     => 'post',
					'object_sub_type' => $data['post_type'],
				);
				/**
				 * TODO (mikes 2011-12-20): Limit this to $postarr->object_forms?
				 */
				foreach( $forms as $form_name => $form ) {
					$form_args['form_name'] = $form_name;
					$fields = Sunrise_Forms::get_fields( $form_args );
					foreach( $fields as $field_name => $field ) {
						if ( $field->get_value ) {
							$field->_raw_value = call_user_func( $field->get_value, $postarr );
							$data[$field->real_name] = $field->_raw_value;  // TODO: Make this work with complex fields like $foo[1][bar][baz]
						}
					}
				}
			}

			// TODO: Verify that the next code is still required.
			if ( $postarr['post_status'] != 'auto-draft' ) {
				if ( empty( $data['post_name'] ) ) {
					$data['post_name'] = sanitize_title_with_dashes( $data['post_title'] );
				}
			}
			/*
			 * This fixes a problem where post_names end up being auto-draft-{$whatever}
			 * Would like to figure out why this happens and then fix why, but for now...
			 * TODO: Verify that the next code is still required.
			 *
			 */
		  if ( substr( $data['post_name'], 0, 11 ) == 'auto-draft-' ) {
				$data['post_name'] = sanitize_title_with_dashes( $data['post_title'] );
			}

			return $data;
		}
		static function save_post( $object_id, $object ) {
			if ( ! isset( $_POST['object_forms'] ) ||
					 ! isset( $_POST['object_type'] ) ||
					 ! isset( $_POST['object_sub_type'] ) )
				return;

			$form_args = Sunrise_Objects::get_args_from_POST();

			if ( ! Sunrise_Forms::has_forms( $form_args ) )
				return;

			foreach( $_POST['object_forms'] as $form_name ) {
				$form_args['form_name'] = $form_name;
				$form = Sunrise_Forms::get_form( $form_name, $form_args );
				$form->object_adapter = $form_args['object_adapter'];
				if ( $form_fields = Sunrise_Forms::get_fields( $form_args ) )
					/**
					 * @var Sunrise_Field $field
					 */
					foreach( $form_fields as $field ) {
						$field->form = $form;
						$field->object_adapter = $form_args['object_adapter'];
						$field->update_from_POST();
					}
			}

		}
		function get_parent_form() {
			$value = false;
			if ( !empty( $this->_parent->form->form_name ) )
				$value = $this->_parent->form->form_name;
			return $value;
		}
		function get_field_value( Sunrise_Field $field, array $args = array() ) {
			if ( 'core' == $field->storage_type ) {
				$post_id = $field->object_id;
				$post = get_post( $post_id );
				$value = $post->{$field->field_name};
			} else {
				$value = false;
				sr_die( 'We\'ve not implemented this yet.' );
			}
			return $value;
		}
		/*
		 * Get an array of values for this post object. It will contain
		 * all the fields and all the custom meta values that are fields
		 * as defined for this post type. Used to create a $post array
		 * similar to the 2nd parameter passed ot the wp_insert_post_data
		 * hook thus allowing us to use the same callback we use for
		 * $field->get_value for both creating new values on an 'add' form
		 * as well as when loading the data from existing. The use-case
		 * we developed this for is for when post_title is hidden but needs
		 * to be calculated. We expect it will be used for other calculated
		 * field too.
		 *
		 * NOTE: We have to postpone loading calculated values (i.e. $get_values) until
		 * after we've loaded all otherwise or we'll get into an infinite loop.
		 *
		 */
		function load_values() {
			if ( ! $this->_values ) {
				$post = $this->object;
				$values = (array)$post;
				$args = $this->as_args();
				$fields = Sunrise_Forms::get_fields( $args );
				$get_values = array();
				/**
				 * Load all the non-calculated values first.
				 *
				 * @var Sunrise_Field $field
				 *
				 */
				foreach( $fields as $field_name => $field ) {
					if ( $field->get_value ) {
						$get_values['field_name'] = $field;
					} else {
						$field->form = $this->parent_form;
						$field->object_adapter = $this;
						$values[$field_name] = $field->load_value( $args );
					}
				}
				$this->_values = $values;
				foreach( $get_values as $field_name => $field ) {
					$field->form = $this->parent_form;
					$field->object_adapter = $this;
					$this->_values[$field_name] = $field->load_value( $args );
				}
			}
			return $this->_values;
		}
		/*
		 * Allows (re-)setting of both field_name and real_name because of field name
		 * schizophrenia crap in _wp_translate_postdata().
		 *
		 * See /wp-admin/includes/post.php to understand why we have to do this.
		 *
		 */
		function do_pre_initialize( $args ) {
			$post_field = $args['field'];
			if ( ! $post_field->is_core ) {
				if ( ! $post_field->real_name )
					$post_field->real_name = $post_field->field_name;
			} else {
				switch ( $post_field->field_name ) {
					case 'post_ID': $post_field->real_name = 'ID';
						break;
					case 'content': $post_field->real_name = 'post_content';
						break;
					case 'excerpt': $post_field->real_name = 'post_excerpt';
						break;
					case 'parent_id': $post_field->real_name = 'post_parent';
						break;
					default:
						if ( ! $post_field->real_name )
							$post_field->real_name = $post_field->field_name;
						switch ( $post_field->field_name ) {
							case 'ID':           $post_field->field_name = 'post_ID';
								break;
							case 'post_content': $post_field->field_name = 'content';
								break;
							case 'post_excerpt': $post_field->field_name = 'excerpt';
								break;
							case 'post_parent':  $post_field->field_name = 'parent_id';
								break;
						}
				}
			}
		}
		function has_method( $method_name ) {
			return false;
		}
		/*
		 * This function will allow classes that extend Sunrise_Object to recognize args and add to or modify them.
		 * For example, if Sunrise_Post_Object' recognizes 'post_type' it will add 'object_type' => 'post', etc.
		 */
		function __has( $property_name ) {
			return in_array( $property_name, array(
				'post','post_id','post_type',
				'object','object_id','object_type','object_sub_type',   // TODO: Can these be predefined in a base class?
			));
		}
		function __set( $property_name, $value ) {
			// TODO: Move most of this code into Sunrise_Object
			switch ( $property_name ) {
				case 'post_id':
					$this->object_id = $value;
					break;

				case 'post':
					$this->object = $value;
					break;

				case 'post_type':
					$this->object_sub_type = $value;
					break;

				default:
					parent::__set( $property_name, $value );
					break;
			}
		}
		function __get( $property_name ) {
			$value = false;
			switch ( $property_name ) {
				case 'post_id':
					$value = $this->object_id;
					break;

				case 'post':
					$value = $this->object;
					break;

				case 'post_type':
					$value = $this->object_sub_type;
					break;

				case 'object_type':
					$value = 'post';
					break;

				case 'parent_form':
					$value = $this->get_parent_form();
					break;

				default:
					$value = parent::__get( $property_name );
					break;
			}
			return $value;
		}
	}
	Sunrise_Post_Object::on_load();
}


