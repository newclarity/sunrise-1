<?php
/*
 * NOTE: Fieldsets currently cannot contain Repeating Fields. The 'top-level' class is used to
 * count fields to allow for adding addition and it is not assigned when a field is in a fieldset.
 * To add support for Repeating Fields in a Fieldset, start by fixing the 'top-level' logic.
 *
 */
if (!class_exists('Sunrise_Field_Feature')) {
	/**
	 * @property bool|string $feature_type
	 */
	class Sunrise_Field_Feature extends Sunrise_Base {
		/**
		 * @var bool|Sunrise_Field
		 */
		var $parent           = false;
		var $html_tag         = false;
		var $text		          = false;
		var $position		      = false;  // 'above', 'below',	'left',	'right'
		var $float		        = false;  // 'left','right' or false
		var $extra		        = array();

		var $accesskey       = false;   // The accesskey attribute for HTML <input>
		var $dir             = false;   // The dir attribute for HTML <input>
		var $lang            = false;   // The lang attribute for HTML <input>
		var $style           = false;   // The style attribute for HTML <input>
		var $tabindex        = false;   // The tabindex attribute for HTML <input>
		var $title           = false;   // The title attribute for HTML <input>
		var $xml_lang        = false;   // The xml:lang attribute for HTML <input>

		var $container_style = false;

		private $_attributes_html = false;
		private $_inner_attributes_html = false;
		private $_container_attributes_html = false;
		protected $_feature_type = false;
		var $_inner_html = false;

		function __construct( $args ) {
			/*
			 * Call to initialize $this->_feature_type.
			 */
			$this->get_feature_type();

			/*
			 * Run any pre-initializations needed
			 */
			Sunrise::do_instance_action( $this, 'pre_initialize', $args );

			/*
			 * Grab all the args that are properties or _properties and set them.
			 * Put everything into $args and everything else in $extra.
			 */
			$this->construct_from_args( $args );

			/*
			 * Let the subclass initialize
			 */
			$this->do_action( 'initialize', $args );

			/*
			 * The default container tag is <span> for almost every feature
			 */
			if ( !$this->html_tag )
				$this->html_tag = 'span';

			/*
			 * The default position will be 'left'
			 */
			if ( !$this->position )
				$this->position = 'left';

		}
		static function get_valid_types() {
			static $valid_types = array(
				'Sunrise_Entry_Field_Feature'     =>  'entry',
				'Sunrise_Label_Field_Feature'     =>  'label',
				'Sunrise_Help_Field_Feature'      =>  'help',
				'Sunrise_Infobox_Field_Feature'   =>  'infobox',
				'Sunrise_Message_Field_Feature'   =>  'message',
			);
			return $valid_types;
		}
		protected function get_feature_type() {
			static $feature_lookup = false;
			if ( ! $feature_lookup )
				$feature_lookup = Sunrise::get_valid_types( 'fields/field-feature');

			if ( ! $this->_feature_type )
				$this->_feature_type = $feature_lookup[get_class( $this )];

			return $this->_feature_type;
		}
		static function get_standard_attributes() {
			static $attributes = array( 'accesskey', 'class', 'dir', 'lang', 'style', 'tabindex', 'title', 'xml_lang' );
			return $attributes;
		}
		static function get_span_attributes() {
			static $attributes = array( 'class', 'dir', 'id', 'lang', 'style', 'title', 'xml_lang' );
			return $attributes;
		}
		static function get_valid_container_attributes() {
			return self::get_standard_attributes();
		}
		function get_valid_attributes() {
			return $this->apply_filters( 'valid_attributes', self::get_standard_attributes() );
		}
		function get_container_class() {
			$container_class = $this->apply_filters( 'container_class', "{$this->feature_type}-container{$this->position_class}" );
			return trim( $this->parent->apply_filters( "{$this->_feature_type}_feature_container_class", $container_class ) );
		}
		function get_class() {
			$value = $this->apply_filters( 'class', "{$this->feature_type}-feature feature" );
			return trim( $this->parent->apply_filters( "{$this->_feature_type}_feature_class", $value ) );
		}
		/**
		 * TODO (mikes): This logic need to be changed so as not to reference field types.
		 * TODO: Plans are to make 'repeating' and 'fieldset' an attribute of any field.
		 */
		function get_contains_fieldsets() {
			$contains_fieldsets = false;
			if ( 'entry' == $this->feature_type ) {
				if ( 'repeating' == $this->parent->field_type ) {
					if ( 'fieldset' == $this->parent->field_args['field_type'] ) {
						$contains_fieldsets = true;
					}
				}
			}
			return $contains_fieldsets;
		}
		function get_html() {
			$html =<<<HTML
<div id="{$this->parent->id}-{$this->feature_type}" {$this->inner_class_html}{$this->inner_attributes_html}>{$this->inner_html}</div>
<div class="clear"></div>
HTML;
			/**
			 * TODO (mikes): Verify this next line is in fact not needed.
			 * TODO: Goal is to minimize hooks to just the ones needed.
			 */
			//$html = $this->apply_filters( 'html', $html );
			return $this->parent->apply_filters( "{$this->_feature_type}_feature_html", $html );
		}
		function get_inner_html() {
			if ( ! $this->_inner_html ) {
				$value = $this->apply_filters( 'inner_html', $this->text );
				$this->_inner_html = $this->parent->apply_filters( "{$this->_feature_type}_feature_inner_html", $value );
			}
			return $this->_inner_html;
		}
//		function get_list_controls_html() {
//			return $this->parent->list_controls_html;
//		}
		function get_class_html() {
			$value = $this->apply_filters( 'class_html', " class=\"{$this->class}\"", $this->class );
			return $this->parent->apply_filters( "{$this->_feature_type}_feature_class_html", $value );
		}
		function get_inner_class() {
			//$has_field_controls = $this->has_field_controls ? ' has-field-controls' : '';
			$contains_fieldsets = $this->contains_fieldsets ? ' contains-fieldsets' : '';
			$template = ( $this->parent->index && 'template' == $this->parent->index ) ? ' template' : '';
//			$class = "{$this->feature_type}-feature-inner{$template}{$has_field_controls}{$contains_fieldsets}";
			$class = "{$this->feature_type}-feature-inner{$template}{$contains_fieldsets}";
			$value = $this->apply_filters( 'inner_class', $class );
			return trim( $this->parent->apply_filters( "{$this->_feature_type}_feature_inner_class", $value ) );
		}
		function get_inner_class_html() {
			$value = $this->apply_filters( 'inner_class_html', " class=\"{$this->inner_class}\"" );
			return $this->parent->apply_filters( "{$this->_feature_type}_feature_inner_class_html", $value );
		}
		function get_inner_attributes_html() {
			if ( ! $this->_inner_attributes_html ) {
				$value = self::_get_attributes_html( $this, $this->get_valid_container_attributes() );
				$value = implode( ' ', $this->apply_filters( 'inner_attributes_html', $value ) );
				$value = $this->parent->apply_filters( "{$this->_feature_type}_feature_inner_attributes_html", $value );
				$this->_inner_attributes_html = $value;
			}
			return $this->_inner_attributes_html;
		}
		function get_container_class_html() {
			$value = $this->apply_filters( 'container_class_html', " class=\"{$this->container_class}\"" );
			return $this->parent->apply_filters( "{$this->_feature_type}_feature_container_class_html", $value );
		}
		function get_container_html() {
			$value = false;
			if ( $inner_html = $this->get_inner_html() ) { // TODO: Determine if there are reasons we need to display even if inner_html is empty
				$float_html = $this->float ? " {$this->float}" : '';
				$html = $this->html;
				$value = $this->apply_filters( 'container_html',
					"<span id=\"{$this->parent->id}-{$this->feature_type}-container\" {$this->container_class_html}{$this->container_attributes_html}{$float_html}>\n
						{$html}\n
					</span>" );
				$value = $this->parent->apply_filters( "{$this->_feature_type}_feature_container_html", $value );
			}
			return $value;
		}
		function get_position_class() {
			$value = $this->apply_filters( 'position', " position-{$this->position}" );
			return $this->parent->apply_filters( "{$this->_feature_type}_feature_position_class", $value );
		}
		function get_checked() {
			$value = $this->apply_filters( 'checked', $this->parent->get_raw_value() == 'checked' );
			return $this->parent->apply_filters( "{$this->_feature_type}_feature_checked", $value );
		}
		function get_container_attributes_html() {
			if ( ! $this->_container_attributes_html ) {
				$value = self::_get_attributes_html( $this, $this->get_valid_container_attributes() );
				$value = implode( ' ', $this->apply_filters( 'container_attributes_html', $value ) );
				$value = $this->parent->apply_filters( "{$this->_feature_type}_feature_container_attributes_html", $value );
				$this->_container_attributes_html = $value;
			}
			return $this->_container_attributes_html;
		}
		function get_attributes_html() {
			if ( ! $this->_attributes_html ) {
				$value = self::_get_attributes_html( $this, $this->get_valid_attributes() );
				$value = implode( ' ', $this->apply_filters( 'attributes_html', $value ) );
				$value = $this->parent->apply_filters( "{$this->_feature_type}_feature_attributes_html", $value );
				$this->_attributes_html = $value;
			}
			return $this->_attributes_html;
		}
		// TODO: Attributes aren't properly escaped when a field is loaded via AJAX (Micah 2013-10-24)
		private static function _get_attributes_html( $element, $valid_attributes ) {
			static $switch_attributes;
			if ( !isset( $switch_attributes ) )
				$switch_attributes = array_flip( array( 'readonly', 'checked', 'selected', 'disabled' ) );

			$attributes_html = array();
			foreach( $valid_attributes as $attribute ) {
				$is_switch = isset( $switch_attributes[$attribute] );
				if ( $attribute == 'value' ) {
					$value = $element->parent->get_attribute_value();
				} else if ( isset( $element->$attribute ) || $is_switch ) {
					$value = $element->$attribute;
					if ( $attribute == 'xml_lang' ) {
						$attribute = 'xml:lang';
					}
				} else if ( isset( $element->parent->$attribute ) ) {
					$value = $element->parent->$attribute;
				} else {
					$value = false;
				}
				if ( empty( $value ) ) {
					$attribute_html = '';
				} else if ( $is_switch ) {        // i.e. readonly="readonly"
					$attribute_html = " {$attribute}=\"{$attribute}\"";
				} else {                          // i.e. size="10"
					if ( is_wp_error( $value ) ) {
						echo 'WTF?!?';
					} else {
						$value = esc_attr( $value );
						$attribute_html = " {$attribute}=\"{$value}\"";
					}
				}
				if ( ! empty( $attribute_html ) )
					$attributes_html[] = $attribute_html;
			}
			return $attributes_html;
		}
		function __get( $property_name ) {
			$value = false;
			switch ( $property_name ) {
				case 'feature_type':
					$value = $this->_feature_type; // This will be set in the constructor, no need to test first.
					break;

				case 'html':
					$value = $this->get_html();
					break;

				case 'inner_html':
					$value = $this->get_inner_html();
					break;

				case 'contains_fieldsets':
					$value = $this->get_contains_fieldsets();
					break;

				case 'class':
					$value = $this->get_class();
					break;

				case 'inner_class':
					$value = $this->get_inner_class();
					break;

				case 'class_html':
					$value = $this->get_class_html();
					break;

				case 'inner_class_html':
					$value = $this->get_inner_class_html();
					break;

				case 'container_class':
					$value = $this->get_container_class();
					break;

				case 'container_html':
					$value = $this->get_container_html();
					break;

				case 'container_class_html':
					$value = $this->get_container_class_html();
					break;

				case 'attributes_html':
					$value = $this->get_attributes_html();
					break;

				case 'inner_attributes_html':
					$value = $this->get_inner_attributes_html();
					break;

				case 'container_attributes_html':
					$value = $this->get_container_attributes_html();
					break;

				case 'position_class':
					$value = $this->get_position_class();
					break;

				case 'checked':
					$value = $this->get_checked();
					break;

				case 'name':  // Here to simplify debugging
					$value = $this->text;
					break;

				case 'help_container_html':
				case 'infobox_container_html':
				case 'label_container_html':
				case 'message_container_html':
					$feature_type = str_replace( '_container_html', '_feature', $property_name );
					$value = $this->parent->{$feature_type}->get_container_html();
					break;

				default:
					sr_die( "Cannot __get() property [{$property_name}] for class [" . get_class( $this ) . "]." );
					break;
			}
			return $value;
		}
	}
}

