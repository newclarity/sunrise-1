<?php
if (!class_exists('Sunrise_Forms')) {
	class Sunrise_Forms extends Sunrise_Static_Base {
		private static $_mode = false;  // 'add' or 'edit'
		static function get_valid_types() {
			return Sunrise::get_valid_types( 'fields/form' );
		}
		static function is_valid_types( $form_type ) {
			return Sunrise::is_valid_type( 'fields/form', $form_type );
		}
		static function get_class_for( $form_type ) {
			return Sunrise::get_class_for( 'fields/form', $form_type );
		}
		static function get_instance_for( $form_type, $args = array() ) {
			return Sunrise::get_instance_for( 'fields/form', $form_type, $args );
		}
		/*
		 * Returns a mode of 'add' or 'edit' depending on the current mode for this form.
		 * Returns false if mode cannot be determined.
		 * TODO: Break this up into calling sub forms.
		 */
		static function get_mode() {
			if ( ! self::$_mode ) {
				global $pagenow;
				if ( 'post-new.php' == $pagenow )
					self::$_mode = 'add';
				else if ( 'admin-ajax.php' == $pagenow )
					self::$_mode = 'ajax';  // TODO: Do we need more than this?
				else if ( 'post.php' == $pagenow )
					if ( ( isset( $_GET['action'] ) && 'edit' == $_GET['action'] ) ||
							 ( isset( $_POST['action'] ) && 'editpost' == $_POST['action'] ) )
					self::$_mode = 'edit';
			}
			return self::$_mode;
		}
		static function get_form_fields( $args = array() ) {
			global $sr_forms;
			$form_fields = false;
			$forms = self::get_forms( $args );
			$form_name = $args['form_name'];
			if ( isset( $forms[$form_name]->fields ) )
				$form_fields = $forms[$form_name]->fields;
			return $form_fields;
		}
		static function get_fields( $args = array() ) {
			global $sr_forms;
			$args = wp_parse_args( $args, array(
		    'form_name'       => false,
				'object_type'     => 'post',
				'object_sub_type' => 'post',
			));
			if ( $args['form_name'] ) {
				$form_fields = self::get_form_fields( $args );
			} else if ( $object_forms = self::get_forms( $args ) ) {
				$form_fields = array();
				foreach( array_keys( $object_forms ) as $form_name ) {
					$args['form_name'] = $form_name;
					$form_fields = array_merge( $form_fields, self::get_fields( $args ) );
				}
			}
			$form_fields = __sr_fields_to_objects( $form_fields, $args );
			return $form_fields;
		}
		static function has_forms( $args ) {
			return false !== self::get_forms( $args );
		}
		/**
		 * @param string $form_name
		 * @param array $args
		 * @return Sunrise_Form
		 */
		static function get_form( $form_name, $args ) {
			$fields = self::get_forms( $args );
			$form = $fields[$form_name];
			return $form;
		}
		static function get_forms( $args ) {
			static $forms = false;
			list( $object_type, $object_sub_type ) = sr_parse_object_type( $args );
			if ( ! isset( $forms[$object_type][$object_sub_type] ) ) {
				if ( WP_DEBUG ) Sunrise_Objects::validate_args( $args );
				global $sr_forms;
				if ( isset( $sr_forms[$object_type][$object_sub_type] ) && isset( $sr_forms[$object_type]['any'] ) ) {
					$type_forms = array_merge( $sr_forms[$object_type]['any'], $sr_forms[$object_type][$object_sub_type] );
				} else if ( isset( $sr_forms[$object_type][$object_sub_type] ) )  {
					$type_forms = $sr_forms[$object_type][$object_sub_type];
				} else if ( isset( $sr_forms[$object_type]['any'] ) ) {
					$type_forms = $sr_forms[$object_type]['any'];
				} else {
					$type_forms = array();
				}
				$type_forms = self::reorder_forms($type_forms);
				$forms[$object_type][$object_sub_type] = apply_filters( 'sr_get_forms', $type_forms, $args );
			}
			return $forms[$object_type][$object_sub_type];
		}
		/**
		 * Sorts forms based on $form->order. It groups 0-99 at top, false in middle, 100+ at bottom.
		 *
		 * @param array $forms
		 * @return array
		 */
		static function reorder_forms( $forms ) {
			$first_forms = $last_forms = array();
			$form_count = count( $forms );
			/**
			 * Split all forms with an explicit order set into two groups: first and last.
			 * Leave the ones with false.
			 */
			foreach( $forms as $form_name => $form )
				if ( false !== $form->order ) {
					if ( $form->order < 100 ) {
						$first_forms[$form->order][] = $form;
					} else {
						$last_forms[$form->order][] = $form;
					}
					unset( $forms[$form_name] );
				}

			/**
			 * Order by sets of forms by their specified order
			 */
			ksort( $first_forms );
			ksort( $last_forms );
			/**
			 * Add in the $form->order===false
			 */
			$last_first = 0 == count( $first_forms ) ? 0 : end( array_keys( $first_forms ) ) + 1;
			foreach( $forms as $form )
				$first_forms[$last_first++] = $form;

			/**
			 * Add the nested forms whose $form->order >= 100
			 */
			foreach( $last_forms as $form_list )
				foreach( $form_list as $form )
					$first_forms[$last_first++] = $form;

			/**
			 * Finally, reassemble the ordered forms using $form->form_name as key.
			 */
			$forms = array();
			foreach( $first_forms as $form )
				$forms[$form->form_name] = $form;

			return $forms;
		}
	}
}

