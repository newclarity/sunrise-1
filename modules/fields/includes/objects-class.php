<?php
if (!class_exists('Sunrise_Objects')) {
	class Sunrise_Objects extends Sunrise_Static_Base {
		/**
		 * @var bool|array $_POST_args
		 */
		static $_POST_args = false;
		/**
		 * @var bool|array $_valid_types
		 */
		static $_valid_types = false;

		/**
		 * Returns the properties of the object adapter as an array, populated from $_POST
		 *
		 * @return array|bool
		 */
		static function get_args_from_POST( $args = false ) {
			if ( ! self::$_POST_args ) {
				if ( ! $args )
					$args =  $_POST;
				if ( isset( $args['object_id'] ) && isset( $args['object_type'] ) ) {
					$object_adapter = Sunrise_Objects::get_instance_for( $args['object_type'], $args );
					$new_args = $object_adapter->as_args();
				} else {
					// If $_POST['object_id'] not set, then get what you can.
					$new_args = array();
					foreach( get_class_vars( 'Sunrise_Object' ) as $name => $value ) {
						if ( '_' == $name[0] ) {
							$name = substr( $name, 1 );
						}
						$new_args[$name] = isset( $args[$name] ) ? esc_attr( $args[$name] ) : false;
					}
				}
				self::$_POST_args = $new_args;
			}
			return self::$_POST_args;
		}
		static function validate_args( $args ) { // TODO: Make this more generic
			extract( $args );
			$class_name = 'Sunrise_Xxx';  // TODO: Use callstack to capture this
			$method_name = 'yyy';         // TODO: Use callstack to capture this
			if ( ! isset( $object_type ) ) {
				sr_die( "{$class_name}::{$method_name}(\$args) must have a value for \$args['object_type']." );
			}
			if ( ! isset( $object_sub_type ) ) {
				sr_die( "{$class_name}::{$method_name}(\$args) not yet implemented to support not passing \$args['object_subtype']." );
			}
		}
		/*
		 * Look for an $args['whatever_type'] and set object_type => 'whatever' and object_sub_type => $args['whatever_type']
		 * Call static function Subrise_Whatever_Object::derive_args($args) for further recognition and processing of args.
		 *
		 * TODO: Move this to Sunrise_Fields?
		 */
		static function derive_args( $args ) {
			$object_sub_type = false;
			foreach( self::get_valid_types() as $valid_type ) {
				if ( isset( $args["{$valid_type}_type"] ) ) {
					$object_sub_type = $args["{$valid_type}_type"];
					break;
				}
			}
			if ( $object_sub_type ) {
				$args['object_type'] = $valid_type;
				$args['object_sub_type'] = $object_sub_type;
				$class = self::get_class_for( $valid_type );
				$args = call_user_func( array( $class, 'derive_args' ), $args );
			}
			return $args;
		}
		static function get_class_for( $object_type ) {
			return Sunrise::get_class_for( 'fields/object', $object_type );
		}
		static function get_instance_for( $object_type, $args = array() ) {
			return Sunrise::get_instance_for( 'fields/object', $object_type, $args );
		}
		static function is_valid_type( $object_type ) {
			return Sunrise::is_valid_type( 'fields/object', $object_type );
		}
		static function get_valid_types() {
			return Sunrise::get_valid_types( 'fields/object' );
		}
		static function parse_type( $type_selector ) {
			if ( is_string( $type_selector ) ) {
				if ( false !== strpos( $type_selector, '/' ) ) {
					list( $object_type, $object_sub_type ) = explode( '/', $type_selector );
				} else {
					$object_type = $type_selector;
					$object_sub_type = 'any';
				}
			} else if ( is_array( $type_selector ) ) {
				$object_type = $type_selector['object_type'];
				$object_sub_type = $type_selector['object_sub_type'];
			}
			return array( $object_type, $object_sub_type );
		}
	}
}


