<?php

if ( ! class_exists( 'Sunrise_Storage' ) ) {
	class Sunrise_Storage extends Sunrise_Static_Base {
		static $_valid_types = array();
		static function on_load() {
			self::_initialize_valid_types();
		}
		/*
		 * Called at the beginning of a storage
		 * Not needed for storage type 'meta' but is needed for storage type 'table'
		 */
		function begin_storage() {
		}
		/*
		 * Called after all storages are completed and you want to commit them.
		 * Not needed for storage type 'meta' but is needed for storage type 'table'
 		 */
		function commit_storage() {  // Called after all storages are completed and you want to commit them.
		}
		/*
		 * Called after all storages are completed and you want to roll them back.
		 * NOT CURRENTLY IMPLEMENTED for 'meta'
 		 */
		function rollback_storage() {
		}
		static function get_storage_key( $field, $args = array() ) {
			return $field->real_name;
		}
		static function get_options() {
			return Sunrise::apply_class_filters( __CLASS__, 'get_options', get_option('sunrise-storages-options') );
		}
		/**
		 * @static
		 * @param string $storage_type
		 * @param Sunrise_Field $field
		 * @param array $args
		 * @return mixed
		 */
		static function load_raw_value( $storage_type, $field, $args = array() ) {
			$value = Sunrise::call_instance_method( "fields/storage/{$storage_type}", 'load_raw_value', $field, $args );
			return $field->apply_filters( 'storage_load_raw_value', $value, $args );
		}
		/**
		 * @static
		 * @param string $storage_type
		 * @param Sunrise_Field $field
		 * @param mixed $value
		 * @param array $args
		 * @return mixed
		 */
		static function update_raw_value( $storage_type, $field, $value, $args = array() ) {
			$value = $field->apply_filters( 'storage_update_raw_value', $value, $args );
			Sunrise::call_instance_method( "fields/storage/{$storage_type}", 'update_raw_value', $field, $value, $args );
		}
		/**
		 * @static
		 * @param string $storage_type
		 * @param Sunrise_Field $field
		 * @param array $args
		 * @return mixed
		 */
		static function update_sub_raw_values( $storage_type, $field, $args = array() ) {
			$field->apply_filters( 'storage_update_sub_raw_values',$args );
			return Sunrise::call_instance_method( "fields/storage/{$storage_type}", 'update_sub_raw_values', $field, $args );
		}
		/**
		 * @static
		 * @param string $storage_type
		 * @param Sunrise_Field $field
		 * @param array $args
		 * @return mixed
		 */
		static function delete_raw_value( $storage_type, $field, $args = array() ) {
			if ( $field->apply_filters( 'storage_delete_raw_value', true, $args ) )
				return Sunrise::call_instance_method( "fields/storage/{$storage_type}", 'delete_raw_value', $field, $args );
		}
		static function get_default_type() {
			$options = Sunrise_Fields::get_options(); // TODO: Verify that we want 'meta' to be default storage type
			$storage_type = isset( $options['default_storage_type'] ) ? $options['default_storage_type'] : 'meta';
			return Sunrise::apply_class_filters( __CLASS__, 'get_default_type', $storage_type );
		}
		/**
		 * @static
		 * @param string $storage_type
		 * @return string
		 */
		static function get_class_for( $storage_type ) {
			return Sunrise::get_class_for( 'fields/storage', $storage_type );
		}
		/**
		 * @static
		 * @param string $storage_type
		 * @param array $args
		 * @return Sunrise_Storage
		 */
		static function get_instance_for( $storage_type, $args = array() ) {
			return Sunrise::get_instance_for( 'fields/storage', $storage_type, $args );
		}
		/**
		 * @static
		 * @return array
		 */
		static function get_valid_types() {
			return Sunrise::apply_class_filters( __CLASS__, 'valid_types', self::$_valid_types );
		}
		/**
		 * @static
		 * @param string $storage_type
		 * @return boolean
		 */
		static function is_valid_type( $storage_type ) {
			return Sunrise::apply_class_filters( __CLASS__, 'is_valid_type', isset( self::$_valid_types[$storage_type] ), array( 'storage_type' => $storage_type ) );
		}
		static function _initialize_valid_types() {
			self::$_valid_types = array();
			// Get parent path of this file + '/types'
			$dir = dirname( dirname( __FILE__ ) ) . '/storage-types';
			if ( is_dir( $dir ) )
				if ( $handle = opendir( $dir ) ) {
					while ( ( $file = readdir( $handle ) ) !== false )
						if ( preg_match( "#^(.+)-storage.php#", $file, $types ) ) {
							self::$_valid_types[$types[1]] = $types[1];
						}
					closedir( $handle );
				}
			return;
		}
	}
	Sunrise_Storage::on_load();
}

