<?php

if ( ! class_exists( 'Sunrise_Post_Types' ) ) {
	class Sunrise_Post_Types extends Sunrise_Static_Base {
		static function on_load() {
			add_action( 'posts_orderby', array( __CLASS__, 'posts_orderby' ), 9, 2 ); // 9 = Called just before priority 10
			add_action( 'posts_join', array( __CLASS__, 'posts_join' ), 9, 2 ); // 9 = Called just before priority 10
		}
		/*
		 * Sets orderby if post type object has a value for "sunrise_args['orderby']" defined.
		 * Also sets order if the post type object has both an "sunrise_args['orderby']" and "sunrise_args['order']."
		 * The "orderby" value can only be a simple field in which case it will be prefixed correct with wp_posts, or
		 * if it contains a period ('.') it can be anything.
		 *
		 * If "sunrise_args['admin_list']" defined then any values for "orderby" or "order" will override those in "sunrise_args".
		 *
		 */
		static function _get_sunrise_args( $query ) {
			$sunrise_args = false;
			if ( isset( $query->query['post_type']) && is_string($query->query['post_type']) ) { // TODO sometimes this gets passed array of post type and fails, how to handle? R.
				$post_type_object = get_post_type_object( $query->query['post_type'] );
				if ( isset( $post_type_object->sunrise_args ) ) {
					$sunrise_args = $post_type_object->sunrise_args;
					if ( isset( $sunrise_args['contexts'] ) && isset( $query->sunrise_args['context'] ) ) {
						$sunrise_args = array_merge( $sunrise_args, $sunrise_args['contexts'][$query->sunrise_args['context']] );
					}
				}
			}
			return $sunrise_args;
		}
		static function posts_orderby( $orderby, $query ) {
			$sunrise_args = self::_get_sunrise_args( $query );
			if ( isset( $sunrise_args['orderby'] ) ) {
				$orderby = $sunrise_args['orderby'];
				if ( strpos( $orderby, '.' ) === false ) {
					global $wpdb;
					$order = isset( $sunrise_args['order'] ) ? $sunrise_args['order'] : 'DESC';
					$orderby = "{$wpdb->posts}.{$orderby} {$order}";
				}
			}
			return $orderby;
		}
		static function posts_join( $join, $query ) {
			$sunrise_args = self::_get_sunrise_args( $query );
			if ( isset( $sunrise_args['join'] ) ) {
				$join .= $sunrise_args['join'];
			}
			return $join;
		}
		static function is_admin_list( $query ) {
			global $pagenow;
			$is_admin_list = is_admin() &&
			  $pagenow == 'edit.php' &&
				isset( $_GET['post_type'] ) &&
				isset( $query->query['post_type'] ) &&
				$_GET['post_type'] == $query->query['post_type'];
			return apply_filters( 'sr_is_admin_list', $is_admin_list, $query );
		}
	}
	Sunrise_Post_Types::on_load();
}

