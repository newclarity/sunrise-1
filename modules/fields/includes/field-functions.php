<?php
/*
 * Registers a list of fields
 *
 * TODO: Need to properly document this
 */

function sr_register_fields( $object_sub_type, $args ) {
	$object_type = $args['object_type'];
	foreach ( $args['fields'] as $field_name => $field ) {
		$field['object_type'] = $object_type;
		$field['object_sub_type'] = $object_sub_type;
		if ( isset($args['container']) ) {
			$field['container'] = $args['container'];
		}
		if ( $field = sr_register_field( $field_name, $field ) ) {
			$args['fields'][$field_name] = $field;
		}
	}
	do_action( 'sr_register_fields', $args );
	return $args['fields'];
}

/*
 * Registers a list of field prototype
 *
 * TODO: Need to properly document this
 */
function sr_register_field_prototype( $field_name, $args ) {
	global $sr_field_prototypes;
	$args['field_name'] = $field_name;
	$args = sr_normalize_field_args( $args );
	$sr_field_prototypes[$field_name] = $args;
}

/*
 * Returns true if a field is a prototype
 *
 * TODO: Need to properly document this
 */
function sr_is_field_prototype( $field_name ) {
	global $sr_field_prototypes;
	return isset($sr_field_prototypes[$field_name]);
}

/*
 * ???
 *
 * TODO: Need to properly document this
 */
function sr_get_field_prototype_args( $field_name ) {
	global $sr_field_prototypes;
	return sr_is_field_prototype( $field_name ) ? $sr_field_prototypes[$field_name] : array();
}

/*
 * Registers a Field
 *
 * TODO: Need to properly document this
 */
function sr_register_field( $field_name, $args ) {
	global $sr_fields;
	global $sr_field_types;

	$args = apply_filters( 'sr_register_field_args', $args, $field_name );
	if ( ! is_array( $args ) ) {
		$field = false;
	} else {
		$args['field_name'] = $field_name;
		$args = Sunrise_Fields::transform_shorthands( $args );
		//	if ( isset( $args['is_prototype'] ) && $args['is_prototype'] ) {
		//		sr_register_field_prototype( $field_name, $args );
		//	} else {

		//	if ( sr_is_field_prototype( $field_name ) ) {
		//		$field_type = $args['field_type'];
		//		$args = wp_parse_args( $args, sr_get_field_prototype_args( $field_name ) );
		//		// TODO: Do we need to set $args['field_type']?
		//	}
		/**
		 * @var Sunrise_Field $field
		 */
		$field = Sunrise_Fields::get_instance_for( $args['field_type'], $args );
		if ( ! $field ) {
			$message = __(
				"The field type [%s] is not valid for the field named [%s] of the object_type/sub_type [%s'/'%s]'."
			);
			sr_trigger_error(
				$message, $args['field_type'], $args['field_name'], $args['object_type'], $args['object_sub_type']
			);
		}
		foreach ( Sunrise_Field_Feature::get_valid_types() as $feature_type ) {
			$feature_property_name = "{$feature_type}_feature";
			$field->{$feature_property_name}->parent = $field;
		}
		$sr_fields[$field->object_type][$field->object_sub_type][$field_name] = $field;
		$sr_field_types[$field->object_type][$field->object_sub_type][$field->field_type][] = $field;
		// }
	}
	/**
	 * TODO: (Micah 2012-06-07) Discuss this filter with Mike
	 * I believe that the below 'sr_register_field' filter is useless.  The fields are initialized
	 * before this point, so any changes to the field are of no effect. I added the 'sr_register_field_args'
	 * filter at the top of this function to be sure args can be effectively changed.
	 */
	/**
	 * TODO (mikes 2012--06-22): I commented out apply_filters(), but leaving here to
	 * TODO                    : remind us in case we do in fact need it.
	 */
	//return apply_filters( 'sr_register_field', $field, $field_name, $args );
	return $field;
}

/*
 * Returns true if a post type form has a list of fields
 *
 * TODO: Need to properly document this
 */
function sr_has_post_fields( $post_type, $form_name ) {
	global $sr_forms;
	return (isset($sr_forms['post'][$post_type][$form_name]->fields));
}

/*
 * Get field attribute html
 *
 * TODO: Need to properly document this
 */
function sr_get_post_fields( $post_type, $form_name = false ) {
	return sr_get_form_fields(
		$form_name, array(
			'object_type' => 'post',
			'object_sub_type' => $post_type,
		)
	);
}

/*
 * Takes a list of fields (names or objects) and returns a list of field objects
 *
 * TODO: Need to properly document this
 */
function __sr_fields_to_objects( $form_fields, $args = array() ) {
	$field_objects = array();
	if ( count( $form_fields ) ) {
		foreach ( $form_fields as $field ) {
			if ( is_string( $field ) ) {
				$field_name = $field;
				$field = sr_get_field_object( $field_name, $args['object_type'], $args['object_sub_type'], $args );
			} else {
				$field_name = $field->field_name;
			}

			if ( ! is_a( $field, 'Sunrise_Field' ) ) {
				sr_die( 'Field [%s] has not been properly defined.', $field_name );
			}

			$field_objects[$field->field_name] = $field;
		}
	}
	return $field_objects;
}

/*
 * Returns a specific post field
 *
 * TODO: Need to properly document this
 */
function sr_get_post_field( $post_type, $field_name, $args = array() ) {
	return sr_get_field_object( $field_name, 'post', $post_type, $args );
}

/*
 * Returns the value of a specific field for a post
 *
 * TODO: Need to properly document this
 */
function sr_get_post_field_value( $field_name, $post, $args = array() ) {
	$args = wp_parse_args(
		$args, array(
			'object_type' => 'post',
			'object_sub_type' => $post->post_type,
		)
	);
	return Sunrise_Fields::get_field( $field_name, $args );
}

/*
 * Returns a specific object field
 *
 * TODO: RENAME THIS so we can reuse the name in place of sr_get_the_field()
 */
function sr_get_field_object( $field_name, $object_type, $object_sub_type = false, $args = array() ) {
	$args = wp_parse_args( $args, compact( 'object_type', 'object_sub_type' ) );
	return Sunrise_Fields::get_field( $field_name, $args );
}

function sr_is_valid_field( $field_name, $object_type, $object_sub_type = false ) {
	$args = wp_parse_args( compact( 'object_type', 'object_sub_type' ) );
	return Sunrise_Fields::is_valid_field( $field_name, $args );
}

/*
 * Returns the value of a specific field for an object
 *
 * TODO: Need to properly document this
 */
function sr_get_field_value( $field_name, $object_type, $object_sub_type = false, $args = array() ) {
	$args = wp_parse_args( $args, compact( 'object_type', 'object_sub_type' ) );
	return Sunrise_Fields::get_field_value( $field_name, $args );
}

/*
 * Returns true if an object_type & sub_object_type has a list of fields based on an $args query
 *
 * See sr_get_form_fields() for more details.
 *
 * TODO: Need to properly document this
 */
function sr_has_form_fields( $form_name, $object_type, $object_sub_type, $args = array() ) {
	return sr_get_form_fields( $form_name, $object_type, $object_sub_type, $args ) !== false;
}

/*
 * Returns a list of fields for a post form
 *
 * TODO: Need to properly document this
 */
function sr_get_form_fields( $form_name, $object_type, $object_sub_type = false, $args = array() ) {
	$args = wp_parse_args( $args, compact( 'form_name', 'object_type', 'object_sub_type' ) );
	return Sunrise_Forms::get_fields( $args );
}

/*
 * Returns a list of fields for a post form
 *
 * TODO: Need to properly document this
 */
function sr_get_field_objects( $object_type, $object_sub_type = false, $args = array() ) {
	$args = wp_parse_args( $args, compact( 'object_type', 'object_sub_type' ) );
	//TODO: Doesn't this need a form value too?
	return Sunrise_Forms::get_fields( $args );
}

/**
 * @param array $args
 *
 * @return array
 */
function sr_normalize_field_args( $args ) {
	return Sunrise_Fields::normalize_args( $args );
}

function sr_the_field( $field_name, $args = array() ) {
	$value = sr_get_the_field( $field_name, $args );
	if ( is_string( $value ) ) {
		echo $value;
	} else {
		if ( WP_DEBUG ) {
			//echo 'The field "'.$field_name.'" cannot be displayed because the value is not a string.';
		}
	}
}

function sr_get_the_field( $field_name, $args = array() ) {
	/**
	 * @var SR_DOCS_Post $post
	 */
	global $post;
	$save_post = $post;

	/**
	 * Allows $args to be a string like 'post_id=123' and still have the code on the next 4-6 lines work.
	 * TODO (mikes 2011-12-12): Refactoring should simplify this function.
	 */
	$args = sr_parse_args( $args );

	if ( isset($args['post_id']) && is_numeric( $args['post_id'] ) && 0 < ($post_id = intval( $args['post_id'] )) )
		$post = get_post( $post_id );

	else if ( isset($args['post']->ID) )
		$post = $args['post'];

	if ( ! isset($args['post_id']) )
		$args['post_id'] = $post->ID;

	$args = wp_parse_args(
		$args, array(
			/** Required args */
			'object_type' => 'post',
			'object_sub_type' => $post->post_type,
			/** Optional args */
			'before' => false, // If returned value is a string, this prepends to value
			'after' => false, // If returned value is a string, this appends to end of value
			// TODO (mikes 2011-12-12): Why not have output=>'callback'?
			'callback' => false, // Callback to be executed to manage output when output is set to 'custom'
			'object_id' => $post->ID, // NOTE: This only affects related posts fields
			'output' => false, // Return desired output type
			'date_format' => 'F j, Y', // Used with 'date' output to determine format
			'image_size' => 'default', // Used with 'html' output to determine image sizes for image fields
			'anchor_text' => false, // Set or override anchor text in text links
			'class' => array(), // Set html classes for returned elements
			'hyperlinked' => false, // Wraps returned value in a link to post
			'self_linked' => false, // When true then it's basically "hyperlinked=true&anchor_text={$value}" ($value is the permalink)
			'target_url' => false, // Used when 'hyperlinked' is set to true, this allows you to set a custom url for links
			'target' => '_self', // HTML target attribute used when 'hyperlinked' is set to true
			'link_type' => false, // Used with 'hyperlinked'.  Set to 'microsite' to get a microsite link.
			'link_title' => false, // The title use for a hyperlink, if one is provided.
			'list_args' => array(), // Used with 'list' output to pass specific arguments to sr_generate_html_list()
			'excerpt_offset' => 0, // Used with 'excerpt' if an offset is desired (rare)
			'excerpt_length' => 40, // Used with 'excerpt' output to determine number of words or characters in excerpt
			'excerpt_type' => 'words', // Used with 'excerpt' output to determine mode: words or characters
		)
	);

	$value = apply_filters( 'sr_pre_get_the_field', null, $field_name, $args );
	if ( is_null( $value ) ) {
		/**
		 * Get an instance of Sunrise_Fields based on it's field name and defined a 'post' value.
		 */
		$field = sr_get_field_object( $field_name, $args['object_type'], $args['object_sub_type'] );

		if ( $field ) {
			$field->object_id = $post->ID;
			$field->_value = false; // TODO (andrey): this is bandaid fix for old value being incorrectly reused on repeated calls
			$field->load_value();
			/**
			 * TODO (mikes 2011-11-06): This needs to be moved down into load_value() or below
			 * TODO (mikes 2011-12-12): This need so be handled in Relationships module with a hook (maybe?)
			 */
			if ( 'related_posts' == $field->field_type ) {
				$value = sr_get_related_objects( $field->relationship_name, $args['object_id'] );
			} else {
				$value = $field->value;
			}
		} else {
			$value = false;
		}

		/**
		 * Custom handling for fieldsets temporarily setup as repeating fields
		 */
		if ( $field && 'repeating' == $field->field_type ) {
			$subkey = preg_match( "#^([^\[]+)\[([^\]]+)\]$#", $field_name, $matches ) ? true : false;
			$subkey = isset($matches[2]) ? $matches[2] : false;
			if ( $subkey ) {
				if ( is_array( $value ) && count( $value ) == 1 ) {
					$value = isset( $value[0][$subkey] ) ? $value[0][$subkey] : false;
				}
			}
		}
	}
	/**
	 * Check to see if apparently empty.
	 */
	if ( empty($value) ) {
		$post = $save_post;
		$value = false;
	} else if ( isset($field->field_type) && 'file' == $field->field_type /* || 'image' == $field->field_type*/ ) {
		if ( is_object( $value ) && ! $value->attachment_id ) {
			$value = false;
		}
	}
	/**
	 * Return early if we still don't have a value (no need for further processing)
	 */
	if ( $value ) {
		/**
		 * Return value in desired output
		 *
		 * TODO: Consider using a filter to perform output conversions?
		 * TODO (mikes 2012-01-05): Consider decoupling $arg['output'] from sr_get_the_field(); move to a 3rd function?
		 */
		if ( isset($field) && isset($args['output']) ) {
			switch ( strtolower( $args['output'] ) ) {
				case 'list':
					switch ( $field->field_type ) {
						case 'repeating':
                        case 'indentable_repeating':
							if ( true == $field->indentable && is_array( $value ) ) {
								$value = sr_transform_list_array( $value, $args['list_args'] );
							}
							break;
					}
					break;
				case 'custom':
					if ( $args['callback'] ) {
						$value = call_user_func( $args['callback'], $field, $value, $args );
					} else {
						// TODO: Provide error when valid callback is not provided.
					}
					break;
				case 'text':
					switch ( $field->field_type ) {
						case 'taxonomy_term':
							$value = $value->name;
							break;
					}
					break;
				case 'commas':
					switch ( $field->field_type ) {
						case 'related_posts':
							$related_posts = array();
							foreach ( $value as $k => $v ) {
								$related_posts[] = $v->post_title;
							}
							$value = $related_posts;
							break;
					}
					if ( is_array( $value ) ) {
						$value = join( ', ', $value );
					}
					break;
				case 'timestamp':
					$value = strtotime( $value );
					break;
				case 'date':
					/**
					 * TODO: (mikes 2012-01-05): This should probably be the default for these data types so 'output=date' not be required.
					 * TODO: The field's definition should be enough to know that it is a date and those default output = date.
					 * TODO: We can also conside if we default $args['date_format'] to false we can use $args['date_format']!==false to indicate 'output=date'.
					 */
					$value = date( $args['date_format'], strtotime( $value ) );
					break;
				case 'permalink':
					switch ( $field->field_type ) {
						case 'file':
							$file = $value;
							$value = is_object( $file ) ? @$file->url : '#';
							break;
						case 'image':
							if ( is_object( $value ) ) {
								$image = $value->as_sized( $args['image_size'] );
								$value = $image->url;
							}
							break;
					}
					break;
				case 'hyperlinked':
					switch ( $field->field_type ) {
						case 'file':
							$file = $value;
							if ( $file ) {
								$args['anchor_text'] = isset($args['anchor_text']) ? $args['anchor_text'] : $file->filename;
								$class = isset($args['class']) ? (array) $args['class'] : array();
								$value = '<a href="' . $file->url . '" class="' . join(
									' ', $class
								) . '" target="' . $args['target'] . '">' . "{$args['anchor_text']}</a>";
							}
							break;
						case 'image':
							/**
							 * @var Sunrise_Image_File $image_file
							 */
							if ( is_object( $value ) ) {
								$image_file = $value->as_sized( $args['image_size'] );
								/**
								 * TODO (micah): Add support for alt attribute
								 */
								$value = '<a href="' . $image_file->url . '"><img src="' . $image_file->url . '" alt="" width="' . $image_file->width . '" height="' . $image_file->height . '" /></a>';
							}
							break;
						default:
							if ( ! isset($args['anchor_text']) ) {
								$args['anchor_text'] = $value;
							} else if ( false !== strpos( $args['anchor_text'], '%value%' ) ) {
								$args['anchor_text'] = str_replace( '%value%', $value, $args['anchor_text'] );
							}
							$value = '<a href="' . $value . '">' . "{$args['anchor_text']}</a>";
					}
					break;
				case 'html':
					/**
					 * TODO (mikes 2012-01-05): We should make 'html' the default output so it need not be specified.
					 * TODO: Actually 'output=html' could be default when using sr_the_field() and 'output=raw' could be default for sr_get_the_field()
					 */
					switch ( $field->field_type ) {
						case 'text':
							if ( ! isset($args['anchor_text']) ) {
								$args['anchor_text'] = $value;
							} else if ( false !== strpos( $args['anchor_text'], '%value%' ) ) {
								$args['anchor_text'] = str_replace( '%value%', $value, $args['anchor_text'] );
							}
							break;
						/**
						 * This 'image' case should be done in Sunrise_Images
						 */
						case 'image':
							/**
							 * @var Sunrise_Image_File $image_file
							 */
							if ( is_object( $value ) ) {
								$image_file = $value->as_sized( $args['image_size'] );
								$class = isset( $args['class'] ) ? (array) $args['class'] : array();
								/**
								 * TODO (micah): Add support for alt attribute
								 */
								$value = '<img src="' . $image_file->url . '" alt="" width="' . $image_file->width . '" height="' . $image_file->height . '" class="' . join(' ', $class) . '" />';
							}
							break;
					}
					break;
				case 'mailto':
					$email = antispambot( sanitize_email( $value ) );
					$value = '<a href="mailto:' . $email . '">' . $email . '</a>';
					break;
				case 'excerpt':
					$words = ('words' == $args['excerpt_type']) ? true : false;
					$text = strip_tags( $value );
					if ( $words ) {
						$text_array = explode( ' ', $text );
					} else {
						$text_array = str_split( $text );
					}
					$slice = array_slice( $text_array, $args['excerpt_offset'], $args['excerpt_length'] );
					if ( $text == implode( '', $slice ) ) {
						$value = $text;
						break;
					}
					if ( $words ) {
						$value = trim( join( ' ', $slice ) ) . '... ';
					} else {
						$word_array = explode( ' ', implode( '', $slice ) );
						array_pop( $word_array );
						// TODO: Remove any trailing punctuation
						//$word_array = preg_replace( "/[,]$/", '', $word_array );
						$value = trim( implode( ' ', $word_array ) ) . '... ';
					}
					break;
				default:
			}
		}

		// TODO: Require HTML output?  Check for link_type = microsite
		if ( $args['hyperlinked'] || $args['self_linked'] ) {
			if ( $args['self_linked'] ) {
				$permalink = $value;
			} else if ( $args['target_url'] ) {
				$permalink = $args['target_url'];
			}
			/**
			 * TODO (mikes): Need a hook here so Microsites can do this for themselves.	If make there is no need for a hook?	Maybe get_permalink() can do this now?
			 */
			else if ( 'microsite' == $args['link_type'] ) {
				// TODO (mikes): Create sr_microsite_permalink( $post->ID, $args ) function in microsites

				// Default is permalink unless we have a valid microsite link
				$permalink = get_permalink( $post->ID );

				$microsite = sr_get_current_microsite();
				if ( $microsite ) {
					if ( isset($args['microsite_page_post_id']) && $args['microsite_page_post_id'] ) {
						$microsite_page = $microsite->get_page_by( 'post_id', $args['microsite_page_post_id'] );
					} else {
						/**
						 * This is for post_types that are part of a microsite but for which
						 * we don't have an appropriate microsite_page_post_id', such as in a
						 * sidebar.
						 */
						$microsite_page = $microsite->get_page_by( 'child_type', $post->post_type );
					}
					if ( $microsite_page ) {
						$permalink = "{$microsite_page->permalink}{$post->post_name}/";
					}
				}
			} else if ( ! $args['self_linked'] ) {
				$permalink = get_permalink( $post->ID );
			}

			// TODO: Don't allow overriding of images, only text
			// TODO (mikes 2012-01-05): Self-linked might become default for 'hyperlinked=true' URL?

			if ( $args['self_linked'] || ! $args['anchor_text'] ) {
				$args['anchor_text'] = $value;
			} else if ( false !== strpos( $args['anchor_text'], '%value%' ) ) {
				$args['anchor_text'] = str_replace( '%value%', $value, $args['anchor_text'] );
			}
			/**
			 * TODO (mikes 2012-01-13): Should rename $args['class'] to $args['link_class']
			 */
			$class = isset($args['class']) ? ' class="' . join( ' ', (array) $args['class'] ) . '"' : '';
			$title_html = $args['link_title'] ? ' title="' . $args['link_title'] . '"' : '';

			$value = "<a href=\"{$permalink}\"{$class} target=\"{$args['target']}\"{$title_html}>{$args['anchor_text']}</a>";
		}

		// Add extra content / html before the value
		if ( isset($args['before']) && is_string( $args['before'] ) ) {
			$value = $args['before'] . $value;
		}
		// Add extra content / html after the value
		if ( isset($args['after']) && is_string( $args['after'] ) ) {
			$value = $value . $args['after'];
		}
		$value = apply_filters( 'sr_get_the_field', $value, $field_name, $args );
	} else {
		$value = apply_filters( 'sr_empty_field', $value, $field_name, $args );
	}
	$post = $save_post;
	return $value;
}

/**
 * A temporary wrapper function for taking indented fields and outputting them in the correct array format
 *
 * NOTE: This function is only intended to handle two levels deep.
 *
 * @param			 $data
 * @param array $args
 *
 * @return string
 */
function sr_transform_list_array( $data, $args = array() ) {

	if ( ! is_array( $data ) ) {
		return false;
	}

	$args['depth'] = (isset($args['depth']) && $args['depth'] < 3) ? $args['depth'] : 2;

	$transformed = array();

	foreach ( $data as $value ) {
		if ( is_array( $value ) ) {
			foreach ( $value as $k => $v ) {
				if ( 0 == $k ) {
					$transformed[] = $v;
				} else {
					unset($value[0]);
					$transformed[] = $value;
					break;
				}
			}
		} else {
			$transformed[] = $value;
		}
	}

	return sr_generate_html_list( $transformed, $args );
}

/**
 * A recursive function that takes a multi-dimensional array in $_POST format and converts it into an HTML list.
 *
 * @param			 $data
 * @param array $args
 *
 * @return string
 */
function sr_generate_html_list( $data, $args = array() ) {
	// Set internal loop counter
	$loop = (isset($args['loop']) && is_int( $args['loop'] )) ? (int) $args['loop'] : 0;
	// Make recursive only to the desired depth
	$depth = (isset($args['depth']) && is_int( $args['depth'] )) ? (int) $args['depth'] : 0;
	// Set current depth
	$loop ++;

	// Determine list type
	$list = (isset($args['list']) && $args['list'] == 'ol') ? 'ol' : 'ul';

	// Get and set classes
	$parent_classes = ! empty($args['parent_class']) ? (array) $args['parent_class'] : array();
	$child_classes = ! empty($args['child_class']) ? (array) $args['child_class'] : array();
	$classes = ($loop == 1) ? $parent_classes : array_merge( $parent_classes, $child_classes );
	$classes = empty($classes) ? '' : ' classes="' . str_replace( '#', $loop, join( ' ', $classes ) ) . '"';

	// Unset parent class
	unset($args['parent_class']);

	// HTML list generation
	$html = array();
	$next = false;

	// Limit recursion to the desired depth
	if ( $depth == 0 || $depth >= $loop ) {
		$html[] = '<' . $list . $classes . '>';
		foreach ( $data as $k => $v ) {
			$next = next( $data );
			if ( is_array( $v ) ) {
				$args['loop'] = $loop;
				$args['depth'] = $depth;
				$html[] = call_user_func( __FUNCTION__, $v, $args );
			} else {
				if ( $next && is_array( $next ) ) {
					$html[] = '<li>' . $v;
				} else {
					$html[] = '<li>' . $v . '</li>';
				}
			}
		}
		$html[] = '</' . $list . '>';
	}

	if ( ! $next && $loop > 1 ) {
		$html[] = '</li>';
	}
	return implode( '', $html );
}
