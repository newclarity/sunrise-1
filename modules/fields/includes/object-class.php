<?php

// TODO: Need to make this an Object Adapter vs. just an Object

if ( ! class_exists( 'Sunrise_Object' ) ) {
	/**
	 *  @property bool|object $object
	 *  @property bool|int $object_id
	 *  @property bool|string $object_type
	 *  @property bool|string $object_sub_type
	 *  @property bool|Sunrise_Base $parent
	 *  @property bool|array $values
	 */
	class Sunrise_Object extends Sunrise_Base {
		protected $_object = false;
		protected $_object_id = false;
		protected $_object_type = false;
		protected $_object_sub_type = false;
		/**
		 * @var bool|Sunrise_Object // TODO (mikes): Verify this doc is correct
		 */
		protected $_values = false;
		function __construct( $args = array() ) {

			if ( WP_DEBUG && ! isset( $args['object_type'] ) )
				sr_die( 'Must set the $object_type property when creating an instance of the ' . __CLASS__ . ' class.' );
			$this->_object_type = $args['object_type'];

			// If we have the object_id
			if ( isset( $args['object_id'] ) ) {
				$this->object_id = $args['object_id'];
			}

			if ( isset( $args['object_sub_type'] ) ) {
				// Set the object sub type
				// Look for an explicitly declared subtype. If found, use it.
				$this->_object_sub_type = $args['object_sub_type'];
			} else if ( isset( $args["{$this->_object_type}_type"] ) ) {
				// If not found look for an imlicitly declared subtype, i.e. 'post_type'. If found, use it.
				$this->_object_sub_type = $args["{$this->_object_type}_type"];
			} else {
				sr_die( 'Must set the $object_sub_type property when creating an instance of the ' . __CLASS__ . ' class.' );
			}

			if ( isset( $args['parent'] ) )
				$this->_parent = $args['parent'];

			$this->do_action( 'initialize', $args );

		}
		/*
		 * Returns the properties of the object adapter as an array
		 */
		function as_args() {
			$args = array(
				'object_type'     => $this->get_object_type(),
				'object_sub_type' => $this->get_object_sub_type(),
				'object_id'       => $this->get_object_id(),
				'parent'          => $this->get_parent(),
				'object_adapter'  => $this,
			);
			$args['object'] = $this->get_object();
			return $args;
		}
		function get_parent() {
			return $this->_parent;
		}
		function load_object() {
			if ( $this->_object_id && method_exists( $this->_parent, 'load_object' ) ) {
				$this->object = $this->_parent->load_object( $this->_object_id );
			}
		}
		function get_object() {
			if ( ! $this->_object ) {
				if ( $this->_parent && ! empty( $this->_parent->object ) && ( $object = $this->_parent->object ) ) {
					$this->object = $object;
				} else if ( $this->_object_id ) {
					$this->object = $this->load_object();
				}
			}
			return $this->_object;
		}
		function get_object_type() {
			if ( ! $this->_object_type ) {
				// Extract the object_type from the name of the class, i.e. Sunrise_Post_Object
				$object_type = strtolower( preg_replace('#^Sunrise_(.+)_Object$#', '$1', get_class( $this ) ) );
				if ( !empty( $object_type ) && 'sunrise_object' != $object_type )
					$this->_object_type = $object_type;
			}
			return $this->_object_type;
		}
		function get_object_id() {
			sr_die( 'Sunrise_Object is an abstract class; you should be using a child class that implements its own get_object_id() method.' );
		}
		function get_object_sub_type() {
			sr_die( 'Sunrise_Object is an abstract class; you should be using a child class that implements its own get_object_sub_type() method.' );
		}
		function __set( $property_name, $value ) {
			switch ( $property_name ) {
				case 'object':
					$this->_object = $value;
					$this->_object_id = $this->get_object_id();
					$this->_object_sub_type = $this->get_object_sub_type();
					break;

				case 'object_id':
					$this->_object_id = $value;
					break;

				case 'object_type':
					if ( WP_DEBUG )
						sr_die( "Setting object_type is invalid; attempt to set to {$value}." );
					//$this->_object_type = $value;
					break;

				case 'object_sub_type':
					$this->_object_sub_type = $value;
					break;

				case 'parent':
					$this->_parent = $value;
					break;

				default:
					break;
			}
		}
		function __get( $property_name ) {
			$value = false;
			switch ( $property_name ) {
			case 'object':
				$value = $this->get_object();
				break;

			case 'object_id':
					$value = $this->get_object_id();
					break;

			case 'object_type':
					$value = $this->get_object_type();
				break;

			case 'object_sub_type':
				$value = $this->get_object_sub_type();
				break;

			case 'parent':
				$value = $this->get_parent();
				break;

			default:
				break;
		}
		return $value;
	}
	}
}

