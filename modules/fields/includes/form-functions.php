<?php
/*
 * Registers a Form for Fields
 *
 * TODO: Need to properly document this
 */
function sr_register_form( $form_name, $args ) {
	global $sr_forms;

	/**
	 * Optimization: Don't register the form if we can't use it for this page.
	 */
	if ( isset( $args['object_type'] ) && 'post' == $args['object_type'] && isset( $args['object_sub_type'] ) ) {
		$context = sr_get_context();
		if ( 'any' != $args['object_sub_type'] && $args['object_sub_type'] != $context->post_type )
			return apply_filters( 'sr_bypass_register_form', $form_name, $args );
	}

	// Make sure we have a form type. Default to do-nothing 'basic_form'
	if ( ! isset( $args['form_type'] ) )
		$args['form_type'] = 'basic_form';

	// If any field's value is false, remove it from the list
	if ( isset( $args['fields'] ) )
		foreach( $args['fields'] as $field_name => $value )
			if ( ! $value )
				unset( $args['fields'][$field_name] );

	$args['form_name'] = $form_name;

	// Derive the object_type from $args['form_type'] when
	// $args['form_type'] => 'whatever_form' then $object_type => 'whatever'
	$object_type = preg_replace( '#^(.+?)_form$#', '$1', $args['form_type'] );

	// Create a new instance of this form based on knowing
	// that class names are in the format 'Sunrise_Whatever_Form'
	$form_class = Sunrise::get_class_for( 'fields/form', $object_type );
	$form = new $form_class( $args );

	// Add it to our global list of forms for later access
	$sr_forms[$form->object_type][$form->object_sub_type][$form_name] = &$form;

	// Let somebody else modify what we just did
 	return apply_filters( 'sr_register_form', $form );
}

/**
 * Adds a named field to a named form.
 *
 * This function is designed to be called in the sr_loaded() hook.
 *
 * @param string $form_name
 * @param string $field_name - NOT a field object. Field must be defined via sr_register_field().
 * @param array $args
 *
 */
function sr_add_form_field( $form_name, $field_name, $args = array() ) {
	global $sr_forms;
	$args = sr_parse_args( $args, array(
		'object_type' => 'post',
		'object_sub_type' => false,
		'after' => false,		// Name of field to add $field_name after
		'before' => false,	// Name of field to add $field_name before (invalid if 'after' also defined)
		'where' => false,		// Position where to add: 'start' (also 'front', 'beginning'), 'end', 'before' or 'after'
		'relative_to' => false	// Name of field to insert relative to. Used when $args['where'] is either 'before' or 'after'
	));
	if ( ! $args['object_sub_type'] ) {
		$error_msg = "The 'object_sub_type' value not specified in the '\$args' parameter when attempting to add field [%s] to form [%s].";
		sr_die( $error_msg, $field_name, $form_name );
	}
	if ( $args['where'] ) {
		if ( ! $args['relative_to'] ) {
			$error_msg = "The 'relative_to' value not specified in the '\$args' parameter when attempting to add field [%s] to form [%s] where 'where' is [%s].";
			sr_die( $error_msg, $field_name, $form_name, $args['where'] );
		}
	} else if ( $args['after'] ) {
		$args['relative_to'] = $args['after'];
		$args['where'] = 'after';
	} else if ( $args['before'] ) {
		$args['relative_to'] = $args['before'];
		$args['where'] = 'before';
	} else {
		$args['where'] = 'end';
	}
	$fields = $sr_forms[$args['object_type']][$args['object_sub_type']][$form_name]->fields;
	switch ( $args['where'] ) {
		case 'end':
			$fields[] = $field_name;
			break;
		case 'start':
		case 'front':
		case 'beginning':
			array_unshift( $fields, $field_name );
			break;
		default:
			sr_array_insert_after( $fields, $field_name, $args['relative_to'], $args['where'] );
			break;
	}
	$sr_forms[$args['object_type']][$args['object_sub_type']][$form_name]->fields = $fields;
}

/**
 * Removes a named field from a named form.
 *
 * This function is designed to be called in the sr_loaded() hook.
 *
 * @param string $form_name
 * @param string $field_name
 * @param array  $args
 */
function sr_remove_form_field( $form_name, $field_name, $args = array() ) {
	global $sr_forms;
	$args = sr_parse_args( $args, array(
		'object_type' => 'post',
		'object_sub_type' => false,
	));
	if ( ! $args['object_sub_type'] ) {
		$error_msg = "The 'object_sub_type' value not specified in the '\$args' parameter when attempting to remove field [%s] from form [%s].";
		sr_die( $error_msg, $field_name, $form_name );
	}
	if( isset( $sr_forms[$args['object_type']][$args['object_sub_type']][$form_name]->fields ) ) {
		$key = array_search( $field_name, $sr_forms[$args['object_type']][$args['object_sub_type']][$form_name]->fields );
		if( false !== $key ) {
			unset( $sr_forms[$args['object_type']][$args['object_sub_type']][$form_name]->fields[$key] );
		}
	}
}

