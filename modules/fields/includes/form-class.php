<?php
if (!class_exists('Sunrise_Form')) {
	/**
	 * @property stdClass|object $object
	 * @property Sunrise_Object $object_adapter
	 */
	class Sunrise_Form extends Sunrise_Base {
		var $form_name    = false;
		var $form_type    = false;   // "{$object_type}_form", i.e. 'post_form'
		var $title        = false;
		var $storage_type = false;
		var $fields       = array();
		var $hidden       = false;
		var $wrapper_id   = false;
		var $parent       = false;
		var $order				= false;
		protected $_object_adapter = false;

		function load_object( $object_id ) {
			$value = false;
			if ( $this->_object_adapter )
				$value = $this->_object_adapter->load_object( $object_id );
			return $value;
		}

		function get_html( $args = array() ) {
			global $sr_forms;
			$dashed_form_name = sr_dashize( $this->form_name );
			if ( ! $this->wrapper_id ) {
				$this->wrapper_id = "{$dashed_form_name}-form";
			}
			$form_class = "{$dashed_form_name}-wrapper";
			$object = $this->object;

			if ( 0 == count( $args ) ) {
				$args = wp_parse_args( $args, $this->get_object_args() );
			}
			$args['form_name'] = $this->form_name;

			$has_required_fields = false;
			$html = array();
			$starting_html = "<div id=\"{$this->wrapper_id}\" class=\"";

			$html[] =<<<HTML
{$starting_html}{$args['object_type']}-{$args['object_sub_type']} {$form_class} fields-wrapper context-{$this->context} priority-{$this->priority}">
	<ul class="field-list">
HTML;
			if ( 0 == count( $available_fields = Sunrise_Forms::get_fields( $args ) ) )
				$available_fields = array();

			$content_field = false;
			/**
			 * @var string|Sunrise_Field $field
			 */
			foreach( $this->fields as $name => $field ) {
				if ( ! is_a( $field, 'Sunrise_Field' ) ) {
					if ( isset( $available_fields[$field] ) ) {
						$name = $field;
						$field = $available_fields[$field];
					} else {
						sr_die( 'Field [%s] has not been properly defined.', $field );
					}
				}
				$field->form = &$this;
				$field->object = $object;

				$html[] = $field->item_html;
				if ( ! $has_required_fields && $field->required )
					$has_required_fields = true;

				if ( 'post_content' == $field->real_name )
					$content_field = true;

			}
/**
 * The purpose of $content_field is to fix the Javacript error that it did not fix:
 *
 *    "TypeError: 'undefined' is not an object (evaluating 'tinyMCE.get("content").save')
 *
 * So I added a srf_demo_disable_autosave() in /demos/attorney-demo.php to fix short term.
 *
 * -Mike Schinkel (Oct 19 2011)
 *
 * TODO: Need to figure out this issue and fix.
 *
 */
 			$content_field = $content_field ? '' : "\n" . '<input type="hidden" id="content" name="content" value="" />';
			$html[] =<<<HTML
	</ul>{$content_field}
	<input type="hidden" id="sunrise-fields" name="sunrise_fields" value="1" />
	<input type="hidden" id="{$dashed_form_name}-object-forms" name="object_forms[]" value="{$this->form_name}" />
</div>
HTML;
			if ($has_required_fields) {
				$html[0] = str_replace( $starting_html, "{$starting_html}has-required ", $html[0] );
				array_splice($html,1,0,'<div class="help">Fields marked with <span class="required-callout">*</span> are required.</div>');
			}
			$html = implode("\n",$html);
			return $html;
		}

		/*
		 * Returns the properties of the object adapter as an array
		 */
		function get_object_args() {
			return $this->_object_adapter->as_args();
		}
		/**
		 * Output hidden fields for objects args.
		 * Do this only once, metaboxes should not have different values for these.
		 *
		 * @return bool|string
		 *
		 * TODO (mikes 2011-12-20): Consider moving these where they won't called but once.
		 */
		function get_object_args_html() {
			static $done_once = false;
			$html = false;
			if ( ! $done_once ) {
				$args = $this->get_object_args();
				$html =<<<HTML
<input type="hidden" id="object_id" name="object_id" value="{$args['object_id']}" />
<input type="hidden" id="object_type" name="object_type" value="{$args['object_type']}" />
<input type="hidden" id="object_sub_type" name="object_sub_type" value="{$args['object_sub_type']}" />
HTML;
				$done_once = true;
			}
			return $html;
		}


		function __construct( array $args = array() ) {
			if ( !isset( $args['form_name'] ) ) {
				sr_die( "You must specify a form_name when creating a Sunrise Fields Form" );
			}
			if ( !isset( $args['title'] ) )
				$args['title'] = __('Information');

			// If $args['form_type'] is set but not $args['object_type'], capture it's value
			if ( isset( $args['form_type'] ) && ! isset( $args['object_type'] ) )
				$args['object_type'] = preg_replace( '#^(.+?)_form$#', '$1', $args['form_type'] );

			// Derive fundamental $args values from specific $args values For example, for Posts this will recognize
			// $args['post_type'] and then set 'object_type' => 'post' and 'object_sub_type' => $args['post_type']
			$args = Sunrise_Objects::derive_args( $args );

			// If $args['form_type'] still not set, get it from the object_type
			if ( ! $args['form_type'] )
				$args['form_type'] = "{$this->object_type}_form";

			/*
			 * Grab all the $args that are properties or _properties and set them.
			 * Put everything into $args and everything else in $extra.
			 */
			$this->construct_from_args( $args );

			/*
			 * Get an instance of the specified object type
			 */
			$this->_object_adapter = Sunrise_Objects::get_instance_for( $args['object_type'], $args );

		}
		/**
		 * @param string $field_name
		 * @return array|Sunrise_Field
		 */
		function get_field( $field_name ) {
			global $sr_fields;
			$object_type = $this->object_type;
			$object_sub_type = $this->object_sub_type;
			$fields = Sunrise_Fields::get_preprocessed_fields( "{$object_type}/{$object_sub_type}" );
			if ( isset( $fields[$field_name] ) ) {
				$field = $fields[$field_name];
			} else {
				sr_die( 'Do we ever follow this logic path?' );
				global $sr_forms;
				if ( isset( $sr_forms[$object_type][$object_sub_type][$this->form_name]->fields[$field_name] ) ) {
					$field = $sr_forms[$object_type][$object_sub_type][$this->form_name]->fields[$field_name];
				} else {
					sr_trigger_error( __( 'No field [%s] defined for form [%s].', 'sunrise-fields' ), $field_name, $this->form_name );
				}
			}
			return $field;
		}
		function get_field_value( $field_name ) {
			$field = $this->get_field( $field_name );
			$field->form = &$this;                    // TODO: Figure out why this does not stay set from upstream set.
			$value = $field->value;
			return $value;
		}
		function get_object_property( $property_name ) {
			$object_property = $this->_object_adapter->__get( $property_name );
			return $this->apply_filters( 'object_property', $object_property );
		}
		function has_object_property( $property_name ) {
			$has_object_property = $this->_object_adapter->__has( $property_name ) !== false;
			return $this->apply_filters( 'has_object_property', $has_object_property );
		}
		function __get( $property_name ) {
			$value = false;
			switch ( $property_name ) {
				case 'object_adapter':
					$value = $this->_object_adapter;
					break;
				default:
					if ( $this->has_object_property( $property_name ) ) {
						$value = $this->get_object_property( $property_name );
					} else {
						$value = parent::__get( $property_name );
					}
					break;
			}
			return $value;
		}
		function __set( $property_name, $value ) {
			switch ( $property_name ) {
				case 'object_adapter':
					$this->_object_adapter = $value;
					break;

				case 'object':
					$this->_object_adapter->object = $value;
					break;

				case 'object_sub_type':
					if ( $this->_object_adapter )
						$this->_object_adapter->object_sub_type = $value;
					break;

				case 'object_type':
					if ( $this->_object_adapter )
						$this->_object_adapter->object_type = $value;
					break;

				default:
					parent::__set( $property_name, $value );
					break;
			}
			return $value;
		}
	}

}
