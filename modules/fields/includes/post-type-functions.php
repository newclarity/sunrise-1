<?php
/*
 * Registers a context.
 * TODO (mikes): This is not tested nor is it in use yet.
 * TODO: It will probably be deleted soon.
 */
function sr_register_context( $context, $args ) {
	global $sr_contexts;
	global $wp_post_types;
	$shared_args = $args;

	if ( ! isset( $args['context_type'] ) )
		$args['context_type'] = 'post_type';

	switch( $args['context_type']) {
		case 'post_type':
			$scope = 'post_types';
			$selector = $post_type = $args['post_type'];
			$wp_post_types[$post_type]->sunrise_args['contexts'][$context] = &$shared_args;
			break;

		default:
			break;
	}
	$sr_contexts[$scope][$selector][$context] = &$shared_args;
 	return apply_filters( 'sr_register_context', $args, $context );
}
/*
 * Returns True if the query is running on an admin list page (i.e. edit.php?post_type=foo)
 *
 * TODO: Decide is $query as a parameter is not too coupled and should isntead be a post_type
 */
function __sr_is_admin_list( $query ) {
 	return Sunrise_Post_Types::is_admin_list( $query );
}
/*
 * Enables extensions for taxonomy registration
 *
 */
function sr_register_taxonomy( $taxonomy, $object_type, $args ) {
	$args = apply_filters( 'sr_taxonomy_registration_args', $args, $taxonomy, $object_type );
 	return register_taxonomy( $taxonomy, $object_type, $args );
}

