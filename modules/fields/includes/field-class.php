<?php
if (!class_exists('Sunrise_Field')) {
	/**
	 *  $base_type => $html_type
	 *  $core => $is_core
	 *
	 *  @property int $id
	 *  @property mixed $value
	 *  @property string $raw_value
	 *  @property int $object_id
	 *  @property string $storage_type
	 *  @property string $entry_name
	 *  @property string $html_type
	 *  @property object $object
	 *  @property string $object_type
	 *  @property string $object_sub_type
	 *  @property int|string $child_index
	 *  @property Sunrise_Field $parent
	 *  @property string $label_text
	 *  @property Sunrise_Field $parent_entry_name
	 *  @property Sunrise_Field $parent_child_index
	 *  @property string $placeholder
	 *
	 *
	 */
	class Sunrise_Field extends Sunrise_Base {
		var $args;
		var $leave_blank = false;    // TODO: HACK, FIX IT!

		protected $_id             = false;
		protected $_entry_name     = false;   // The name of the HTML <input> field, also the $_POST key
		protected $_html_type      = false;		// The type used by "<input type='$html_type'>"
		protected $_class          = false;   //
		protected $_container_class= false;   //
		protected $_attributes_html= false;   //
		protected $_storage_type   = 'meta';  // Current only 'meta' meaning wp_postmeta, wp_usermeta, etc.

		// Base Properties
		var $field_name     = false;
		var $real_name      = false;		// Used for fields like 'post_content' which are displayed as 'content'; we don't use anywhere but might need to
		var $field_type     = false;    // The field type, can be basic (i.e.=$html_type) or advanced (i.e.phone, money, etc.)
		var $registered_type= false;		// Set to type name if the type is actually a 'defaults' registered type
		var $prefix         = null;		  // Using for prefixing fields with $ signs, etc.
		var $postfix        = null;		  // Using for postfixing fields with % signs, etc.
		var $styles         = false;		// This is for setting styles by name in the input container (NOT CSS styles, classnames instead)
		var $meta_data      = false;		// Array of field attribute names whose value to add to end of field for access from jQuery
		var $required       = false;		// Not required by default
		var $no_label       = false;		// If true, omit the label when displaying the HTML
		var $index          = false;		// This is used for repeating fields. If is_numeric($this->index) then it will use an index
		var $fieldset_key   = false;		// This is used for fieldset fields.
		var $form           = false;    // ??
		var $float		      = false;    //
		var $blank          = false;    //
		var $default        = false;    // Default value

		var $mode           = false;    //  'add', 'edit', etc.  TODO: Complete this list

		var $accesskey       = false;   // The accesskey attribute for HTML <input>
		var $dir             = false;   // The dir attribute for HTML <input>
		var $lang            = false;   // The lang attribute for HTML <input>
		var $style           = false;   // The style attribute for HTML <input>
		var $tabindex        = false;   // The tabindex attribute for HTML <input>
		var $title           = false;   // The title attribute for HTML <input>
		var $xml_lang        = false;   // The xml:lang attribute for HTML <input>

		var $_value         = false;    // For use by subclasses to cache
		var $_raw_value     = false;    //
		var $get_default    = false;    // Callback to retrieve the default. Will be passed a post object
		var $get_value      = false;    // Callback to retrieve the value. Will be passed a post array from $_POST
		var $fields_loaded  = false;

		/**
		 * @var bool|Sunrise_Object
		 */
		var $_object_adapter = false;     //
		var $_object_values  = false;     //

		/**
		 * @var bool|Sunrise_Entry_Field_Feature
		 */
		var $entry_feature    = false;		//
		/**
		 * @var bool|Sunrise_Label_Field_Feature
		 */
		var $label_feature    = false;		//
		/**
		 * @var bool|Sunrise_Help_Field_Feature
		 */
		var $help_feature     = false;		//
		/**
		 * @var bool|Sunrise_Message_Field_Feature
		 */
		var $message_feature  = false;		// List of warning or error messages if applicable.
		/**
		 * @var bool|Sunrise_Infobox_Field_Feature
		 */
		var $infobox_feature  = false;		//

		/*
		 * TODO: The following three are yet to be implemented
		 * TODO (mikes 2012-01-29): Probably will remove the subkeys. In hindsight, half-baked.
		 */
		var $sub_keys       = false;    // Array of Subkeys for this field, i.e. like images have a crop.
		var $sub_raw_values = false;  	// Associative array of sub values for this field, i.e. like images have a crop.
		var $repeating      = false;    // Is this a repeating field with controls for add, delete, indent, etc?
		var $fieldset       = false;    // If this field a collection of other specific fields?

		/**
		 * @var bool|array
		 */
		var $capabilities		= true;	// Rare - this one defaults to true meaning view and edit.

		function __construct( $args ) {

			if ( ! isset( $args['mode'] ) )
				$args['mode'] = Sunrise_Forms::get_mode();  // TODO: Extract mode and make it an $arg

			/*
			 * Start with the field_name, ensure it is set. Can't do anything else without it.
			 */
			/*
			 * Cascade call of pre_initialize() and pre_add_intialize()/pre_edit_initialize() from $this->_object_adapter to $this
			 * Add a reference to $this instance so we can pass if via the $args to functions expecting args below
			 */
			$args['field'] = &$this;

			/*
			 * Transform any shorthands for field names
			 */
			$this->args = Sunrise_Fields::transform_shorthands( $args );

			/*
			 * Grab all the args that are properties or _properties and set them.
			 * Put everything into $args and everything else in $extra.
			 */
			$this->construct_from_args( $this->args );

			/*
			 * Initialize the appropriate object_adapter if it wasn't passed already
			 */
			if ( ! $this->_object_adapter )
				$this->object_adapter = Sunrise_Objects::get_instance_for( $this->args['object_type'], $this->args );

			/*
			 * Cascade call of pre_initialize() and pre_add_intialize()/pre_edit_initialize() from $this->_object_adapter to $this
			 * Add a reference to $this instance so we can pass if via the $args to functions expecting args below
			 */
			$this->_cascade_action( 'pre_initialize', $this->args );
			if ( $this->mode )
				$this->_cascade_action( "pre_{$this->mode}_initialize", $this->args );

			/*
			 * Initialize all the features based on prefixed args (i.e. 'label_position' assigned to $this->label_feature->position)
			 */
			$this->_initialize_features( $this->args );

			/*
			 * Run general initialization code and call initialization methods for $this->_object_adapter and for subclasses
			 */
			if ( ! $this->_html_type )
				$this->html_type = $this->field_type;   // Assumes that if html_type is not set we'll just default to field_type

			if ( $this->real_name != $this->field_name )
					$this->_class .= " {$this->real_name}";

			/*
			 * Cascade call of initialize() and add_intialize()/edit_initialize() from $this->_object_adapter to $this
			 */
			$this->_cascade_action( 'initialize', $this->args );
			if ( $this->mode )
				$this->_cascade_action( "{$this->mode}_initialize", $this->args );

			/*
			 * For add post only,
			 * cascade call of set_raw_value() method from $this->_object_adapter to $this
			 */
			$this->_raw_value = $this->_cascade_filter( 'initial_value', $this->_raw_value, $this->args );
			if ( $this->mode )
				$this->_raw_value = $this->_cascade_filter( "initial_{$this->mode}_raw_value", $this->_raw_value, $this->args );

			do_action( 'sr_initialize_field', $this, $args );

		}
		private function _cascade_action( $method, $args ) {
			$this->_object_adapter->do_action( $method, $args );  // If object_adapter defines this, call it
			$this->do_action( $method, $args );                   // If this instance defines this, call it
		}
		private function _cascade_filter( $method, $value, $args ) {
			$value = $this->_object_adapter->apply_filters( $method, $value, $args );   // If object_adapter defines this, call it
			$value =  $this->apply_filters( $method, $value, $args );                   // If this instance defines this, call it
			return $value;
		}
		/*
		 * Extract feature-specific args and then create instances of each feature, storing into a property of the class.
		 * Properties this function will initialize include:
		 *
		 *    $this->label_feature
		 *    $this->help_feature
		 *    $this->infobox_feature
		 *    $this->message_feature
		 *
		 */
		private function _initialize_features( $args ) {
			$feature_args = array();
			foreach( Sunrise_Field_Feature::get_valid_types() as $feature_type ) {
				$feature_args[$feature_type] = array();
				foreach( $args as $name => $value ) {
					// Check for any args like 'label_position' and 'help_html_feature'
					$local_name = preg_replace( "#^{$feature_type}_(.+)$#", '$1', $name );
					if ( $local_name == $name ) {
						// Not found? Check for any args like 'foo_label' and 'bar_message'
						// TODO: Do we really need this anymore since I can remember any examples?
						$local_name = preg_replace( "#^(.+)_{$feature_type}$#", '$1', $name );
					}
					if ( $local_name == $name ) {
						// Still not found? Not an $args for this feature
						continue;
					}
					// Record
					$feature_args[$feature_type][$local_name] = $value;
				}
				// Store a reference to the feature's parent, i.e. $this field
				$feature_args[$feature_type]['parent'] = &$this;

				// Now create an instance for each feature and store into the class
				$feature_name = "{$feature_type}_feature";
				$class = Sunrise::get_class_for( 'fields/field-feature', $feature_type );
				$this->{$feature_name} = new $class( $feature_args[$feature_type] );
			}
		}
		function get_html_type() {
			return $this->apply_filters( 'html_type', $this->_html_type );
		}
		function set_html_type( $html_type ) {
			$this->_html_type = $html_type;
		}
		/**
		 *
		 * @return mixed
		 */
		function get_viewing_html() {
			$html = <<<HTML
<span id="{$this->id}-viewer" class="viewer">{$this->value}</span>
<input id="{$this->id}" type="hidden" name="{$this->entry_name}" value="{$this->value}" />
HTML;
			return $this->apply_filters( 'viewing_html', $html );
		}
		function get_entry_html() {
			$html = "<input id=\"{$this->id}\" type=\"{$this->html_type}\" name=\"{$this->entry_name}\" {$this->class_html}{$this->attributes_html} />";
			return $this->apply_filters( 'entry_html', $html );
		}
		function get_required_html() {
			return $this->apply_filters( 'required_html', $this->required ? 'field-required' : '' );
		}

		function can_edit() {
			return $this->can_view() && (
				true === $this->capabilities || (
					isset( $this->capabilities['edit'] ) && false !== $this->capabilities['edit'] && (
						true === $this->capabilities['edit'] || current_user_can( $this->capabilities['edit'] )
					)
				)
			);
		}
		function can_view() {
			return true === $this->capabilities ||
						 ( isset( $this->capabilities['view'] ) && false !== $this->capabilities['view'] &&
						 		( true === $this->capabilities['view'] || current_user_can( $this->capabilities['view'] ) )
						 );
		}

		function get_item_class() {
			$value = "field-row {$this->field_type}{$this->required_html}";
			if ( ! $this->can_view() )
				$value .= ' cannot-view';
			if ( $this->field_type != $this->html_type)
				$value .= " {$this->html_type}";
			if ( $this->_class )	// TODO (mikes 2012-07-25): Determine if this should stay here or go elsewhere in Sunrise 2.
				$value .= " {$this->_class}";
			return $this->apply_filters( 'item_class', $value );
		}
		function get_item_html() {
			$label = $this->no_label ? '' : $this->label_container_html;
			$html = "<li id=\"{$this->id}-field-row\" class=\"{$this->item_class}\">{$label}{$this->container_html}<div class=\"clear\"></div></li>";
			return $this->apply_filters( 'item_html', $html );
		}
		function get_field_value( $args = array() ) { // TODO: Figure out why we need $args and do differently
			return $this->apply_filters( 'field_value', $this->_object_adapter->get_field_value( $args ) );
		}
		/*
		 * Returns the value that needs to be stored in the HTML vs. the value that we might expect.
		 * For example, instead of value of true or false attribute value for checkbox is always "1"
		 *  <input type="checkbox" value="1" checked="checked" />
		 */
		function get_attribute_value( $args = array() ) { // TODO: Figure out why we need $args and do differently
			$args['field'] = $this;
			return $this->apply_filters( 'attribute_value', $this->get_raw_value( $args ), $args );
		}
		/*
		 * Retrieves the RAW value and then returns the value we might expect, such as an array or an object.
		 * For example, with checkboxes instead of raw_value of "checked" or "" value is true or false.
		 */
		function get_value( $args = array() ) {
			return $this->apply_filters( 'value', $this->get_raw_value( $args ) );
		}
		function load_value( $args = array() ) {
			$this->_raw_value = $this->load_raw_value( $args );
			$value = $this->apply_filters( 'value', $this->_raw_value );
			/*
			 * $this->sub_raw_values will be set in $this->load_raw_value()
			 *  if $this->sub_keys has  array of sub_keys to load.
			 *
			 */
			if ( is_a( $this, 'Sunrise_Field' ) && $this->sub_raw_values ) {
				if ( ! is_array( $value ) )
					$value = array( 'value' => $value );

				if ( ! isset( $value['value'] ) )
					$value['value'] = $value;

				$value = array_merge( $this->sub_raw_values, $value );
				$value = $this->apply_filters( 'load_sub_key_value', $value );
			}
      		$this->_value = $this->apply_filters( 'load_value', $value );
			return $this->_value;
		}
		/*
		 * TODO: Move ::get_mode() to caller, and add a $this->mode property
		 */
		function load_raw_value( $args = array() ) {
			static $mode;
			$value = false;
			if ( ! isset( $mode ) )
				$mode = Sunrise_Forms::get_mode();
			if ( 'add' == $mode  || 'template' == $this->index ) {
				if ( $this->get_default ) {
					$value = call_user_func( $this->get_default, $this );
				} else if ( false !== $this->default ) {
					$value = $this->default;
				} else {
					$value = '';
				}
			} else if ( 'edit' == $mode ) {
				if ( $this->get_value ) {
//					$value = Sunrise_Storage::load_raw_value( $this->storage_type, $this, $args );
					$values = $this->load_object_values();
					$value = call_user_func( $this->get_value, $values, $this ); // Get the
					if ( 'post_title' == $this->field_name && empty( $values ) ) {
						// This is a very common problem so we need to tell the developer if it happens.
						$get_value = is_array( $this->get_value ) ? $this->get_value[1] : $this->get_value;
						$message = __( "An empty title value was returned for the field named [post_title] by the [get_value</b>' callback [%s()</b>' of the object_type/sub_type [%s'/'%s</b>'." );
						sr_die( $message,	$get_value, $this->object_type, $this->object_sub_type );
					}
				}
			}
			if ( $value === false ) {
				$value = Sunrise_Storage::load_raw_value( $this->storage_type, $this, $args );
			}
			/*
			 * If this field has subkeys then a raw_value will be an array of the values.
			 * For example, and Image Field might have $this->sub_keys = array( 'crop' ):
			 *
			 *    '_photo'        // $post_id for the $post->post_type=='attachment'
			 *    '_photo[crop]'  // Comma separated list of crop values.
			 */
			if ( $this->sub_keys && ! $this->sub_raw_values ) {
				$sub_keys = (array)$this->sub_keys;
				$this->sub_keys = false;
				$storage_key = $args['storage_key'] = $this->get_storage_key( $args );
				$this->sub_raw_values = array();
				foreach( $sub_keys as $sub_key ) {
					$args['storage_key'] = "{$storage_key}[{$sub_key}]";
					$this->sub_raw_values[$sub_key] = $this->load_raw_value( $args );
				}
				$this->sub_keys = $sub_keys;
				$this->sub_raw_values = $this->apply_filters( 'sub_raw_value', $this->sub_raw_values, $this, $args );
			}
			return $value;
		}
		/*
		 * TODO (mikes): Need to document exactly what get_raw_value() function does and what it is expected to do since there are
		 *  severalcontexts in which values are retrieved. We need to make sure we address all the use-cases and determine
		 * if they should all be handled by this method or if other more explicit methods should be added/used.
		 *
		 */
		function get_raw_value( $args = array() ) {
			if ( ! $this->_raw_value ) {
				$raw_value = $this->load_raw_value( $args );
				$this->_raw_value = $this->apply_filters( 'raw_value', $raw_value );
			}
			return $this->_raw_value;
		}
		/**
		 * @param array $args
		 * @return string
		 */
		function get_storage_key( $args = array() ) {
			if ( ! isset( $args['storage_class'] ) ) {
				/*
				 * If not passed then get it. This is done to allow this function to be called in a loop where the class doesn't change.
				 * TODO (mikes): Improve lookup for storage_class. This can be done better.
				 */
				$args['storage_class'] = Sunrise::parse_class( "fields/storage/{$this->storage_type}" );
			}
			$storage_key = Sunrise::call_instance_method( $args['storage_class'], 'get_storage_key', $this );
			return $storage_key;
		}

		function set_raw_value( $value ) {
			$this->_raw_value = $value;
		}
		/*
		 * Use to retrieve a Complex Value object that contains both a $value and any $sub_raw_values
		 */
		function get_complex_raw_value() {
			/*
			 * Get raw value. This will call $this->load_raw_value() if needed which will then load $this->sub_raw_values.
			 */
			$value = $this->get_raw_value();
			return new Sunrise_Complex_Value( $value, $this->sub_raw_values );
		}
		/*
		 * Use to retrieve a Complex Value object that contains both a $value and any $sub_raw_values
		 */
		function set_complex_raw_value( $complex_value ) {
			/*
			 * Get raw value. This will call $this->load_raw_value() if needed which will then load $this->sub_raw_values.
			 */
			$this->raw_value = $complex_value->value;
			$this->sub_raw_values = $complex_value->sub_values;
		}
		function get_container_class() {
			$container_class = "{$this->field_type} {$this->id}";

			if ( $this->id != $this->name )
				$container_class .= " {$this->name}";

			if ( $this->float )
				$container_class .= " {$this->float}";

			if ( $this->_container_class )
				$container_class .= $this->_container_class;

			return $this->apply_filters( 'container_class', $container_class );
		}
		function in_fieldset() {  // TODO: Make this a property...
			return $this->parent && 'fieldset' == $this->parent->field_type;
		}
		function in_repeating() {  // TODO: Make this a property...
			return $this->parent && 'repeating' == $this->parent->field_type;
		}
		function get_parent_entry_name() {
			$entry_name = false;
			if ( $this->parent && 'fieldset' == $this->parent->field_type ) {
				$fieldset_field = $this->parent;
				$entry_name = $fieldset_field->entry_name;
				if ( $fieldset_field->parent && 'repeating' == $fieldset_field->parent->field_type ) {
					$repeating_field = $fieldset_field->parent;
					if ( 'template' == $repeating_field->child_index ) {
						$entry_name = "{$repeating_field->entry_name}:{$repeating_field->child_index}";
					} else {
						$entry_name = "{$repeating_field->entry_name}[{$repeating_field->child_index}]";
					}
				}
			}
			return $entry_name;
		}
		function get_repeating_fieldset_index() {
			$index = false;
			if ( isset( $this->parent ) ) {
				$parent = $this->parent;
				if ( isset( $parent->parent ) ) {
					$parent = $parent->parent;
					if ( isset( $parent->child_index ) ) {
						$index = $parent->child_index;
					}
				}
			}
			return $index;
		}
		function set_entry_name( $entry_name ) {
			$this->_entry_name = $entry_name;
		}
		function get_parent_child_index() {
			$child_index = false;
			if ( $this->parent ) {
				$child_index = $this->parent->child_index;
				$child_index = is_array( $child_index ) ? implode( '][', $child_index ) : $child_index;
			}
			return $child_index;
		}
		function get_entry_name() {
			$entry_name = $this->_entry_name; // This here for debugging
			if ( ! $this->_entry_name ) {
				if ( $this->in_fieldset() ) {
					$entry_name = "{$this->parent_entry_name}[{$this->real_name}]";
                } else if ( $this->parent &&  (strpos($this->parent->field_type, 'repeating') !== false) && $this->field_type != 'fieldset' ) {
					$entry_name = $this->real_name . ( 'template' == $this->index ? ':template' : "[{$this->parent_child_index}]" );
                } else {
					$entry_name = $this->real_name;
                }
				$this->_entry_name = $this->apply_filters( 'entry_name',  $entry_name );
				$entry_name = $this->_entry_name;   // This here for debugging
            }
            return $this->_entry_name;
		}
		function get_id() {
			if ( ! $this->_id ) {
				$this->_id = sr_dashize( $this->field_name );
				if ( $this->in_fieldset() ) {
					$this->_id = "{$this->parent->id}-{$this->_id}";
				} else if ( $this->index ) {
					// TODO: Need make index_to_string function somehow
					$index = is_array( $this->index ) ? implode( '-', $this->index ) : $this->index;
					$this->_id .= "-{$index}";
				}
			}
			return $this->apply_filters( 'id', $this->_id );
		}
		function get_label_text() {
			return $this->label_feature->text;
		}
		function get_form_mode() {
			$value = false;
			if ( $this->form )
				$value = $this->form->mode;
			return $value;
		}
		function get_colon() {
			return ( $this->label_feature->no_colon || $this->hidden ) ? '' : ':';
		}
		function load_object_values( $force = false ) {
			if ( $force || ! $this->_object_values ) {
				$object_adapter = $this->object_adapter;
				$this->_object_values = $object_adapter->load_values();
			}
			return $this->_object_values;
		}
		function get_object_adapter() {
			$value = false;
			if ( $this->_object_adapter )
				$value = $this->_object_adapter;
			return $value;
		}
		function get_object() {
			$value = false;
			if ( $this->_object_adapter )
				$value = $this->_object_adapter->object;
			return $value;
		}
		function get_object_id() {
			$value = false;
			if ( $this->_object_adapter )
			  $value = $this->_object_adapter->object_id;
			return $value;
		}
		function get_object_type() {
			$value = false;
			if ( $this->_object_adapter )
				$value = $this->_object_adapter->object_type;
			return $value;
		}
		function get_object_sub_type() {
			$value = false;
			if ( $this->_object_adapter )
				$value = $this->_object_adapter->object_sub_type;
			return $value;
		}
		function get_name() {
			return $this->field_name;
		}
		function get_description() {
			return parent::get_description( "field_name={$this->field_name}" );
		}
		function get_is_core() {
			return $this->storage_type == 'core';
		}
		function get_is_hidden() {
			return $this->field_type == 'hidden';
		}
		function get_is_checked() {
			return $this->get_raw_value() == 'checked';
		}
		function get_storage_type() {
			if ( ! $this->_storage_type )
				$this->_storage_type = Sunrise_Storage::get_default_type();
			return $this->_storage_type;
		}
		function get_feature_html( $property_name ) {
			$value = false;
			if ( ! $this->hidden  || $property_name == 'entry_html' ) {
				$feature_name = str_replace( '_html', '_feature', $property_name );
				$value = $this->{$feature_name}->get_html();
			}
			return $value;
		}
		function get_container() {
			return $this->parent;
		}
		function get_container_feature_html( $property_name ) {
			$value = false;
			if ( ! $this->hidden  || $property_name == 'entry_container_html' ) {
				$feature_property = str_replace( '_container_html', '_feature', $property_name );
				$value = $this->{$feature_property}->get_container_html();
			}
			return $value;
		}
		function get_required_callout_html() {
			$value = false;
			if ( ! $this->required ) {
				$value = '';
			} else if ( $this->field_type == 'radio' ) { // TODO: Handle Radio Fields
				echo 'Note to Developer: Time to figure out how to handle required radio fields';
				exit;
			} else {
				$value = '<span class="required-callout">* </span>';
			}
			return $value;
		}
		function set_placeholder( $placeholder ) {
			$this->entry_feature->placeholder = $placeholder;
		}
		function get_placeholder() {
			$default = $this->apply_filters( 'default_placeholder_attribute', '', $this );
			$value = $this->entry_feature->placeholder;
			return empty( $value ) ? $default : $value;
		}
		function get_entry_class( $args = array() ) {
			return $this->entry_feature->get_class();
		}
		function get_entry_class_html( $args = array() ) {
			return $this->entry_feature->get_class_html();
		}
		function get_entry_container_class( $args = array() ) {
			return $this->entry_feature->get_container_class();
		}
		function get_entry_attributes_html( $args = array() ) {
			return $this->entry_feature->get_attributes_html();
		}
		function get_entry_container_html( $args = array() ) {
			return $this->entry_feature->get_container_html();
		}
		function update_value( $value, $args = array() ) {
			$value = $this->apply_filters( 'update_value', $value, $args );
			return $this->update_raw_value( $value, $args );
		}
		function update_raw_value( $value, $args = array() ) {
			$value = $this->apply_filters( 'update_raw_value', $value, $args );
			Sunrise_Storage::update_raw_value( $this->storage_type, $this, $value, $args );
		}
		function update_sub_raw_values( $args = array() ) {
			$this->apply_filters( 'update_sub_raw_values', $args );
			return Sunrise_Storage::update_sub_raw_values( $this->storage_type, $this, $args );
		}
		function delete_value( $args = array() ) {
			if ( $this->apply_filters( 'delete_value', true, $args ) )
				$this->delete_raw_value( $args );
		}
		function delete_raw_value( $args = array() ) {
			if ( $this->apply_filters( 'delete_raw_value', true, $args ) )
				Sunrise_Storage::delete_raw_value( $this->storage_type, $this, $args );
		}
		/*
		 * This is used for repeating fields to enable nesting/outlining.
		 */
		function __call( $method_name, $args ) {
			switch ( $method_name ) {
				case '_____':
					break;
				default:
					$object_class = get_class( $this->_object_adapter );
					if ( ! method_exists( $this->_object_adapter, 'has_method' ) ) {
						sr_die( 'Class [%s] has no [has_method()] yet.', $object_class );
					} else {
						if ( ! $this->_object_adapter->has_method( $method_name ) ) {
							$class = get_class( $this );
							sr_die( '%s->__call(\'%s\') not yet configured.', $class, $method_name );
						} else {
							if ( ! method_exists( $this->_object_adapter, 'call_method' ) ) {
								sr_die( 'Class [%s] has no [call_method()] yet.', $object_class );
							} else {
								$this->_object_adapter->call_method( $method_name, $args );
							}
						}
					}
					break;
			}
		}
		// TODO: Figure out if we need this and what it's name should be.
		private static function value_to_raw_value( $value ) {
			return $value;  // TODO: Figure out what, if anything we need to do here.
		}
		function get_object_property( $property_name ) {
			$object_property = $this->_object_adapter->__get( $property_name );
			return $this->apply_filters( 'get_object_property', $object_property );
		}
		function has_object_property( $property_name ) {
			$has_object_property = $this->_object_adapter->__has( $property_name ) !== false;
			return $this->apply_filters( 'has_object_property', $has_object_property );
		}
		function has_POST() {
			return $this->apply_filters( 'has_POST', $this->_get_POST_value() !== false );
		}
		/*
		 * Drills down into a field name and gets a value from $_POST.
		 *
		 * Example field names might include:
		 *
		 *  single: '_attorney_post_title'
		 *  multiple: '_attorney_products[3]'
		 *  composite: '_attorney_file[description]' and 'file[url]'
		 *  multiple/composite: '_attorney_orders[7][customer_name]'
		 *
		 */
		function _get_POST_value() {
			$names = explode( '[', $this->entry_name );
			$value = $_POST;
			foreach( $names as $name ) {
				$name = trim( $name, ']' );
				if ( isset( $value[$name] ) ) {
					$value = $value[$name];
				} else {
					$value = false;
					break;
				}
			}
			return $this->apply_filters( 'POST_value', $value );
		}
		function update_from_POST() {
			if ( $this->storage_type != 'core' ) {
				$value = $this->_get_POST_value();
				$args = array( 'object_id' => $this->object_id );
				$value = $this->apply_filters( 'update_from_POST', $value, $args );
				$this->update_value( $value, $args );
			}
		}
		function set_parent( $parent ) {
			$this->_parent = $parent;				// Should we call parent::set_parent($parent)?
			if ( $parent->_object_adapter )
				$this->_object_adapter = $parent->_object_adapter;
		}
		function __set( $property_name, $value ) {
			switch ( $property_name ) {
				case 'raw_value':
					$this->set_raw_value( $value );
					break;

				case 'placeholder':
					$this->set_placeholder( $value );
					break;

				case 'value':
					$this->_raw_value = self::value_to_raw_value( $value );
					//$this->_object_adapter = $value;
					break;

				case 'object_adapter':
					$this->_object_adapter = $value;
					$this->_object_adapter->parent = $this;
					break;

				case 'object':
					$this->_object_adapter->object = $value;
					break;

				case 'object_id':
				case 'object_type':
				case 'object_sub_type':
					$this->_object_adapter->__set( $property_name, $value );
					break;

				case 'entry_name':
					$this->_entry_name = $value;
					break;

				case 'class':
					$this->_class = $value;
					break;

				case 'container':
					$this->parent = $value;
					break;

				case 'container_class':
					$this->_container_class = $value;
					break;

				case 'checked':
					$this->_raw_value = $value ? 'checked' : '';
					break;

				case 'html_type':
					$this->set_html_type( $value );
					break;

				case 'parent':
					$this->set_parent( $value );

				default:
					if ( $this->_object_adapter->__has( $property_name ) ) {
						$this->_object_adapter->__set( $property_name, $value );
					} else {
						parent::__set( $property_name, $value );
					}
					break;
			}
		}
		function __get( $property_name ) {
			$value = false;

			if ( $this->sub_keys ) {
				/*
				 * Look for sub keys.  Try get_methods first but if not get the raw values
				 */
				if ( isset( $this->sub_keys[$property_name] ) ) {
					if ( method_exists( $this, "get_{$property_name}" ) ) {
						$value = call_user_func( array( &$this, "get_{$property_name}" ) );
					} else if ( isset( $this->sub_raw_values[$property_name] ) ) {
						$value = $this->sub_raw_values[$property_name];
					}
				}
			}
			switch ( $property_name ) {
				case 'id':
				case 'value':
				case 'raw_value':
				case 'attribute_value':
				case 'is_hidden':
				case 'is_checked':
				case 'is_core':
				case 'is_checked':
				case 'storage_type':
				case 'colon':
				case 'item_html':
				case 'item_class':
				case 'container':
				case 'parent_entry_name':
				case 'entry_name':
				case 'required_callout_html':
				case 'object_adapter':
				case 'object':
				case 'object_id':
				case 'object_type':
				case 'object_sub_type':
				case 'form_mode':
				case 'required_html':
				case 'name':
				case 'html_type':
				case 'label_text':
				case 'parent_child_index':
				case 'placeholder':
					$value = call_user_func( array( &$this, "get_{$property_name}" ) );
					break;

				case 'class':
				case 'class_html':
				case 'container_class':
				case 'attributes_html':
				case 'container_html':
					$value = call_user_func( array( &$this, "get_entry_{$property_name}" ) );
					break;

				case 'core':
				case 'hidden':
				case 'checked':
					$value = call_user_func( array( &$this, "get_is_{$property_name}" ) );
					break;

				case 'entry_html':
				case 'help_html':
				case 'infobox_html':
				case 'label_html':
				case 'message_html':
					$value = $this->get_feature_html( $property_name );
					break;

				case 'entry_container_html':
				case 'help_container_html':
				case 'infobox_container_html':
				case 'label_container_html':
				case 'message_container_html':
					$value = $this->get_container_feature_html( $property_name );
					break;

				default:
					// Try delegating this to the object before failing; the object may know things like post, user, post_id, user_id, etc.
					if ( $this->has_object_property( $property_name ) ) {
						$value = $this->get_object_property( $property_name );
					} else {
						$value = parent::__get( $property_name );
					}
					break;

			}
			return $value;
		}
	}
}
