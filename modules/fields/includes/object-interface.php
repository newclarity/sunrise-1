<?php

if ( ! interface_exists( 'Sunrise_Object_Interface' ) ) {

	interface Sunrise_Object_interface {
		public function get_object_id();
		public function get_object_sub_type();
		public function load_object( $object_id = null );
		public function __has( $property_name );  // This is NOT a magic method, but it's name is consistent with __get() and __set()
		public function __get( $property_name );
		public function __set( $property_name, $value );
		/*
		 * OPTIONAL:
		 *  function do_initialize_field( $args ); $args['object_sub_type'] should be set
		 *  function do_pre_initialize( $post_field )
		 */
	}

}

