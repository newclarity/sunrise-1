<?php

function sr_get_query( $query_name, $args = array() ) {
	global $post, $paged;
	$args = sr_parse_args( $args, array(
		'post_type' => isset( $post->post_type ) ? $post->post_type : 'post',
		'paged' => $paged,
	));
	return new WP_Query( apply_filters( 'sr_get_query', $args, $query_name ) );
}
