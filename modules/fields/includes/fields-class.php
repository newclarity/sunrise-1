<?php
if (!class_exists('Sunrise_Fields')) {
	/**
	 *
	 * TODO: The enqueue_css() and enqueue_js() need to be modifed and moved to Sunrise Core
	 */
	class Sunrise_Fields extends Sunrise_Static_Base {
		static $field_types = array();
		/*
		 * Enqueue scripts within the admin
		 *
		 * TODO: Make sure to only include these when needed
		 *
		 */
		static function check_field_name( $field_name ) {
			if ( WP_DEBUG && ! preg_match( '#^[0-9a-z_[\]]+$#', $field_name ) ) {
				sr_die( __('[%s] is not a valid field name. " .
					"Field names can only contain numbers ["0"-"9"], lowercase letters ["a"-"z"] an underscore ["_"] or square brackets["[".."]"].' ), $field_name );
			}
			return $field_name;
		}
		static function admin_init() {
			self::enqueue_css( __FILE__, 'base', array(
        		'handle'   => 'sunrise-fields-admin-style',
				'filepath' => '/css/admin-style.css',
			));
			self::enqueue_js( __FILE__, 'base', array(
				'handle'   => 'sunrise-fields-admin-script',
				'filepath' => '/js/admin-script.js',
				'dependencies' => array('jquery'),
			));
			self::enqueue_js( __FILE__, 'base', array(
				'handle'   => 'sunrise-fields-watermark-script',
				'filepath' => '/js/jquery.watermark.js',
				'dependencies' => array('jquery'),
			));
			self::enqueue_js( __FILE__, 'base', array(
				'handle'   => 'sunrise-fields-tooltips-script',
				'filepath' => '/js/jquery.sr-tooltips.js',
				'dependencies' => array('jquery'),
			));

			/*
			 * Add hooks for those fields that have an admin_init.
			 * TODO: Consider adding late priority admin_init hook to enqueue default .js and .css files for fields, i.e. ones not already added.
			 */
			$context = Sunrise::get_context();
			foreach( Sunrise_Fields::get_valid_types() as $field_class => $field_type ) {
				if ( class_exists( $field_class ) && method_exists( $field_class, 'admin_init' ) ) {
					add_action( "admin_init", array( $field_class, 'admin_init' ) );
				}
			}

		}
		static function normalize_args( $args ) {
      return Sunrise::normalize_args( $args, array(
				'name'    		=> 'field_name',
				'type'    		=> 'field_type',
				'storage' 		=> 'storage_type',
				'no_colon'		=> 'label_no_colon',
			));
		}
		/**
		 * Ensure we have the proper capabilities
		 *
		 * 	If false === $this->capabilities this the field cannot be viewed or edited.
		 * 	If true === $this->capabilities this the field can be viewed and edited.
		 * 	If isset( $this->view_capability ) or isset( $this->edit_capability ) then
		 * 		$capabilities defaults to array('view' => true,'edit' => false) then assigned the capability(s).
		 * 	If is_array( $this->capabilities ) assume it's all good.
		 *
		 * @param array $args
		 * @return array
		 *
		 * TODO (mikes 2012-01-30): Make capabilities a virtual method. Also, I made this code to complicated to document. Make it easier to understand.
		 */
		static function normalize_capabilities( $args ) {
			if ( ! isset( $args['capabilities'] ) || false !== $args['capabilities'] ) {
				if ( isset( $args['capabilities'] ) && is_string(  $args['capabilities'] ) ) {
					$args['capabilities'] = array(
						'view' => $args['capabilities'],
						'edit' => $args['capabilities'],
					);
				} else {
					$done_once = false;
					$capabilities = true;
					foreach( array( 'view_capability', 'edit_capability' ) as $capability ) {
						if ( isset( $args[$capability] ) ) {
							if ( ! $done_once  ) {
								$capabilities = array(
									'view' => true,
									'edit' => false,
								);
								if ( ! empty( $args['capabilities'] ) ) {
									if ( is_array( $args['capabilities'] ) ) {
									$capabilities = array_merge( $capabilities, $args['capabilities'] );
									} else {
										sr_die( __( "The capabilities field property must be specified as false, true or an associative array with 'view' and 'edit' elements containing values of either true or a capability string." ) );
									}
								}
								$done_once = true;
							}
							switch ( $capability[0] ) {
								case 'v': $capabilities['view'] = $args[$capability]; break;
								case 'e': $capabilities['edit'] = $args[$capability]; break;
							}
							unset( $args[$capability] );
						}
					}
					if ( true === $capabilities || ( true === $capabilities['view'] && true === $capabilities['edit'] ) ) {
						$args['capabilities'] = true;
					} else {
						$args['capabilities'] = $capabilities;
					}
				}
			}
			return $args;

		}
		/*
		 * Sets field defaults
		 *
		 * TODO: Need to properly document this
		 */
		static function transform_shorthands( $args ) {
			static $entry_vars;

			if ( ! isset( $args['_shorthands_transformed'] ) ) {

				if ( ! isset( $args['type'] ) && ! isset( $args['field_type'] ) )
					$args['field_type'] = 'text';

				/**
				 * Normalize the capabilities
				 */
				$args = self::normalize_capabilities( $args );

				/**
				 * Normalize $args; transform any shorthands into longhands for both $args and $args['field_args']
				 */
				$args = self::normalize_args( $args );
				if ( isset( $args['field_args'] ) )
					$args['field_args'] = self::normalize_args( $args['field_args'] );

				/**
				 * Test to make sure they provided a 'field_name'
				 */
				if ( ! isset( $args['field_name'] ) || ! $args['field_name'] ) {
					sr_die( "No 'field_name' was set!" );
				}

				/*
				 * Allow control names to be added to array as values instead of requiring them all to be keys.
				 */
				if ( isset( $args['controls'] ) && is_array( $args['controls'] ) ) {
					$args['controls'] = sr_expand_args( $args['controls'] );
				}

				if ( ( isset( $args['core'] ) && $args['core'] ) ||
						 ( isset( $args['is_core'] ) && $args['is_core'] ) ) {
					if ( ! $args['storage_type'] )
						$args['storage_type'] = 'core';
					else if ( $args['storage_type'] != 'core' )
						sr_die( "Properties for field [{$args['field_name']}] in conflict:".
							" Cannot have 'storage_type'=>{$args['storage_type']} when 'core'=>true or 'is_core'=>true." );
				}
				/*
				 * Allow for a shorthand for the text values, i.e. label => label_text, help => help_text, etc.
				 */
				foreach( Sunrise_Field_Feature::get_valid_types() as $feature_type ) {
					$args["{$feature_type}_text"] = isset( $args[$feature_type] ) ? $args[$feature_type] : false;
					unset( $args[$feature_type] );
				}

				/*
				 * Capture all input $args like 'size' and transform them to 'entry_size'
				 */
				if ( ! isset( $entry_vars ) ) {
					$entry_vars = get_class_vars( 'Sunrise_Entry_Field_Feature' );
					unset( $entry_vars['parent'] );	 // TODO (mikes): This is a hack we need to get rid of later
					$entry_vars = array_keys( $entry_vars );
				}
				foreach( $entry_vars as $entry_var ) {
					if ( isset( $args[$entry_var] ) ) {
						$args["entry_{$entry_var}"] = $args[$entry_var];
						unset( $args[$entry_var] );
					}
				}
				$args['_shorthands_transformed'] = true;
			}
			return $args;
		}
		/**
		 * This function is a NASTY UGLY HACK to allow templates for repeating fieldsets to validate the nonce.
		 *
		 * The nonce will have been generated using a fieldname like "firm-timeline-2-photo" yet the nonce will have
		 * been generated for the field using "firm-timeline-template-photo." We used the HTML <input> name in the form
		 * of "firm_timeline[2][photo]" to find and replace the numeric. Should also work with repeating photo uploads,
		 * but this wasn't tested.
		 *
		 * @param string $nonce_action_prefix
		 * @param array $args
		 * @return string
		 */
		private static function _discover_nonce_action( $nonce_action_prefix, $args ) {
			$nonce_action = "{$nonce_action_prefix}_{$args['field_id']}";
			if ( ! wp_verify_nonce( $args['nonce'], $nonce_action ) ) {
				$segment_count = count( explode( '[', $args['field_name'] ) );
				$regex_prefix = "^([^[]+)\[([1-9][0-9]?)\]";
				if ( 3 == $segment_count ) {
					$nonce_action_suffix = preg_replace( "#{$regex_prefix}\[(.+)\]$#", '$1-template-$3', $args['field_name'] );
				} else {
					$nonce_action_suffix = preg_replace( "#{$regex_prefix}$#", '$1-template', $args['field_name'] );
				}
				$nonce_action_suffix = sr_dashize( $nonce_action_suffix );
				// Look to see if it should have been a template
				$new_nonce_action = "{$nonce_action_prefix}_{$nonce_action_suffix}";
				if ( wp_verify_nonce( $args['nonce'], $new_nonce_action ) )
					$nonce_action = $new_nonce_action;
			}
			return $nonce_action;
		}
		/**
		 * Retrieve field_id from $_POST values and instantiate field.
		 * Validate the referrer based on action.
		 *
		 * @param string $action Ajax action
		 * @return Sunrise_Field the field according with the request
		 */
		static function get_field_from_ajax_POST( $action ) {
			$field = false;
			if ( empty( $_POST['field_id'] ) )
				die( Sunrise::format_ajax_response( 'Field is not submitted properly.', 'error' ) );

			$args = Sunrise_Objects::get_args_from_POST();
			$args['field_id']   = $_POST['field_id'];
			$args['field_name'] = $_POST['field_name'];
			$args['nonce']      = $_POST['_wpnonce'];
			check_admin_referer( self::_discover_nonce_action( "sunrise_fields_{$action}", $args ) );

			/*
			 * Get field object - Need to perform field actions
			 *
			 * @var Sunrise_Field $field
			 */
			$field = Sunrise_Fields::get_field( $args['field_name'], $args );

			if ( ! $field )
				die( Sunrise::format_ajax_response( 'Field has not been registered.', 'error' ) );

			if ( isset( $args['object_id'] ) ) {
				$field->object_id = $args['object_id'];
				$field->load_raw_value();
				// TODO: Maybe need to set the actual object here? To do that we might need a function to set object based on object type?
			}

			return $field;
		}
		static function on_load() {
			sr_add_action( __CLASS__, 'admin_init', 0 );

			if ( is_admin() || defined( 'DOING_AJAX' ) )
				sr_add_action( __CLASS__, array( 'admin_init', 'forms_fields_init' ) );
			else
				sr_add_action( __CLASS__, array( 'wp', 'forms_fields_init' ) );

		}
		static function forms_fields_init() {
			do_action( 'sr_forms_init' );
			do_action( 'sr_fields_init' );
		}
		/**
		 * @static
		 * @param Sunrise_Field $field
		 * @param array $args
		 * @return string
		 */
		static function get_storage_key( $field, $args = array() ) {
			return $field->get_storage_key( $args );
		}
		/**
		 * Tests to determine is a field is valid based on the parameters.
		 *
		 *  @param string $field_name Lowercase name of the field as it was provided to sr_register_field()
		 *  @param array $args        Contains elements object_type and, if relevent, object_sub_type.
		 *  @return bool              Returns true if the field was registered, false if not.
		 */
		static function is_valid_field( $field_name, $args = array() ) {
			$is_valid = false;
			global $sr_fields;
			if ( $fields = self::get_preprocessed_fields( $args ) ) {
				$is_valid = isset( $fields[$field_name] );
				if ( ! $is_valid  && preg_match( '#^(.*?)-(\d+)-(.*?)$#', $field_name, $match ) ) {
					if ( isset( $fields[$match[1]] ) ) {
						/**
						 * @var Sunrise_Repeating_Field $repeating_field
						 */
						$repeating_field = $fields[$match[1]];
						if ( 'repeating' == $repeating_field->field_type ) {
							$sub_fields = $repeating_field->field_args['fields'];
							$is_valid = isset( $sub_fields[$match[3]] );
						}
					}
				}
			}
			return $is_valid;
		}
		/**
		 * Takes a fieldname and parses it into its different parts
		 *
		 * @example
		 *
		 *  people[2][last_name] ==> (object)array(
		 *		'base_fieldname'  => 'people',
		 *		'repeating_index' => '2'
		 *		'fieldset_key'    => 'last_name'
		 *  )
		 *
		 * @param string $field_name
		 * @return object
		 */
		static function parse_fieldname( $field_name ) {
			$repeating_index = $fieldset_key = false;
			preg_match( '#^([^[]+)(\[([^]]+)\](\[([^]]+)\])?)?$#', $field_name, $match );
			$field_name = $match[1];
			if ( 4 <= count( $match ) ) {
				if ( is_numeric( $match[3] ) ) {
					$repeating_index = intval( $match[3] );
					if ( 6 == count( $match ) ) {
						$fieldset_key = $match[5];
					}
				} else {
					$fieldset_key = $match[3];
				}
			}
			return (object)array(
				'base_fieldname'  => $field_name,
				'repeating_index' => $repeating_index,
				'fieldset_key'    => $fieldset_key,
				);
		}
		/**
		 * Returns an array of pre-processed fields for a given object_sub_type.
		 * Will merge in fields defined for the 'any' object_sub_type.
		 *
		 * @param array|string $type_selector  $args with 'object_type' and 'object_sub_type' elements
		 * 																	   or string in format of "{$object_type}/{$object_sub_type}"
		 * @return array|bool
		 *
		 */
		static function get_preprocessed_fields( $type_selector ) {
			$fields = false;
			list( $object_type, $object_sub_type ) = sr_parse_object_type( $type_selector );
			global $sr_fields;
			if ( isset( $sr_fields[$object_type][$object_sub_type] ) && isset( $sr_fields[$object_type]['any'] ) ) {
				$fields = array_merge( $sr_fields[$object_type]['any'], $sr_fields[$object_type][$object_sub_type] );
			} else if ( isset( $sr_fields[$object_type]['any'] ) ) {
				$fields = $sr_fields[$object_type]['any'];
			} else if ( isset( $sr_fields[$object_type][$object_sub_type] ) ) {
				$fields = $sr_fields[$object_type][$object_sub_type];
			}
			return $fields;
		}
		/**
		 * Gets a registered Sunrise Field object based on it's name and object_type, and object_sub_type if relevant.
		 *
		 *  @param string $field_name Lowercase name of the field as it was provided to sr_register_field()
		 *  @param array $args        Contains elements object_type and, if relevent, object_sub_type.
		 *  @return Sunrise_Field     The named field as registered and initialized.
		 */
		static function get_field( $field_name, $args = array() ) {
			if ( WP_DEBUG ) {
				self::check_field_name( $field_name );
				Sunrise_Objects::validate_args( $args );
			}
			/**
			 * @var Sunrise_Field|bool $field
			 */
			$field = false;
			if ( $fields = self::get_preprocessed_fields( $args ) ) {
				$object_type = $args['object_type'];
				$object_sub_type = $args['object_sub_type'];
				$field_name = self::parse_fieldname( $field_name );
				$field = @$fields[$field_name->base_fieldname];
				if ( ! $field ) {
					$msg = false;
					if ( false !== strpos( $field_name->base_fieldname, '-' ) )
						$msg = 'The field named [%s] for [%s/%s] contains underscores in its name.';
					else if ( isset( $fields[sr_dashize( $field_name->base_fieldname )] ) )
						$msg = 'The field named [%s] for [%s/%s] <em>is</em> defined in the fields array but the name contains dashes; change to underscores.';
					else
						$msg = 'Could not create the field named [%s] for [%s/%s] for unknown reasons.';
					if ( $msg )
						sr_die( $msg, $field_name->base_fieldname, $object_type, $object_sub_type );
				} else if ( $field_name->repeating_index && $field_name->fieldset_key ) {
					/**
					 * @var Sunrise_Repeating_Field $field
					 */
					if ( 'repeating' == $field->field_type && 'fieldset' == $field->field_args['field_type'] ) {
						$field->child_index = $field_name->repeating_index;
						$fieldset_args = array_merge( $args, $field->field_args );
						// TODO (mikes): Will this cause any trouble, having the fieldset field name = repeating field name?
						$fieldset_args['field_name'] = $field_name->base_fieldname;
						$fieldset_field = new Sunrise_Fieldset_Field( $fieldset_args );
						$fieldset_field->index = $field_name->repeating_index;
						$fieldset_field->parent = $field;
						/**
						 * @var Sunrise_Fieldset_Field $fieldset_field
						 */
						$field = @$fieldset_field->fields[$field_name->fieldset_key];
						$field->parent = $fieldset_field;
						$field->index = $field_name->fieldset_key;
					} else {
						$field->construct_from_args( $args );
					}
				}
			}
			return $field;
		}
		/**
		 * Gets the current value for a registered Sunrise Field object
		 *
		 *  @param string $field_name Lowercase name of the field as it was provided to sr_register_field()
		 *  @param array $args        Contains elements object_type and, if relevent, object_sub_type.
		 *  @return mixed             The (potentially complex data type) value representing the field's current value.
		 */
		static function get_field_value( $field_name, $args = array() ) {
			$value = false;
			$field = self::get_field( $field_name, $args );
			if ( $field ) {
				if ( ! empty( $args['reload'] ) && $args['reload'] ) {
					if ( ! empty( $args['object_id'] ) ) {
						$field->object_id = $args['object_id'];
					}
					$value = $field->load_value();
				} else {
					$value = $field->value;
				}
			}
			return $value;
		}
		/**
		 * @static
		 * @param $file
		 * @param $field_type
		 * @param array $args
		 * @return void
		 *
		 */

		static function enqueue_css( $file, $field_type, $args = array() ) {
			self::_enqueue_via_http( 'css', $file, $field_type, $args );
		}
		static function enqueue_js( $file, $field_type, $args = array() ) {
			self::_enqueue_via_http( 'js', $file, $field_type, $args );
		}
		/**
		 * @param $file_type
		 * @param $file
		 * @param $field_type
		 * @param array $args
		 * @return void
		 *
		 * TODO (mikes): Consolidate this and __sr_enqueue_resource()
		 *
		 * TODO (mikes 2011-12-18): Implement method to support automatic use of non-minified/non-packed for debugging.
		 */
		static function _enqueue_via_http( $file_type, $file, $field_type, $args = array() ) {

			$s_type = $file_type == 'js' ? 'script' : 'style';
			$args = wp_parse_args( $args, array(
				'dependencies' => array(),
				'filepath'     => false,
				'context'      => Sunrise::get_context(),
//				'query_args'   => array( 'field_type' => $field_type ),
				'handle'       => "sunrise-fields-{$field_type}",
			));
			extract( $args );

			if ( 'js' == $file_type && ! in_array( 'jquery', $dependencies ) )
				$dependencies[] = 'jquery';   // Ya always gotta have jQuery!

			// $has_field_type = sr_has_form_fields( $context->object_type, $context->object_sub_type, $query_args );
			$has_field_type = true; // TODO: Need to implement this but the sr_has_form_fields() function has changed.

			if ( $context->object_type && ( 'base' == $field_type || $has_field_type ) ) {
				$dir = dirname( dirname( $file ) );

				if ( ! $filepath )
					$filepath = sr_dashize( "/{$file_type}/{$field_type}-field.{$file_type}" );

				if ( file_exists( "{$dir}{$filepath}" ) ) {
					$src = dirname( plugins_url( '', $file ) ) . $filepath;
					__sr_enqueue_resource( $s_type, $handle, $src, array(
						'file' => "{$dir}{$filepath}",
						'dependencies' => $dependencies,
					));
				}
			}
		}
		static function get_class_for( $field_type ) {
			return Sunrise::get_class_for( 'fields/field', $field_type );
		}
		static function get_instance_for( $field_type, $args = array() ) {
			return Sunrise::get_instance_for( 'fields/field', $field_type, $args );
		}
		static function get_options() {
			return get_option('sunrise-fields-options');
		}
		static function is_valid_types( $field_type ) {
			return Sunrise::is_valid_type( 'fields/field', $field_type );
		}
		/*
		 *  Get valid sunrise field types
		 *
		 * TODO: Need to properly document this
		 */
		static function get_valid_types() {
			return Sunrise::get_valid_types( 'fields/field' );
		}
		static function load_raw_value( $field, $args = array() ) {
			$args['storage_class'] = Sunrise_Storage::get_class_for( $field->storage_type );
			$args['storage_key'] = Sunrise_Fields::get_storage_key( $field, $args );
			if ( isset( $args['sub_key'] ) ) {
				$args['storage_key'] .= "[{$args['sub_key']}]";
			}
			return Sunrise::call_instance_method( $args['storage_class'], 'load_raw_value', $field, $args );
		}
		static function update_raw_value( $field, $value, $args = array() ) {
			$args['storage_class'] = Sunrise_Storage::get_class_for( $field->storage_type );
			$args['storage_key'] = Sunrise_Fields::get_storage_key( $field, $args );
			if ( isset( $args['sub_key'] ) ) {
				$args['storage_key'] .= "[{$args['sub_key']}]";
			}
			$args['value'] = $value;  // This is needed here in some cases. TODO: Need to document those cases
			return Sunrise::call_instance_method( $args['storage_class'], 'update_raw_value', $field, $value, $args );
		}
		/*
		 * TODO: This has not yet been tested
		 */
		static function delete_raw_value( $field, $args = array() ) {
			$args['storage_class'] = Sunrise_Storage::get_class_for( $field->storage_type );
			$args['storage_key'] = Sunrise_Fields::get_storage_key( $field, $args );
			if ( isset( $args['sub_key'] ) ) {
				$args['storage_key'] .= "[{$args['sub_key']}]";
			}
			return Sunrise::call_instance_method( $args['storage_class'], 'delete_raw_value', $field, $args );
		}
	}
	Sunrise_Fields::on_load();
}

