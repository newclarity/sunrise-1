jQuery(function($){

	var instantiateTinyMCE = function(){
		var richtextSettings = $.parseJSON( $("<div/>").html( sunriseFieldsRichText.settings ).text() );
		$('textarea.richtext').each(function(){
			var id = $(this).attr('id');
			if( undefined !== tinymce ){
				var ed = tinymce.getInstanceById(id);
				if( undefined === ed ){
					//console.log('Editor undefined: ' + id);
					ed = new tinymce.Editor(id, richtextSettings );
					//console.log( ed );
					ed.render();
				} else {
					//console.log('Editor defined: ' + id);
				}
			}
		});
	}

	$.sunrise.addAction( 'preInsertRepeatingField', function( item ){
		instantiateTinyMCE();
	} );

	$.sunrise.addAction( 'ajaxFieldLoad', function(){
		instantiateTinyMCE();
	} );

});