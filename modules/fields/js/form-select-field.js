(function( $ ){

	$.fn.formSelect = function(){

		return this.each( function(){

			var fieldId = $(this).attr('id');
            var fieldName = $(this).attr('name');
            var $formContainer = $('#' + fieldId + '-entry .form-select-form');
            var $selectField = $('#' + fieldId);
            var $fieldWrapper = $('#' + fieldId + '-entry');
            var imageDropdown = false;

            if( $selectField.hasClass('image_dropdown') ){
                imageDropdown = true;
            }

            // Check if we need to create an image dropdown or not
            if( imageDropdown ){

                var $hiddenOverlay = $('.hiddenOverlay', $fieldWrapper);

                // Create hidden overlay for select field
                var hiddenOverlay = $('<div>').addClass('hiddenOverlay');

                // Set unalterable CSS for overlay
				$(hiddenOverlay).css({
                    'position'  :   'absolute',
                    'top'       :   0,
                    'left'      :   0,
                    'width'     :   $selectField.outerWidth(),
                    'height'    :   $selectField.outerHeight(),
                    'zindex'    :   100
                });

                // Make select field relative and add overlay to the DOM
                $selectField.css({'position':'relative'}).after(hiddenOverlay);

                // Create document fragment
                var options = document.createDocumentFragment();

                // Create container div
                var container = document.createElement('div');
                $(container).addClass('form-select-options').addClass('hidden');

                // Add container div to document fragment
                options.appendChild(container);

                // Add divs to container div
                $.each( $('#' + fieldId + ' option'), function(index, element){
                    var option = document.createElement('div');
                    $(option).addClass( element.value );
                    var inset = document.createElement('span');
                    $(inset).addClass('inset');
                    option.appendChild(inset);
                    container.appendChild( option );
                });

                // Append document fragment to select field
                $('#' + fieldId).after(options);

                $('div', container).click(function(){
                    // Remove all previous active stati
                    $('.inset').removeClass('active');
                    // Give the current div active status
                    $('.inset', this).addClass('active');
                    // Update select field with our selected element and force an onchange event
                    $selectField.val( $(this).attr('class') ).change();
                    // Hide dropdown
                    $(container).addClass('hidden');
                });

				$selectField.hover(function(){
					$(hiddenOverlay).css({
						'position'  :   'absolute',
						'top'       :   0,
						'left'      :   0,
						'width'     :   $selectField.outerWidth(),
						'height'    :   $selectField.outerHeight(),
						'zindex'    :   100
					});
				});

                // Create a click function for our pseudo-select
                $hiddenOverlay.live('click focusout', function(e){
                    if( e.type == 'click' ){
                        $(container).toggleClass('hidden');
                    } else if( e.type == 'focusout' ){
                        window.setTimeout( function(){
                            $(container).addClass('hidden');
                        }, 200);
                    }
                });

                // Add hover event for our options
                $('.inset', container).hover( function(){
                    $(this).addClass('current');
                }, function(){
                    $(this).removeClass('current');
                });

                // Make sure the currently active option has the correct class
                $('.form-select-options .' + $('#' + fieldId + ' option:selected').val() + ' .inset').addClass('active');

            }

			$(this).change(function(){
				var data = {
					action:          'form_select_field::load_form',
					form_name:       $('option:selected', this).val(),
					field_id:		 fieldId,
					field_name:      fieldName,
					object_id:       $('#object_id').val(),
					object_type:     $('#object_type').val(),
					object_sub_type: $('#object_sub_type').val()
				};

				// TODO: ( Micah 2012-03-23 ) We need to allow easy access to this from any admin page!
				if( 'microsite-page' == data.object_sub_type ) {
					var microsite = {
						post_id : $('input[name="microsite_post_id"]').val(),
						post_type : $('input[name="microsite_post_type"]').val(),
						page_type : $('input[name="microsite_page_type"]').val(),
						page_key : $('input[name="microsite_page_key"]').val(),
						page_template_name : $('input[name="microsite_page_template_name"]').val(),
						page_title : $('input[name="microsite_page_title"]').val(),
						page_status : $('input[name="microsite_post_status"]').val(),
						page_post_id : $('input[name="microsite_page_post_id"]').val()
					};
					data = $.extend( data, microsite );
				}

				$.post( ajaxurl, data,
					function(response){
                        if ('success' == response.status) {
                            // Display the form (after decoding html)
                            $formContainer.html( $("<div/>").html( response.html ).text() );
							$.sunrise.doAction( 'ajaxFieldLoad' );
							$( $formContainer ).trigger( 'sunrise.ajaxFieldLoad' );
                        } else if ('error' == response.status) {
                            // Alert the user of an error
                            alert( response.msg );
                        }
					},
                    'json'
				);
			} );

		});
	}
})( jQuery );

jQuery(document).ready( function($){

    $('select.form_select').formSelect();

} );
