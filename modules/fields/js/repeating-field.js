jQuery(document).ready(function($){

	$(".field-row.repeating a.delete-field").live( 'click', function() {
		getFieldState(this).removeEntry();
		return false;
	});
	$(".field-row.repeating a.add-field").live( 'click', function() {
		var listId = $(this).parents(".entry-feature-inner").attr("id");  // add-bar-admissions-link
		var fieldId = listId.replace("-entry","");                        // bar-admissions
		var fieldName = fieldId.replace("-","_");                         // bar_admissions
		var nonce = $('#nonce-add-repeating-' + fieldId).val();           //
		var fieldCount = $("#"+listId+" .entry-feature-inner.top-level").length; // {n}
		var templateName = fieldName + "\\\:template";                    // bar_admissions\:template
		var objectType = $("#object_type").val();                         //
		var objectSubType = $("#object_sub_type").val();                  //
		var templateItem = $('#'+fieldId+"-template-entry");              //
		var indentable = templateItem.parents(".entry-feature-list.indentable");  // If this an indentable repeating field?
		var newItem = fixupTemplate( templateItem.clone(), fieldCount, indentable );
		newItem.css("visibility","visible");
		$.sunrise.doAction( 'preInsertRepeatingField', newItem );
		templateItem.before(newItem);
		$( 'input, textarea, select', newItem ).trigger('sunrise.postInsertRepeatingField');
		return false;
	});
	$(".entry-feature-list.indentable").each(function(index,element){
		var fieldId = $(element).attr("id").match(/(.+)-list/)[1];
		adjustWidths(fieldId,1);
	});
	$('.field-row.repeating a.indent-field').live('click', function() {
		getFieldState(this).indentField();
		return false;
	});
	$('.field-row.repeating a.outdent-field').live('click', function() {
		getFieldState(this).outdentField();
		return false;
	});
	/*
	 * Capture information about current control, field, and related elements into a fieldState object
	 */
	function getFieldState(control) {
		this.construct = function(control) {
			this.control = $(control);                                        //
			this.controlId = this.control.attr("id");                         // bar-admissions-{n}-{controlType}
			this.controlList = this.control.parents(".controls");             //
			this.controlListId = this.controlList.attr("id");                 // bar-admissions-{n}-controls
			this.entryId = this.controlListId.replace("-controls","");        // bar-admissions-{n}
			this.controlType = this.controlId.replace(this.entryId+"-","");   // {controlType}
			this.controlNonce = $("#"+this.controlId+"-nonce").val();         //
			this.entry = $("input#"+this.entryId);                            // TODO: Verify this works with SELECT, etc.
			this.entryContainerId = this.entryId+"-entry";                    // bar-admissions-{n}-entry
			this.entryContainer = $("#"+this.entryContainerId);               //
			this.fieldRow = this.control.parents(".field-row.repeating");     //
			this.fieldRowId = this.fieldRow.attr("id");                       // bar-admissions-field-row
			this.fieldId = this.fieldRowId.replace("-field-row","");          // bar-admissions
			this.entryIndex = this.entryId.substr(this.fieldId.length+1);     // {n} or {n}-{m}
			this.indexer = "["+this.entryIndex.replace("-","][")+"]";         // [{n}] or [{n}][{m}]
			this.entryName = this.fieldId+this.indexer;                       // bar-admissions[{n}]
		};
		/*
		 * Remove an entry based on entryId.
		 */
		this.removeEntry = function(entryId) {
			if (undefined==entryId) entryId = this.entryId;
			var replaceIndex = this.getEntryIndex(entryId);
			$("#"+this.entryContainerId).slideUp("slow",function(){ $(this).remove(); });
			var findIndex = this.getNextEntryIndex();
			this.adjustIndentEntries(findIndex,replaceIndex);
		};
		/*
		 * Make sure only entries with 'indent-level-0' class have 'top-level' class.
		 */
		this.adjustTopLevel = function() {
			$(".field-row.repeating .entry-feature-list.indentable .entry-feature-inner").each(function(index,element){
				element = $(element);
				if (element.hasClass("indent-level-0")) {
					if (!element.hasClass("top-level")) {
						element.addClass("top-level")
					}
				} else {
					element.removeClass("top-level");
				}
			});
		};
		/*
		 * Take a field id and repeating index and indent the specific subfield's container
		 */
		this.indentField = function() {
			var prevIndex = this.getPrevEntryIndex();
			var thisIndex = this.getEntryIndex(this.entryId);
			var findIndex = this.getNextEntryIndex();
			if (!findIndex) findIndex = thisIndex;
			var replaceIndex = this.getIndexForIndent();
			var thisState = this.getEntryObject(thisIndex);
			var replaceState = this.getEntryObject(replaceIndex);
			this.showIndent();
			changeNameAndId(thisState,replaceState);
			// If there are any following entries that are newly on peer, adjust them
			if (findIndex.length==replaceIndex.length) {
				replaceIndex[replaceIndex.length-1]++;
				this.adjustIndentEntries(findIndex,replaceIndex);
			}
			// Now adjust
			findIndex = thisIndex.slice(0);
			replaceIndex = thisIndex.slice(0);
			findIndex[findIndex.length-1]++;
			this.adjustIndentEntries(findIndex,replaceIndex);
			this.adjustTopLevel();
			return false;
		};
		/*
		 * Used during indent, given an index to find and an index to replace with,
		 * traverses forward and down and adjusts any entries indexes.
		 */
		this.adjustIndentEntries = function(findIndex,replaceIndex) {
			var findEntry,replaceEntry,childFindIndex,childReplaceIndex;
			var i = 0;
			while (i++ < 100) {  // Test for i < 100 is a failsafe to avoid browser lockup
				findEntry = this.getEntryObject(findIndex);
				if ( ! findEntry.entryExists() ) break;
				replaceEntry = this.getEntryObject(replaceIndex);
				changeNameAndId(findEntry,replaceEntry);
				childFindIndex = findIndex.concat([1]);
				childReplaceIndex = replaceIndex.concat([1]);
				this.adjustIndentEntries(childFindIndex,childReplaceIndex);
				findIndex = this.getNextIndex(findIndex);
				replaceIndex = this.getNextIndex(replaceIndex);
			}
		};
		/*
		 * Take a field id and repeating index and outdent the specific subfield's container
		 */
		this.outdentField = function() {
			this.hideIndent();
			var thisIndex = this.getEntryIndex(this.entryId);
			var thisState = this.getEntryObject(thisIndex);
			var outdentIndex = this.getIndexForOutdent(thisIndex);
			var findIndex = this.getLastIndex(outdentIndex);
			var replaceIndex = findIndex.slice(0);
			replaceIndex[replaceIndex.length-1]++;
			this.adjustOutdentEntries(findIndex,replaceIndex,outdentIndex[outdentIndex.length-1]);

			var replaceEntry = this.getEntryObject(outdentIndex);
			changeNameAndId(thisState,replaceEntry);

			var lastIndex = this.getLastIndex(thisIndex);
			if (!this.indexesAreEqual(thisIndex,lastIndex)) {
				findIndex = lastIndex.slice(0);
				var indexDelta = lastIndex[lastIndex.length-1]-thisIndex[thisIndex.length-1];
				replaceIndex = thisIndex.slice(0);
				replaceIndex[replaceIndex.length-2]++;
				replaceIndex[replaceIndex.length-1]= indexDelta;
				this.adjustOutdentEntries(findIndex,replaceIndex);
			}
			this.adjustTopLevel();
		};
		/*
		 * Used during outdent, given an index to find and an index to replace with,
		 * traverses forward and down and adjusts any entries indexes.
		 */
		this.adjustOutdentEntries = function(findIndex,replaceIndex,limit) {
			var findEntry,replaceEntry,childFindIndex,childReplaceIndex;
			if (undefined==limit) limit = 0;
			var i = 0;  // Test for i < 100 is a failsafe to avoid browser lockup
			while (limit<replaceIndex[replaceIndex.length-1] && i++<100) {
				findEntry = this.getEntryObject(findIndex);
				if ( ! findEntry.entryExists() ) break;
				replaceEntry = this.getEntryObject(replaceIndex);
				changeNameAndId(findEntry,replaceEntry);
				childFindIndex = findIndex.concat([1]);
				childReplaceIndex = replaceIndex.concat([1]);
				this.adjustIndentEntries(childFindIndex,childReplaceIndex);
				findIndex = this.getPrevIndex(findIndex);
				replaceIndex = this.getPrevIndex(replaceIndex);
			}
		};
		/*
		 * For Outdent, finds the last possible Index at the current indent level.
		 */
		this.getLastIndex = function(tryIndex) {
			var lastIndex;
			var i = 0;
			tryIndex = tryIndex.slice(0);
			while (i++<100) {  // Test for i < 1000 is a failsafe to avoid browser lockup
				lastIndex = tryIndex.slice(0);  // clone the 'try' index
				tryIndex[tryIndex.length-1]++;  // Looking for a *more* last entry
				var lastEntry = this.getEntryObject(tryIndex);
				if ( ! lastEntry.entryExists() ) break;
			}
			return lastIndex;
		};
		/*
		 * Returns the index for the current element to be used after the element is outdented.
		 */
		this.getIndexForOutdent = function(thisIndex) {
			thisIndex = thisIndex.slice(0);
			thisIndex.pop();
			thisIndex[thisIndex.length-1]++;
			return thisIndex;
		};
		/*
		 *
		 */
		this.indexesAreEqual = function(index1,index2) {
			var areEqual = true;
			if (index1.length!=index2.length) {
				areEqual = false;
			} else {
				for(i=0; i<index1.length; i++) {
					if (index1[i]!=index2[i]) {
						areEqual = false;
						break;
					}
				}
			}
			return areEqual;
		};
		/*
		 * Gets the indent level by looking at entryName, i.e.
		 *
		 * Examples:
		 *
		 *  2 = foo[2][1][1]
		 *  0 = bar[3]
		 *  1 = baz[3][2]
		 *  1 = foo[2][caption][1]
		 *  2 = foo[2][caption][1][2]
		 *  1 = foo[2][1][caption]
		 *  2 = foo[caption][1][2]
		 *  ? = foo[2][1][caption][1][2]
		 *
		 */
		function getIndentLevel(state) {
			var level = 0;
			var parts = state.entryName.replace(/]$/,"").split("][");
			for(var i=1; i<parts.length; i++) {
				if (!isNaN(parts[i])) {
					level++;
				}
			}
            return level;
		}
		this.getPrevEntry = function() {
			return this.entryContainer.prevAll(".entry-feature-inner").first();
		};
		this.getNextEntry = function() {
			return this.entryContainer.nextAll(".entry-feature-inner").first();
		};
		this.getPrevEntryIndex = function() {
			var entryId = this.getPrevEntry().attr("id");     // 'bar-admissions-2-1-entry'
			return this.getEntryIndex(entryId);
		};
		this.getNextEntryIndex = function() {
			var entryId = this.getNextEntry().attr("id");     // 'bar-admissions-2-1-entry'
			return this.getEntryIndex(entryId);
		};
		this.getEntryIndex = function(entryId) {
			var index = entryId.replace(this.fieldId+"-","").replace("-entry","").split("-"); // Convert to array, i.e. [2,1]
			for(var i = 0; i < index.length; i++) {
				index[i] = parseInt(index[i]);          // Make sure they are numbers and not strings
				if (isNaN(index[i])) {
					index = false;
					break;
				}
			}
			return index;
		};
		/*
		 * Returns the index for the current element to be used after the element is indented.
		 */
		this.getIndexForIndent = function() {
			var prevIndex = this.getPrevEntryIndex();
			var thisIndex = this.getEntryIndex(this.entryId);
			var nextIndex = this.getNextEntryIndex();
			var replaceIndex;
			if (!nextIndex) { // We are at very last entry in the list
				replaceIndex = prevIndex.slice(0);        // clone
				if (thisIndex.length==prevIndex.length) {
					replaceIndex.push(1);
				} else {  // Previous was already indented
					replaceIndex[replaceIndex.length-1]++;
				}
			} else if (thisIndex.length==prevIndex.length) { // If they were peers
				replaceIndex = thisIndex.slice(0);        // clone
				replaceIndex[replaceIndex.length-1]--;    // Decrement the last instead
				replaceIndex.push(1);                     // If previous entryId is same indent, add 2nd dim starting at 1.
			} else {
				replaceIndex = prevIndex.slice(0);        // clone
				replaceIndex[replaceIndex.length-1]++;    // Increment the 2nd dimension
			}
			return replaceIndex;
		};
		/*
		 * Take an index and return an entry object containing:
		 *
		 *    entryId
		 *    entryName
		 *    entryContainerId
		 *    entryContainer  (only when for existing entryContainerId)
		 *
		 * Parameter: index
		 *
		 *    int, or array of int.
		 *    Used to assign Id or Name.
		 *      For fieldId='foo' and index=1:
		 *        entryId   = 'foo-1'
		 *        entryName = 'foo[1]'
		 *      For fieldId='foo' and index=[2,3]:
		 *        entryId   = 'foo-2-3'
		 *        entryName = 'foo[2][3]'
		 *
		 *  Note this object is a subset of a FieldState object, i.e. it has the same properties.
		 *  It is designed to be passed to the changeNameAndId() function.
		 */
		this.getEntryObject = function(index) {
			var entry = {};
			if (!(index instanceof Array)) {
				index = [index];
			}
			entry.entryId = this.fieldId+"-"+index[0];
			entry.entryName = underscorize(this.fieldId)+"["+index[0]+"]";
			var i= 1;
			while (i<index.length) {
				entry.entryId += "-"+index[i];
				entry.entryName += "["+index[i]+"]";
				i++;
			}
			entry.entryContainerId = entry.entryId+"-entry";
			entry.entryContainer = $("#"+entry.entryContainerId);
			entry.entryExists = function () {
				return 0 != this.entryContainer.length;
			};
			return entry;
		};
		this.showIndent = function() {
			this.hideControl("indent");
//			this.entry.width( this.entry.width() - 16 );          // Wop 16px off width of input field
			this.entryContainer.removeClass("indent-level-0");
			this.entryContainer.addClass("indent-level-1");
			adjustWidths(this.fieldId,-1);
			this.showControl("outdent");
		};
		this.hideIndent = function() {
			this.hideControl("outdent");
			this.showControl("indent");
			this.entryContainer.removeClass("indent-level-1");
			this.entryContainer.addClass("indent-level-0");
			adjustWidths(this.fieldId,1);
//			this.entry.width( this.entry.width() + 32 );          // Add back 24px to width of input field
		};
		this.showControl = function(buttonType) {
			var button = this.getControl(buttonType);
			button.removeClass("hidden");
		};
		this.hideControl = function(buttonType) {
			var button = this.getControl(buttonType);
			button.addClass("hidden");
		};
		this.getControl = function(buttonType) {
			return $("#"+this.entryId+"-controls").children("."+buttonType+"-field");
		};
		this.getUpperIndex = function(lowerIndex) {
			var i = 0; // Test for i < 1000 is a failsafe to avoid browser lockup
			while ( $("#"+this.fieldId+"-"+lowerIndex+"-entry").length && i++ < 1000 ) {
				lowerIndex++;
			}
			return lowerIndex;  // After increment lowerIndex should in fact now be the upper index
		};
		/*
		 *  Return the next index given a current array-based index.
		 *  [1] => [2]
		 *  [4,2] => [4,3]
		 *  [2,3,1] => [2,3,2]
		 */
		this.getPrevIndex = function(index) {
			index.push(index.pop()-1);
			return index;
		};
		/*
		 *  Return the next index given a current array-based index.
		 *  [1] => [2]
		 *  [4,2] => [4,3]
		 *  [2,3,1] => [2,3,2]
		 */
		this.getNextIndex = function(index) {
			index.push(index.pop()+1);
			return index;
		};
		/*
		 * Take a container and scan through all ids and names to update them.
		 *
		 * Parameters:
		 *    fieldState = Initial fieldState of the field and its related elements
		 *    replaceState = New attribute values for the field and its related elements
		 *
		 * For example:
		 *
		 *    findState.entryName = 'field_name[{n}]'
		 *    findState.entryId   = 'field_name-{n}'
		 *    replaceState.entryName  = 'field_name[{n+m}]'
		 *    replaceState.replaceId      = 'field_name-{n+m}'
		 *
		 */
    function underscorize(s) {
      while(-1!=s.indexOf("-"))
        s = s.replace("-","_");
      return s;
    }
    function dashize(s) {
      while(-1!=s.indexOf("_"))
        s = s.replace("_","-");
      return s;
    }
		function changeNameAndId(findState,replaceState) {
			var attr;
			findState.entryContainer.attr("id",replaceState.entryContainerId);
			findState.entryContainer.find("*").each(function(index,element){
				element = $(element);
				attr = element.attr("id");
				if ( undefined != attr && 0 != attr.length ) {
					element.attr("id",attr.replace(findState.entryId,replaceState.entryId));
				}
				attr = element.attr("name");
				if ( undefined != attr && 0 != attr.length  ) {
					element.attr("name",attr.replace(findState.entryName,replaceState.entryName));
				}
			});
		}
		this.construct(control);
		return this;
	}
	function fixupTemplate( item, index, indentable ) {
		if (typeof(item)=="object") {
			var suffix  ="-"+index;
			var indexer  ="["+index+"]";
			var attr;
			if ( indentable.length ) {
				indexer += "[0]";
			}
			for (var i = 0; i < item[0].attributes.length; i++) {
				attr = item[0].attributes[i];
				if (attr.specified && attr.value.length>0) {  // If attribute has been specified and is not empty
					switch (attr.name) { // TODD: NOTE: This list of attributes may need to be expanded
						case "class":
							attr.value = attr.value.replace(" template","");  // Delete any template references in the classes
							break;
						case "id":
						case "for":
							// Convert all '-template' references to '-{$index}', i.e. '-3'
							attr.value = attr.value.replace(/-template/g,suffix);
							break;
						case "name":
							// Convert all '-template' references to '[{$index}]', i.e. '[3]'
							// Matches 'foo:template' => foo[n], and 'foo:template[bar]' => foo[n][bar] where [b] is [indexer]
							attr.value = attr.value.replace(/(:template$|:template(\[))/g,indexer+'$2');
							break;
						case "href":    // TODO: DO WE NEED THIS?  OLD CODE HAD IT BUT I'M NOT SURE IT'S NEEDED.
							if (attr.name!="href" || attr.value[0]=="#") {  // Filter out non-local hrefs, just in case
								attr.value = attr.value.replace(" template","");  // Delete any template references in the classes
							}
							break;
						default:
							// Move along; nothing to see here, nothing to see.
							continue;
					}
					//TODO: Remove these three (3) if checks after testing
					if ( attr.value.indexOf("[template]") != -1 ) {
						alert( "Unhandled attribute value ["+attr.value+"] for attribute ["+attr.name+"]." );
					}
					if (attr.value.indexOf("-")+1==attr.value.length) {
						alert( "Unhandled trailing dash in value for attribute "+attr.name+" = ["+attr.value+"]." );
					}
					if (attr.value.indexOf("--")!=-1) {
						alert( "Unhandled duplicate dash in value for attribute "+attr.name+" = ["+attr.value+"]." );
					}
					//TODO: Remove this next line after testing
					//attr.value = attr.value.replace(/\-+/g,"-").replace(/^(.*)-$/,"\$1"); // Get rid of repeated and trailing dashes
					attr.value = attr.value.replace(/^\s*/,"").replace(/\s*$/,"") // Strip leading and trailing spaces
					$(item[0]).attr(attr.name,attr.value);  // Setting item value directly can cause problems in some cases.
				}
			}
			// Now drill down and do the same for all children elements
			item.children().each(function(i,element) {
				fixupTemplate($(element),index,indentable);
			});
		}
		return item;
	}
  function adjustWidths(fieldId,sign) {
    var firstWidth = $("#"+fieldId+"-1").outerWidth(true);
    var bw="",pw="";
    $("#"+fieldId+"-list .entry-feature-inner:not(.template)").each( function(index,element) {
      element = $(element);
      var indentLevelMatch = element.attr("class").match(/indent-level-(\d+)/);
      var indentLevel = null != indentLevelMatch && 1 <= indentLevelMatch.length ? indentLevelMatch[1] : 0;
      var input = element.find(":input");
      var marginWidth = getExtraWidth(input,["margin"]);
      var paddingWidth = getExtraWidth(input,["padding"]);
      input.width(firstWidth-marginWidth-indentLevel*(18+paddingWidth));
      element.css("visibility","visible");
    });
  }
  function getExtraWidth(element,properties) {
    if (undefined==properties) {
      properties = ["border","margin","padding"];
    }
    var alignments = ["left","right"];
    var extraWidth = 0;
    for(var a=0;a<alignments.length;a++) {
      for(var p=0;p<properties.length;p++) {
        var value = parseInt(element.css(properties[p]+"-"+alignments[a]));
        if (!isNaN(value)) {
          extraWidth += value;
        }
      }
    }
    return extraWidth;
  }
});