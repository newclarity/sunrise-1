/*
 * TODO: This entire file needs to be rewritten to clean it up (mikes)
 */
jQuery(document).ready(function($) {
  $.sunrise.fields.file = {
    name: 'sunrise.fields.file',
    parent: $.sunrise.fields,
    getInstance: function($object) {//
      var parent = this.parent.getInstance($object);
      return $.extend({},parent,{
        namespace: $.sunrise.fields.file,
        parent: parent,
        getAjaxData: function() {
            return $.extend({},this.parent.getAjaxData(),{});
        },
        getSpinner: function() {
            return $(this.getFieldSelector()+" .spinner");
        },
        showSpinner: function() {
            return this.getSpinner().removeClass("hidden");
        },
        hideSpinner: function() {
            return this.getSpinner().addClass("hidden");
        }
      });
    }
  }
});
jQuery(document).ready(function($) {
	if ($.browser.msie && $.browser.version < 9) {
		// TODO (mikes 2012-01-22): Remove this if statement after moving to WordPress 3.3. jQuery should solve this problem.
		// TODO: See: http://stackoverflow.com/a/7269082/102699
		$('.field-row input:file').live('focus', function() {
			var el = $(this);
			setTimeout( function() {
				el.blur();
			}, 500 );
		});
	}
	$('.field-row input:file').live('change', function() {
		uploadFile( $(this) );
	});

	function uploadFile( $this ) {
    var field_id = $this.attr('id'),
    field_name = $this.attr('name'),
    field_type = $('#' + field_id + '-field-type').val(),
    object_type = $('#object_type').val(),
    object_sub_type = $('#object_sub_type').val(),
    object_id = $('#object_id').val(),
    ajaxData = {
      action         : 'sunrise_fields_upload_file',
      field_id       : field_id,
      field_name     : field_name,
      field_type     : field_type,
      object_type    : object_type,
      object_sub_type: object_sub_type,
      object_id      : object_id,
      _wpnonce       : $('#' + field_id + '-upload-nonce').val()
    },
    fileOk = true;

    $this.siblings('.message').remove(); // Remove previous error message if present


    /*
     * Check file size before upload
     * Supported by modern browser only.
     * See more: http://www.html5rocks.com/en/tutorials/file/dndfiles/
     * TODO: Implement the filesize check from original Rainmaker code.
     *
     */
    var maxSize = $('#' + field_id + '-max-size').val();
    /*
     * Check browser support and only when maxSize > 0 (i.e. field has file size limit or its dimensions are in pixels
     */
    if (window.FileList && window.File && maxSize) {
      if (!$this[0].files.length) { // No file selected
        $('<span class="message error">ERROR: No file selected.</span>').insertAfter($this);
        fileOk = false;
      } else if ($this[0].files[0].size > maxSize) {
        $('<span class="message error">ERROR: The file you are attempting to upload is too large in bytes.</span>').insertAfter($this);
        $this.val('');
        fileOk = false;
      }
    }
    /*
     * If file size is too big, don't upload it
     */
    if (!fileOk)
      return;

    var spinner = $this.prevAll(".spinner");
    spinner.removeClass("hidden");

    ajaxData = $.sunrise.applyFilters('onPreFileUpload', ajaxData, {target:$this});
    $this.upload(
        ajaxurl,
        $.param(ajaxData),
        function(response) {

          // Trigger events for hooking into
          $.sunrise.doAction('onFileUpload', {response:response,ajaxData:ajaxData,target:$this});
          $this.trigger('onFileUpload.sunriseFileField', response, ajaxData );

          $this.val('');
          if ('success' == response.status) {
            $this.addClass('hidden'); // Hide the upload input box if upload is successful
          }
          response.html = response.html.replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&#039;/g, "'"); // Decode HTML entities
          $(response.html).insertAfter($this);     // Show error message or HTML markup

          // Trigger events for hooking into
          $.sunrise.doAction('afterFileUpload', {response:response,ajaxData:ajaxData,target:$this});
          $this.trigger('afterFileUpload.sunriseFileField', response, ajaxData );

          spinner.addClass("hidden");
        },
        'json'
    );
  }

	function initFileUploadDropzones () {

		if ( ! $('body').hasClass('js-supports-files-drag-drop') ) {
			return;
		}

		var $inputElements = $('.field-row.file .entry-feature-inner input[type="file"], .field-row.image-file .entry-feature-inner input[type="file"]');
		$inputElements.each(function () {

			var $parentWrapper = $(this).closest('.entry-feature-inner');
			var hasFile = $('.uploaded-file:parent', $parentWrapper).length > 0;

			if ( hasFile ) {
				$parentWrapper.addClass('has-uploaded-file');
			} else {
				$parentWrapper.removeClass('has-uploaded-file');
			}

			if( ! $('.js-file-upload-faux-dropzone', $parentWrapper).length ) {
				$('<div class="js-file-upload-faux-dropzone"></div>').insertBefore($(this));
			}

		});

	}

  // Ajax delete file, use delegate to handle uploaded files in the future
  $('.fields-wrapper').delegate('.sunrise-fields-delete-file-button', 'click', function() {
    var $this = $(this);
    var field_id = $this.parents(".entry-feature-inner").attr('id').replace('-entry', '');
    var $input = $('#' + field_id);
    var field_name = $input.attr("name");
    var ajaxData = {
      action         : 'sunrise_fields_delete_file',
      field_id       : field_id,
      field_name     : field_name,
      field_type     : $('#' + field_id + '-field-type').val(),
      object_type    : $('#object_type').val(),
      object_sub_type: $('#object_sub_type').val(),
      object_id      : $('#object_id').val(),
      _wpnonce       : $('#' + field_id + '-delete-nonce').val()
    };
    var parent = $this.parent();
    var spinner = parent.prevAll(".spinner");
    spinner.removeClass("hidden");
    ajaxData = $.sunrise.applyFilters('onPreFileDelete', ajaxData, {target:$this});
    var defaultAction = true;
		parent.fadeOut().remove();         // Remove the HTML element of uploaded file
    $.post(ajaxurl, ajaxData, function(response) {
      var args = {response:response,ajaxData:ajaxData,target:$this}
      $.sunrise.doAction('onFileDeleteResponse', args);
      if ('success' == response.status) {            // If delete action is successful

        $input.removeClass('hidden'); // Show the upload input box
        defaultAction = $.sunrise.applyFilters('onFileDeleteSuccess', defaultAction, args);
        $input.trigger('onFileDeleteSuccess.sunriseFileField', response, ajaxData);
      } else if ('error' == response.status) {                                       // If an error occurred
        response.html = response.html.replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&#039;/g, "'"); // Decode HTML entities
        $($this).append(response.html);                                       // Show error message
        defaultAction = $.sunrise.applyFilters('onFileDeleteError', defaultAction, args);
      }
      spinner.addClass("hidden");
    }, 'json');
    return !defaultAction;
  });

	/**
	 * Determine file support (i.e., drag and drop)
	 * TODO: (MHS 2013-11-25) Is this the most surefire way to determine drag and drop support? Does the existence of window.FileReader necessarily indicate drag-n-drop support?
	 */
	var jsFileDragDropSupported = false;
	try {
		if ( window.File && window.FileList && window.FileReader ) {
			jsFileDragDropSupported = true;
		}
	}
	catch ( err ) {
	}

	if ( jsFileDragDropSupported ) {
		$('body').addClass('js-supports-files js-supports-files-drag-drop');
	}

	initFileUploadDropzones();

	// Initialize drop-zones after potentially adding new file inputs via a select change
	$('.form-select-form').live('sunrise.ajaxFieldLoad ', function () {
		initFileUploadDropzones();
	});

	// Initialize drop-zones after potentially adding new file inputs via repeating field addition
	$('.field-row.file .entry-feature-inner input[type="file"]').live('sunrise.postInsertRepeatingField', function () {
		initFileUploadDropzones();
	});

	// Update field/dropzpone UI after deleting a file
	$('.field-row.file .entry-feature-inner input[type="file"], .field-row.image-file .entry-feature-inner input[type="file"]').live('onFileDeleteSuccess.sunriseFileField', function ( e ) {
		var $senderParentWrapper = $(e.currentTarget).closest('.entry-feature-inner');
		$senderParentWrapper.removeClass('has-uploaded-file');
	});

	// Update field/dropzpone UI after uploading a file
	$('.field-row.file .entry-feature-inner input[type="file"], .field-row.image-file .entry-feature-inner input[type="file"]').live('afterFileUpload.sunriseFileField', function ( e, response ) {
		if ( response.status == 'error' ) {
			return;
		}
		var $senderParentWrapper = $(e.currentTarget).closest('.entry-feature-inner');
		$senderParentWrapper.addClass('has-uploaded-file');
	});

});