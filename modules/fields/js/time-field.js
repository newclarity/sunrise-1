jQuery(document).ready(function($) {
	function check_format(field_id, time, ampm) {
		var $field = $('#' + field_id),
			time_value = time + ampm,
			nonce = $('#nonce-' + field_id).val(),
			data = {
				action: 'sunrise_fields_check_time_format',
				field_id: field_id,
				field_name: $field.attr('name'),
				object_id: $('#object_id').val(),
				object_type: $('#object_type').val(),
				object_sub_type: $('#object_sub_type').val(),
				time: time_value,
				_wpnonce: nonce
			};

		/*
		 * Remove all previous messages
		 */
		$field.siblings('.message').remove();

		/*
		 * Check time format via Ajax
		 */
		$.post(ajaxurl, data, function(response) {
			if ('error' === response.status) {
				/*
				 * Decode HTML entities and show error message
				 */
				response.html = response.html.replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&#039;/g, "'");
				$($field).parent().append($(response.html));
			}
		}, 'json');
	}

	/*
	 * When time is changed
	 */
	$('.time').blur(function() {
		var $this = $(this),
			time = $this.val(),
			field_id = $this.attr('name').replace(/_time$/, ''),
			ampm = $('select[name=' + field_id + '_ampm]').val();

		/*
		 * Append ':00' if time is number only
		 */
		if ( time && !(/:/.test(time)) ) {
			time += ':00';
			$this.val(time);
		}

		check_format(field_id, time, ampm);

		return false;
	});

	/*
	 * When AM-PM is changed
	 */
	$('.ampm').change(function() {
		var $this = $(this),
			  ampm = $this.val(),
			  field_name = $this.attr('name').replace(/_ampm$/, ''),
			  time = $('input[name=' + field_name + ']').val();
		check_format(field_name, time, ampm);

		return false;
	});
});