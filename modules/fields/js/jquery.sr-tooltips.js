(function ($) {

	$.fn.srTooltips = function (options) {

		var settings = {
			className:'srTooltip',
			content:'This is a tooltip. Use the options to configure the content.',
			verticalPosition: 'top', // top, bottom, 0%, or 100%
			horizontalPosition: 'center', // center, left, right, 25%, 50%, or 75%
			verticalOffset: 0, // Measurement is in pixels
			callback:function () {
			}
		};

		return this.each(function (index) {

			var $anchor = $(this);

			/**
			 * If options are provided, overwrite the defaults.
			 */
			if (options) {
				if ('object' == typeof( options )) {
					$.extend(settings, options);
				} else if ('string' == typeof( options ) || 'function' == typeof( options )) {
					settings.content = options;
				}
			}

			/**
			 * If content is a function, evaluate and use the value
			 */
			if ('function' == typeof( settings.content )) {
				settings.content = settings.content.call($(this));
			}

			/**
			 * Replace title attribute and related functionality with tooltip.
			 * A numeric index links elements with the correct tooltip.
			 */
			$(this).removeAttr('title').data('srTooltip', index);

			/**
			 * Create and hide the tooltip
			 */
			var $tooltip = $('<div>')
					.addClass(settings.className)
					.attr('data-srTooltip', index)
					.html(settings.content + '<div class="arrow"></div>')
					.hide()
					.appendTo('body');

			/**
			 * Attach events to element
			 */
			$(this).bind({
				'touchstart.srTooltips, mouseenter.srTooltips': function () {

					/**
					 * Get current position of element relative to the document
					 */
					var position = $(this).offset();

					/**
					 * Position the tooltip just before display to ensure it is in the right location
					 */

					var css = {
						'position': 'absolute',
						'top': position.top - $tooltip.outerHeight() - settings.verticalOffset,
						'left': position.left - ( $tooltip.outerWidth() / 2 ) + ( $(this).outerWidth() / 2 )
					};

					if( settings.verticalPosition == 'bottom' || settings.verticalPosition == '100%' ) {
						css.top = position.top + $(this).outerHeight() + settings.verticalOffset;
					}

					if( settings.horizontalPosition == 'left' || settings.horizontalPosition == '25%' ) {
						css.left = position.left - ( ( $tooltip.outerWidth() / 4 ) * 3 ) + ( $(this).outerWidth() / 2 );
					}

					if( settings.horizontalPosition == 'right' || settings.horizontalPosition == '75%' ) {
						css.left = position.left - ( ( $tooltip.outerWidth() / 4 ) ) + ( $(this).outerWidth() / 2 );
					}

					$tooltip.css(css);

					/**
					 * Display the tooltip on hover
					 */
					$('div[data-srTooltip=' + $(this).data('srTooltip') + ']').show();

					/**
					 * Ensure that touch devices properly fade out the tooltips
					 */
					if( ! $('html').hasClass('no-touch') ) {
						$(document).bind({
							'touchstart.srTooltips': function(e) {
								if( ! $( e.target ).is( $anchor ) && ! $( e.target ).is( $tooltip ) ) {
									$anchor.trigger('mouseleave.srTooltips');
									$(document).unbind( 'touchstart.srTooltips' );
								}
							}
						});
					}
				},
				'mouseleave.srTooltips': function () {

					/**
					 * Make the tooltip fade away after hover
					 */
					$('div[data-srTooltip=' + $(this).data('srTooltip') + ']').fadeOut();

					/**
					 * Do callback
					 */
					settings.callback.call($(this));

				}
			});

		});

	};

	$(document).ready( function() {
		// Infobox tooltips
		$('.infobox-container').srTooltips({
			content: function(){
				return $('.infobox-text', this).html();
			}
		} );
	} );

})(jQuery);