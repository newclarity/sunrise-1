(function($){

    $.fn.placeholder = function() {
        return this.each( function() {
			var input = $(this);
			var watermark = input.attr('placeholder');
			if( watermark ) {
				if( input.val().length < 1 ) {
					input.val( watermark ).addClass('placeholder');
				}
				if( input.val() != watermark ) {
					input.addClass('user-input');
				}
				input.bind( {
					blur : function() {
						if( '' == input.val() ) {
							input.val( watermark ).addClass('placeholder').removeClass('user-input');
						}
					},
					focus : function() {
						input.addClass('user-input');
						if( watermark == input.val() ) {
							input.val('').removeClass('placeholder');
						}
					}
				} );
				input.parents('form').bind( {
					submit : function() {
						if( watermark == input.val() ) {
							input.val('');
						}
					}
				} );
			}
		} );
	};

	$(function() {
		$.support.placeholder = false;
		test = document.createElement('input');
		if('placeholder' in test) $.support.placeholder = true;
	});

	$(document).ready(function(){
		if( ! $.support.placeholder ) {
			$('input[placeholder]').placeholder();
		}
	});

})(jQuery);