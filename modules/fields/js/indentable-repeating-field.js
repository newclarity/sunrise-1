(function($){

	$.fn.indentableRepeatingFields = function(){

		function reorder(list) {
		    var node = 0;
		    var index = 0;
		    var id = $(list).attr('id');
		    var parts = id.split('-list');
		    var fieldName = parts[0].replace(/-/g, '_');
		    var nodes = $('#' + id + '> li > div > input.entry-feature');
		    $(nodes).each(function(n) {
		        node = n + 1;
		        var name = fieldName + '[' + node + '][0]';
		        $(this).attr('name', name);
		        var obj = $(this).parent().next().next();
		        var items = $('input.entry-feature', obj);
		        $(items).each(function(i) {
		            index = i + 1;
		            var name = fieldName + '[' + node + '][' + index + '][0]';
		            $(this).attr('name', name);
		        });
		    });
		}

        function setup_actions(element) {
            element.bind(
                {
                    'mouseenter': function(){ $(this).addClass('hover'); $(this).siblings('ol').addClass('children'); },
                    'mouseleave': function(){ $(this).removeClass('hover'); $(this).siblings('ol').removeClass('children'); }
                }
            ).prepend('<span class="handle">&nbsp;</span>').append('<span class="close"></span>');
        }

		return this.each( function(){

			var $field = $(this),
				sortable = $('ol.sortable', this);

			sortable.delegate('span.close', 'click', function(){
				$(this).closest('li').fadeOut(function(){
					$(this).remove();
				});
				reorder( sortable );
			});

            setup_actions($('li div.entry-feature-inner', $field));

			$('a.add-field', $field).click( function() {
				$('<li><div class="entry-feature-inner" style="visibility: visible;"><input type="text" value="" class="entry-feature feature" ></div><div class="clear"></div></li>').appendTo( $(sortable) );
                setup_actions($('li:last div.entry-feature-inner', sortable));
				reorder( sortable );
				return false;
			});

			$.getScript( sunriseIndentableRepeating.filePath, function() {
				sortable.nestedSortable({
					forcePlaceholderSize: true,
					helper:	'clone',
					items: 'li',
					maxLevels: 2,
					opacity: .6,
					placeholder: 'placeholder',
					revert: 250,
					tabSize: 25,
					tolerance: 'move',
					toleranceElement: 'div',
					handle: 'span.handle',
					start: function() {
						$('#normal-sortables').removeClass('ui-sortable');
					},
					stop: function() {
						$('#normal-sortables').addClass('ui-sortable');
						reorder( sortable );
					}
				});
			} );

		} );

	};

	$(document).ready(function(){
		$('.field-row.indentable_repeating').indentableRepeatingFields();
		$('.form-select-form').bind( 'sunrise.ajaxFieldLoad', function(e) {
			$('.field-row.indentable_repeating', e.target).indentableRepeatingFields();
		} );
	});

})(jQuery);