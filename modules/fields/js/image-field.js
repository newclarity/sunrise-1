(function(win, doc, $, undefined){

    $.fn.sunriseImageField = function() {

        return this.each(function() {

            var $input = $(this);
            var field_id = $input.attr('id');
            var field_name = $input.attr('name');
            var field_type = $('#' + field_id + '-field-type').val();

            if( 'image' === field_type && ! $input.data('sunriseImageField.initialized') ) {

                $input.data('sunriseImageField.initialized', true);

                var jcrop_api;
                var $wrap = $('#' + field_id + '-entry');
                var $coordinatesInput = $('.crop-coordinates', $wrap);
                var $spinner = $( '.spinner', $wrap );
                var object_type = $('#object_type').val();
                var object_sub_type = $('#object_sub_type').val();
                var object_id = $('#object_id').val();
                var colorboxOptions = {
                    arrowKey: false,
                    escKey: true,
                    fixed: true,
                    href: '#' + field_id + '-crop-window',
                    inline: true,
                    maxHeight: '90%',
                    maxWidth: 440,
                    opacity: 0.85,
                    open: true,
                    overlayClose: false,
                    scrolling: false,
                    onLoad: function(){
                        $("#cboxClose").hide();
                    },
                    onOpen: function(){
                        // Setup data
                        var actualSize = $( '#' + field_id + '-actual-image-dimensions').val().split('x');
                        var aspectRatio = $( '#' + field_id + '-aspect-ratio').val().split(':');

                        function Dimensions( width, height ) {
                            this.width = parseInt( width, 10 );
                            this.height = parseInt( height, 10 );
                            this.asArray = function() {
                                return [ this.width, this.height ];
                            };
                            this.asDecimal = function() {
                                return this.width / this.height;
                            };
                        }

                        function Coordinates( x, y, x2, y2 ) {
                            this.x = parseInt( x, 10 );
                            this.y = parseInt( y, 10 );
                            this.x2 = parseInt( x2, 10 );
                            this.y2 = parseInt( y2, 10 );
                            this.asArray = function() {
                                return [ this.x, this.y, this.x2, this.y2 ];
                            };
                            this.moveDown = function( px ) {
                                this.y += parseInt( px, 10 );
                                this.y2 += parseInt( px, 10 );
                            };
                            this.moveRight = function( px ) {
                                this.x += parseInt( px, 10 );
                                this.x2 += parseInt( px, 10 );
                            }
                        }

                        var ratio = new Dimensions( aspectRatio[0], aspectRatio[1] );
                        var size = new Dimensions( actualSize[0], actualSize[1] );
                        var crop = new Coordinates( 0, 0, size.width, size.height );

                        if( ratio.asDecimal() < size.asDecimal() ) {
                            var cropWidth = Math.floor( size.height * ratio.asDecimal() );
                            crop.moveRight( Math.floor( ( size.width - cropWidth ) / 2 ) );
                        } else {
                            var cropHeight = Math.floor( size.width / ratio.asDecimal() );
                            crop.moveDown( Math.floor( ( size.height - cropHeight ) / 2 ) );
                        }

                        var cropCoordinates = $coordinatesInput.val() || false ? $coordinatesInput.val().split(',') : crop.asArray();

                        // Enable Jcrop jQuery plugin
                        $('img.actual-image', $wrap).Jcrop({
                            allowSelect: false,
                            keySupport: false,
                            aspectRatio: aspectRatio[0] / aspectRatio[1],
                            boxHeight: ( $(win).outerHeight() * .9 ) - 200,
                            boxWidth: 400,
                            setSelect: cropCoordinates,
                            trueSize: actualSize,
                            onSelect: function(c) {
                                var coordinates = [
                                    Math.round( c.x ),
                                    Math.round( c.y ),
                                    Math.round( c.x2 ),
                                    Math.round( c.y2 )
                                ];
                                $input.data('sunriseImageField.cropCoordinates', coordinates.join() );
                            }
                        }, function() {
                            jcrop_api = this;
                        });
                        $( '#' + field_id + '-crop-button-cancel')
                            .unbind('click.sunriseImageField')
                            .bind('click.sunriseImageField', function(e) {
                                e.preventDefault();
                                $.colorbox.close();
                            });
                        $( '#' + field_id + '-crop-button-save')
                            .unbind('click.sunriseImageField')
                            .bind('click.sunriseImageField', function(e) {
                                e.preventDefault();
                                $coordinatesInput.val( $input.data('sunriseImageField.cropCoordinates') );
                                var c = $coordinatesInput.val().split(',');
                                $spinner.show();
                                $.colorbox.close();
                                $.post(
                                    ajaxurl,
                                    {
                                        action         : 'sunrise_fields_crop',
                                        field_id       : field_id,
                                        field_name     : field_name,
                                        field_type     : field_type,
                                        image_type     : $('#' + field_id + '-image-type').val(),
                                        object_type    : object_type,
                                        object_sub_type: object_sub_type,
                                        object_id      : object_id,
                                        selection      : {
                                            x1: c[0],
                                            y1: c[1],
                                            x2: c[2],
                                            y2: c[3],
                                            width: c[2] - c[0],
                                            height: c[3] - c[1]
                                        },
                                        _wpnonce       : $('#' + field_id + '-crop-nonce').val()
                                    },
                                    function(response) {
                                        switch (response.status) {
                                            case "success":
                                                $('#uploaded-file-' + field_id).replaceWith( $("<div/>").html(response.html).text() );
                                                // Show colorbox when user clicks on the image link
                                                $('a.show-crop-link', $wrap)
                                                    .unbind('click.sunriseImageField')
                                                    .bind('click.sunriseImageField', function(e) {
                                                        e.preventDefault();
                                                        $.colorbox( colorboxOptions );
                                                    });
                                                break;
                                            case "error":
                                                alert( response.message );
                                                break;
                                            default:
                                                alert( "Unexpected and unspecified weirdness. Ask the developer to debug this." );
                                                break;
                                        }
                                        $spinner.hide();
                                    },
                                    'json'
                                );
                            });
                    },
                    onCleanup: function() {
                        jcrop_api.destroy();
                    }
                };

		            $input
			            .data('sunriseImageField.cropCoordinates', false)
			            .bind('afterFileUpload.sunriseFileField.sunriseImageField', function ( e, response ) {
				            if ( 'success' === response.status ) {

					            if( response.hasOwnProperty('omit_cropping') && response.omit_cropping ) {
						            return;
					            }

					            $.colorbox(colorboxOptions);
					            // Show colorbox when user clicks on the image link
					            $('a.show-crop-link', $wrap)
						            .unbind('click.sunriseImageField')
						            .bind('click.sunriseImageField', function ( e ) {
							            e.preventDefault();
							            $.colorbox(colorboxOptions);
						            });
				            }
			            })
			            .bind('onFileDeleteSuccess.sunriseFileField', function ( e, response ) {
				            if ( 'success' === response.status ) {
					            $coordinatesInput.val('');
				            }
			            });

                // Show colorbox when user clicks on the image link
                $('a.show-crop-link', $wrap).bind('click.sunriseImageField', function(e) {
                    e.preventDefault();
                    $.colorbox( colorboxOptions );
                });

            }

        });

    };

    $(doc).ready(function() {

        $('input:file').sunriseImageField();

    });

})( window, document, jQuery );