jQuery(document).ready(function($) {

  var datepickerInit = function( datepickers ) {
    $.each( datepickers, function() {
      var $el = $(this);
      //console.log( $el.get(0) );
      $el.datepicker({
        showButtonPanel: true,
        dateFormat: $( '#datepicker_format-' + $el.attr('id') ).val(),
        defaultDate: $el.val()
      });
    } );
  };

  var datepickers = $('.datepicker').not('.template .datepicker');
  datepickerInit( datepickers );

  $('.repeating .entry-feature-inner')
    .bind('sunrise.postInsertRepeatingField', function() {
      var datepickers = $('.datepicker').not('.template .datepicker').not('.hasDatepicker');
      datepickerInit( datepickers );
    } );

});
