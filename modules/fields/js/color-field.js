jQuery(document).ready(function ($) {
	$('.field-color-picker').each(function () {
		var $this = $(this),
				id = $this.attr('rel');
		if ($('#' + id).val() == "") {
			$('#' + id).val("#ffffff");
		}
		$this.farbtastic('#' + id);
	});

	$('.form-select-form').bind('sunrise.ajaxFieldLoad', function (e) {
		$('.field-color-picker', e.target).each(function () {
			var $this = $(this),
					id = $this.attr('rel');
			if ($('#' + id).val() == "") {
				$('#' + id).val("#ffffff");
			}
			$this.farbtastic('#' + id);
		});
	});
});