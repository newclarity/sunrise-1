jQuery(document).ready(function ( $ ) {

	$.sunrise.fields = {
		name: 'sunrise.fields',
		parent: $.sunrise,
		getInstance: function ( $object ) {
			if ( "string" == typeof $object && "#" != $object[0] ) {
				var fieldRowId = $("#" + $object + "-field-row");
				if ( 1 == fieldRowId.length ) {
					$object = fieldRowId; // We were passed a fieldname, turn into a field row ID
				}
			}
			var parent = this.parent.getInstance($object, "field-row");
			return $.extend({}, parent, {
				namespace: $.sunrise.fields,
				parent: parent,
				getFieldId: function () {
					return this.$object.attr("id");
				},
				getFieldSelector: function () {
					return "#" + this.getFieldId();
				},
				getEntryId: function () {
					return this.getFieldId().replace("-" + this.objectClass, "");
				},
				getEntrySelector: function () {
					return "#" + this.getEntryId();
				},
				getEntryObject: function () {
					return $(this.getEntrySelector());
				},
				getEntryName: function () {
					return this.getEntryObject().attr("name");
				},
				getFieldType: function () {
					return "image";   // TODO: This need to be changed
				},
				getAjaxData: function () {
					return {
						field_id: this.getEntryId(),
						field_name: this.getEntryName(),
						field_type: this.getFieldType(),
						object_id: $.sunrise.getObjectId(),
						object_type: $.sunrise.getObjectType(),
						object_sub_type: $.sunrise.getObjectSubType()
					}
				}
			});
		}
	};

	/**
	 * TODO: ( Micah 2012-11-12 ) Converted watermarks to use jquery.watermark.js - See notes for follow-up details:
	 *
	 * NOTES: After enabling the watermark plugin, there are a few code snippets that may still be required:
	 *
	 *    $.sunrise.addAction( 'onCropWindowClosed', function(){
	 *			initiateWatermarks();
	 *		} );
	 *
	 *    $('[placeholder]').live( 'change', function() {
	 *			// This is required to make datepicker placeholders to work properly
	 * 			if( $(this).val() != $(this).attr('placeholder') ) {
	 *				$(this).removeClass('placeholder');
	 *			}
	 *		});
	 *
	 * Obviously, if any of these snippets are still required, they will need to be adapted.
	 */

		// Enable cleditor Field Instances
	$('textarea.cleditor').live('click', function () {
		$(this).cleditor();
	});
	$.each($('textarea.cleditor'), function () {
		var el = $(this);
		if ( el.is(':visible') ) {
			el.cleditor();
		} else {
			el.bind({
				SunriseAJAXFieldLoad: function () {
					el.cleditor();
				}
			});
		}
	});

	// Setup redactor
	var load_redactor = function () {
		// Call the redactor plugin on all redactor textareas that aren't templates
		$('textarea.redactor').not('.template textarea.redactor').redactor({
			iframe: true,
			minHeight: 300,
			buttons: [
				'formatting', '|', 'bold', 'italic', 'superscript', '|', 'unorderedlist', 'orderedlist', 'outdent', 'indent',
				'|', 'file', 'table', 'link', '|', 'html'
			],
			buttonsCustom: {
				superscript: {
					title: 'Superscript',
					callback: function ( buttonName, buttonDOM, buttonObject ) {
						this.execCommand('superscript');
					}
				},
				subscript: {
					title: 'Subscript',
					callback: function ( buttonName, buttonDOM, buttonObject ) {
						this.execCommand('subscript');
					}
				}
			},
			autosave: false,
			allowedTags: [
				"div", "a", "br", "p", "b", "i", "img", "video", "audio", "object", "embed", "param",
				"blockquote", "mark", "cite", "ul", "ol", "li", "hr", "dl", "dt", "dd", "figure", "figcaption",
				"strong", "em", "table", "tr", "td", "th", "tbody", "thead", "tfoot", "h3", "sup", "sub"
			],
			formattingTags: ['p', 'h3'],
			linkEmail: true,
			langs: {
				en: {
					html: 'HTML',
					video: 'Insert Video',
					image: 'Insert Image',
					table: 'Table',
					link: 'Link',
					link_insert: 'Insert link',
					link_edit: 'Edit link',
					unlink: 'Unlink',
					formatting: 'Formatting',
					paragraph: 'Body',
					quote: 'Quote',
					code: 'Code',
					header1: 'Header 1',
					header2: 'Header 2',
					header3: 'Subhead',
					header4: 'Header 4',
					bold: 'Bold',
					italic: 'Italic',
					fontcolor: 'Font Color',
					backcolor: 'Back Color',
					unorderedlist: 'Unordered List',
					orderedlist: 'Ordered List',
					outdent: 'Outdent',
					indent: 'Indent',
					cancel: 'Cancel',
					insert: 'Insert',
					save: 'Save',
					_delete: 'Delete',
					insert_table: 'Insert Table',
					insert_row_above: 'Add Row Above',
					insert_row_below: 'Add Row Below',
					insert_column_left: 'Add Column Left',
					insert_column_right: 'Add Column Right',
					delete_column: 'Delete Column',
					delete_row: 'Delete Row',
					delete_table: 'Delete Table',
					rows: 'Rows',
					columns: 'Columns',
					add_head: 'Add Head',
					delete_head: 'Delete Head',
					title: 'Title',
					image_position: 'Position',
					none: 'None',
					left: 'Left',
					right: 'Right',
					image_web_link: 'Image Web Link',
					text: 'Text',
					mailto: 'Email',
					web: 'URL',
					video_html_code: 'Video Embed Code',
					file: 'Insert File',
					upload: 'Upload',
					download: 'Download',
					choose: 'Choose',
					or_choose: 'Or choose',
					drop_file_here: 'Drop file here',
					align_left: 'Align text to the left',
					align_center: 'Center text',
					align_right: 'Align text to the right',
					align_justify: 'Justify text',
					horizontalrule: 'Insert Horizontal Rule',
					deleted: 'Deleted',
					anchor: 'Anchor',
					link_new_tab: 'Open link in new tab',
					underline: 'Underline',
					alignment: 'Alignment',
					filename: 'Name (optional)'
				}
			}
		});
	};
	// Load redactor
	load_redactor();

	$('.form-select-form').bind('sunrise.ajaxFieldLoad', function () {
		// Load redactor after an ajax field load
		load_redactor();
		$('.infobox-container').srTooltips({
			content: function () {
				return $('.infobox-text', this).html();
			}
		});
		// Run sunrise image field js after an ajax field load
		$('input:file', $(this)).sunriseImageField();
	});

	// Load redactor when inserting a new repeating field
	$('textarea.redactor').live('sunrise.postInsertRepeatingField', function () {
		load_redactor();
	});

	$('.entry-feature-inner').bind('sunrise.postInsertRepeatingField', function () {
		$('input:file', $(this)).sunriseImageField();
	});

	// Select2
	if ( $.fn.select2 ) {

		$('select.select2')
			.select2({
				placeholder: 'first',
				allowClear: true
			})
			.find('option:first')
			.attr('disabled', 'disabled');

		$('.multi-select select.select2').on('change', function () {
			var self = this;
			if ( $(self).val() ) {
				//$(':selected', self).attr('disabled', 'disabled');
				$('option[value="' + $(':selected', self).val() + '"]', self).attr('disabled', 'disabled');
			}
			setTimeout(function () {
				$(self).select2('val', 0);
			}, 100);
		});

	}

});