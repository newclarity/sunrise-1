( function( $ ){

  $.fn.taxonomyTerms = function(){

    return this.each( function(){

      var elementType = this.tagName;
      var $this = $(this);
      var id = $this.attr('id');
      var name = $this.attr('name');
      var $wrapper = $(this).closest('li');
      var $list = $('ul.sortable', $wrapper);

      // Features specific to the select element
      if( elementType == 'SELECT' ) {

        // Disable options in select field that are already present in our list
        $.each( $('input', $list), function(){
          $('option[value="' + $(this).val() + '"]', $this).attr('disabled', 'disabled');
        });

        // Create an event listener for the onChange event
        $this.change(function(){
            var val = this.value;
			var data            = {
                action         : 'insert_list_element',
                field_id       : id,
                field_name     : name,
                field_type     : $('#' + id + '-field-type').val(),
                object_type    : $('#object_type').val(),
                object_sub_type: $('#object_sub_type').val(),
                object_id      : $('#object_id').val(),
                _wpnonce       : $('#' + id + '-insert-list-element').val()
            };

            if( this.value != 0 ){  // Don't add empty option to our list
                data.term_id = this.value;
                $.post(
                  ajaxurl,
                  data,
                  function( response ){
                    if ('success' == response.status) {
                      // Disable select option
                      $('option:selected', $this).attr('disabled', 'disabled');
                      // Append element to list
                      $list.append( $('<div>').html( response.html ).text() );
                      // Set selector back to default
                      $this.val(0);
                    } else if ('error' == response.status) {
                      // Alert the user of an error
                      alert( response.message );
                    }
                  },
                  'json'
                );
            }
        });
      }

      $list.delegate('span', 'click', function(){
        $(this).closest('li').fadeOut(function(){
          // Enable option in dropdown
          if( elementType == 'SELECT' ){
            $('option[value="' + $('input', this).val() + '"]', $this).removeAttr('disabled');
          }
          // Remove element from list
          $(this).remove();
        });
      });

      // Make list items sortable
      $list.sortable();

    });

  }

} )( jQuery );

jQuery(document).ready( function( $ ){

	$('li.taxonomy_term .multi-select select').taxonomyTerms();

} );
