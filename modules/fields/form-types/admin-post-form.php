<?php
if (!class_exists('Sunrise_Admin_Post_Form')) {
	/**
	 * @property SR_DOCS_Post $post
	 * @property Sunrise_Object $object
	 */
	class Sunrise_Admin_Post_Form extends Sunrise_Form {
		var $metabox = false;
		var $context = false;
		var $priority = false;
		var $meta_key_prefix = false;
		var $view_state = false;

		function __construct( array $args = array() ) {
			$args['object_type'] = 'post';

			if ( isset( $args['post_type'] ) ) {
				$args['object_sub_type'] = $args['post_type'];
				unset( $args['post_type'] );
			}

			$this->view_state = isset( $args['view_state'] ) ? $args['view_state']: 'initially_open';

			$args = wp_parse_args( $args, array(
				'object_sub_type' => 'post',
				'context'         => 'normal',
				'priority'        => 'high',
			));

			parent::__construct( $args );

			if ( 'any' == $this->object_sub_type ) {
				// TODO (mikes 2011-12-19): Need to address how to add to ALL post types here...?
			} else {
				global $wp_post_types;
				$wp_post_types[$this->object_sub_type]->sunrise_args['forms'][$args['form_name']] = &$this;
			}

		}
		function __get( $property_name ) {
			switch ( $property_name ) {
				case 'post':
					$value = $this->object;
					break;

				default:
					$value = parent::__get( $property_name );
					break;
			}
			return $value;
		}
		function __set( $property_name, $value ) {
			switch ( $property_name ) {
				case 'post':
					$this->object = $value;
					break;

				default:
					parent::__set( $property_name, $value );
					break;
			}
			return $value;
		}
	}
	/*
	 * Returns a specific post form
	 *
	 * TODO: Need to properly document this
	 */
	function sr_get_form( $object_type, $object_sub_type, $form_name ) {
		global $sr_forms;
		$object_form = false;
		if ( isset( $sr_forms[$object_type][$object_sub_type][$form_name] ) )
			$object_form = $sr_forms[$object_type][$object_sub_type][$form_name];
		return $object_form;
	}
	/*
	 * Returns true if an object type and sub type has registered forms
	 *
	 * TODO: Need to properly document this
	 */
	function sr_has_forms( $object_type, $object_sub_type, $args = array() ) {
		return Sunrise_Forms::has_forms( array_merge( $args, array(
			'object_type'     => $object_type,
			'object_sub_type' => $object_sub_type,
		)));
	}

//	function sr_has_forms( $object_classifer, $args = array() ) {
//		$args['object_classifer'] = $object_classifer;
//		return Sunrise_Forms::has_forms( $args );
//	}

	/*
	 * Returns array of forms for an object type and sub type
	 *
	 * TODO: Need to properly document this
	 */
	function sr_get_forms( $object_type, $object_sub_type, $args = array() ) {
		return Sunrise_Forms::get_forms( array_merge( $args, array(
			'object_type'     => $object_type,
			'object_sub_type' => $object_sub_type,
		)));
	}
	/*
	 * Returns a specific post form
	 *
	 * TODO: Need to properly document this
	 */
	function sr_get_post_form( $form_name, $post_type  ) {
		return sr_get_form( $form_name, 'post', $post_type );
	}
	/*
	 * Returns true if a post type has registered forms
	 *
	 * TODO: Need to properly document this
	 */
	function sr_has_post_forms( $post_type, $args = array() ) {
		return sr_has_forms( 'post', $post_type, $args );
	}
	/*
	 * Returns array of forms for a post type
	 * Registers a Form for Fields
	 *
	 * TODO: Need to properly document this
	 */
	function sr_get_post_forms( $post_type, $args = array() ) {
		return sr_get_forms( 'post', $post_type, $args );
	}
}

