<?php

if ( sr_file_changed( __FILE__ ) )
	sr_register_capabilities( array(
		'edit_sunrise_field',
		'view_sunrise_field',
	));
