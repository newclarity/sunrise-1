<?php
/* site-config-demo.php */

if ( ! class_exists( 'Sunrise_Fields_Site_Config_Demo' ) ) {

	class Sunrise_Fields_Site_Config_Demo {
		static function on_load() {
			sr_add_action( __CLASS__, 'sr_post_types_init' );
			sr_add_action( __CLASS__, 'sr_images_init' );
			sr_add_action( __CLASS__, 'sr_forms_init' );
			sr_add_action( __CLASS__, 'sr_fields_init' );
		}
		static function sr_post_types_init() {
			sr_register_post_type( 'site-config', array(
				'label'           => 'Site Config',
				'public'          => true,
				'show_ui'         => true,
				'query_var'       => 'site-config',
				'rewrite'         => array( 'slug' => 'configurations' ),
				'hierarchical'    => true,
				'supports'        => array('dummy'),
				'sunrise_args'    => array(),
			));
		}
		static function sr_images_init() {
			sr_register_image_type( 'screenshot', array(
				'aspect_ratio' => '1024:768',                    // Contrain aspect ratio when cropping
				'sizes' => array(
					'search' => '100x100',
					'tiny'   => '64x48',
					'small'  => '128x96',
					'medium' => '256x192',
					'large'  => '512x384',
					'cinema' => '512x288',
				),
			));
		}
		static function sr_forms_init() {
			sr_register_form( 'basic_info', array(
			 'form_type' => 'admin_post_form',
			 'post_type' => 'site-config',
			 'title'     => 'Basic Information',
			 'priority'  => 'high',
			 'fields'    => array(
				 'post_title',
				 'components',
				),
			));
		}
		static function sr_fields_init() {
			sr_register_fields( 'site-config', array(
				'object_type' => 'post',
				'fields' => array(
					'post_title' => array(
						'type'          => 'text',
						'label'         => 'Site Config Name',
						'storage'       => 'core',
						'size'          => 50,
					),
					'components' => array(
						'type'            => 'repeating',
						'label'           => 'Components',
						'indentable'      => false,
						'no_colon'        => false,
						'label_position'  => 'left',
						'controls' => array( 'add_field', 'delete_field' ),
						'field_args' => array(
							'type'      => 'fieldset',
							'fields'    => array(
								'component_type' => array(
									'type'      => 'select',
									'label'     => 'Type',
									'options'   => array(
										'---'     => 'Select a Component Type',
										'core'    => 'WordPress Core',
										'plugin'  => 'Plugin',
										'theme'   => 'Theme',
										'lib'     => 'Library',
										'other'   => 'Other',
									),
								),
								'document' => array(
									'label'      => 'File',
									'size'       => 40,
									'extensions' => 'pdf,doc,docx',
									'type'       => 'file',
								),
//								'screenshot' => array(
//									'type'             => 'image',
//									'label'            => 'Screen',
//									'image_type'       => 'screenshot',
//									'view_size'        => 'small',      // Size used to view uploaded image in admin. Refers to a 'small' headshot.
//									'force_delete'     => false,        // false=>delete or true=>trash
//									'max_size'         => '1mb',
//									'help_text'        =>  'Photos you upload must be 512x384 pixels.',
//									'warn_if_small'    => true,         // Warn if uploaded image is too small
//									'max_crop'         => 1024,         // The maxmimum crop width (integer) or maximum width and height (WxH)
//									'show_crop_atts'   => true,         // Show crop attributes (width and height)
//								),
								'component_name' => array(
									'label'     => 'Name',
									'size'      => 50,
								),
								'component_url' => array(
									'label'     => 'URL',
									'size'      => 50,
								),
								'component_version' => array(
									'label'     => 'Version',
									'size'      => 10,
								),
								),
							),
						),
					),
			));
		}
	}
}
Sunrise_Fields_Site_Config_Demo::on_load();



