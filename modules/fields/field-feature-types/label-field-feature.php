<?php
class Sunrise_Label_Field_Feature extends Sunrise_Field_Feature {
	var $is_blank = false;
	var $no_colon = false;
	function do_initialize() {
		if ( $this->is_blank ) {
			$this->text = '&nbsp;';
			$this->no_colon = true;
		}
		if ( ! $this->position ) {
			$this->position = 'left';
		}
	}
	function get_inner_html() {
		$colon = $this->no_colon ? '' : ':';
		return "<label for=\"{$this->parent->field_name}\">{$this->text}{$colon}</label>&nbsp;";
	}
}

