<?php
class Sunrise_Help_Field_Feature extends Sunrise_Field_Feature {
	function do_initialize() {
		if ( ! $this->position )
			$this->position = 'below';
	}
	function get_inner_html() {
		$html = false;
		$help_text = $this->text;
		if ( ! empty( $help_text ) ) {
			$html =<<<HTML
<span id="{$this->parent->id}-help-feature" {$this->class_html}{$this->attributes_html}>{$help_text}</span>
HTML;
		}
		return $html;
	}
}
