<?php
class Sunrise_Message_Field_Feature extends Sunrise_Field_Feature {
	function do_initialize() {
		if ( !$this->position )
			$this->position = 'below';
	}
	function get_inner_html() {
		$html = false;
		$message_text = $this->text;
		if ( ! empty( $message_text ) ) {
			$html =<<<HTML
<span id="{$this->parent->id}-message-feature" {$this->class_html}{$this->attributes_html}>{$message_text}</span>
HTML;
		}
		return $html;
	}
}


