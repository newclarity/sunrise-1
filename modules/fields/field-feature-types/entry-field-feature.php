<?php
class Sunrise_Entry_Field_Feature extends Sunrise_Field_Feature {
	var $accept          = false;   // The accept attribute for HTML <input>
	var $alt             = false;   // The alt attribute for HTML <input>
	var $disabled        = false;   // The disabled attribute for HTML <input>
	var $maxlength       = false;   // The maxlength attribute for HTML <input>
	var $placeholder     = false;   // The placeholder attribute for HTML <input>
	var $readonly        = false;   // The readonly attribute for HTML <input>
	var $size		         = false;   // The size attribute for HTML <input>

	function do_initialize() {
		$this->html_tag = 'input';
	}
	function get_position_class() {
		return '';
	}
	function get_inner_html() {
		if ( ! $this->_inner_html ) {
			if ( $this->parent->can_edit() ) {
				$entry_html = $this->parent->get_entry_html();
			} else {
				$entry_html = $this->parent->get_viewing_html();
			}
			$this->_inner_html = "{$this->parent->required_callout_html}{$entry_html}{$this->infobox_container_html}{$this->message_container_html}{$this->help_container_html}";
			/**
			 * TODO (mikes): Only enable this next filter if needed.
			 */
			//$this->_inner_html = $this->apply_filters( 'inner_html', $this->_inner_html );
			$this->_inner_html = $this->parent->apply_filters( "entry_feature_inner_html", $this->_inner_html );
		}
		return $this->_inner_html;
	}
	function filter_valid_attributes( $attributes ) { // 'value' is a special case, not a field but handled by get_value()
		return $this->parent->apply_filters( 'entry_feature_valid_attributes',
			array_merge( array( 'accept', 'alt', 'disabled', 'maxlength', 'placeholder', 'readonly', 'size', 'value' ), $attributes )
		);
	}
}
