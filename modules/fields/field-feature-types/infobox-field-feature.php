<?php
class Sunrise_Infobox_Field_Feature extends Sunrise_Field_Feature {
	function do_initialize() {
		if ( !$this->position )
			$this->position = 'right';
	}
	function get_inner_html() {
		$html = false;
		$infobox_text = $this->text;
		if ( ! empty( $infobox_text ) ) {
			$parent_id = $this->parent->id;
			$infobox_icon_url = $this->apply_filters( 'infobox_icon_url', plugins_url( 'images/info-icon.gif', dirname( __FILE__ ) ) );
			list( $width, $height ) = explode( 'x', $this->apply_filters( 'infobox_icon_dimensions', '20x20' ) );
			$html =<<<HTML
<span id="{$parent_id}-infobox-feature" {$this->class_html}{$this->attributes_html}>
	<span id="{$parent_id}-infobox-icon" class="infobox-icon icon"><img src="{$infobox_icon_url}" width="{$width}" height="{$height}"/></span>
	<span class="infobox-text hidden">{$infobox_text}</span>
</span>
HTML;
		}
		return $html;
	}
}

