<?php
if ( ! class_exists( 'Sunrise_Meta_Storage' ) ) {
	class Sunrise_Meta_Storage extends Sunrise_Storage {
		/*
		 * Load field values as name/value pairs
		 * TODO: This assumes all fields are the same; need to move to handle each field storage separately
		 */
		static function load_raw_value_pairs( $field, $args = array() ) {
			$new_values          = array();
			$values              = get_post_custom( $field->object_id );
			$field->index        = false; // We want the base key, not an indexed one.
			$field->fieldset_key = false; // Make sure this is not set.
			$storage_key         = self::get_storage_key( $field, $args );
			$pattern = "#^{$storage_key}($|\[)#";
			foreach ( $values as $key => $value_array ) {
				if ( preg_match( $pattern, $key ) ) {
					$new_values[$key] = $value_array[0];
				}
			}
			ksort( $new_values );
			return $new_values;
		}

		static function load_raw_value( $field, $args = array() ) {
			if ( method_exists( get_class( $field ), 'load_values' ) ) {
				$value = $field->load_values( $args );
			} else {
				$post_id     = $field->object_id;
				$storage_key = isset($args['storage_key']) ? $args['storage_key'] : self::get_storage_key( $field );
				$value       = get_post_meta( $post_id, $storage_key, true );
			}
			return $value;
		}

		/**
		 * @static
		 *
		 * @param Sunrise_Field $field
		 * @param mixed         $value
		 * @param array         $args
		 *
		 * @return void
		 *
		 * TODO (mikes): Should we change this name to save_value()?
		 */
		static function update_raw_value( $field, $value, $args = array() ) {
 			$post_id = $field->object_id;
			$args    = wp_parse_args( $args, array(
				'storage_key' => self::get_storage_key( $field ),
			) );
			if ( ! method_exists( get_class( $field ), 'update_values' ) ) {
				update_post_meta( $post_id, $args['storage_key'], $value );
			} else {
				$field->update_values( $value, $args );
			}
		}

		static function update_sub_raw_values( $field, $args = array() ) {
			$post_id = $field->object_id;
			$args    = wp_parse_args( $args, array(
				'storage_key' => self::get_storage_key( $field ),
			) );

			foreach ( $field->sub_raw_values as $key => $value ) {
				$storage_key = "{$args['storage_key']}[{$key}]";
				update_post_meta( $post_id, $storage_key, $value );
			}

		}

		static function delete_raw_value( $field, $args = array() ) {
			$post_id = $field->object_id;
			$args    = wp_parse_args( $args, array(
				'storage_key' => self::get_storage_key( $field ),
			) );
			$value   = isset($args['value']) ? $args['value'] : '';
			return delete_post_meta( $post_id, $args['storage_key'], $value );
		}

		/*
				 * TODO: Consider delegating this back to the field?
				 */
		/**
		 * Returns storage key for this field. Considers parents to determine if it should store
		 *
		 * @param Sunrise_Field $field
		 *
		 * @return string
		 *
		 * @examples:
		 *
		 *  phone_number		- 1st phone # - No parent, or Repeating Fieldset parent
		 *  phone_number[2]	 - 2nd phone # - Repeating Field parent
		 *
		 *  education		   - 1st entry -  No parent, or Repeating Fieldset parent
		 *  education[1][1]	 - 2nd entry, child of 1st entry - Repeating Field parent
		 *  education[2]		- 3rd entry, 2nd top level entry - Repeating Field parent
		 *
		 *  document[title]	 - 1st document title - Fieldset Field parent
		 *  document[photo]	 - 1st document photo - Fieldset Field parent
		 *  document[2][title]  - 2nd document title - Fieldset Field parent, Repeating Field grandparent
		 *  document[2][photo]  - 2nd document photo - Fieldset Field parent, Repeating Field grandparent
		 *
		 */
		static function get_storage_key( $field ) {
			/**
			 * @var string $entry_name - Regex strips off trailing '[0]', if one is there, which it will be for indentable.
			 */
			$entry_name               = preg_replace( '#^(.*?)(\[0\])?$#', '$1', $field->entry_name );
			$is_first_repeating_field = ($field->parent && 'repeating' == $field->parent->field_type && 1 == $field->index);
			$is_first_fieldset_field  = ($field->parent && 'fieldset' == $field->parent->field_type && 1 == $field->parent->index);
			if ( $is_first_repeating_field || $is_first_fieldset_field ) {
				$entry_name = str_replace( '[1]', '', $entry_name );
			}
			return '_' . self::get_meta_key_prefix( $field ) . $entry_name;
		}

		static function get_meta_key_prefix( $field ) {
			if ( isset($field->extra['meta_key_prefix']) && $field->extra['meta_key_prefix'] !== false ) {
				// There's a value been set for this field.
				$meta_key_prefix = $field->extra['meta_key_prefix'];
				//			} else if ( $field->form && $field->form->meta_key_prefix !== false ) {
				//				// There's a value been set for the form. Grab it.
				//				$meta_key_prefix = $field->form->meta_key_prefix;
			} else {
				// Hasn't been set anywhere, so look at the options, default to nothing
				$options         = Sunrise_Storage::get_options();
				$meta_key_prefix = isset($options['meta_key_prefix']) ? $options['meta_key_prefix'] : '';
			}
			return $meta_key_prefix;
		}
	}
}


