<?php
/*
 * This is used for the Sunrise_Taxonomy_Term_Field to save and restore a single related term
 * TODO: (Maybe?) Enable storage of multiple taxonomy terms?
 * TODO (mikes): Consider using the field's 'storage_key' for the taxonomy? [2011-09-30]
 */
if (!class_exists('Sunrise_Taxonomy_Storage')) {
	class Sunrise_Taxonomy_Storage extends Sunrise_Storage {
		static function load_raw_value_pairs( $field, $args = array() ) {
			$new_values = array();
			return $new_values;
		}
		private static function test_taxonomy_property( $field ) {
			if ( WP_DEBUG && ! property_exists( get_class( $field ), 'taxonomy' ) ) {
				sr_die( "The field [{$field->field_name}] is using 'taxonomy' storage but does not have a 'taxonomy' property." );
			}
		}
		static function load_raw_value( $field, $args = array() ) {
			self::test_taxonomy_property( $field );
			$terms = get_the_terms( $field->object_id, $field->taxonomy );
            if( is_array( $terms ) ){
                $terms = array_values( $terms );
            }
			/*if ( WP_DEBUG && 1 < count( $terms ) ) {
				sr_die( "The 'taxonomy_term' field does not (yet?) support multiple taxonomy values." );
			}*/
			/*
			 * This is single value right now. In future it should be comma-separated for multivalue fields.
			 */
			return $terms ? ( 1==count( $terms ) ? $terms[0]->term_id : '' ): '';
		}
		static function update_raw_value( $field, $term_taxonomy_id, $args = array() ) {
			if ( $term_taxonomy_id = intval( $term_taxonomy_id ) ) {
				self::test_taxonomy_property( $field );
				/*
				 * false => do not append, i.e. overwrite other terms
				 */
				$append = isset( $args['append'] ) && true == $args['append'] ? true : false;
				wp_set_object_terms( $field->object_id, $term_taxonomy_id, $field->taxonomy, $append );
			}
			return $term_taxonomy_id;
		}
		static function delete_raw_value( $field, $args = array() ) {
			wp_delete_object_term_relationships( $field->object_id, $field->taxonomy );
		}
		static function get_storage_key( $field ) {
			return $field->storage_key;
		}
	}
}

