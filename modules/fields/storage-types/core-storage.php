<?php

if (!class_exists('Sunrise_Core_Storage')) {
	class Sunrise_Core_Storage extends Sunrise_Storage {
		static function on_load() {
			add_action( 'admin_init', array( __CLASS__, 'admin_init' ) );
		}
		/*
		 * Set post_title in case it is empty.
		 *    See: http://core.trac.wordpress.org/ticket/18713
		 */
		static function admin_init() {
			global $pagenow;
			if ( count( $_POST ) && isset( $_POST['sunrise_fields'] ) && empty( $_POST['post_title'] ) ) {
				if ( 'post.php' == $pagenow ) {
					$_POST['post_title'] = 'To be replaced';
				} else {
					sr_die( "Unexpected value for \$pagenow when \$_POST['post_title'] is empty: {$pagenow} " );
				}
			}
		}
		static function load_raw_value( Sunrise_Field $field, $args = array() ) {
			$value = $field->get_field_value( $field, $args );
			return $value;
		}
		static function update_raw_value( Sunrise_Field $field, $meta_value, $args = array() ) {
		}
		static function delete_raw_value( Sunrise_Field $field, $args = array() ) {
		}
	}
	Sunrise_Core_Storage::on_load();
}


