<?php

class Sunrise_Date_Field extends Sunrise_Field {
	var $format = 'm/d/Y';
	var $datepicker_format = 'mm/dd/yy'; // Date format, different from PHP. See here: http://docs.jquery.com/UI/Datepicker/formatDate
	var $date_picker = false;
	var $size = false;

//	function __construct($args) {
//		parent::__construct($args);
//		$this->format = sr_get_date_format('short');
//		$this->datepicker_format = $this->convert_format( $this->format );
//	}
	/**
	 * Add 'size' attribute for entry feature if it hasn't been defined
	 * Use 'do_pre_initialize' function instead of 'initialize' because field features are built on input arguments
	 * ('do_initialize' function cannot change input arguments)
	 */
	function do_pre_initialize() {
		if ( !$this->size ) {
			/*
			 * Hardcoded date format because the {$this->datepicker_format} is used for Javascript
			 */
			$this->args['entry_size'] = strlen( date( 'm/d/Y', time() ) ) + 2;
		}
	}

	function filter_attribute_value( $value, $args = array() ) {
		return $value ? date( $this->format, strtotime($value) ) : '';
	}
	function filter_update_value( $value, $args = array() ) {
		/*
		 * Save in YYYY-MM-DD format
		 */
		return date( 'Y-m-d', strtotime( $value ) );
	}

	function do_initialize() {
		/*
		 * Set HTML type = text
		 */
		$this->html_type = 'text';
	}

	/**
	 * Enqueue scripts and styles for date picker
	 */
	static function admin_init() {
		Sunrise_Fields::enqueue_css( __FILE__, 'date', array(
			'handle' => 'jqueryui-datepicker',
			'filepath' => '/css/jquery.ui.datepicker-1.8.16.css'
		) );

		Sunrise_Fields::enqueue_js( __FILE__, 'date', array(
			'handle'   => 'jqueryui-datepicker',
			'filepath' => '/js/jquery.ui.datepicker-1.8.16.min.js',
			'dependencies' => array( 'jquery-ui-core' )
		) );
		Sunrise_Fields::enqueue_js( __FILE__, 'date', array(
			'dependencies' => array( 'jqueryui-datepicker' )
		) );
	}

	/**
	 * Add 'datepicker' class to input box
	 * @param string $classes Default classes
	 * @return string Classes
	 */
	function filter_entry_feature_class( $classes ) {
		return "{$classes} datepicker";
	}

	/**
	 * Add date format to HTML markup
	 *
	 * @param $html Default HTML of the field
	 * @return string HTML to display for this field
	 */
	function filter_entry_html( $html ) {
		$hidden_inputs = $this->hidden_inputs();

		return "{$hidden_inputs}{$html}";
	}

	/**
	 * Generate hidden fields for date format
	 * @return string HTML markup for nonces fields
	 */
	function hidden_inputs() {
		$fields = "<input type=\"hidden\" id=\"format-{$this->id}\" value=\"{$this->format}\" />";
		return $fields;
	}

	/**
	 * Converts from PHP date format to jQuery datepicker date format
	 *
	 * @param $php_format
	 * @return string
	 * @see: 	http://wordpress.stackexchange.com/questions/38935/convert-wordpress-date-format-to-jquery-ui-datepicker-format
	 */
	function convert_format( $php_format ) {
		return strtr( (string)$php_format, array(
			//day
			'd' => 'dd',	//day of the month
			'j' => 'd',		//3 letter name of the day
			'l' => 'DD',	//full name of the day
			'z' => 'o',		//day of the year

			//month
			'F' => 'MM',	//Month name full
			'M' => 'M',		//Month name short
			'n' => 'm',		//numeric month no leading zeros
			'm' => 'MM',	//numeric month leading zeros

			//year
			'Y' => 'yy', 	//full numeric year
			'y' => 'y' 		//numeric year: 2 digit
		));
	}


}
