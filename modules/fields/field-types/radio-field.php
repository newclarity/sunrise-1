<?php

//TODO: This was written to show someone how it works so it is not complete. Needs a lot of work.

class Sunrise_Radio_Field extends Sunrise_Field {

	var $options = array();

	function do_initialize( $args ) {
		$this->html_type = 'radio';
	}

	function get_entry_html() {
		$html = array();
		$i = 1;
		foreach( $this->options as $value => $text ) {
			$selected = $this->value == $value ? ' checked="checked"' : '';
			$id = $this->field_name . '-' . $i;
			$html[] = "<label for=\"{$id}\"><input name=\"{$this->entry_name}\" id=\"{$id}\" type=\"radio\" class=\"{$this->class}\" value=\"{$value}\"{$selected}>{$text}</label>";
			$i++;
		}
		return $this->apply_filters( 'select_field_entry_html', implode( '', $html ) );
	}

}
