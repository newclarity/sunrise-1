<?php

if ( ! class_exists( 'Sunrise_Select_Field' ) ) {
	/**
	 * @property string $default_text
	 * @property array $options
	 */
	class Sunrise_Select_Field extends Sunrise_Field {

		var $disabled = false;
		var $multiple = false;
		protected $_select2 = false;
		protected $_default_text = false;
		protected $_options = false;

		function do_initialize( $args ) {
			$this->html_type = 'select';
			wp_register_style( 'select2', '//cdnjs.cloudflare.com/ajax/libs/select2/3.4.1/select2.css', array(), null );
			wp_register_script( 'select2', '//cdnjs.cloudflare.com/ajax/libs/select2/3.4.1/select2.min.js', array( 'jquery' ), null );
			if( ! empty( $args['select2'] ) ) {
				$this->_select2 = true;
			}
		}

		function filter_entry_feature_valid_attributes( $attributes, $args ) {
			$attributes[] = 'disabled';
			$attributes[] = 'multiple';
			$attributes[] = 'name';
			/**
			 * Remove the 'value' attribute for select by exchanging keys & values (values are unique here)
			 * and then using unset( ['value'] ) and then exchanging keys & values back.
			 */
			$attributes = array_flip( $attributes );
			unset($attributes['value']);
			$attributes = array_flip( $attributes );
			return $attributes;
		}

		function filter_entry_feature_class_html( $class_attr ) {
			if( $this->_select2 ) {
				preg_match( '#.*?"(.*)".*#', $class_attr, $matches );
				if( $matches[1] ) {
					$class = trim( "{$matches[1]} select2" );
					$class_attr = " class=\"{$class}\"";
					wp_enqueue_script( 'select2' );
					wp_enqueue_style( 'select2' );
				}
			}
			return $class_attr;
		}

		function update_value( $value, $args = array() ) {
			if( $value && is_array( $value ) )
				$value = join(',', $value);
			if( ! $value )
				$value = '';
			$value = $this->apply_filters( 'update_value', $value, $args );
			return $this->update_raw_value( $value, $args );
		}

		function set_default_text( $default_text ) {
			$this->_default_text = $default_text;
		}

		function get_default_text() {
			return $this->apply_filters( 'select_field_default_text', $this->_default_text );
		}

		function set_options( $options ) {
			$this->_options = $options;
		}

		function get_options() {
			$this->_options = $this->apply_filters( 'select_field_options', $this->_options );
			if ( ! $this->_options ) {
				$this->_options = array();
			}
			return $this->_options;
		}

		function add_default_option() {
		    if( $this->default_text ) {
			    $this->set_options( array( $this->default_text ) + $this->options );
		    }
		}

		function get_entry_html() {
			$brackets = $this->multiple ? '[]': false;
			$html  = array( "<select id=\"{$this->id}\" name=\"{$this->entry_name}{$brackets}\" {$this->class_html}{$this->attributes_html}>" );
			$this->add_default_option();
			$raw_value = $this->value;
			$multiple_values = explode( ',', $raw_value );
			foreach ( $this->options as $value => $text ) {
				if( $multiple_values && is_array( $multiple_values ) ) {
					$selected = in_array( $value, $multiple_values ) ? ' selected="selected"': '';
				} else {
					$selected = selected( $value, $this->value, false );
				}
				if( is_array( $text ) ) {
					$html[] = '<optgroup label="'. $value .'">';
					foreach( $text as $k => $v ) {
						$html[]   = "<option value=\"{$k}\"{$selected}>{$v}</option>\n";
					}
					$html[] = '</optgroup>';
				} else {
					$html[]   = "<option value=\"{$value}\"{$selected}>{$text}</option>\n";
				}
			}
			$html[] = "</select>\n";
			return $this->apply_filters( 'select_field_entry_html', implode( '', $html ) );
		}

		function __set( $property_name, $value ) {
			switch ( $property_name ) {
				case 'default_text':
				case 'options':
					call_user_func( array( &$this, "set_{$property_name}" ), $value );
					break;

				default:
					parent::__set( $property_name, $value );
					break;
			}
		}

		function __get( $property_name ) {
			$value = false;
			switch ( $property_name ) {
				case 'default_text':
				case 'options':
					$value = call_user_func( array( &$this, "get_{$property_name}" ) );
					break;

				default:
					$value = parent::__get( $property_name );
					break;
			}
			return $value;
		}
	}

}
