<?php

class Sunrise_Time_Field extends Sunrise_Field {
	var $format      = 'g:ia';
	var $time_picker = false;
	var $size        = false;

	/**
	 * Add 'size' attribute for entry feature if it hasn't been defined
	 * Use 'do_pre_initialize' function instead of 'initialize' because field features are built on
	 * input arguments ('initialize' function cannot change input arguments)
	 * TODO: Add Feature Initialize Action Hooks for Fields
	 */
	function do_pre_initialize() {
		if ( !$this->size ) {
			/*
			 * Total characters = strlen(date) - 2 (for 'am', 'pm') + 1 (for leading zero if needed)
			 */
			$this->args['entry_size'] = strlen( date( $this->format, strtotime('12:00:00') ) ) - 1;
		}
	}

	function do_initialize() {
		/*
		 * Change the input type to 'text'
		 */
		$this->html_type = 'text';

		/*
		 * Check time format via Ajax
		 */
		add_action( 'wp_ajax_sunrise_fields_check_time_format', array( __CLASS__, 'wp_ajax_check_time_format' ) );
	}

	static function admin_init() {
		Sunrise_Fields::enqueue_js( __FILE__, 'time' );
	}

	function filter_value( $value, $args = array() ) {
		if( $value ) {
			if ( ! $this->_value ) {
				$this->_value = date( $this->format, strtotime( $value ) );
			}
		}
		return $this->_value;
	}
	function filter_attribute_value( $value, $args = array() ) {
		return $this->_value ? $this->_value : $this->value;
	}
	function filter_update_value( $value, $args = array() ) {
		if( $value ) {
			/*
			 * Save in HH:MM:SS format where HH is 01-23
			 */
			$value = date( 'H:i:s', strtotime($value) );
		}
		return $value;
	}

	/**
	 * Set the correct time value to the input box and select box
	 * Also change the 'name' attribute of input box to prevent WP from saving its value
	 *
	 * @param $html Default HTML of the field
	 * @return string HTML to display for this field
	 */
	function filter_entry_html( $html ) {
		/*
		 * Set the 'name' attribute to {$this->id}_time to prevent WP from saving the raw field value
		 *
		 * Also change the value that is shown in the input box to time value only (w/o 'AM-PM')
		 */
		$attributes = array();
		$replacement = array();
		$value = $this->value;
		$ampm = '';
		if ( $value ) {
			$time_only = str_ireplace( array( 'am', 'pm' ), '', $value );
			$attributes[] = "value=\"{$value}\"";
			$replacement[] = "value=\"{$time_only}\"";

			$ampm = false === strpos( $value, 'am' ) ? 'pm' : 'am';
		}

		$html = str_replace( $attributes, $replacement, $html );
		$select_box = "<select name=\"{$this->field_name}_ampm\" class=\"ampm\">
				<option value=\"am\"" . selected($ampm, 'am', false) . ">AM</option>
				<option value=\"pm\"" . selected($ampm, 'pm', false) . ">PM</option>
			</select>";
		$hidden_inputs = $this->hidden_inputs();

		return "{$html} {$select_box}{$hidden_inputs}";
	}

	/**
	 * Generate hidden fields for saving the "combined" time in correct format
	 * Add nonce for checking time format
	 * @return string HTML markup
	 */
	function hidden_inputs() {
		$nonce = wp_create_nonce( "sunrise_fields_check_time_format_{$this->id}" );
		$input_html = "<input type=\"hidden\" id=\"nonce-{$this->id}\" value=\"{$nonce}\" />";

		return $input_html;
	}

	/**
	 * Add class to input box
	 * @param string $classes Default classes
	 * @return string Classes
	 */
	function filter_entry_feature_class( $classes ) {
		return "{$classes} time";
	}

	/**
	 * Set the field value based on 'time' and 'ampm' values
	 * @param $value
	 * @param $args
	 * @return string Raw field value in {$this->format} format. Used for raw storage, not for display
	 */
	function filter_update_from_POST( $value, $args ) {
		$time = sr_get_from_POST( "{$this->field_name}" );
		if( $time ) {
			if ( false === strpos( $time, ':' ) )
				$time = "{$time}:00";
			$ampm = sr_get_from_POST( "{$this->field_name}_ampm" );
			/*
			 * Format is hardcoded to {$this->format} for raw storage, not for display
			 */
			$value = date( $this->format, strtotime( "{$time}{$ampm}" ) );
		}
		return $value;
	}

	/**
	 * Check time format via Ajax
	 */
	static function wp_ajax_check_time_format() {
		$field = Sunrise_Fields::get_field_from_ajax_POST( 'check_time_format' );

		$time = isset( $_POST['time'] ) ? $_POST['time'] : '';
		$formatted_time = date( $field->format, strtotime( $time ) );
		if( $time == 'am' || $time == 'pm' ) {
			die( Sunrise::format_ajax_response( '', 'success' ) );
		} else if ( $time !== $formatted_time )
			die( Sunrise::format_ajax_response( 'Invalid time format', 'error' ) );
		else {
			die( Sunrise::format_ajax_response( '', 'success' ) );
		}
	}

}
