<?php
/*
 * Sunrise Checkbox field.
 *
 * Stores a value of 'checked' if the checkbox is in fact checked, nothing if it is not.
 *
 */
class Sunrise_Checkbox_Field extends Sunrise_Field {
	function filter_viewing_html( $html ) {
		$raw_value = $this->raw_value;
		$display_value = "" == $raw_value ? "&nbsp;" :  "X";
		$html = str_replace( '></span>', ">[&nbsp;{$display_value}&nbsp;]</span>", $html );
		$html = str_replace( 'value=""', "value=\"{$raw_value}\"", $html );
		return $html;
	}
	function filter_initial_add_value( $value, $args ) {
		return 'checked';
	}
	function filter_attribute_value( $value, $args ) {
		return $value == 'checked' ? '1' : false;
	}
	function filter_update_value( $value, $args ) {
		return $value ? 'checked' : '';
	}
	function filter_entry_feature_valid_attributes( $attributes, $args ) {
		$attributes[] = 'checked';
		return $attributes;
	}
}


