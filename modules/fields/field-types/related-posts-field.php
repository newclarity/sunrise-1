<?php
/*
 * TODO (mikes): We should see how much of this we can move into Sunrise_Relationship_Field.
 * TODO (Micah): - Add option to limit number of related posts that can be selected?  Need to implement single relationship (in addition to multi select)
 */
require_once(dirname( __FILE__ ) . '/relationship-field.php'); // Make sure it has been defined first.

if ( ! class_exists( 'Sunrise_Related_Posts_Field' ) ) {

	class Sunrise_Related_Posts_Field extends Sunrise_Relationship_Field {

		var $relationship_name = false;
		var $query_args = array();
		var $multi_select = false; // If true, users can select more than one related post type
		protected $_related_object = false; // set using get_post_type_object( $post_type )

		static function on_load() {
			add_action( 'admin_init', array( __CLASS__, 'admin_init' ), 0 ); // 0 - Do this very early
		}

		static function admin_init() {
			Sunrise_Fields::enqueue_css( __FILE__, 'related_posts' );
			Sunrise_Fields::enqueue_js( __FILE__, 'related_posts', array(
				'dependencies' => array( 'jquery-ui-core' ),
			) );
			// Setup AJAX action for data source
			add_action( 'wp_ajax_suggest', array( __CLASS__, 'wp_ajax_suggest' ) );
			// Setup AJAX action for list item insertion
			add_action( 'wp_ajax_insert_element', array( __CLASS__, 'wp_ajax_insert_element' ) );
		}

		function do_initialize( $args ) {
			if ( isset($args['query']) && is_array( $args['query'] ) ) {
				$this->query_args = $args['query'];
			}
			if ( ! sr_has_relationship( $this->relationship_name ) ) {
				sr_die( 'The relationship [%s] has not been registered.', $this->relationship_name );
			}
			parent::do_initialize( $args );
		}

		function update_values( $values, $args = array() ) {
			/**
			 * TODO (mikes): We need a 'relationship-storage' type instead of this
			 */
			if( '0' == $values ) { // This is essentially 'false' or 'not set', so don't store this as an ID
				$values = array();
			}
			if( is_string( $values ) ) {
				$values = explode( ',', $values );
			}
			$values = array_unique( (array) $values );
			$args['object_id'] = isset( $args['object_id'] ) ? $args['object_id']: $this->object_id;
			update_post_meta( $args['object_id'], $args['storage_key'], implode( ',', $values ) );
			$new_args = array( 'flag' => $args['storage_key'] );
			if( is_admin() ) {
				$new_args['post_status'] = array( 'publish', 'draft', 'hidden' );
			}
			return sr_update_relationships( $this->relationship_name, $args['object_id'], $values, $new_args );
		}

		function filter_entry_feature_container_class( $class ) {
			$class = trim( "{$class} suggested" . ($this->multi_select ? ' multi-select' : '') );
			return $this->apply_filters( 'related_posts_field_entry_class', $class );
		}

		function filter_entry_feature_html( $html ) {
			$html  = array( "<select id=\"{$this->id}\" name=\"{$this->entry_name}\" {$this->class_html}{$this->attributes_html}>" );
			foreach ( $this->options as $value => $text ) {
				if( is_string( $text ) ) {
					if( $this->multi_select ){
						$selected = false;
					} else {
						$selected = selected( $value, $this->value, false );
					}
					$html[]   = "<option value=\"{$value}\"{$selected}>{$text}</option>\n";
				} else if( is_array( $text ) ) {
					$html[] = "<optgroup label=\"{$value}\">\n";
					foreach( $text as $k => $v ) {
						if( $this->multi_select ){
							$selected = false;
						} else {
							$selected = selected( $value, $this->value, false );
						}
						$html[] = "<option value=\"{$k}\"{$selected}>{$v}</option>\n";
					}
					$html[] = "</optgroup>\n";
				}
			}
			$html[] = "</select>\n";
			if( $this->multi_select ) {
				$html[] = '<ul class="sortable">';
				// Get objects that are related to this one
				$storage_key = '_' . $this->_entry_name;
				$storage_key = preg_replace('#\[1\]#', '', $storage_key);
				$args = array( 'flag' => $storage_key );
				if( is_admin() && ( !defined('DOING_AJAX') || !DOING_AJAX ) ) {
					$args['post_status'] =  get_post_stati();
				}
				if ( $related_objects = sr_get_related_objects( $this->relationship_name, $this->object_id, $args ) ) {
					// Setup list of current related posts
					foreach ( $related_objects as $related_object ) {
						$html[] = $this->_insert_list_item(
							$related_object->ID,
							$related_object->post_title
						);
					}
				} else if ( ! empty( $this->default ) ) {
					/**
					 * If none are set and default is set, add it
					 */
					foreach ( explode( ',', $this->default ) as $default_post_id ) {
						$html[] = $this->_insert_list_item(
							$default_post_id,
							get_the_title( $default_post_id )
						);
					}
				}
				$html[] = '</ul><div class="clear"></div>';
			}
			$html[] = $this->hidden_inputs();
			$html   = implode( PHP_EOL, $html );
			return $this->apply_filters( 'related_posts_filter_entry_html', $html );
		}

		/**
		 * Generate hidden fields for saving the "combined" time in correct format
		 * Add nonce for checking time format
		 * @return string HTML markup
		 */
		function hidden_inputs() {
			$field_id   = $this->id;
			$nonce      = wp_create_nonce( "sunrise_fields_insert_element_{$field_id}" );
			$input_html = '<input type="hidden" id="' . $field_id . '-field-type" value="' . $this->field_type . '" />';
			$input_html .= '<input type="hidden" id="' . $field_id . '-insert-element" value="' . $nonce . '" />';
			$input_html = $this->apply_filters( 'related_posts_field_hidden_inputs_html', $input_html );
			return $input_html;
		}

		function filter_select_field_default_text() {
            $default_text = $this->_default_text ? $this->_default_text : sprintf( __( "Select %s to Add" ), $this->related_object->labels->name );
            return $default_text;
		}

		function filter_select_field_options( $options ) {
			if ( ! $this->_options ) {
				$this->_options = array();
				if ( ! sr_has_relationship( $this->relationship_name ) ) {
					if ( WP_DEBUG ) {
						sr_die( 'The relationship [%s] has not been registered', $this->relationship_name );
					}
				} else {
					// Shortcut variable
					$relationship = sr_get_relationship( $this->relationship_name );
					// Initiate base array for getting posts
					$args = array(
						//'post_status' => 'publish',
						'nopaging'    => true, // Don't limit results
						'posts_per_page' => -1,
						'orderby'     => 'title', // Sort by post title
						'order'       => 'ASC',
					);
					// Check if we are on the other end of a reciprocal relationship
					if ( $relationship->is_reciprocal && $this->post_type == $relationship->related_sub_type ) {
						$args['post_type'] = $relationship->object_sub_type;
					} else { // Get the related post type
						$args['post_type'] = $relationship->related_sub_type;
					}
					$args = apply_filters( 'sr_related_posts_options_args', $args, $this->relationship_name );
					$this->query_args = array_merge( $args, $this->query_args );
					$query            = new WP_Query($this->query_args);
					if ( count( $query->posts ) ) {
						foreach ( $query->posts as $post )
						{
							$this->_options[$post->ID] = get_the_title( $post->ID );
						}
					}
				}
			}
			// TODO: Temporary hook, please do NOT use!  Will be removed soon.
			$this->_options = apply_filters( '_sr_temp_filter_related_posts_field_options', $this->_options, $this->relationship_name );
			return $this->_options;
		}

		static function wp_ajax_insert_element() {
			/**
			 * @var Sunrise_Related_Posts_Field $field
			 */
			$field = Sunrise_Fields::get_field_from_ajax_POST( 'insert_element' );

			$status = 'error';
			if ( ! isset($_POST['related_object_id']) ) {
				$message = __( 'ERROR: ' ) . sprintf( __( "No \$_POST['related_object_id'] for field: %s." ), $field->field_name );
			} else {
				// Get post
				$post = get_post( $_POST['related_object_id'] );
				if ( $post ) {
					$status  = 'success';
					$message = esc_html( $field->_insert_list_item( $post->ID, $post->post_title ) );
				} else {
					$message = 'The requested post_id is invalid. Unable to generate list item.';
				}
			}
			die(Sunrise::format_ajax_response( $message, $status ));
		}

		protected function _insert_list_item( $post_id, $post_title ) {
			$list_item = '<li>' . $post_title . '<input type="hidden" name="' . $this->_entry_name . '[]" value="' . $post_id . '" /><span>X</span></li>';
			return $list_item;
		}

		function wp_ajax_suggest() {
			if ( isset($_POST['suggest']) ) {
				$suggest = esc_attr( $_POST['suggest'] );
				$options = $this->options;
				asort( $options );
				$results = preg_grep( "/^{$suggest}/i", $options );
				echo empty($results) ? '' : json_encode( $results );
			}
			exit();
		}

		function get_related_object() {
			if ( ! $this->_related_object ) {
				$relationship = Sunrise_Relationships::get_relationship( $this->relationship_name );
				if ( ! isset($relationship->related_sub_type) ) {
					sr_die( __( 'Post type not found for relationship %s' ), $this->relationship_name );
				}
				$this->_related_object = get_post_type_object( $relationship->related_sub_type );
			}
			return $this->_related_object;
		}

		function set_related_object( $value ) {
			return $this->_related_object = $value;
		}

		function __set( $property_name, $value ) {
			switch ( $property_name ) {
				case 'related_object':
					$this->set_related_object( $value );
					break;

				default:
					parent::__set( $property_name, $value );
					break;
			}
			return $value;
		}

		function __get( $property_name ) {
			$value = false;
			switch ( $property_name ) {
				case 'related_object':
					$value = $this->get_related_object();
					break;

				default:
					$value = parent::__get( $property_name );
					break;
			}
			return $value;
		}
	}

	Sunrise_Related_Posts_Field::on_load();
}
