<?php

if( !class_exists('Sunrise_Textarea_Field') ){

    class Sunrise_Textarea_Field extends Sunrise_Field {

      var $rows = false;
      var $cols = false;

      function do_initialize( $args ) {
        $this->html_type = 'textarea';
      }
      function filter_entry_feature_valid_attributes( $attributes, $args ) {
      	$attributes = array_flip( $attributes );
      	unset( $attributes['value'] );
        return array_merge( array( 'cols', 'name', 'rows', 'tabindex', 'wrap' ), array_keys( $attributes ) );
      }
      function get_entry_html() {
        $html = array( "<textarea id=\"{$this->id}\" name=\"{$this->entry_name}\" {$this->class_html}{$this->attributes_html}>" );
        $html[] = "{$this->value}\n";
        $html[] = "</textarea>\n";
        return Sunrise::apply_instance_filters( $this, 'textarea_field_entry_html', implode( '', $html ) );
      }
    }

}
