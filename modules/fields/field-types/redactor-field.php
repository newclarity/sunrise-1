<?php

require_once( dirname( __FILE__ ) . '/textarea-field.php' ); // Make sure it has been defined first.

if ( ! class_exists( 'Sunrise_Redactor_Field' ) ) {

	class Sunrise_Redactor_Field extends Sunrise_Textarea_Field {

		static function admin_init(){
			Sunrise_Fields::enqueue_js( __FILE__, 'redactor', array(
				'dependencies' => array( 'jquery' ),
		    ) );
			Sunrise_Fields::enqueue_css( __FILE__, 'redactor' );
		}

		function filter_value( $value ) {
			// Process shortcodes only on front end
			$value = is_admin() ? $value : do_shortcode( $value );
			return $value;
		}

		function filter_entry_feature_class( $class ) {
			return trim( "{$class} redactor" );
		}

	}

}