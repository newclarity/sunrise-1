<?php

class Sunrise_Fieldset_Field extends Sunrise_Field {
	var $fields = array();
	var $child_index = false; // TODO: Verify this is needed; where and why
	var $class = '';

	function do_initialize( $args ) {
		$this->fields = sr_register_fields( $this->object_sub_type, array(
			'object_type' => $this->object_type,
			'fields'      => isset($args['fields']) ? $args['fields'] : array(),
			// TODO (mikes): Review reason to need 'class' as it should be defined in Sunrise_Field?
			'class'       => isset($args['class']) ? $args['class'] : false, // Micah added for proper show/hide functionality
		) );
		foreach ( array_keys( $this->fields ) as $field_name ) {
			$this->fields[$field_name]->parent = $this;
		}
	}

	static function admin_init() {
		Sunrise_Fields::enqueue_css( __FILE__, 'fieldset' );
		Sunrise_Fields::enqueue_js( __FILE__, 'fieldset' );
	}

	function filter_entry_feature_container_class( $class ) {
		return "{$class} fieldset";
	}

	function filter_entry_feature_valid_attributes( $attributes, $args ) {
		return Sunrise_Field_Feature::get_span_attributes();
	}

	function load_values( $args = array() ) {
		$values      = array();
		$this->index = 1;
		/**
		 * @var Sunrise_Field $field
		 */
		foreach ( $this->fields as $field_name => $field ) {
			$args['storage_class']        = Sunrise_Storage::get_class_for( $this->storage_type );
			$args['storage_key']          = $storage_key = $field->get_storage_key( $args );
			$values[$args['storage_key']] = Sunrise::call_instance_method( $args['storage_class'], 'load_raw_value', $field, $args );
		}
		return $this->apply_filters( 'fieldset_load_value', $values, $args );
	}
	function update_values( $values, $args ) {
		$class = Sunrise::parse_class( "fields/storage/{$this->storage_type}" );
		if ( ! $values || count( $values ) < count( $this->fields ) ) {
			$values = array();
			foreach( array_keys( $this->fields ) as $field_name ) {
				if ( ! isset( $values[$field_name] ) )
					$values[$field_name] = '';
			}
		}
		foreach( $values as $storage_key => $value ) {
			/**
			 * @var Sunrise_Field $field
			 */
			$field = $this->fields[$storage_key];
			$field->post_id = $args['object_id'];
			$args['storage_key'] = "_{$this->field_name}[{$storage_key}]";
			$field->update_value( $value, $args );
		}
	}
	function initialize_fields( $args = array() ) {
		foreach ( $this->fields as $field_name => $field ) {
			WP_DEBUG && Sunrise_Fields::check_field_name( $field_name );
			/** TODO: leave_blank IS A HACK, FIX IT! */
			$field->leave_blank        = $this->leave_blank;
			$field->form               = $this->form;
			$field->parent             = $this;
			$field->object             = $this->object;
			$field->entry_name         = false;
			$this->child_index         = false; // This sets indent level to 1
			$this->fields[$field_name] = $field;
		}
	}

	function load_fields( $args = array() ) {
		if ( ! $this->fields_loaded ) {
			$this->initialize_fields( $args );
			$field_values = $this->load_values();
			foreach ( $this->fields as $field_name => $field ) {
				$storage_key  = Sunrise_Fields::get_storage_key( $field, $args );
				$field->value = $field_values[$storage_key];
			}
		}
		return $this->fields;
	}

	function filter_entry_feature_class( $class ) {
		$classes = "entry-feature-fieldset {$class}";
		if ( ! empty($this->class) ) {
			$classes .= "{$classes} {$this->class}";
		}
		return $this->apply_filters( 'fieldset_field_entry_class', "entry-feature-fieldset {$class}" );
	}

	function filter_item_html( $html ) {
		return $html;
	}

	function set_value( $values ) {
		foreach ( array_keys( $this->fields ) as $name ) {
			$this->fields[$name]->value = isset($values[$name]) ? $values[$name] : '';
		}
		return $values;
	}

	function get_entry_html( $args = array() ) {
		$this->load_fields( $args );
		$html   = array();
		$html[] = "<fieldset id=\"{$this->id}-fieldset\"{$this->class_html}{$this->attributes_html}>";
		$html[] = '<ul class="fieldset">';
		foreach ( $this->fields as $field_name => $field ) {
			$this->child_index = $field->index = $field_name;
			if ( ! $field->object ) {
				$field->object_adapter = $this->object_adapter;
			}
			$html[] = $field->item_html;
		}
		$html[] = '</ul>';
		$html[] = "</fieldset>\n";
		$html   = $this->apply_filters( 'fieldset_field_entry_html', implode( '', $html ) );
		return $html;
	}

	function get_container() {
		return $this->parent;
	}

	function __set( $property_name, $value ) {
		switch ( $property_name ) {
			case 'value':
				$this->set_value( $value );
				break;

			case 'container':
				$this->_cannot_set_error( $property_name, $value );
				break;

			default:
				parent::__set( $property_name, $value );
				break;
		}
	}

	function __get( $property_name ) {
		$value = false;
		switch ( $property_name ) {
			case 'container':
				$value = call_user_func( array( &$this, "get_{$property_name}" ) );
				break;

			default:
				$value = parent::__get( $property_name );
				break;
		}
		return $value;
	}


}

