<?php

require_once(dirname( __FILE__ ) . '/textarea-field.php'); // Make sure it has been defined first.

if ( ! class_exists( 'Sunrise_CLEditor_Field' ) ) {

	class Sunrise_CLEditor_Field extends Sunrise_Textarea_Field {

		function do_initialize( $args ) {

		}

		static function admin_init(){
			Sunrise_Fields::enqueue_js( __FILE__, 'cleditor' );
			Sunrise_Fields::enqueue_css( __FILE__, 'cleditor' );
		}

    function filter_value( $value ) {
			// Process shortcodes only on front end
			if( !is_admin() ) {
				$value = do_shortcode( $value );
			}
			return wpautop($value);
		}

    function filter_entry_feature_class( $class ) {
			return trim( "{$class} cleditor" );
		}

	}

}
