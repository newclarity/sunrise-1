<?php

if( ! class_exists('Sunrise_Number_Field') ){

	class Sunrise_Number_Field extends Sunrise_Field {

		var $min;
		var $max;
		var $step;

		function do_initialize($args) {
			$this->html_type = 'number';
		}

		function filter_entry_feature_valid_attributes( $attributes, $args ) {
			$attributes[] = 'min';
			$attributes[] = 'max';
			$attributes[] = 'step';
			return $attributes;
		}

	}

}
