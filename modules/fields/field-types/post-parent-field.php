<?php

require_once(dirname(__FILE__ ) . '/select-field.php' );

if ( !class_exists('Sunrise_Post_Parent_Field' ) ) {

	class Sunrise_Post_Parent_Field extends Sunrise_Select_Field {
		var $query_args = array();
		var $post_type = false;

		function do_initialize( $args = array() ) {
			/**
			 * First parse the $args as passed looking for 'query'
			 */
			$args = sr_parse_args( $args, array(
				'query' => isset( $args['query'] ) && is_array( $args['query'] ) ? $args['query'] : $this->query_args,
			));
			/**
			 * Second parse 'query' and assign to $query_args property.
			 */
			$this->query_args = sr_parse_args( $args['query'], array(
				'post_type' => $this->post_type,
				'posts_per_page' => -1,
				'orderby' => 'title',
				'order' => 'ASC',
			));
		}

		function filter_select_field_options($options ) {
			if ( ! $this->_options ) {
				$this->_options = array();
				/*
				 * Get list of posts using WP_Query, and assign to $this->options
				 */
				$query = new WP_Query( $this->query_args );
				foreach ( $query->posts as $post ) {
					$this->_options[$post->ID] = get_the_title( $post->ID );
				}
			}
			return $this->_options;

		}
	}

}
