<?php

if ( ! class_exists( 'Sunrise_File_Field' ) ) {
	/**
	 * File Field for use with Sunrise Fields.
	 *
	 * @property int $max_size
	 * @property Sunrise_File $file
	 * @property int $attachment_id
	 *
	 * @property string $filepath   Delegates to Sunrise_File
	 * @property string $dirname    Delegates to Sunrise_File
	 * @property string $basename   Delegates to Sunrise_File
	 * @property string $filename   Delegates to Sunrise_File
	 * @property string $extension  Delegates to Sunrise_File
	 * @property string $filemode   Delegates to Sunrise_File
	 * @property string $mime_type  Delegates to Sunrise_File
	 * @property string $size       Delegates to Sunrise_File
	 * @property string $url        Delegates to Sunrise_File
	 */
	class Sunrise_File_Field extends Sunrise_Field {
		static $upload_field = false;   // Used by wp_ajax methods to capture the current field and access within hooks.

		public $extensions    = false;  // Contains a comma-separated list of valid uploadable file extension
		protected $_max_size  = false;  // Must be 'public' to enable it to be set dynamically
//		protected $_file      = false;  // Capture array so a subclass can access within instance filters

		function do_initialize( $args ) {
			/*
			 * Handle file upload via ajax
			 */
			add_action( 'wp_ajax_sunrise_fields_upload_file', array( __CLASS__, 'wp_ajax_upload_file' ) );

			/*
			 * Delete uploaded file via ajax
			 */
			add_action( 'wp_ajax_sunrise_fields_delete_file', array( __CLASS__, 'wp_ajax_delete_file' ) );

			/*
			 * Capture uploaded file information
			 */
			add_filter( 'wp_handle_upload', array( __CLASS__, 'wp_handle_upload' ), 10, 2 );

			/*
			 * Filter attachment metadata; allow other fields to process such as generate thumbnails.
			 */
			add_filter( 'wp_generate_attachment_metadata', array( __CLASS__, 'wp_generate_attachment_metadata' ), 10, 2 );

			/*
			 * Prefilter uploaded file; ensure it is not too large
			 */
			add_filter( 'wp_handle_upload_prefilter', array( __CLASS__, 'wp_handle_upload_prefilter' ) );

		}
		/*
		 * Filter Hook to allow subclass::file_field_upload() to filter data if available
		 *
		 * @param array $file Elements of info about uploaded file, elements include: url, path, mime
		 * @param int $attachment_id Post ID of the attached postwhere post_type=='attachment' for the uploaded file
		 * @returns array $file
		 *
		 */
		static function wp_handle_upload( $file_args, $mode ) {
			if ( self::is_file_upload() ) {
				/*
				 * Check max file size when required
				 * Change from is_int() to is_numeric(): http://jeremy.zawodny.com/blog/archives/001503.html
				 */
				$file_args = self::$upload_field->apply_filters( 'file_field_handle_upload', $file_args );
				/*
				 * Save so subclass can access it.
				 */
				if ( isset( $file_args['file_obj'] ) ) {
					self::$upload_field->file = $file_args['file_obj'];
				} else {
					self::$upload_field->initialize_from_upload( $file_args );
				}
			}
			return $file_args;
		}
		/*
		 * @param array $file_args - Contains elements file, url and type.
		 */
		function initialize_from_upload( $file_args ) {
			$file_args['filepath'] = $file_args['file'];
			$file_args['mime_type'] = $file_args['type'];
			$file_args['size'] = filesize( $file_args['file'] );
			$this->file = $this->apply_filters( 'new_upload_file', $file_args );
		}
		function filter_new_upload_file( $file_args ) {
			return new Sunrise_File( $file_args );
		}
		/*
		 * Filter Hook to validate uploaded $file
		 *
		 * @param array $file Elements of info about uploaded file, elements include: name, type, tmp_name, error, size
		 *
		 */
		static function wp_handle_upload_prefilter( $file ) {
			if ( self::is_file_upload() ) {
				/*
				 * Check max file size when required
				 * Change from is_int() to is_numeric(): http://jeremy.zawodny.com/blog/archives/001503.html
				 */
				$field = self::$upload_field;
				if ( $field->max_size > 0  && intval( $file['size'] ) > $field->max_size ) {
					$size = sr_format_bytes( $file['size'], 1 );
					$max_size = sr_format_bytes( $field->max_size, 1 );
					$error_msg = __( 'The file uploaded [%s] is too large at %s. The maximum size allowed is %s.' );
					$file['error'] = sprintf( $error_msg, $file['name'], $size, $max_size );
				} else {
					$file = $field->apply_filters( 'file_field_handle_upload_prefilter', $file );
				}
			}
			return $file;
		}
		/*
		 * Filter Hook to allow specialized fields to post process files, i.e. Image Fields to generate thumbnails, etc.
		 *
		 * @param array $metadata
		 * @param int $attachment_id
		 * @returns array $metadata
		 *
		 */
		static function wp_generate_attachment_metadata( $metadata, $attachment_id ) {
			if ( self::is_file_upload() ) {
				$filter_name = 'file_field_generate_attachment_metadata';
				$metadata = self::$upload_field->apply_filters( $filter_name, $metadata, $attachment_id );
				self::$upload_field->attachment_id = $attachment_id; // Save so subclass can access it.
			}
			return $metadata;
		}
		/*
		 * Check if this current HTTP request is a Sunrise Field File Upload request.
		 *
		 * @returns bool
		 *
		 */
		static function is_file_upload() {
			return isset( $_POST['action'] ) && 'sunrise_fields_upload_file' == $_POST['action'];
		}
		function get_max_size() {
			if ( is_string( $this->_max_size ) )  {
				$this->_max_size = $this->_convert_max_size( $this->_max_size );
			}
			return $this->_max_size;
		}
		function set_max_size( $max_size ) {
			if ( is_numeric( $max_size ) ) {
				$this->_max_size = $max_size;
			} else {
				$this->_max_size = $this->_convert_max_size( $max_size );
			}
		}
		/*
		 * Calculate max file size in bytes
		 */
		protected function _convert_max_size( $max_size ) {
			if ( is_string( $max_size ) ) {
				$size = $max_size;
				$max_size = floatval( $size );        // Allow user to use value such as '1.3mb' as well
				if ( stripos( $size, 'mb' ) ) {
					$max_size *= 1048576;
				} elseif ( stripos( $size, 'kb' ) ) {
					$max_size *= 1024;
				}
			}
			return intval( $max_size ); // From now $this->_max_size is always integer, so we don't need to check is_numeric anymore
		}
		/**
		 * Enqueue scripts and styles to handle ajax functionality
		 * Defined as static because we just need to enqueue once (for all upload fields)
		 */
		static function admin_init() {
			Sunrise_Fields::enqueue_css( __FILE__, 'file' );

			Sunrise_Fields::enqueue_js( __FILE__, 'file', array(
				'dependencies'  => array( 'jquery-upload','sunrise-core' ),
			));

			Sunrise_Fields::enqueue_js( __FILE__, 'file', array(
				'handle'        => 'jquery-upload',
				'filepath'      => '/js/jquery.upload-1.0.2.min.js',
				'dependencies'  => array( 'jquery' ),
			));
		}
		/**
		 * Add additional information (object information, nonces) required to perform ajax actions
		 * Add field value HTML if present
		 *
		 * @param $html Default HTML of the field
		 * @return string HTML to display for this field
		 */
		function filter_entry_html( $html ) {
			global $sr_fields;

			$spinner =  esc_url( admin_url( "images/wpspin_light.gif" ) );
			$html = <<<HTML
<img src="{$spinner}" class="spinner hidden" />{$html}
HTML;

			/*
			 * $html contains default input box
			 * We just need to add HTML for field value if present
			 */

			$html .= $this->get_upload_html();

			/*
			 * Add the hiddle fields containing nonces, field type
			 */
			$hidden_inputs = $this->get_hidden_inputs_html();

			return $this->apply_filters( 'file_field_entry_html', "{$hidden_inputs}{$html}" );
		}
		/**
		 * Generate hidden fields for nonces and other needed parameters
		 * Used for ajax upload and delete files
		 *
		 * @return string HTML markup for nonces fields
		 */
		function get_hidden_inputs_html() {
			/*
			 * Nonces required to perform ajax actions
			 * Each action should have its own nonce
			 */
			$field_id = $this->id;
			$nonce_upload = wp_create_nonce( "sunrise_fields_upload_file_{$field_id}" );
			$nonce_delete = wp_create_nonce( "sunrise_fields_delete_file_{$field_id}" );

			$fields_html = <<<HTML
<input type="hidden" id="{$field_id}-field-type" value="{$this->field_type}" />
<input type="hidden" id="{$field_id}-upload-nonce" value="{$nonce_upload}" />
<input type="hidden" id="{$field_id}-delete-nonce" value="{$nonce_delete}" />
HTML;

			$fields_html = $this->apply_filters( 'file_field_hidden_inputs_html', $fields_html );

			return $fields_html;
		}
		/**
		 * Generate HTML for field value
		 *
		 * @return HTML of the field if it has any value
		 */
		function get_upload_html() {
			$html = '';
			$upload_class = $this->get_upload_class();
			$inner_upload_html = $this->get_inner_upload_html();
			$html = <<<HTML
<span id="uploaded-file-{$this->id}" class="{$upload_class}">{$inner_upload_html}</span>
HTML;
			return $html;
		}
		/*
		 *
		 */
		function get_inner_upload_html() {
			$html = '';
			if ( $this->has_valid_file_attached() ) {
				$file = $this->value;
				$delete_button_html = $this->get_delete_button_html();
				$html = <<<HTML
<a href="{$file->url}"><input type="text" value="{$file->basename}" disabled="disabled" size="{$this->entry_feature->size}" /></a>
{$delete_button_html}
<input type="hidden" if="{$this->id}-value" name="{$this->entry_name}" value="{$this->raw_value}" />
HTML;
			}
			return $html;
		}
		/*
		 *
		 */
		function get_upload_class() {
			$class = "uploaded-file";
			$class = $this->apply_filters( 'file_field_upload_class', $class );
			return $class;
		}
		/*
		 * TODO: This needs to be replaced with a Sunrise_Control in the future.
		 */
		function get_delete_button_html() {
			$delete = __( 'Delete' );
			$html =<<<HTML
<a href="#" class="sunrise-fields-delete-file-button button">{$delete}</a>
HTML;
			$html = $this->apply_filters( 'file_field_delete_button_html', $html );
			return $html;
		}
		/*
		 * Determine if this field has a valid file attached.
		 *
		 * @returns bool TRUE is a valid file is attached.
		 */
		function has_valid_file_attached() {
			return sr_has_valid_file_attached( $this->raw_value );
		}
		/**
		 * Add hidden class to input box if the field has a value
		 * @param string $classes Default classes
		 * @return string Classes
		 */
		function filter_entry_feature_class( $classes ) {
			if ( $this->has_valid_file_attached() ) {
				/*
				 * Let's hide the <input> field because we have a valid attachment
				 * and we'll be displaying it instead.
				 */
				$classes .= ' hidden';
			}
			return $classes;
		}
		/**
		 * Delete attachment when delete field value
		 * @return bool true if need to delete value, false if not
		 */
		function filter_delete_raw_value() {
			$attachment_id = $this->raw_value;
			if ( empty( $attachment_id ) )
				return false;

			wp_delete_attachment( $attachment_id );

			return $attachment_id; // let the sunrise::storage delete field value
		}
		/**
		 * Get value for file upload field
		 * @param int $attachment_id the attachment ID (raw field value)
		 * @return Sunrise_File
		 */
		function filter_value( $attachment_id ) {
			if ( ! $this->_value ) {
				$file = new Sunrise_File();
				$file->initialize_from( 'attachment_id', $attachment_id );
				$this->_value = $file;
			}
			return $this->_value;
		}
		/**
		 * Handle file upload error, used by self::wp_ajax_upload_file()
		 *
		 * @param array &$file
		 * @param string $message
		 * @return array
		 *
		 */
		static function wp_handle_upload_error( &$file, $message ) {
			if ( __( 'Sorry, this file type is not permitted for security reasons.' ) == $message ) {
				$message = __( 'The file [%s] is not an allowed type. Valid types are: %s' );

				/*
				 * Format the extensions list to look like the following:
				 *
				 *    vcf,vcard         => .VCF or .VCARD
				 *    gif,jpg,jpeg,png  => .GIF, .JPG, .JPEG or .PNG
				 *
				 */
				$extensions = explode( ',', self::$upload_field->extensions );
				if ( 1 >= count( $extensions ) ) {
					$last_extension = '';
				} else {
					$last_extension = array_pop( $extensions );
					$last_extension = __(' or ' ) . '.' . strtoupper( $last_extension );
				}
				$extensions = '.' . strtoupper( implode( ', .', $extensions ) ) . $last_extension;
				$message = sprintf( $message, $file['name'], $extensions );
			}
			return array( 'error' => $message );
		}
		/**
		 * Handle file upload via ajax
		 */
		static function wp_ajax_upload_file() {
			/**
			 *  @var Sunrise_File_Field $field
			 */
			$field = Sunrise_Fields::get_field_from_ajax_POST( 'upload_file' );

			$update_args = array();
			if ( $field->parent && 'fieldset' == $field->parent->field_type ) {
				$fieldset = $field->parent;
				if ( $fieldset->parent && 'repeating' == $fieldset->parent->field_type ) {
					$_FILES = sr_transform_uploaded_FILES( $_FILES );
					$update_args = array( 'storage_key' => $field->get_storage_key() );
				} else {
					sr_die( __( "Sunrise Fields does not yet support File Uploads for Fieldset not contained in a Repeating Field." ) );
				}
			}

			if ( ! isset( $_FILES[$field->entry_name] ) ) {
				sr_die( __( 'Unexpected: File not uploaded for the field named [%s].' ), $field->entry_name );
			}

			/*
			 * Check file upload for error
			 * If file is uploaded successfully, $uploaded_file_info contains array of file info
			 * Else it contains error string
			 */



			// Use hook 'wp_handle_upload_prefilter'
			self::$upload_field = $field;
			$attachment_id = media_handle_upload( $field->entry_name, $field->object_id, $_POST, array(
				'action' => 'sunrise_fields_upload_file',
				'upload_error_handler' => array( __CLASS__, 'wp_handle_upload_error' ),
				'mimes'  => $field->upload_mimes(), // Provide valid MIMEs based on extensions
				'time'   => current_time('mysql'),  // Don't use the /year/month/ from post_date for the upload directory
			));

			if ( is_wp_error( $attachment_id ) ) {
				die( Sunrise::format_ajax_response( $attachment_id->get_error_message() , 'error' ) );
			}

			do_action( 'sunrise_file_field_ajax_upload_post_media_handle_upload', $field, $attachment_id, $update_args ); // Intended to allow a sub-class to potentially override the default storage handling and JSON response (i.e., for sub-class specific file validation; i.e., Image Field)

			/*
			 * Write field value (attachment ID) to the storage
			 *
			 * TODO: Passing storage_key should not be required.
			 */
			$field->update_raw_value( $attachment_id, $update_args );

			$html = $field->apply_filters( 'file_field_upload_html', $field->get_upload_html() );

			$ajax_response = Sunrise::format_ajax_response( $html, 'success' );

			$ajax_response = apply_filters( // Intended to allow a sub-class to modify the response.
				'sunrise_file_field_ajax_upload_response',
				$ajax_response,
				$field,
				$attachment_id,
				$update_args
			);

			die( $ajax_response );
		}

		protected function extensions_as_array() {
			/**
			 * Find 1st comma (,) or vertical bar (|); really should only be one or the other
			 * but developers might provide the wrong one.
			 */
			if ( preg_match( '#.+?([,|]).+#', 'foo|bar|baz', $match ) )
				/**
				 * To array, array_filter() -> Remove duplicates
				 */
				$extensions = array_filter( explode( $match[1], $this->extensions ) );
			else
				$extensions = array( $this->extensions );
			return $extensions;
		}
		/*
		 * Returns list of MIME types based on provided extensions in the form returned by get_allowed_mime_types()
		 *
		 * @returns array $mime_types
		 *
		 */
		function upload_mimes() {
			/*
			 * If extensions are specified then we need to replace default list of acceptable $mime_types
			 */
			$mime_types = get_allowed_mime_types();

			if ( ! $this->extensions ) {
				$new_mime_types = $mime_types;

			} else {
				$new_mime_types = array();
				/**
				 * To array, array_filter() -> Remove duplicates
				 */
				$extensions = $this->extensions_as_array();
				/*
				 * Look thru each of the mime types provided by WordPress
				 */
				foreach( $mime_types as $mime_types_key => $mime_types_value ) {
					/*
					 * If a key has multiple extensions, explode it so we can do an isset() lookup
					 */
					if ( false !== strpos( $mime_types_key, '|' ) ) {
						$mime_types_keys = explode( '|', $mime_types_key );   // Break out multiple extensions
						foreach( $mime_types_keys as $mime_types_subkey ) {   // Explode mime types with multiple extensions
							$mime_types[$mime_types_subkey] = $mime_types[$mime_types_key];
						}
					}
					/*
					 * Scan thru each extension looking for a mime_type
					 * If found, remember it in the $new_mimes_types and remove from the potential extensions.
					 */
					foreach( $extensions as $index => $extension ) {
						$extension = strtolower( trim( $extension ) );
						if ( isset( $mime_types[$extension] ) ) {
							$new_mime_types[$extension] = $mime_types[$extension];
							unset( $extensions[$index] );
						}
					}
					/*
					 * If we've gotten MIMEs for all extensions, we're done.
					 */
					if ( 0 == count( $extensions ) ) {
						break;
					}
				}
				if ( count( $extensions ) ) {
					/*
					 * If we have any extensions left, see if we can find a hardcoded mime for them.
					 */
					foreach( $extensions as $extension ) {
						switch ( $extension ) {
							case 'vcard':
							case 'vcf':
								$new_mime_types[$extension] = 'text/vcard';
								break;

							default:
								Sunrise::_die( 'The extension [%s] specified for the field [%s] has no associated MIME type.', $extension, $this->field_name );
						}
					}
				}
			}
			return $new_mime_types;
		}
		/**
		 * Delete uploaded file via ajax
		 */
		static function wp_ajax_delete_file() {
			$field = Sunrise_Fields::get_field_from_ajax_POST( 'delete_file' );

			// self::ajax_check( $field );

			$attachment_id = $field->raw_value;
			if ( empty( $attachment_id ) )
				die( Sunrise::format_ajax_response( 'Field has no value.', 'error' ) );

			$file = $field->value;

			$field->delete_value();


			die( Sunrise::format_ajax_response( 'File deleted successfully.', 'success' ) );
		}

		function __set( $property_name, $value ) {
			switch ( $property_name ) {
				case 'file':
				case 'max_size':
				case 'attachment_id':
					call_user_func( array( &$this, "set_{$property_name}" ), $value );
					break;

				case 'basename':
				case 'dirname':
				case 'extension':
				case 'filemode':
				case 'filename':
				case 'filepath':
				case 'mime_type':
				case 'size':
				case 'url':
					$this->set_file_property( $property_name, $value );
					break;

				default:
					parent::__set( $property_name, $value );
					break;
			}
		}
		function __get( $property_name ) {
			$value = false;
			switch ( $property_name ) {
				case 'file':
				case 'max_size':
				case 'attachment_id':
					$value = call_user_func( array( &$this, "get_{$property_name}" ) );
					break;

				case 'basename':
				case 'dirname':
				case 'extension':
				case 'filemode':
				case 'filename':
				case 'filepath':
				case 'mime_type':
				case 'size':
				case 'url':
					$value = $this->get_file_property( $property_name );
					break;

				default:
					$value = parent::__get( $property_name );
					break;
			}
			return $value;
		}
		function get_attachment_id() {
			$attachment_id = $this->get_file_property( 'attachment_id' );
			if ( $false ) {
				$attachment_id = $this->_raw_value;
				if ( $attachment_id )
					$this->set_attachment_id( $attachement_id );
			}
			return $attachment_id;
		}
		function set_attachment_id( $attachment_id ) {
			$this->set_file_property( 'attachment_id', $attachment_id );
			$this->raw_value = $attachment_id;
		}
		function get_file() {
			if ( ! $this->_value ) {
				if ( $this->attachment_id ) {
					$this->_value = new Sunrise_File( array( 'attachment_id' => $this->attachment_id ) );
				} else {
					$this->_not_assigned_error( 'file' );
				}
			}
			return $this->_value;
		}
		function set_file( $file ) {
			$this->_value = $file;
		}
		function get_file_property( $property_name ) {
			$file = $this->get_file();
			if ( ! $file )
				$this->_not_assigned_error( 'file' );
			if ( property_exists( get_class( $file ), $property_name ) ) {
				$value = $file->{$property_name};
			} else if ( method_exists( get_class( $file ), $method_name = "get_{$property_name}" ) ) {
				$value = call_user_func( array( &$file, $method_name ) );
			} else {
				$this->_cannot_get_error( $property_name );
			}
			return $value;
		}
		function set_file_property( $property_name, $value ) {
			$file = $this->get_file();
			if ( ! $file )
				$this->_not_assigned_error( 'file' );
			if ( property_exists( get_class( $file ), $property_name ) ) {
				$file->{$property_name} = $value;
			} else if ( method_exists( get_class( $file ), $method_name = "set_{$property_name}" ) ) {
				call_user_func( array( &$file, $method_name ), $value );
			} else {
				$this->_cannot_set_error( $property_name, $value );
			}
		}
	}
}
