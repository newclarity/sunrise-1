<?php
/*
 *
 *
 * INDEPENDENT FIELD
_photo 		 => $post_id
_photo[crop] => '12,30'

AS A REPEATING FIELD
_photo 			=> $post_id
_photo[crop] 	=> '12,30'
_photo[2] 		=> $post_id
_photo[2][crop] => '12,30'
_photo[3] 		=> $post_id
_photo[3][crop] => '12,30'

AS A FIELDSET FIELD
_photos[caption] 	 => 'Foo Bar!'
_photos[image]   	 => $post_id
_photos[image][crop] => 24,16


AS A REPEATING FIELDSET FIELD
_photos[caption] 	 	=> 'Foo Bar!'
_photos[image]   	 	=> $post_id
_photos[image][crop] 	=> 24,16
_photos[2][caption] 	=> 'Foo Bar!'
_photos[2][image]   	=> $post_id
_photos[2][image][crop] => 24,16

 */

require_once( dirname( __FILE__ ) . '/file-field.php' );

if ( !class_exists( 'Sunrise_Image_Field' ) ) {
	/**
	 * @property Sunrise_Image_File $file
	 * @property int $width
	 * @property int $height
	 * @property int $max_width
	 * @property int $max_height
	 * @property int $max_crop
	 * @property Sunrise_Image_File $image_file
	 * @property int $attachment_id
	 */
	class Sunrise_Image_Field extends Sunrise_File_Field {
		public $image_type     = false;
		public $view_size      = false;
		public $show_crop_atts = false;  // Show crop attributes (width and height)
		public $required_size = false; // Require the image dimensions match those of the specified Registered Image Size

		protected $_width         = false;
		protected $_height        = false;
		protected $_max_width     = false;
		protected $_max_height    = false;
		protected $_max_crop      = false;


		function filter_new_upload_file( $file_args ) {
			return new Sunrise_Image_File( $this->image_type, $file_args );
		}
		function do_initialize( $args ) {
			/*
			 * First call File_Field::initialize()
			 */
			parent::do_initialize( $args );

			if ( ! isset( $args['image_type'] ) ) {

				$owner = $this->object_type;

				if ( $this->object_sub_type )
					$owner .= "/{$this->object_sub_type}";

				sr_die( __( "Image Type is not set for field [%s] in [%s]." ), $this->field_name, $owner );

			}

			if( isset( $args['required_size'] ) ) {
				$this->required_size = $args['required_size'];

				if ( ! sr_is_valid_image_type_size( $this->image_type, $this->required_size ) ) {
					sr_die( 'Required Size [%s] is not a valid image size for [%s]', $this->required_size, $this->image_type );
				}
			}

			/*
			 * Set 'default' for view_size
			 */
			if ( ! $this->view_size )
				$this->view_size = 'default';

			/*
			 * Set allowed extensions for images
			 */
			$this->extensions = Sunrise_Image_File::VALID_EXTENSIONS;

			/*
			 * Set HTML type = file
			 */
			$this->html_type = 'file';

			/*
			 * Handle image cropping via ajax
			 */
			add_action( 'wp_ajax_sunrise_fields_crop', array( __CLASS__, 'wp_ajax_crop' ) );

			/*
			 * Allow us to add image_type to attachment metadata
			 */
			add_action( 'filter_file_field_generate_attachment_metadata', array( __CLASS__, 'filter_file_field_generate_attachment_metadata' ), 10, 2 );

			add_action( 'sunrise_file_field_ajax_upload_post_media_handle_upload', array( $this, 'sunrise_file_field_ajax_upload_post_media_handle_upload' ), 9, 4 );
			add_filter( 'sunrise_file_field_ajax_upload_response', array( $this, 'filter_file_field_ajax_upload_response' ), 9, 4 );
		}
		static function filter_file_field_generate_attachment_metadata( $metadata, $attachment_id ) {
			if ( isset( $_POST['image_type'] ) ) {
				$image_type = $_POST['image_type'];
				$metadata['image_type'] = $image_type;
				sr_update_attached_image_type( $attachment_id, $image_type );
			}
			return $metadata;
		}
		/**
		 * Enqueue scripts and styles to handle ajax functionality
		 * Defined as static because we just need to enqueue once (for all upload fields)
		 */
		static function admin_init() {
			wp_enqueue_script( 'imgareaselect' );
			wp_enqueue_style( 'imgareaselect' );

			Sunrise_Fields::enqueue_css( __FILE__, 'image', array(
				'handle' => 'jquery-colorbox',
				'filepath' => '/colorbox-1.3.19/jquery.colorbox.css'
			) );

			Sunrise_Fields::enqueue_js( __FILE__, 'image', array(
				'handle' => 'jquery-colorbox',
				'filepath' => '/colorbox-1.3.19/jquery.colorbox.js',
//				'filepath' => '/colorbox-1.3.19/jquery.colorbox.min.js'
					'dependencies' => array( 'jquery' )
			) );
			Sunrise_Fields::enqueue_css( __FILE__, 'image', array(
				'dependencies' => array(
					'jquery-colorbox',
					'jcrop',
				)
			));
			Sunrise_Fields::enqueue_js( __FILE__, 'image', array(
				'dependencies' => array(
					'jquery-colorbox',
					'jcrop',
					'sunrise-core',
					'sunrise-fields-admin-script',
					'sunrise-fields-file',
				)
			) );
		}
		/**
		 *  Get the full filepath for the current file
		 *
		 *  @param string $image_size A valid image size for the current image type. See sr_register_image_type();
		 *  @return string|bool The full filepath if $image_size is valid, or false.
		 */
		function get_sized_filepath( $image_size ) {
			$filepath = false;
			if ( $this->file ) {
				$filepath = $this->file->get_sized_filepath( $image_size );
			}
			return $filepath;
		}
		/**
		 *  @return void
		 */
		function delete_sized() {
			/**
			 * @var Sunrise_Image_File $file
			 */
			if ( $file = $this->value ) {
				$file->delete_sized();
				$image_crop = new Sunrise_Image_Crop( $file );
				$image_crop->delete( $this->attachment_id );
			}
		}
		/**
		 * Generate hidden fields
		 *
		 * @param string $html HTML of hidden fields
		 * @return string HTML of hidden fields
		 *
		 */
		function filter_file_field_hidden_inputs_html( $html ) {

			$field_id = $this->id;
			$nonce_crop = wp_create_nonce( "sunrise_fields_crop_{$field_id}" );

			$html .= <<<HTML
\n<input type="hidden" id="{$field_id}-crop-nonce" value="{$nonce_crop}" />
<input type="hidden" id="{$field_id}-max-size" value="{$this->max_size}" />
<input type="hidden" id="{$field_id}-image-type" value="{$this->image_type}" />
HTML;

			if ( $aspect_ratio = sr_get_image_type_aspect_ratio( $this->image_type ) ) {
				$html .= <<<HTML
<input type="hidden" id="{$field_id}-aspect-ratio" value="{$aspect_ratio}" />
HTML;
			}

			/**
			 * @var Sunrise_Image_File $image_file
			 */
			$image_file = $this->value;
			if ( empty( $image_file ) || ! $this->has_valid_file_attached() ) {
				$crop_selection = '';
			} else {
				$image_crop = new Sunrise_Image_Crop( $image_file, array(
					'image_size'    => $this->view_size,
					'attachment_id' => $this->attachment_id,
				));
				/*
				 * Add crop position if needed
				 * The array $crop_attributes must be in the order of left, top, width, height
				 *   which matches
				 */
				$crop_selection = $image_crop->has_crop ? implode( ',', $image_crop->as_selection() ) : '';
			}
			$html .= <<<HTML
<input type="hidden" id="{$field_id}-crop-coordinates" class="crop-coordinates" value="{$crop_selection}" />
HTML;

			return $html;
		}

		function sunrise_file_field_ajax_upload_post_media_handle_upload ( $field, $attachment_id, $update_args ) {
			if ( ! is_object( $field ) || ! is_a( $field, 'Sunrise_Image_Field' ) ) {
				return; // Cannot validate
			}

			if ( ! $field->required_size ) {
				return; // No additional validation is required.
			}

			$image_size_data = sr_get_image_type_size( $field->image_type, $field->required_size );

			$image_file = sr_get_image_file(
				array(
					'attachment_id' => $attachment_id,
					'image_type'    => $field->image_type
				)
			);

			if ( $image_file->get_width() !== $image_size_data['width'] ||
					 $image_file->get_height() !== $image_size_data['height']
			) {

				// Delete the attachment
				wp_delete_attachment( $attachment_id, true );

				// Exit with a JSON error
				die( Sunrise::format_ajax_response(
					sprintf(
						__( 'This photo must be exactly %d pixels wide and %d pixels tall.' ),
						$image_size_data['width'],
						$image_size_data['height']
					),
					'error'
				) );
			}
		}

		function filter_file_field_ajax_upload_response ( $json_encoded, $field, $attachment_id, $update_args ) {
			if ( ! is_object( $field ) || ! is_a( $field, 'Sunrise_Image_Field' ) ) {
				return $json_encoded; // Cannot process
			}

			if ( ! $field->required_size ) {
				return $json_encoded; // No modification to JSON response is required (cropping does not need to be omitted)
			}

			$json_decoded = json_decode( $json_encoded );

			$json_decoded->omit_cropping = true;

			return json_encode( $json_decoded );
		}

		/**
		 * Handle image cropping via ajax
		 */
		static function wp_ajax_crop() {
			/**
			 * @var Sunrise_Image_Field $field
			 */
			$field = Sunrise_Fields::get_field_from_ajax_POST( 'crop' );

			$image_type = filter_input( INPUT_POST, 'image_type', FILTER_SANITIZE_STRING );
			if( empty( $image_type ) ) {
				$image_type = $field->image_type;
			}

			$field->file = new Sunrise_Image_File( $image_type );
			$field->file->initialize_from( 'attachment_id', $field->attachment_id );

			$field->delete_sized();

			$image_crop = new Sunrise_Image_Crop( $field->file, array(
				'selection' => sr_get_from_POST( 'selection' ),
			));
			$image_crop->update( $field->attachment_id );
			sr_update_attached_image_type( $field->attachment_id, $image_type );

			$html = $field->get_upload_html();

			die( Sunrise::format_ajax_response( $html, 'success' ) );
		}
		function filter_file_field_upload_class( $class ) {
			return "uploaded-image {$class}";
		}
		/**
		 * Get value for file upload field
		 * @param int $attachment_id the attachment ID (raw field value)
		 * @return Sunrise_File
		 */
		function filter_value( $attachment_id ) {
			if ( $attachment_id && ! $this->_value ) {
				$image_file = new Sunrise_Image_File( $this->image_type );
				$image_file->initialize_from( 'attachment_id', $attachment_id );
				$this->_value = $image_file;
			}
			return $this->_value;

		}
		function filter_file_field_upload_html( $html ) {
			$this->image_file->attachment_id = $this->raw_value;
			return $html;
		}
		/**
		 * Delete sized images after calling file to delete attachment
		 * @return bool true if need to delete value, false if not
		 */
		function filter_delete_raw_value() {
			$attachment_id = parent::filter_delete_raw_value();
			if ( ! empty( $attachment_id ) ) {
				$this->delete_sized();
			}
			return $attachment_id; // true => let the sunrise::storage delete field value
		}
		/**
		 *  NOTE: Important!!! The href MUST match the wrapper ID for the crop html.
		 */
		function get_inner_upload_html() {
			$html = '';
			if ( $this->has_valid_file_attached() ) {
			/**
			 * @var Sunrise_Image_File $image_file
			 */
				$image_file = $this->value;
				$crop_window = $this->get_crop_html();
				/**
				 * @var Sunrise_Image_File $sized_image_file
				 */
				$sized_image_file = $image_file->as_sized( $this->view_size );
				$hwstring = $sized_image_file->hwstring;
				$delete_button_html = $this->get_delete_button_html();
				$timestamp = time();

				$after_image_html = apply_filters( 'sunrise_image_field-after_uploaded_image_html', '', $sized_image_file );
				/**
				 * DO NOT CHANGE href WITHOUT READING NOTE ABOVE
				 */
				$show_crop_link_anchor_open = '<a class="show-crop-link" href="#' . $this->id . '-crop-window">';
				$show_crop_link_anchor_close = '</a>';

				if ( $this->required_size ) { // Omit the show-crop-link anchor class if cropping isn't needed.
					$show_crop_link_anchor_open = '<a>'; // Keeping the anchor element for layout/styling purposes
				}

				$html = <<<HTML
{$show_crop_link_anchor_open}
<img src="{$sized_image_file->url}?{$timestamp}&nocache&ModPagespeed=off" {$hwstring} class="cropped-image" />
{$show_crop_link_anchor_close}
{$delete_button_html}
<div style="display:none">
{$crop_window}
</div>
<input type="hidden" id="{$this->id}-actual-image-dimensions" value="{$image_file->dimensions}" />
<input type="hidden" if="{$this->id}-value" name="{$this->entry_name}" value="{$this->raw_value}" />
{$after_image_html}
HTML;
			}
			return str_replace( array( "\t", "\n" ), '', $html );
		}
		/**
		 * Get HTML for cropping window
		 *
		 * @return string HTML markup for cropping window on success, error string on failure
		 *
		 *  NOTE: Important!!! The ID for the crop html MUST match the href for the link that triggers cropping
		 */
		function get_crop_html() {
			list( $width, $height ) = $this->get_constrained_image_dimensions();
			$help = __( 'Click and drag box to desired position. Toggles on crop box can be clicked and dragged to resize the box. When finished, click the save button.' );
            $help = apply_filters( 'sunrise_fields_image_cropper_help_text', $help );
			/**
			 * DO NOT CHANGE id WITHOUT READING NOTE ABOVE
			 */
			$html = <<<HTML
<div class="crop-window" id="{$this->id}-crop-window">
<img src="{$this->file->url}" width="{$width}" height="{$height}" class="actual-image" />
<p class="help">{$help}</p>
<div class="crop-window-footer">
HTML;
			if ( $this->show_crop_atts ) {
				$w = __( 'W', 'sunrise-fields' );
				$h = __( 'H', 'sunrise-fields' );
				$html .= <<<HTML
<p class="select-properties">
	<strong>{$w}</strong> <input class="w" disabled="disabled" />
	<strong>{$h}</strong> <input class="h" disabled="disabled" />
</p>
HTML;
			}
			$cancel = apply_filters( 'sunrise_fields_image_cropper_cancel_text', __( 'Cancel', 'sunrise-fields' ) );
			$crop_n_save = apply_filters( 'sunrise_fields_image_cropper_submit_text', __( 'Crop & Save', 'sunrise-fields' ) );
			$html .= <<<HTML
<p class="crop-buttons">
	<a href="#" id="{$this->id}-crop-button-cancel" class="button cancel">{$cancel}</a>
	<a href="#" id="{$this->id}-crop-button-save"  class="button save">{$crop_n_save}</a>
</p>
</div>
</div>
HTML;
			return $html;
		}
		function set_max_width( $value ) {
			$this->_max_width = $value;
		}
		function get_max_width() {
			if ( ! $this->_max_width ) {
				$this->_assign_max_crops();
			}
			return $this->_max_width;
		}
		function set_max_height( $value ) {
			$this->_max_height = $value;
		}
		function get_max_height() {
			if ( ! $this->_max_height ) {
				$this->_assign_max_crops();
			}
			return $this->_max_height;
		}
		function set_image_file( $image_file ) {
			$this->value = $image_file;
		}
		function get_image_file() {
			return $this->value;
		}
		function set_attachment_id( $attachment_id ) {
			$attachment_id = false === $attachment_id ? false : intval( $attachment_id );
			$this->raw_value = $attachment_id;
			/**
			 * @var Sunrise_Image_File $image_file
			 */
			if ( $image_file = &$this->value ) {
				$image_file->attachment_id = $attachment_id;
			}
		}
		function get_attachment_id() {
			return false === $this->raw_value ? false : intval( $this->raw_value );
		}
		private function _assign_max_crops() {
			if ( is_numeric( $this->_max_crop ) ) {
				$this->_max_width = $this->_max_height = $this->_max_crop;
			} else if ( is_string( $this->_max_crop ) && false !== strpos( $this->max_crop, 'x' ) ) {
				list( $this->_max_width, $this->_max_height ) = explode( 'x', $this->max_crop );
			}
		}
		/*
		 *
		 */
		function get_constrained_image_dimensions() {
			$max_width = '*' == $this->max_width ? 0 : $this->max_width;
			$max_height = '*' == $this->max_height ? 0 : $this->max_height;
			$image_file = $this->file;
			return wp_constrain_dimensions( $image_file->width, $image_file->height, $max_width, $max_height );
		}
		function __set( $property_name, $value ) {
			switch ( $property_name ) {
				case 'max_width':
				case 'max_height':
				case 'crop':
				case 'image_file':
				case 'attachment_id':
					call_user_func( array( &$this, "set_{$property_name}" ), $value );
					break;

				default:
					parent::__set( $property_name, $value );
					break;
			}
		}
		function __get( $property_name ) {
			$value = false;
			switch ( $property_name ) {
				case 'max_width':
				case 'max_height':
				case 'image_file':
				case 'attachment_id':
					$value = call_user_func( array( &$this, "get_{$property_name}" ) );
					break;

				default:
					$value = parent::__get( $property_name );
					break;
			}
			return $value;
		}
	}

	/**
	 * Get's the URL for a named image field. Expects $post_id as an $arg.
	 *
	 * @param string $field_name
	 * @param array $args
	 * @return bool|string
	 *
	 */
	function sr_get_image_field_url( $field_name, $args = array() ) {
		$url = false;
		$args = sr_parse_args( $args, array(
			'image_size' => 'default',
			'post_id' => @$GLOBALS['post']->ID,
		));
		/**
		 * @var Sunrise_Image_File $image_file
		 */
		$image_file = sr_get_the_field( $field_name, array( 'post_id' => $args['post_id'] ) );
		if ( $image_file ) {
			$image_file->filepath = $image_file->get_sized_filepath( $args['image_size'] );
			$url = $image_file->url;
		}
		return $url;
	}
}
