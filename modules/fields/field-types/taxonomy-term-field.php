<?php
/*
 * Assocate a SINGLE term to the object for which it is a field.
 * Current ONLY supports 'taxonomy' storage, not in 'meta' or other.
 * TODO: Add ability to associate multiple terms?
 * TODO: Decide if there any reason to support any other storage besides 'taxonomy'?
 */

/**
 * @property array $exclude_ids - Array of term IDs to exclude.
 * @property array $selected
 *
 */
class Sunrise_Taxonomy_Term_Field extends Sunrise_Select_Field {
	var $taxonomy     = false;
	var $display      = false;
	/**
	 * @var bool|array $exclude - Array of excluded taxonomy terms
	 */
	var $exclude 	  = false;
	var $exclude_tree = false;
	var $hide_empty   = false;
	var $orderby      = false;
	/**
	 * @var bool $multi_select - If true, users can select more than one taxonomy term
	 */
	var $multi_select = false;

	protected $_exclude_ids = false;

	public static function on_load() {
		add_action( 'admin_init', array( __CLASS__, 'admin_init' ), 0 ); // 0 - Do this very early
	}

	static function admin_init() {
		Sunrise_Fields::enqueue_js( __FILE__, 'taxonomy_term', array(
			'dependencies' => array( 'jquery-ui-core' ),
		) );
		// Setup AJAX action for list item insertion
		add_action( 'wp_ajax_insert_list_element', array( __CLASS__, 'wp_ajax_insert_list_element' ) );
	}

	static function wp_ajax_insert_list_element() {
		/**
		 * @var Sunrise_Taxonomy_Term_Field $field
		 */
		$field = Sunrise_Fields::get_field_from_ajax_POST( 'insert_list_element' );


		$status = 'error';
		if( ! isset( $_POST['term_id'] ) ) {
			$message = sprintf( __( 'ERROR: term_id not set for field: %s.', 'sunrise' ), $field->field_name );
		} else {
			if( $html = $field->_insert_list_item( $_POST['term_id'], $field->taxonomy, $field->field_name ) ) {
				$status = 'success';
				$message = esc_html( $html );
			} else {
				$message = __( 'ERROR: The requested post_id is invalid. Unable to generate list item.', 'sunrise' );
			}
		}
		die( Sunrise::format_ajax_response( $message, $status ) );
	}

	protected function _insert_list_item( $term_id, $taxonomy, $field_name ) {
		if( $term = get_term( $term_id, $taxonomy ) ) {
			return '<li>' . $term->name . '<input type="hidden" name="' . $field_name . '[]" value="' . $term->term_id . '" /><span>X</span></li>';
		}
		return false;
	}

	function do_initialize( $args ) {
		if ( $args['storage_type'] && 'taxonomy' != $args['storage_type'] ) {
			sr_die( "Taxonomy Term fields currently only support the 'taxonomy' storage method." );
		}
		$args['storage_type'] = 'taxonomy';
		$this->exclude = isset($args['exclude']) ? $args['exclude']: $this->exclude;
        $this->exclude = apply_filters( 'sr_fields_tax_term_field_exclusions', $this->exclude, $this );

		/*
		 * $this->exclude can be comma-separated term slugs, or array of term slug
		 * Convert comma-separated to array if needed
		 */
		if ( $this->exclude ) {
			if ( is_string( $this->exclude ) ){
				$this->exclude = explode( ',', $this->exclude );
			}
			if ( ! is_array( $this->exclude ) ){
				$this->exclude = array( $this->exclude );
			}
			/**
			 * TODO (mikes): Add validation if we now have an array that is not a list of IDs
			 */
		}

		if ( ! $this->orderby )
			$this->orderby = 'name';  // TODO: Verify that we should default here and not elsewhere

		parent::do_initialize( $args );

	}

	function filter_select_field_default_text( $default_text ) {
		if( ! $this->_default_text ) {
			$taxonomy = get_taxonomy( $this->taxonomy );
			if ( empty( $taxonomy->labels->singular_name ) ) {
				$default_text = __( 'Please select one' );
			} else {
				$default_text = sprintf( __( 'Please select a %s' ), strtolower( $taxonomy->labels->singular_name ) );
			}
			$this->default_text = $default_text;
		}
		return $default_text;
	}

	function filter_entry_feature_container_class( $class ) {
		$class = trim( "{$class} suggested" . ($this->multi_select ? ' multi-select' : '') );
		return $this->apply_filters( 'taxonomy_term_field_entry_class', $class );
	}

	function get_exclude_ids() {
		if( !$this->_exclude_ids && $this->exclude ){
			/**
			 * @var stdClass $terms - SR_DOCS_Taxonomy_Terms
			 */
			$terms = get_terms( $this->taxonomy, array(
				'hide_empty' => false
			) );
			$this->_exclude_ids = array();
			foreach ( $terms as $term ) {
				if ( in_array( $term->slug, $this->exclude ) )
					$this->_exclude_ids[] = $term->term_id;
				else if ( in_array( $term->term_id, $this->exclude ) )
					$this->_exclude_ids[] = $term->term_id;
			}
			asort( $this->_exclude_ids );
		} else {
			$this->_exclude_ids = array();
		}
		return $this->_exclude_ids;
	}

	function filter_value( $value ) {
		$term = get_term( $value, $this->taxonomy );
		return is_wp_error( $term ) ? false : $term;
	}

	/**
	 * Display HTML of the field
	 * @return string HTML
	 */
	function get_entry_html() {
		global $post;
		$args = Sunrise::args_from_object( $this,
			array( 'name', 'id', 'taxonomy', 'hide_empty', 'orderby', 'selected' )
		);
		$args['echo'] = false;

		if ( $this->exclude_tree ){
			$args['exclude_tree'] = $this->_exclude_ids;
		} else {
			/**
			 * 'Exclude' should be a comma separated list of ids in ascending order
			 */
			// TODO: Find out why $this->exclude_ids is set to 'taxonomy' and fix the hack  below!
			$exclude = $this->exclude_ids;
			$args['exclude'] = implode( ',', $this->_exclude_ids );
		}

		/*
		 * Show dropdown list as a 'tree' if needed
		 * Note: 'depth' is set to a big enough number to make sure all term levels are listed
		 */
		if ( 'tree' == $this->display ) {
			$args['hierarchical'] = true;
			$args['depth'] = 99;
		}

		$args['show_option_all'] = $this->default_text;
		$args['selected'] = $this->multi_select ? false : $this->selected;

		// Ensure the field name is set properly.
		$args['name'] = $this->entry_name;

		$html = wp_dropdown_categories( $args );

		if( $this->multi_select ) {
			$html .= '<ul class="sortable">';
			if( $terms = wp_get_object_terms( $post->ID, $this->taxonomy, array( 'orderby' => 'term_order' ) ) ) {
				foreach ( $terms as $term ) {
					$html .= $this->_insert_list_item( $term->term_id, $this->taxonomy, $this->field_name );
				}
			}
			$html .= '</ul><div class="clear"></div>';
		}
		$html .= $this->hidden_inputs();
		return $html;
	}

	function get_selected() {
		$term = $this->value;
		$selected = $term ? $term->term_id : 0;
		if( ! $selected && $this->default ) {
			$selected = $this->default;
		}
		return $selected;
	}

	function get_storage_key() {
		/**
		 * @var string $entry_name - Regex strips off trailing '[0]', if one is there, which it will be for indentable.
		 */
		$entry_name               = preg_replace( '#^(.*?)(\[0\])?$#', '$1', $this->entry_name );
		$is_first_repeating_field = ($this->parent && 'repeating' == $this->parent->field_type && 1 == $this->index);
		$is_first_fieldset_field  = ($this->parent && 'fieldset' == $this->parent->field_type && 1 == $this->parent->index);
		if ( $is_first_repeating_field || $is_first_fieldset_field ) {
			$entry_name = str_replace( '[1]', '', $entry_name );
		}
		$storage_key = '_' . $entry_name;
		return $storage_key;
	}

	/**
	 * NOTE: In order for taxonomy terms to be orderable, the taxonomy must set the 'sort' argument to true.
	 *
	 * @param       $value
	 * @param array $args
	 */
	function update_value( $value, $args = array() ) {
		$value = array_filter( array_unique( array_map( 'intval', (array) $value ) ) );
		$value = $this->apply_filters( 'update_value', $value, $args );
		wp_set_object_terms( $args['object_id'], $value, $this->taxonomy );
		// TODO: Not sure why WordPress stopped updating the post count for taxonomies, but we are going to force it here.  This is a hack. ( Micah 2013-4-17 )
		_update_generic_term_count( wp_get_object_terms( $args['object_id'], $this->taxonomy, array( 'fields' => 'ids' ) ), $this->taxonomy );
	}

	/**
	 * Generate hidden fields for saving the "combined" time in correct format
	 * Add nonce for checking time format
	 * @return string HTML markup
	 */
	function hidden_inputs() {
		$field_id   = $this->id;
		$action = "sunrise_fields_insert_list_element_{$field_id}";
		$nonce      = wp_create_nonce( $action );
		$input_html = '<input type="hidden" id="' . $field_id . '-field-type" value="' . $this->field_type . '" />';
		$input_html .= '<input type="hidden" id="' . $field_id . '-insert-list-element" value="' . $nonce . '" />';
		$input_html = $this->apply_filters( 'taxonomy_term_field_hidden_inputs_html', $input_html );
		return $input_html;
	}

	function __set( $property_name, $value ) {
		switch ( $property_name ) {
			case 'exclude_ids':
				$this->_cannot_set_error( $property_name, $value );
				break;

			default:
				parent::__set( $property_name, $value );
				break;
		}
	}

	function __get( $property_name ) {
		switch ( $property_name ) {
			case 'name':
				$value = $this->field_name;
				break;

			case 'exclude_ids':
			case 'selected':
			case 'storage_key':
				$value = call_user_func( array( &$this, "get_{$property_name}" ) );
				break;
			case 'storage_type':
				$value = 'taxonomy';
				break;

			default:
				$value = parent::__get( $property_name );
				break;
		}
		return $value;
	}

}

Sunrise_Taxonomy_Term_Field::on_load();