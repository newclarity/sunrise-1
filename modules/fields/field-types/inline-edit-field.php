<?php

class Sunrise_Inline_Edit_Field extends Sunrise_Field {

	var $post_type = 'post';
	var $form_name = false;

	function do_initialize( $args ) {
		$this->post_type = isset( $args['post_type'] ) ? $args['post_type']: 'post';
		$this->form_name = isset( $args['form_name'] ) ? $args['form_name']: false;
	}

	function get_entry_html() {
		global $sr_forms, $sr_fields, $sr_field_types;
		$html = '';
		$form = isset( $sr_forms['post'][$this->post_type][$this->form_name] ) ? $sr_forms['post'][$this->post_type][$this->form_name]: false;
		if( $form ) {
			$html = self::get_form_html( $this->form_name, $form->args );
		}
		return $html;
	}

	static function get_form_html( $form_name, $args = array() ){
		$form = Sunrise_Forms::get_form( $form_name, $args );
		$form->object = $args['object'];
		$html = $form->get_html( $args );
		return $html;
	}

}
