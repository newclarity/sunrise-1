<?php
/**
 * Color Picker Field
 *
 * @author Valery Satsura <valera@newclarity.net>
 */

if ( ! class_exists( 'Sunrise_Color_Field' ) ) {
	class Sunrise_Color_Field extends Sunrise_Field {

		function do_initialize( $args ) {
			$this->html_type = 'text';
		}

		/**
		 * Enqueue scripts and styles to handle ajax functionality
		 * Defined as static because we just need to enqueue once (for all upload fields)
		 */
		static function admin_init() {
			wp_enqueue_script( 'farbtastic' );
			wp_enqueue_style( 'farbtastic' );

			Sunrise_Fields::enqueue_js( __FILE__, 'color', array(
				'dependencies' => array( 'farbtastic' )
			) );

			Sunrise_Fields::enqueue_css( __FILE__, 'color', array(
				'dependencies' => array( 'farbtastic' )
			) );
		}

		function get_entry_html() {
			$html = <<<HTML
<input id="{$this->id}" name="{$this->entry_name}" type="text" {$this->class_html}{$this->attributes_html} />
<div class="field-color-picker" rel="{$this->id}"></div>
HTML;

			return $html;			
		}
	}
}