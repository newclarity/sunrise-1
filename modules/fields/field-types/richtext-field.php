<?php

require_once(dirname( __FILE__ ) . '/textarea-field.php'); // Make sure it has been defined first.

if ( ! class_exists( 'Sunrise_Richtext_Field' ) ) {

	class Sunrise_Richtext_Field extends Sunrise_Textarea_Field {

		function do_initialize( $args ) {
			// Detect if WordPress has already enqueued TinyMCE
			add_action( 'admin_footer', array( $this, 'admin_footer' ) );
			// Customize layout of richtext field
			add_filter( 'tiny_mce_before_init', array( $this, 'tinyMCE' ) );
		}

		static function admin_init(){
			Sunrise_Fields::enqueue_js( __FILE__, 'richtext' );
			wp_localize_script( 'sunrise-fields-richtext', 'sunriseFieldsRichText', array('settings' => json_encode( self::json_settings() ) ) );
			Sunrise_Fields::enqueue_css( __FILE__, 'richtext' );
		}

		function admin_footer() {
			// Check if WordPress (or this class) has already included TinyMCE script
			if ( ! has_action( 'admin_print_footer_scripts', 'wp_tiny_mce' ) ) {
				add_action( 'admin_print_footer_scripts', 'wp_tiny_mce', 25 );
			}
		}

		static function json_settings(){
			$settings = array (
				'mode' => 'specific_textareas',
				'editor_selector' => 'richtext',
				'width' => '500px',
				'theme' => 'advanced',
				'skin' => 'wp_theme',
				'theme_advanced_buttons1' => 'pastetext,|,bold,italic,removeformat,|,bullist,numlist,|,indent,outdent,|,link,unlink,|,code,fullscreen',
				'theme_advanced_buttons2' => '|',
				'theme_advanced_buttons3' => 'formatselect,|,undo,redo,|,spellchecker,iespell,|,charmap,|,help',
				'theme_advanced_buttons4' => '',
				'language' => 'en',
				'spellchecker_languages' => '+English=en,Danish=da,Dutch=nl,Finnish=fi,French=fr,German=de,Italian=it,Polish=pl,Portuguese=pt,Spanish=es,Swedish=sv',
				'theme_advanced_toolbar_location' => 'top',
				'theme_advanced_toolbar_align' => 'left',
				'theme_advanced_statusbar_location' => 'none',
				'theme_advanced_resizing' => true,
				'theme_advanced_resize_horizontal' => true,
				'dialog_type' => 'modal',
				'formats' => array(
					'alignleft' => array(
						array(
							'selector' => 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li',
							'styles' => array(
								'textAlign' => 'left'
							)
						),
						array(
							'selector' => 'img,table',
							'classes' => 'alignleft',
						),
					),
					'aligncenter' => array(
						array(
							'selector' => 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li',
							'styles' => array(
								'textAlign' => 'center'
							)
						),
						array(
							'selector' => 'img,table',
							'classes' => 'aligncenter',
						),
					),
					'alignright' => array(
						array(
							'selector' => 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li',
							'styles' => array(
								'textAlign' => 'right'
							)
						),
						array(
							'selector' => 'img,table',
							'classes' => 'alignright',
						),
					),
					'strikethrough' => array(
						'inline' => 'del',
					),
				),
				'relative_urls' => false,
				'remove_script_host' => false,
				'convert_urls' => false,
				'apply_source_formatting' => false,
				'remove_linebreaks' => true,
				'gecko_spellcheck' => true,
				'keep_styles' => false,
				'entities' => '38,amp,60,lt,62,gt',
				'accessibility_focus' => true,
				'tabfocus_elements' => 'major-publishing-actions',
				'media_strict' => false,
				'paste_remove_styles' => true,
				'paste_remove_spans' => true,
				'paste_strip_class_attributes' => 'all',
				'paste_text_use_dialog' => true,
				'extended_valid_elements' => 'article[*],aside[*],audio[*],canvas[*],command[*],datalist[*],details[*],embed[*],figcaption[*],figure[*],footer[*],header[*],hgroup[*],keygen[*],mark[*],meter[*],nav[*],output[*],progress[*],section[*],source[*],summary,time[*],video[*],wbr',
				'wpeditimage_disable_captions' => '',
				'wp_fullscreen_content_css' => includes_url('js/tinymce') . '/plugins/wpfullscreen/css/wp-fullscreen.css',
				'plugins' => 'inlinepopups,spellchecker,tabfocus,paste,media,wordpress,fullscreen,wpeditimage,wpgallery,wplink,wpdialogs',
				'content_css' => SUNRISE_FIELDS_URL . 'css/richtext-field.css',
				'theme_advanced_blockformats' => 'Body=p,Subhead=h3',
			);
			return $settings;
		}

		function tinyMCE( $settings ) {
			$settings['editor_selector'] = 'richtext';
			$settings['width'] = '500px';
			/**
			 * TODO (mikes 2012-02-10): Why are theme_advanced_buttons also set in $this->json_settings()?
			 * TODO: Two places to change means chances of
			 */
			$settings['theme_advanced_buttons1'] = 'pastetext,|,bold,italic,removeformat,|,bullist,numlist,|,indent,outdent,|,link,unlink,|,code,fullscreen';
			$settings['theme_advanced_buttons2'] = '|';
			$settings['theme_advanced_buttons3'] = 'formatselect,|,undo,redo,|,spellchecker,iespell,|,charmap,|,help';
			$settings['theme_advanced_buttons4'] = '';
			$settings['theme_advanced_statusbar_location'] = 'none';
			$settings['plugins'] = 'inlinepopups,spellchecker,tabfocus,paste,media,wordpress,fullscreen,wpeditimage,wpgallery,wplink,wpdialogs';
			$settings['content_css'] = SUNRISE_FIELDS_URL . 'css/richtext-field.css';
			$settings['theme_advanced_blockformats'] = 'Body=p,Subhead=h3';
			return $settings;
		}

		function filter_value( $value ) {
			// Process shortcodes only on front end
			if( !is_admin() ) {
				$value = do_shortcode( $value );
			}
			return wpautop($value);
		}

		function filter_entry_feature_class( $class ) {
			return trim( "{$class} richtext" );
		}

	}

}
