<?php

require_once( dirname( __FILE__ ) . '/select-field.php' ); // Make sure it has been defined first.

if ( ! class_exists( 'Sunrise_Form_Select_Field' ) ) {

	class Sunrise_Form_Select_Field extends Sunrise_Select_Field {

		var $form_type = false;
		var $forms = array();
		var $image_dropdown = false;

		static function on_load () {
			add_action( 'admin_init', array( __CLASS__, 'admin_init' ), 0 ); // 0 - Do this very early!
		}

		static function admin_init () {
			Sunrise_Fields::enqueue_js( __FILE__, 'form_select' );
			Sunrise_Fields::enqueue_css( __FILE__, 'form_select' );
			add_action( 'wp_ajax_form_select_field::load_form', array( __CLASS__, 'wp_ajax_load_form' ) );
		}

		function do_initialize ( $args ) {
			// Set options
			$options = array();
			foreach ( $args['forms'] as $name => $form ) {
				$options[$name] = $form['title'];
			}
			$this->options = $options;

			foreach ( $this->forms as $form_name => $form ) {
				$form = wp_parse_args(
					$form,
					array(
						'form_type'       => $this->form_type,
						'object_sub_type' => $this->object_sub_type,
						'storage_type'    => $this->storage_type,
						'hidden'          => true,
						'image_dropdown'  => false,
					)
				);
				$form[$form_name] = sr_register_form( $form_name, $form );
			}
		}

		function load_object ( $object_id ) {
			$object = $this->_object_adapter->load_object( $object_id );
			return $object;
		}

		/*
		 * If value is empty (i.e. no form key set), default to the first one.
		 */
		function filter_value ( $value ) {
			if ( empty( $value ) || ! isset( $this->forms[$value] ) ) {
				$form_names = array_keys( $this->forms );
				$value = reset( $form_names );
			}
			return $value;
		}

		function filter_entry_feature_class ( $class ) {
			$classes = trim( "{$class} form_select" );
			if ( $this->image_dropdown ) {
				$classes = trim( "{$classes} image_dropdown" );
			}
			return trim( $classes );
		}

		function filter_select_field_entry_html ( $html ) {
			$html = array( "{$html}<div class=\"form-select-form\">" );
			$args = $this->_object_adapter->as_args();
			$args['parent'] = $this;
			$html[] = self::get_form_html( $this->value, $args );
			$html[] = "</div>";
			$html = implode( '', $html );
			return $html;
		}

		static function get_form_html ( $form_name, $args = array() ) {
			$form = Sunrise_Forms::get_form( $form_name, $args );
			$form->object = $args['object'];
			$html = $form->get_html( $args );
			return $html;
		}

		static function wp_ajax_load_form () {
			$status = 'error';
			$msg = 'Sorry, we were unable to retrieve the selected form!';
			$html = null;
			if ( isset( $_POST['form_name'] ) && isset( $_POST['field_name'] ) ) {
				$form_name = esc_attr( $_POST['form_name'] );
				$args = Sunrise_Objects::get_args_from_POST();
				$html = self::get_form_html( $form_name, $args );
				if ( ! empty( $html ) ) {
					$status = 'success';
					$msg = null;
				}
			}
			die( json_encode(
				array(
					'status' => $status,
					'msg'    => $msg,
					'html'   => esc_html( $html )
				)
			) );
		}
	}

	Sunrise_Form_Select_Field::on_load();
}