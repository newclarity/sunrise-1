<?php
/*
 * Sunrise Indentable Repeating field.
 *
 * Allows for a list of repeating fields with ability to add, delete, etc.
 *
 */
if ( ! class_exists( 'Sunrise_Indentable_Repeating_Field' ) ) {
	/**
	 * @property array  $child_indent_level
	 * @property array  $controls
	 * @property array  $scoped_controls
	 * @property string $child_id
	 */
	class Sunrise_Indentable_Repeating_Field extends Sunrise_Field {

		var $min_count = false;
		var $fields = array();
		var $field_args = array();
		var $indentable = false;

		/**
		 * @var bool|string|array
		 */
		var $child_index = false;

		var $labels = false; // TODO (mikes): Should this move up to Sunrise_Field?

		/**
		 * @var bool|array
		 */
		protected $_controls = false;

		/**
		 * @var bool|array
		 */
		protected $_scoped_controls = false;
		protected $_child_indent_level = false;
		protected $_has_controls = null; // TODO: Verify this needs to be null.

		function filter_child_container_class ( $class ) {
			if ( $this->indentable ) {
				$class .= " indent-level-{$this->child_indent_level}";
			}
			return $class;
		}

		function get_controls () {
			$this->_controls = Sunrise_Controls::expand_controls(
				$this,
				'indentable-repeating-field-controls',
				$this->_controls
			);
			return $this->_controls;
		}

		function get_scoped_controls () {
			if ( ! $this->_scoped_controls ) {
				$this->_scoped_controls = array();
				$controls = $this->controls;
				foreach ( $controls as $control_name => $control ) {
					$this->_scoped_controls[$control->control_scope][$control_name] =& $controls[$control_name];
				}
			}
			return $this->_scoped_controls;
		}

		/**
		 * Allow a subclass to set the controls but don't allow other classes to set controls.
		 *
		 * @param $controls
		 * @return
		 */
		protected function set_controls ( $controls ) {
			return $this->_controls = $controls;
		}

		protected function _get_list_controls_html () {
			$html = '';
			/**
			 * Make sure the controls are not scoped to an item.
			 */
			$this->child_index = false;
			if ( isset( $this->scoped_controls['list'] ) ) {
				$html = Sunrise_Controls::as_html(
					$this->id,
					$this->scoped_controls['list'],
					"scope=list&class={$this->id}-list-controls"
				);
				/**
				 * TODO (mikes): Need to make this more generic!
				 */
				$html = str_replace( '%add_new_label%', $this->labels['add_new'], $html );
			}
			return $html;
		}

		function filter_child_entry_feature_inner_html ( $html, $args ) {
			return $html . $this->_get_item_controls_html( $args['child'] );
		}

		protected function _get_item_controls_html ( $field ) {
			$html = '';
			if ( isset( $this->scoped_controls['item'] ) ) {
				$controls = $this->scoped_controls['item'];
				if ( 1 == $this->child_index ) {
					$settings = array( 'indent' => false, 'outdent' => false );
				} else if ( 'template' == $this->child_index ) {
					$settings = array( 'indent' => true, 'outdent' => false );
				} else {
					$settings = array(
						'indent'  => ! is_array( $this->child_index ),
						'outdent' => is_array( $this->child_index ),
					);
				}
				isset( $controls['indent_field'] ) && $controls['indent_field']->visible = $settings['indent'];
				isset( $controls['outdent_field'] ) && $controls['outdent_field']->visible = $settings['outdent'];
				$html = Sunrise_Controls::as_html( $this->child_id, $controls, 'clear=' );
			}
			return $html;
		}

		function filter_child_control_id ( $control_id ) {
			return "{$this->child_id}-{$control_id}";
		}

		function get_child_id () {
			$child_index = is_array( $this->child_index ) ? implode( '-', $this->child_index ) : $this->child_index;
			$id = $this->id . ( $child_index ? "-{$child_index}" : '' );
			return $id;
		}

		function do_initialize ( $args ) {
			static $first_time = true;
			if ( $first_time ) {
				sr_register_control_set(
					'indentable-repeating-field-controls',
					array(
						'controls' => array(
							'no_control'   => array(
								'text'  => false,
								'shape' => false,
								'link'  => false,
							),
							'delete_field' => array(
								'text'  => __( 'Delete', 'sunrise-fields' ),
								'shape' => 'button',
								'link'  => '#delete',
							),
							'add_field'    => array(
								'text'  => __( '+ %add_new_label%', 'sunrise-fields' ),
								'scope' => 'list',
								'link'  => '#add',
							),
						),
					)
				);
				$first_time = false;
			}

			if ( ! $this->min_count ) {
				$this->min_count = 1;
			}

			if ( ! $this->labels ) {
				$this->labels = array();
			}

			if ( isset( $args['add_new_label'] ) ) {
				$this->labels['add_new'] = $args['add_new_label'];
			} else if ( ! isset( $this->labels['add_new'] ) ) {
				$this->labels['add_new'] = __( 'Add Another ', 'sunrise-fields' ) . trim( $this->label_text, 's' );
			}

			if ( ! $this->_controls ) {
				if ( $this->indentable ) {
					$this->_controls = array( 'add_field' );
				} else {
					$this->_controls = array( 'add_field', 'delete_field' );
				}
			}
		}

		static function admin_init () {
			global $pagenow;
			if ( 'post.php' == $pagenow || 'post-new.php' == $pagenow ) {
				wp_enqueue_script(
					'indentable-repeating',
					plugins_url( '/js/indentable-repeating-field.js', dirname( __FILE__ ) ),
					array( 'jquery-ui-core' ),
					null
				);
				wp_localize_script(
					'indentable-repeating',
					'sunriseIndentableRepeating',
					array( 'filePath' => plugins_url( '/js/jquery.ui.nestedSortable.js', dirname( __FILE__ ) ) )
				);
				Sunrise_Fields::enqueue_css( __FILE__, 'indentable-repeating' );
			}
		}

		function filter_child_entry_feature_inner_class ( $class ) {
			if ( $this->indentable ) {
				$class .= " indent-level-{$this->child_indent_level}";
			}
			if ( 0 == $this->child_indent_level ) {
				$class .= " top-level";
			}
			return $class;
		}

		function is_indentable () { // TODO: Make this a property...
			return $this->parent && isset( $this->parent->indentable ) && $this->parent->indentable;
		}

		function filter_child_entry_name ( $entry_name ) {
			if ( $this->indentable && 'template' != $this->child_index ) {
				$entry_name .= "[0]"; // HTML Forms + PHP $_POST need this
			}
			return $entry_name;
		}

		function filter_child_entry_html ( $html ) {
			if ( 'template' == $this->child_index ) {
			} else if ( 'template' == $this->child_index ) {
				$html .= $this->_get_item_controls_html( $this->fields[$this->child_index] );
			}
			return $html;
		}

		/**
		 * @return int
		 * TODO: Don't cache this value or it will not change as child_index changes.
		 * NOTE:  1 < count( 'template' ) is false.
		 */
		function get_child_indent_level () {
			$child_indent_level = 0;
			$child_index = $this->child_index;
			if ( isset( $this->indentable ) && 1 < count( $this->child_index ) ) {
				$child_indent_level = count( $this->child_index ) - 1;
			}
			return $child_indent_level;
		}

		function filter_entry_feature_container_class ( $class ) {
			return "{$class} indentable-repeating";
		}

		function filter_entry_feature_valid_attributes ( $attributes, $args ) {
			return Sunrise_Field_Feature::get_span_attributes();
		}

		function load_fields () {
			if ( ! $this->fields_loaded ) {
				if ( 'fieldset' == $this->field_args['field_type'] ) {
					$values = $this->load_values();
					$this->fields = array();
					foreach ( $values as $index => $value ) {
						$field = $this->get_field_instance();
						$field->fields_loaded = true;
						$field->value = $value;
						$this->fields[$index] = $field;
					}
				} else {
					$fields = array();
					$value_pairs = $this->load_raw_value_pairs();
					$this->field_args['field_name'] = $this->field_name;
					$this->field_args['object_type'] = $this->object_type;
					$this->field_args['object_sub_type'] = $this->object_sub_type;
					$root_storage_key = false;
					foreach ( $value_pairs as $meta_key => $meta_value ) {
						if ( ! $root_storage_key ) {
							$meta_key = false == strpos( $meta_key, '[1]' ) ? "{$meta_key}[1]" : $meta_key;
							$root_storage_key = str_replace( '[1]', '', $meta_key );
						}
						/**
						 * @var Sunrise_Field $field
						 */
						$field = Sunrise_Fields::get_instance_for( $this->field_args['field_type'], $this->field_args );
						$index = explode( '][', preg_replace( "#{$root_storage_key}\[(.*)\]#", '$1', $meta_key ) );
						$field->index = ( 1 == count( $index ) ? ( is_numeric( $index[0] ) ? $index[0] : 1 ) : $index );
						$field->raw_value = $meta_value;
						$field->parent = $this;
						$fields[$meta_key] = $field;
					}
					$this->fields = $fields;
				}
				$this->fields_loaded = true;
			}
			return $this->fields;
		}

		function filter_entry_feature_class ( $class ) {
			$class = 'entry-feature-list' . ( $this->indentable ? ' indentable sortable' : '' );
			return $this->apply_filters( 'indentable_repeating_field_entry_class', $class );
		}

		function get_entry_html () {
			if ( WP_DEBUG && $this->indentable && $this->min_count > 1 && $this->min_count > count( $this->fields ) ) {
				sr_die(
					__( '%s sets both [min_count=%d] and [indentable] which is not yet allowed.' ),
					$this->get_description(),
					$this->min_count
				);
			}

			$html = array( "<ol id=\"{$this->id}-list\"{$this->class_html}{$this->attributes_html}>" );
			$this->load_fields();
			$field_count = max( count( $this->fields ), $this->min_count );
			$index = 1;
			/**
			 * @var Sunrise_Field $field
			 */
			$is_start = false;
			if ( $this->fields ) {
				foreach ( $this->fields as $field_key => $field ) {
					$field = $this->initialize_field( $field );
					$field->leave_blank = false; // TODO: THIS IS A HACK, IMPROVE IT!
					if ( ! $field->index ) {
						$field->index = $index;
					}
					$this->child_index = $field->index;
					$start = '';
					$end = '';
					if ( is_array( $field->index ) ) {
						if ( $field->index[1] == 1 ) {
							$is_start = true;
							$start = '<ol>';
						}
					} else {
						if ( $is_start ) {
							$is_start = false;
							$start = '</ol>';
						}
					}
					$html[] = $start . '<li>' . $field->entry_html;
					$index ++;
				}
			} else if ( $field_count ) { // Displays initial fields equal to $field_count, which is determined by $this->min_count
				for ( $i = 1; $i <= $field_count; $i ++ ) {
					$field = Sunrise_Fields::get_instance_for( $this->field_args['field_type'], $this->field_args );
					$field->index = $i;
					$field->raw_value = '';
					$field->parent = $this;
					$html[] = '<li>' . $field->entry_html . '</li>';
				}
			}

			if ( ! $this->indentable ) {
				for ( $index; $index <= $field_count; $index ++ ) {
					$field = $this->get_field_instance();
					$field->leave_blank = true; // TODO: THIS IS A HACK, IMPROVE IT!
					if ( ! $field->index ) {
						$field->index = $index;
					}
					$this->child_index = $field->index;
					$html[] = $field->entry_html;
				}
			}
			$html[] = "</ol>\n";
			return $this->apply_filters( 'indentable_repeating_field_entry_html', implode( '', $html ) );
		}

		function get_item_html () {
			$label = $this->no_label ? '' : $this->label_container_html;
			$html = "<span class=\"container\"><li id=\"{$this->id}-field-row\" class=\"{$this->item_class}\">{$label}{$this->container_html}<div class=\"clear\"></div>{$this->_get_list_controls_html(
			)}<br/></li></span>";
			return $this->apply_filters( 'item_html', $html );
		}

		function get_template_field_html () {
			$field = $this->get_field_instance();
			$field->parent->child_index = $field->index = 'template';
			/*
			 *  This will keep the fieldset template from loading the first fieldset
			 *  TODO: Might want to revisit since this only applies to Fieldsets; can this be handled in the Fieldset class itself?
			 */
			$field->fields_loaded = true;
			return $field->entry_html;
		}

		function get_field_instance () {
			$args = is_array( $this->field_args ) ? $this->field_args : array( 'type' => 'text' );
			$args['field_name'] = $this->field_name;
			$args = array_merge( $args, $this->object_adapter->as_args() ); // Pick up object, object_type and object_sub_type
			$args['field_type'] = isset( $args['field_type'] ) ? $args['field_type'] : 'text';
			$field = Sunrise_Fields::get_instance_for( $args['field_type'], $args );
			return $this->initialize_field( $field );
		}

		function initialize_field ( $field ) {
			$field->object = $this->object;
			$field->parent = $this;
			$field->no_label = true;
			$this->child_index = false; // This sets indent level to 1
			return $field;
		}

		function update_values ( $values, $args = array() ) {
			/*
			 * TODO: Should all this logic be moved into Sunrise_Object? Doing do would enable $this->is_repeating
			 * TODO: and $this->is_fieldset vs $this->field_type=='repeating' and $this->field_type=='fieldset',
			 * TODO: respectively.
			 */
			$class = Sunrise::parse_class( "fields/storage/{$this->storage_type}" );
			$flat_values = $this->flatten_values( $values );
			$loaded_values = $this->load_raw_value_pairs( $args );
			$delta = array_diff_key( $loaded_values, $flat_values );
			foreach ( array_keys( $delta ) as $storage_key ) {
				$args['storage_key'] = $storage_key;
				Sunrise::call_instance_method( $class, 'delete_raw_value', $this, $args );
				unset( $flat_values[$storage_key] );
			}
			$field = $this->get_field_instance();
			foreach ( $flat_values as $storage_key => $value ) {
				if ( ! isset( $loaded_values[$storage_key] ) || $flat_values[$storage_key] != $loaded_values[$storage_key] ) {
					$args['storage_key'] = $storage_key;
					// Flat values don't have context within a fieldset field // TODO: Make 'fieldset' a field attribute, not a field type (Micah)
					if ( 'fieldset' == $field->field_type ) {
						preg_match( '#\[([^\[\]]+)\]$#', $storage_key, $matches );
						$field_name = $matches[1];
						$current_field = $field->fields[$field_name];
						$current_field->object_id = $args['object_id'];
					} else {
						$current_field = $field;
					}
					// TODO: Verify this next call doesn't need to be replaced with: $field->update_value($value, $args);
					Sunrise::call_instance_method( $class, 'update_raw_value', $current_field, $value, $args );
				}
			}
		}

		/*
		 * Array
		 *  	(
		 *				[0] => foo[bar]
		 * 				[1] => foo
		 *				[2] => bar
		 *		)
		 * Array
		 *		(
		 *				[0] => foo[2][baz]
		 *				[1] => foo
		 *				[2] => 2
		 *				[3] => [baz]
		 *				[4] => baz
		 *		)
		 */
		function __extract_fieldsets ( $values ) {
			$new_values = array();
			foreach ( $values as $name => $value ) {
				// Look for foo[bar] or foo[2][baz]
				if ( ! preg_match( '#^([^[]+)\[([^]]+)\](\[([^]]+)\])?$#', $name, $matches ) ) {
					sr_die(
						"Unexpected format for Indentable Repeating Fieldset storage key, expected _xxx[yyy] or _xxx[nn][yyy]: {$name}."
					);
				}
				$index = $matches[2]; // Will be 'bar' or '2'
				if ( is_numeric( $index ) ) {
					$field_key = $matches[4]; // Will be 'baz'
				} else {
					$index = 1;
					$field_key = $matches[2]; // Will be 'bar'
				}
				$new_values[$index][$field_key] = $value;
			}
			ksort( $new_values );
			return $new_values;
		}

		/*
		 * Return only custom meta key/values for this field
		 */
		function load_values ( $args = array() ) {
			$values = $this->load_raw_value_pairs( $args );
			if ( 'fieldset' == $this->field_args['field_type'] ) {
				$values = $this->__extract_fieldsets( $values );
			} else {
				$values = $this->unflatten_values( $values );
			}
			self::deep_ksort_numeric( $values );

			/**
			 * Remove empty rows.  is_array($row)==true when $row is a fieldset.
			 * TODO: Come up with an extensible way to do this.
			 */
			foreach ( $values as $index => $row ) {
				if ( is_array( $row ) && 0 == strlen( trim( implode( '', $row ) ) ) ) {
					unset( $values[$index] );
				}
			}
			$values = array_values( $values );
			return $this->apply_filters( 'indentable_repeating_field_load_values', $values, $args );
		}

		public static function deep_ksort_numeric ( &$arr ) {
			ksort( $arr, SORT_NUMERIC );
			foreach ( $arr as &$a ) {
				if ( is_array( $a ) && ! empty( $a ) ) {
					self::deep_ksort_numeric( $a );
				}
			}
		}

		/*
		 * Unflattens a flat array into a tree structure.
		 *
		 * These are example keys:
		 *
		 *  $education = array(
		 *		'_education'        => 'Georgia Tech',
		 *		'_education[1][1]'  => 'Mechanical Engineering',
		 *		'_education[1][2]'  => 'Computer Science',
		 *		'_education[2]'     => 'Agnes Scott',
		 *		'_education[2][1]'  => 'Religious History',
		 *		'_education[2][2]'  => 'Women's Studies',
		 *		'_education[3]'     => 'Georga State',
		 *		'_education[3][1]'  => 'Business Administration',
		 *  );
     *
		 * These will be the output keys:
		 *
		 *		$education => array(
		 *      1 => array(
		 *        0 => 'Georgia Tech',
		 *		    1 => array( 0 => 'Mechanical Engineering' ),
		 *        2 => array( 0 => 'Computer Science' ),
		 *      ),
		 *      2 => array(
		 *        0 => 'Agnes Scott',
		 *		    1 => array( 0 => 'Religious History' ),
		 *        2 => array( 0 => 'Women's Studies' ),
		 *      ),
		 *      3 => array(
		 *        0 => 'Georga State',
		 *		    1 => array( 0 => 'Business Administration' ),
		 *      ),
		 *    );
		 *
		 */
		private function unflatten_values ( $values ) {
			reset( $values );
			foreach ( $values as $key => $value ) {
				if ( false === strpos( $key, '[' ) ) {
					array_shift( $values );
					$values = array_merge( array( "{$key}[1]" => $value ), $values );
				}
				break;
			}
			/*
			 * This logic thanks to Logan F. Smyth
			 * http://stackoverflow.com/questions/7116796/convert-flat-php-array-to-nested-array-based-on-array-keys/7116957#7116957
			 */
			$index = key( $values );
			$new_values = array();
			foreach ( $values as $key => $value ) {
				$indexes = explode( '][', substr( $key, strpos( $index, '[' ) + 1, - 1 ) );
				$this->__set_nested_value( $new_values, $indexes, $value );
			}
			return $new_values;
		}

		/*
		 * This logic thanks to Logan F. Smyth
		 * http://stackoverflow.com/questions/7116796/convert-flat-php-array-to-nested-array-based-on-array-keys/7116957#7116957
		 */
		private function __set_nested_value ( &$values, $indexes, $value ) {
			$these_values = & $values;
			/*
			 * Drill down through the array indexes.
			 */
			foreach ( $indexes as $index ) {
				/*
				 * Make sure $these_values is an array.
				 */
				if ( ! is_array( $these_values ) ) {
					$these_values = array( $these_values );
				}
				/*
				 * Make sure $these_values is an array.
				 */
				if ( ! isset( $these_values[$index] ) ) {
					$these_values[$index] = array();
				}
				/*
				 * Swap parent with child.
				 */
				$these_values = & $these_values[$index];
			}
			/*
			 * Finally, assign the value to the node.
			 */
			$these_values = $value;
		}

		/*
		 * Return only custom meta key/values for this field
		 */
		function load_raw_value_pairs ( $args = array() ) {
			$new_values = array();
			$class = Sunrise::parse_class( "fields/storage/{$this->storage_type}" );
			$values = Sunrise::call_instance_method( $class, 'load_raw_value_pairs', $this, $args );
			//			foreach( $this->field_args['fields'] as $name => $field ) {
			//				$this->initialize_field( $field );
			//				$storage_key = Sunrise::call_instance_method( $class, 'get_storage_key', $field, $args );
			//				$new_values[$storage_key] = $field->value;
			//			}
			$values = $this->_correctly_order_value_pairs( $values );
			return $this->apply_filters( 'indentable_repeating_field_load_raw_value_pairs', $values, $args );
		}

		/*
		 * Reorders keys based on index value instead of text sorting order.
		 */
		private function _correctly_order_value_pairs ( $values, $level = 1 ) {
			/**
			 * This is only a potential issue for keys of 10 or more.
			 */
			if ( 10 <= count( $values ) ) {
				// Unflatten values so we can sort them
				$new_values = $this->unflatten_values( $values );
				// Deep (recursive) sort numerically by key
				self::deep_ksort_numeric( $new_values );

				/**
				 * TODO (Micah 10/18/2012)
				 * Why don't any of the 'flatten' functions work properly for this?
				 * The code below will work for up to two levels deep, which is currently
				 * all that is supported by the rest of the indentable repeating field codebase.
				 */
				// Return keys back to flattened state
				$values = array();
				$field_key = '_' . $this->field_name;
				foreach ( $new_values as $key => $value ) {
					if ( is_string( $value ) ) {
						$values[$field_key . "[{$key}]"] = $value;
					} else if ( is_array( $value ) ) {
						foreach ( $value as $k => $v ) {
							if ( is_string( $v ) ) {
								if ( ! $k ) {
									$values[$field_key . "[{$key}]"] = $v;
								} else {
									$values[$field_key . "[{$key}][{$k}]"] = $v;
								}
							}
						}
					}
				}
			}
			return $values;
		}

		/*
		 * Process values to generate a flat meta_key/value array.
		 */
		function flatten_values ( $values, $parent_index = 0 ) {
			/*
			 * You can delete single value fields by leaving values empty,
			 * but not Fieldsets fields.
			 */
			if ( WP_DEBUG && ! is_array( $values ) && ! empty( $values ) ) {
				sr_die(
					__(
						'The $value parameter for Sunrise_Indentable_Repeating_Field->flatten_values($value) is not an array: %s'
					),
					$values
				);
			} else if ( ! is_array( $values ) ) {
				$values = array(); // Give is an empty default.
			}
			if ( 'fieldset' == $this->field_args['field_type'] ) {
				$value = $this->flatten_fieldset_values( $values );
			} else {
				$values = $this->prune_empty_values( $values );
				if ( $this->indentable ) {
					$value = $this->flatten_values_indentable( $values, $parent_index );
				} else {
					$value = $this->flatten_values_non_indentable( $values );
				}
			}
			return $value;
		}

		private function flatten_values_indentable ( $values, $parent_index = 0 ) {
			$new_values = array();
			$class = Sunrise::parse_class( "fields/storage/{$this->storage_type}" );
			$field = $this->get_field_instance();
			foreach ( $values as $index => $value ) {
				$this->child_index = $new_index = ( 0 == $parent_index ) ? $index + 1 : array( $parent_index, $index + 1 );
				$field->entry_name = false;
				$field->index = $new_index;
				$storage_key = $field->get_storage_key();
				$new_values[$storage_key] = $value[0];
				array_shift( $value ); // Drop off the zero value
				if ( count( $value ) ) {
					$new_values = array_merge( $new_values, $this->flatten_values( $value, $index + 1 ) );
				}
			}
			return $new_values;
		}

		private function flatten_fieldset_values ( $values ) {
			$new_values = array();
			if ( count( $values ) ) {
				$class = Sunrise::parse_class( "fields/storage/{$this->storage_type}" );
				if ( 'fieldset' == $this->field_args['field_type'] ) {
					/**
					 * @var Sunrise_Fieldset_Field $fieldset_field
					 */
					$fieldset_field = $this->get_field_instance();
					$this->child_index = 1;
					$fieldset_field->load_values();
					foreach ( $values as $index => $fieldset_values ) {
						$this->child_index = $fieldset_field->index = $index;
						foreach ( $fieldset_values as $key => $value ) {
							if ( isset( $fieldset_field->fields[$key] ) ) {
								/**
								 * @var Sunrise_Field $field
								 */
								$field = $fieldset_field->fields[$key];
								$field->entry_name = false;
								$new_values[$field->get_storage_key()] = $value;
							}
						}
					}
				} else {
					$new_values = array();
					$this->index = 0;
					$class = Sunrise::parse_class( "fields/storage/{$this->storage_type}" );
					foreach ( $values as $index => $fieldset_values ) {
						$this->index ++;
						foreach ( $fieldset_values as $key => $value ) {
							$this->fieldset_key = $key;
							$storage_key = Sunrise::call_instance_method( $class, 'get_storage_key', $this );
							$new_values[$storage_key] = $value;
						}
					}
				}
			}
			return $new_values;
		}

		private function flatten_values_non_indentable ( $values ) {
			$new_values = array();
			$this->index = 0;
			$class = Sunrise::parse_class( "fields/storage/{$this->storage_type}" );
			foreach ( $values as $index => $value ) {
				$this->index ++;
				$storage_key = Sunrise::call_instance_method( $class, 'get_storage_key', $this );
				$new_values[$storage_key] = $value;
			}
			return $new_values;
		}

		/*
		 * Return only custom meta key/values for this field
		 */
		private function prune_empty_values ( $values ) {
			$new_values = array();
			foreach ( $values as $index => $value ) {
				if ( ! is_array( $value ) ) {
					if ( ! empty( $value ) ) {
						$new_values[] = $value;
					}
				} else {
					if ( 1 < count( $value ) ) {
						$values[$index] = $this->prune_empty_values( $value );
					}
					if ( ! empty( $value[0] ) || 1 > count( $value ) ) {
						$new_values[] = $values[$index];
					}
				}
			}
			return $new_values;
		}

		function __set ( $property_name, $value ) {
			switch ( $property_name ) {
				case '__temp__':
					call_user_func( array( &$this, "set_{$property_name}" ), $value );
					break;

				case 'controls':
				case 'child_indent_level':
				case 'scoped_controls':
				case 'child_id':
					$this->_cannot_set_error( $property_name, $value );
					break;

				default:
					parent::__set( $property_name, $value );
					break;
			}
		}

		function __get ( $property_name ) {
			$value = false;
			switch ( $property_name ) {
				case 'controls':
				case 'scoped_controls':
				case 'child_indent_level':
				case 'child_id':
					$value = call_user_func( array( &$this, "get_{$property_name}" ) );
					break;

				default:
					$value = parent::__get( $property_name );
					break;
			}
			return $value;
		}
	}
}

