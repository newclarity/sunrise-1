<?php

class Sunrise_Literal_Field extends Sunrise_Field {

	protected $content;

	function do_initialize( $args ) {
		$this->content = isset( $args['content'] ) ? $args['content']: false;
	}

	function get_entry_html() {
		$html  = "<span id=\"{$this->id}\" name=\"{$this->entry_name}\" {$this->class_html}{$this->attributes_html}>{$this->content}</span>";
		return $this->apply_filters( 'custom_field_entry_html', $html );
	}

}