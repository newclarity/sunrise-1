<?php

if( !class_exists('Sunrise_Url_Field') ){

	class Sunrise_Url_Field extends Sunrise_Field {

		function do_initialize($args) {
			$this->html_type = 'url';
			$this->set_placeholder( $this->get_placeholder() );
		}

		function filter_default_placeholder_attribute( $placeholder ) {
		    return 'http://';
		}

	}

}