<?php

class Sunrise_Text_Field extends Sunrise_Field {

	var $required;
	var $pattern;

	function filter_entry_feature_valid_attributes( $attributes, $args ) {
		$attributes[] = 'required';
		$attributes[] = 'pattern';
		return $attributes;
	}

}