<?php

if ( ! class_exists( 'Sunrise_Admin_Skin' ) ) {
	/**
	 * @property
	 */
	class Sunrise_Admin_Skin extends Sunrise_Base {
		public $hide_help_link = false;
		public $hide_screen_options_link = false;
		public $omit_quick_edit = false;
	}
}

