<?php

if ( ! class_exists( 'Sunrise_Admin_Skins' ) ) {
	class Sunrise_Admin_Skins extends Sunrise_Static_Base {
		/**
		 * @var bool|Sunrise_Admin_Skin
		 */
		static $current_skin = false;
		private static $_output_buffering = false;
		private static $_page_index = false;
		/**
		 * @var bool|array
		 */
		private static $_menu = false;
		/**
		 * @var bool|array
		 */
		private static $_submenu = false;
		/**
		 * @var bool|array
		 */
		private static $_section_menus = false;

		static function on_load() {
			sr_add_filter( __CLASS__, 'contextual_help' );
			sr_add_action( __CLASS__, 'admin_notices' );
			sr_add_action( __CLASS__, array( 'page_row_actions',  'row_actions' ), 10 /*, 2 */ );
			sr_add_action( __CLASS__, array( 'post_row_actions', 'row_actions' ), 10 /*, 2 */ );
			sr_add_filter( __CLASS__, 'parent_file' );
			sr_add_filter( __CLASS__, 'sr_add_admin_menu_option', 99, 3 ); // Do it late, give others a chance.
			sr_add_action( __CLASS__, 'sunrise_init', 80 ); // Priority order is important

			self::$current_skin = new Sunrise_Admin_Skin();

			sr_add_action( __CLASS__, '_admin_menu', 999 );		// May sure to save very, very late.
			sr_add_action( __CLASS__, 'admin_menu', 999 );		// May sure to save very, very late.

		}
		static function sunrise_init() {
			do_action( 'sr_admin_skins_init' );
		}

		/**
		 * First capture the raw values of $menu and $submenu before pruned to exclude items the current role can't see.
		 */
		static function _admin_menu() {
			global $menu,$submenu;
			self::$_menu = $menu;
			self::$_submenu = $submenu;
		}
		/**
		 * Then merge with any new $menu and $submenu values that have been added since '_admin_menu' hook.
		 */
		static function admin_menu() {
			global $menu,$submenu;
			self::$_menu = array_merge( self::$_menu, $menu );
			self::$_submenu = array_merge( self::$_submenu, $submenu );
		}

		/**
		 * Capture the Row Actions and remove the 'Quick Edit' option is self::$current_skin->omit_quick_edit == true
		 *
		 * @param array $actions
		 * @return array
		 */
		static function row_actions( $actions /*, $page  */ ) {
			if ( self::$current_skin->omit_quick_edit ) {
				unset($actions['inline hide-if-no-js']); // Remove Quick Edit
				unset($actions['inline']); // The Old way. Maybe still used in some places?
			}
			return $actions;
		}

		/**
		 * Starts output buffering if self::$hide_help_link == true.
		 * Coordinates with start/stop of output buffering with admin_notices()
		 * @param $contextual_help
		 * @return
		 */
		static function contextual_help( $contextual_help ) {
			if ( self::$current_skin->hide_help_link )
				self::$_output_buffering = true;
			ob_start();
			return $contextual_help;
		}

		/**
		 * Starts output buffering if self::$hide_screen_options_link == true.
		 * Coordinates with start/stop of output buffering with admin_notices()
		 * @param $screen_options
		 * @return
		 */
		static function screen_options( $screen_options ) {
			if ( self::$current_skin->hide_screen_options_link )
				self::$_output_buffering = true;
			ob_start();
			return $screen_options;
		}

		/**
		 * Used to remove the Help & Screen Options Links
		 * Stops output buffering if previously enabled.
		 *
		 * Coordinates with start/stop of output buffering with contextual_help() and screen_options()
		 * @return void
		 */
		static function admin_notices() {
			if ( self::$_output_buffering ) {
				$html = ob_get_clean();
				if ( self::$current_skin->hide_help_link ) {
					$html = preg_replace( '#<div id="contextual-help-link-wrap".*>.*</div>#Us', '', $html );
				}
				if ( self::$current_skin->hide_screen_options_link ) {
					$html = preg_replace( '#<div id="screen-options-link-wrap".*>.*</div>#Us', '', $html );
				}
				echo $html;
			}
		}

		/**
		 * Clear memory held by admin menu index after no longer needed.
		 */
		static function clear_admin_menu_index() {
			self::$_page_index = false;
		}

		static function initialize_admin_menu_index() {
			if ( ! self::$_page_index ) {
				$index = array();
				foreach ( self::$_submenu as $parent_slug => $submenu_menu ) {
					foreach ( $submenu_menu as $menu_page ) {
						$menu_page[99] =  $parent_slug;
						$index[$menu_page[2]] = $menu_page;
					}
				}
				unset( $submenu_menu );
				unset( $parent_slug );
				foreach ( self::$_menu as $menu_page ) {
					$menu_page[99] =  $menu_page[2];
					$index[$menu_page[2]] = $menu_page;
				}
				foreach ( $index as $menu_slug => $menu_page ) {
					if ( 'separator' != substr( $menu_page[2], 0, 9 ) && false === strpos( $menu_page[2], '.php' ) ) {
						if ( false === strpos( $menu_page[99], '.php' ) ) {
							$php_page = 'admin.php';
						} else {
							list($php_page) =  explode( '?', $menu_page[99] );
						}
						$index["{$php_page}?page={$menu_page[2]}"] = $menu_page;
					}
				}
				self::$_page_index = $index;
			}
		}

		static function sr_add_admin_menu_option( $add_option, $submenu_value, $option ) {
			return $add_option || $option == $submenu_value[2];
		}

		static function get_admin_menu() {
			return self::$_section_menus;
		}

		static function clear_admin_menu() {
			self::$_section_menus = false;
		}
		static function _make_menu_page( $menu_item_key, $menu_item ) {
			$menu_slug = $menu_item['slug'];

			if ( isset( self::$_page_index[$menu_slug] ) ) {
				$menu_page = self::$_page_index[$menu_slug];
			} else {
				$menu_page = array( $menu_item_key, 'edit_posts', $menu_slug, '', 'menu-top', "menu-{$menu_slug}", "div" );
			}

			if ( ! empty( $menu_item['title'] ) )
				$menu_page[0] = $menu_item['title'];

			if ( ! empty( $menu_item['capability'] ) )
				$menu_page[1] = $menu_item['capability'];

			$menu_page[2] = $menu_item['slug'];
			$menu_page[3] = ''; // Does nothing, Doh!

			foreach ( array( 4, 5, 6 ) as $menu_item_index ) {
				switch ( $menu_item_index ) {
					case 4:
						$menu_item_key = 'css_class';
						$default_value = 'menu-top';
						break;
					case 5:
						$menu_item_key = 'hook_name';
						$default_value = '';
						break;
					case 6:
						$menu_item_key = 'icon_url';
						$default_value = esc_url( admin_url( 'images/generic.png' ) );
						break;
				}
				if ( isset( $menu_item[$menu_item_key] ) ) {
					$menu_page[$menu_item_index] = $menu_item[$menu_item_key];
				} else if ( ! isset( $menu_page[$menu_item_index] ) ) {
					$menu_page[$menu_item_index] = $default_value;
				} else { 	// Will this ever be used?
					$menu_page[$menu_item_index] = '';
				}
			}
			return $menu_page;
		}
		/**
		 * Use this hook to remove the redundant menu options
		 *
		 * @param string $parent_file
		 * @return string
		 *
		 */
		static function parent_file( $parent_file ) {
			global $menu, $submenu, $parent_file, $submenu_file;
			/**
			 * Allow the plugin/theme to build its admin menu system
			 */
			do_action( 'sr_register_admin_menus' );
			if ( self::$_section_menus ) {
				/**
				 * Copy Gather an index of menu options by slug
				 */
				$menu = $left_overs = array();
				$original_submenu = $submenu;
				$submenu = array();
				$section_no = 1;
				/**
				 * Grab a separator we can reuse. Assume separator1 will be there.
				 */
				foreach ( self::$_section_menus as $section_slug => $section ) {
					foreach ( $section as $menu_item_key => $menu_item ) {
						if ( ! empty( $menu_item['capability'] ) && ! current_user_can( $menu_item['capability'] ) )
							continue;

						/**
						 * If menu item was not found that matched the slug, and if it was optional
						 * then a plugin that can be disabled, skip it.
						 */
						if ( empty( $menu_item['title'] ) && ! empty( $menu_item['optional'] ) )
							continue;

						/**
						 * This is required for WordPress Admin Menu Voodoo
						 */
						$menu_item['slug'] = $menu_slug = isset( $menu_item['slug'] ) ? $menu_item['slug'] : $menu_item_key;
						$submenu[$menu_slug] = isset( $menu_item['submenu'] ) && is_array( $menu_item['submenu'] ) ? $menu_item['submenu'] : array();

						/**
						 * This is required for WordPress Admin Menu Voodoo
						 */
						if ( $submenu_file == $menu_slug && $parent_file != $menu_slug ) {
							$parent_file = $menu_slug;
						}

						/**
						 * Grab everything in the $menu_item and fixup the menu page.
						 */
						$menu_page = self::_make_menu_page( $menu_item_key, $menu_item );

						if ( isset( self::$_page_index[$menu_slug] ) ) {
							$menu[] = apply_filters( 'sr_admin_menu_option', $menu_page, $section_slug, $original_submenu );
							// TODO: Eliminate the following IF() code. Too specific.
							if ( isset($menu_item['options']) ) {
								foreach ( $menu_item['options'] as $option_key => $option ) {
									foreach ( $original_submenu[$menu_item['slug']] as $submenu_key => $submenu_value ) {
										if ( apply_filters( 'sr_add_admin_menu_option', false, $submenu_value, $option, $section_slug ) ) {
											$submenu[$menu_item['slug']][] = $submenu_value;
											break;
										}
									}
								}
							}
						} else if ( current_user_can( 'manage_options' ) ) { // TODO (mikes 2012-01-09): Consider a developer capability instead
							$left_overs[] = $menu_page;
						}
					}
					$separator = isset( self::$_page_index["separator1"] ) ? self::$_page_index["separator1"] : false;
					if ( $separator ) {
						$separator[2] = "separator{$section_no}";
						//					$separator[2] = $separator_slug = "separator{$section_no}";
						//					$menu[$separator_slug] = $separator;
						$menu[] = $separator;
					}
					$section_no ++;
				}
				$separator[2] = "separator{$section_no}";
				$menu[] = $separator;
				/**
				 * If no leftovers then remove the trailing separator, otherwise add the leftovers after the trailing separator.
				 */
				if ( 0 == count( $left_overs ) ) {
					array_pop( $menu );
				} else {
					$menu = array_merge( $menu, $left_overs );
				}

				self::clear_admin_menu_index();
			}
			/**
			 * Let go of the memory that held all the menus.
			 */
			self::clear_admin_menu();
			return $parent_file;
		}

		static function register_admin_menu_section( $section_name, $menu_items, $args = array() ) {
			$args = sr_parse_args( $args, array( 'submenu' => false, ) );
			$menu_items = apply_filters( 'sr_register_admin_menu_sections_items', $menu_items, $section_name, $args );

			/**
			 * Gather an index of menu options by slug
			 *
			 * @var string|array $menu_item - Can be passed in either as a string or as an array.
			 */
			self::initialize_admin_menu_index();
			$section_menu_items = array();
			foreach ( $menu_items as $page_name => $menu_item ) {
				if ( ! is_array( $menu_item ) ) {
					$page_name = $menu_item;
					$menu_item = array();
				}
				$section_menu_items[$page_name] = self::_fixup_admin_menu_item( $page_name, $menu_item, $args );
			}
			self::$_section_menus[$section_name] = $section_menu_items;
			/**
			 * Release the memory held by this function.
			 */
		}
		private static function _fixup_admin_menu_item( $page_name, $menu_item, $args ) {
			$fixup_slug = true;
			$value = false;
			if ( isset( $menu_item['slug'] ) ) { // && $page_name == $menu_item['slug']
				$slug = $menu_item['slug'];
			} else {
				list( $name, $value ) = explode( '=', "{$page_name}=" );
				if ( ! empty( $value ) ) {
					if ( 'index' == $name ) {
						$fixup_slug = false;
						$menu_item['slug'] = $slug = "index.php";
					} else if ( 'post_type' == $name ) {
						$fixup_slug = false;
						$menu_item['slug'] = $slug = "edit.php" . ( 'post' == $value ? '' : "?post_type={$value}" );
					} else if ( 'taxonomy' == $name ) {
						$post_type_arg = isset( $menu_item['post_type'] ) ? "&amp;post_type={$menu_item['post_type']}" : '';
						$menu_item['slug'] = $slug = "edit-tags.php?taxonomy={$value}{$post_type_arg}";
					} else if ( in_array( $name, array( 'admin', 'tools' ) ) ) {
						$menu_item['title'] = ! empty( self::$_page_index[$value] ) ? self::$_page_index[$value][0] : false;
						$menu_item['slug'] = $slug = "{$name}.php?page={$value}";
					} else {
						sr_die( __( 'No viable admin menu found for [%s].' ), $page_name );
					}
				} else if ( isset( self::$_page_index[$page_name] ) ) {
					$menu_item['title'] = self::$_page_index[$page_name][0];
					$menu_item['slug'] = $slug = (false === strpos( $page_name, '.php' ) ? "admin.php?page={$page_name}" : $page_name);
				} else if ( isset( self::$_page_index[$slug = "{$page_name}.php" ]) ) {
					$menu_item['title'] = self::$_page_index[$slug][0];
					$menu_item['slug'] = $slug;
					$fixup_slug = false;
				}
/**
 * This will happen when a user doesn't have the rights to see the menu.
 */
//				else {
//					sr_die( __( 'No viable admin menu found for [%s].' ), $page_name );
//				}
			}
			if ( $fixup_slug && ! isset( self::$_page_index[$slug] ) && isset( self::$_page_index[$page_name] ) ) {
				self::$_page_index[$slug] = self::$_page_index[$page_name];
				unset(self::$_page_index[$page_name]);
			}

			if ( ! isset( $menu_item['submenu'] ) ) {
				/**
				 * Accept the default if there is a submenu available.
				 */
				$menu_item['submenu'] = isset( self::$_submenu[$slug] ) && $args['submenu'];
			}
			if ( true === $menu_item['submenu'] ) {
				if ( isset( self::$_submenu[$slug] ) ) {
					/**
					 * This is used when...
					 */
					$menu_item['submenu'] = self::$_submenu[$slug];
				} else if ( isset( self::$_submenu[$page_name] ) ) {
					/**
					 * This is used when the $page_name
					 */
					$menu_item['submenu'] = self::$_submenu[$page_name];
				} else if ( isset( self::$_submenu[$value] ) ) {
					/**
					 * This is used when the $page_name started with 'admin=', etc.
					 */
					$menu_item['submenu'] = self::$_submenu[$value];
				} else {
					/**
					 * This will happen when a user doesn't have the rights to see the submenu.
					 */
					$menu_item['submenu'] = false;  // sr_die( __( 'No viable submenu found for [%s].' ), $page_name );
				}
				/**
				 * If the submenu pages just have a name and not a full slug, add 'admin.php?page=' to their front.
				 * Not sure why this is needed, but it seems to be needed in some cases (i.e. BackupBuddy submenu pages.)
				 */
				if ( $menu_item['submenu'] && isset( $name ) && 'admin' == $name )
					foreach( $menu_item['submenu'] as $index => $menu_page ) {
						if ( false == strpos( $menu_page[2], '.php' ) ) {
							$menu_item['submenu'][$index][2] = "admin.php?page={$menu_page[2]}";
						}
					}
			}

			if ( isset( self::$_page_index[$slug] ) ) {
				$menu_page = self::$_page_index[$slug];
				if ( ! isset( $menu_item['title'] ) )
					$menu_item['title'] =  $menu_page[0];

				if ( ! isset( $menu_item['capability'] ) )
					$menu_item['capability'] =  $menu_page[1];

				if ( ! isset( $menu_item['css_class'] ) )
					$menu_item['css_class'] = ! empty( $menu_page[4] ) ? $menu_page[4] : 'menu-top';

				if ( ! isset( $menu_item['hook_name'] ) )
					$menu_item['hook_name'] = ! empty( $menu_page[5] ) ? $menu_page[5] : '';

				if ( ! isset( $menu_item['icon_url'] ) )
					$menu_item['icon_url'] = ! empty( $menu_page[6] ) ? $menu_page[6] : esc_url( admin_url( 'images/generic.png' ) );
			}

			return $menu_item;
		}

	}

	Sunrise_Admin_Skins::on_load();
}

