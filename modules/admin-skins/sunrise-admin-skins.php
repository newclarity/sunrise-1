<?php
/*
Plugin Name: Sunrise Admin Skins
Plugin URL: http://docs.getsunrise.org/admin-skins/
File Name: sunrise-admin-skins.php
Author: The Sunrise Team
Author URL: http://getsunrise.org
Version: 0.0.0
Description: Adds Skinning ability to the WordPress Admin
*/

define( 'SUNRISE_ADMIN_SKINS_DIR', dirname( __FILE__ ) );
require( SUNRISE_ADMIN_SKINS_DIR . '/includes/admin-skin-functions.php');
require( SUNRISE_ADMIN_SKINS_DIR . '/includes/admin-skin-class.php');
require( SUNRISE_ADMIN_SKINS_DIR . '/includes/admin-skins-class.php');
require( SUNRISE_ADMIN_SKINS_DIR . '/includes/admin-menu-functions.php');

//sr_load_plugin_files( 'admin-skins', array() );
//sr_load_demos( SUNRISE_ADMIN_SKINS_DIR );
