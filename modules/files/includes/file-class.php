<?php
if ( ! class_exists( 'Sunrise_File' ) ) {
	/**
	 * A File Class to encapsulate local path and URL and enable easy manipulation of same for a named file.
	 * This will often by Sunrise functions and other Sunrise classes when they need to work with files.
	 *
	 * @property string $filepath   The complete path that uniquely identifies a local file, i.e '/path/to/filename.ext'.
	 * @property string $dirname    The directory name sans trailing slash, i.e. '/path/to'
	 * @property string $basename   The relative filename with extension, i.e. 'filename.ext'
	 * @property string $filename   The filename without path or extension, i.e. 'filename'
	 * @property string $extension  The file extension, i.e. 'ext'
	 * @property string $filemode   The read/write mode for this file: 'r' for readonly, 'w' for writeable
	 * @property string $mime_type  The MIME/Content Type for this file: i.e. 'image/jpeg', 'text/plain', etc.
	 * @property string $size       The size of this file in bytes.
	 * @property string $url        The full URL to access this file.
	 */
	class Sunrise_File extends Sunrise_Base {
		protected static $_pause_clear = false;

		protected $_filepath      = false;
		protected $_dirname       = false;
		protected $_basename      = false;
		protected $_filename      = false;
		protected $_extension     = false;
		protected $_filemode      = false;
		protected $_mime_type     = false;
		protected $_size          = false;
		protected $_url           = false;

		public $attachment_id     = false;

		function __construct( $args = array() ) {
			/*
			 * If filepath was passed, make it's subparts (_filename, _dirname, _extension) are assigned.
			 * But allow other values to be set individually afterwards.
			 */
			if ( isset( $args['filepath'] ) ) {
				$this->filepath = $args['filepath'];
			}

			/*
			 * Capture args and use them to set properties.
			 */
			$this->construct_from_args( $args );

			return $this;
		}
		/*
		 * Returns array that is the same as the array returned by PHP's pathinfo().
		 * @return array
		 */
		function as_attachment_args( $parent_post_id = false ) {
			$attachment_args = array(
				'guid'           => $this->get_url(),
				'post_parent'    => $parent_post_id,
				'post_title'     => preg_replace( '#\.[^.]+$#', '', $this->get_filename() ),
				'post_content'   => '',
				'post_mime_type' => $this->get_mime_type()
			);
			return $attachment_args;
		}
		function as_pathinfo() {
			return array(
				'dirname'   => $this->get_dirname(),
				'basename'  => $this->get_basename(),
				'extension' => $this->get_extension(),
				'filename'  => $this->get_filename(),
			);
		}
		function initialize_from( $selector, $lookup_value ) {
			switch ( $selector ) {
				case 'attachment_id':
					if ( $attachment_id = $lookup_value ) {
						if ( $filepath = get_attached_file( $attachment_id ) ) {
							$this->filepath = $filepath;
							$this->url = wp_get_attachment_url( $attachment_id );
							$this->attachment_id = $attachment_id;
						}
					}
					break;
				default:
					// TODO: Improve this error message
					sr_die( __( 'Selector passed to Sunrise_Image_Field->initialize_from() was not valid.' ) );
					break;
			}
		}
		function get_basename() {
			if ( ! $this->_basename ) {
				if ( $this->_filepath ) {
					$this->_basename = basename( $this->_filepath );
				} else {
					$extension = $this->get_extension();
					$this->_basename = $this->get_filename() . ( empty( $extension ) ? '' : ".{$extension}" );
				}
			}
			return $this->_basename;
		}
		function get_dirname() {
			return $this->_dirname;
		}
		function get_extension() {
			return $this->_extension;
		}
		/*
		 * Returns 'r' for readonly or 'w' for writable
		 * See 'mode' parameter: http://php.net/manual/en/function.fopen.php
		 */
		function get_filemode() {
			return $this->_filemode ? $this->_filemode : 'r'; // Default to read only
		}
		function get_filename() {
			return $this->_filename;
		}
		function get_filepath() {
			static $slash;

			if ( ! $this->_filepath ) {

				/*
				 * Determine the appropriate slash: Windows => '\'; Mac/*nix => '/'
				 */
				if ( ! isset( $slash ) )
					$slash = substr( PHP_OS, 0, 3 ) == "WIN" ? '\\' : '/';

				/*
				 * If we don't have a $dirname yet then provide a relative filename.
				 */
				$dirname = $this->get_dirname();

				$this->_filepath = ( empty( $dirname ) ? '' : "{$dirname}{$slash}" ) . $this->get_basename();

			}
			return $this->_filepath;
		}
		function get_mime_type() {
			if ( ! $this->_mime_type ) {
				$extension = $this->extension;
				/*
				 * Firstly, try WordPress supported types
				 */
				foreach ( get_allowed_mime_types() as $extensions => $mime_type ) {
					if ( preg_match( "#^({$extensions})$#i", $extension ) ) {
						$this->_mime_type = $mime_type;
						break;
					}
				}
				if ( ! $this->_mime_type ) {
					/*
					 * If WordPress doesn't support that file type, try with PHP finfo_file() function
					 *  Works only from PHP 5.3.0
					 */
					if ( function_exists( 'finfo_file' ) ) {
						$finfo = finfo_open( FILEINFO_MIME_TYPE );
						$this->_mime_type = finfo_file( $finfo, $filepath );
						finfo_close( $finfo );
					} else {   // See if it is an image
						$image_size = getimagesize( $filepath );
						if ( isset( $image_size['mime'] ) ) {
							$this->_mime_type = $image_size['mime'];
						} elseif ( function_exists( 'mime_content_type' ) ) { // Is deprecated, so we use it at last
							$this->_mime_type = mime_content_type( $filepath );
						} else {
							// TODO (Anh): Question Do we need to check some more MIME file types here?
							$this->_mime_type = 'application/octet-stream';          // If nothing works, set default file MIME type
						}
					}
				}
			}
			return $this->_mime_type;
		}
		function get_size() {
			if ( ! $this->_size ) {
				$this->_size = filesize( $filepath );
			}
			return $this->_size;
		}
		function get_url() {
			if ( ! $this->_url ) {
				$content_folder = array_pop( explode( '/', WP_CONTENT_DIR ) );
				$relative_url = '/' . $content_folder . str_replace( WP_CONTENT_DIR, '', $this->dirname );
				$this->_url = home_url("{$relative_url}/" . $this->get_basename());
			}
			return $this->_url;
		}
		function set_basename( $basename ) {
			$pathinfo = pathinfo( $basename );
			$this->_filename = $pathinfo['filename'];
			$this->_extension = $pathinfo['extension'];
			$this->_clear_calculated_properties();
		}
		function set_dirname( $dirname ) {
			$this->_dirname = $dirname;
		}
		function set_extension( $extension ) {
			$this->_extension = trim( $extension );
			$this->_clear_calculated_properties();
		}
		/*
		 * Sets filemode for image output
		 *
		 * @param string $filemode Either 'r' for readonly or 'w' for writable
		 *
		 * See 'mode' parameter: http://php.net/manual/en/function.fopen.php
		 */
		function set_filemode( $filemode ) {
			if ( preg_match( '#^(r|w|R|W)$#', $filemode ) )
				$this->_filemode = strtolower( $filemode );
			else if ( WP_DEBUG )
				sr_die(  __( 'Invalid filemode passed to %s: %s. Must be \'r\' or \'w\'' ), __METHOD__, $filemode );
		}
		function set_filename( $filename ) {
			$this->_filename = $filename;
			$this->_clear_calculated_properties();
		}
		function set_filepath( $filepath ) {
			self::$_pause_clear = true;
			$pathinfo = pathinfo( $filepath );
			$this->_dirname   = isset( $pathinfo['dirname'] ) ? $pathinfo['dirname'] : '';
			$this->_extension = isset( $pathinfo['extension'] ) ? $pathinfo['extension'] : '';
			$this->_filename  = isset( $pathinfo['filename'] ) ? $pathinfo['filename'] : '';
			self::$_pause_clear = false;
			$this->_clear_calculated_properties();
			$this->_basename = $this->get_basename();
			$this->_filepath =  $filepath;
		}
		function set_mime_type( $mime_type ) {
			$this->_mime_type = $mime_type;
		}
		function set_size( $size ) {
			$this->_size = $size;
		}
		function set_url( $url ) {
			$this->_url = $url;
		}
		protected function _clear_calculated_properties() {
			if ( ! self::$_pause_clear ) {
				$this->_filepath = false;
				$this->_basename = false;
				$this->_mime_type = false;
				$this->_size = false;
				$this->_url = false;
			}
		}
		function __set( $property_name, $value ) {
			switch ( $property_name ) {
				case 'basename':
				case 'dirname':
				case 'extension':
				case 'filename':
				case 'filepath':
				case 'filemode':
				case 'mime_type':
				case 'size':
				case 'url':
				case 'size':
					call_user_func( array( &$this, "set_{$property_name}" ), $value );
					break;

				default:
					parent::__set( $property_name, $value );
					break;
			}
		}
		function __get( $property_name ) {
			$value = false;
			switch ( $property_name ) {
				case 'basename':
				case 'dirname':
				case 'extension':
				case 'filepath':
				case 'filename':
				case 'filemode':
				case 'mime_type':
				case 'url':
				case 'size':
					$value = call_user_func( array( &$this, "get_{$property_name}" ) );
					break;

				default:
					$value = parent::__get( $property_name, $value );
					break;
			}
			return $value;
		}
		function __toString() {
			$output = '';
			if( $this->get_filepath() ) {
				$before = isset( $this->extra['before'] ) ? $this->extra['before'] : '';
				$after = isset( $this->extra['after'] ) ? $this->extra['after'] : '';
				$class = esc_attr( $this->get_extension() );
				$anchor_text = isset( $this->extra['anchor_text'] ) ? $this->extra['anchor_text'] : $this->get_basename();
				$link = '<a href="'. $this->get_url() .'" class="'. $class .'">'. $anchor_text .'</a>';
				$output = $before . $link . $after;
			}
			return $output;
		}
	}
}

