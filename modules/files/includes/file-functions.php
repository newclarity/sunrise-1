<?php
/**
 * Converts a complex $_FILES format to the simpler format:
 *
 * COMPLEX
 * =======
 *  $_FILES[components] =
 *    [name] =
 *      [1] =
 *        [screenshot] = mikeschinkel.jpg
 *    [type] =
 *      [1] =
 *        [screenshot] = image/jpeg
 *    [tmp_name] =
 *      [1] =
 *        [screenshot] = /private/var/tmp/php55OuSd
 *    [error] =
 *      [1] =
 *        [screenshot] = 0
 *    [size] =
 *      [1] =
 *        [screenshot] = 52831
 *
 * COMPLEX 2
 * =======
 *  $_FILES[components] =
 *    [name] =
 *      [2] =
 *        [2] =
 *          [screenshot] = mikeschinkel.jpg
 *    [type] =
 *      [2] =
 *        [2] =
 *          [screenshot] = image/jpeg
 *    [tmp_name] =
 *      [2] =
 *        [2] =
 *          [screenshot] = /private/var/tmp/php55OuSd
 *    [error] =
 *      [2] =
 *        [2] =
 *          [screenshot] = 0
 *    [size] =
 *      [2] =
 *       [2] =
 *          [screenshot] = 52831
 *
 * SIMPLE
 * =======
 *  $_FILES[components[screenshot]] =
 *    [name]     = mikeschinkel.jpg
 *    [type]     = image/jpeg
 *    [tmp_name] = /private/var/tmp/php55OuSd
 *    [error]    = 0
 *    [size]     = 52831
 *
 *
 * @param array $files
 * @return array
 */
function sr_transform_uploaded_FILES( $files ) {
	foreach( $files as $file_name => $file ) {
		foreach( $file as $attribute_name => $attribute ) {
			$indexer = reset( array_keys( $attribute ) );
			$indexer = "[{$indexer}]";
			while ( is_array( $attribute ) ) {
				$field_name = reset( array_keys( $attribute ) );  // Grab the first one
				$attribute = $attribute[$field_name];
			}
			if ( ! isset( $new_files ) )
				$new_files[$key = "{$file_name}{$indexer}[{$field_name}]"] = array(); // Add an element
			$new_files[$key][$attribute_name] = $attribute;
		}
	}
	return $new_files;
}
