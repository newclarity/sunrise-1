<?php
/*
 * TODO (mikes): Ensure we address all these issues:
 * TODO (mikes):    http://www.php.net/manual/en/features.file-upload.common-pitfalls.php
 *
 * TODO (mikes): How should calculated properties like filepath work for non-existent files?
 *
 */
if ( ! class_exists( 'Sunrise_Image_File' ) ) {
	/**
	 *  @property int $height
	 *  @property int $width
	 *  @property int $max_height
	 *  @property int $max_width
	 *  @property bool $exists
	 *  @property string $hwstring  Used in an HTML image tag for height & width of this file, i.e.: 'width="75" height="100"'
	 */
	class Sunrise_Image_File extends Sunrise_File {
		const VALID_EXTENSIONS = 'gif|jpg|jpeg|png';
		static $valid_extensions = false;

		public $image_type     = false;
		public $image_size     = 'default';

		protected $_width      = false;
		protected $_height     = false;
		protected $_max_width  = false;
		protected $_max_height = false;
		protected $_hwstring   = false;

		private $_imagesize_initialized = false;

		static function on_load() {
			self::$valid_extensions = array_flip( explode( '|', self::VALID_EXTENSIONS ) );
			add_filter( 'status_header', array( __CLASS__, 'status_header' ), 10, 4 );
			/*
			 * Do early (3rd param => 0) so something else can override
			 */
			add_action( 'intermediate_image_sizes_advanced', array( __CLASS__, 'intermediate_image_sizes_advanced' ),0 );

			add_action( 'sr_generate_sized_image_file', array( __CLASS__, 'generate_sized_image_file' ), 10, 3 );

		}
		/*
		 * This gets rid of all the standard WordPress Image Sizes and uses Sunrise Image Sizes instead.
		 */
		static function intermediate_image_sizes_advanced( $image_sizes ) {
			return array();
		}
		/*
		 * If this page request is a 404 request for an image file, see if it is a registered image size
		 * for a valid files and if so generate it then out it to the browser. Next time the file will be
		 * there and this code will not need to run.
		 *
		 * TODO: Need to limit resizing to valid image types for the file in question.
		 *
		 */
		static function status_header( $status_header, $header, $text, $protocol ) {
			remove_action( 'status_header', array( __CLASS__, 'status_header' ) );

			list( $request_uri ) = explode( '?', "{$_SERVER['REQUEST_URI']}?" );

			$regex = '#^(.*)\.regenerate\.(' . self::VALID_EXTENSIONS . ')$#i';
			$regenerate = preg_match( $regex, $request_uri, $uri_parts );

			if ( '404' != $header && ! $regenerate )
				return $status_header;

			if ( $regenerate )
				$request_uri = "{$uri_parts[1]}.{$uri_parts[2]}";

			$extension = strtolower( pathinfo( $request_uri, PATHINFO_EXTENSION ) );

			$filename = basename( $request_uri );
			$relative_path = str_replace( $filename, '', $request_uri );
			$upload_dir = wp_upload_dir();

			/**
			 * The following lines of code basically combine the uploads directory and the relative path in a reliable
			 * fashion by detecting the intersections in the file paths.
			 */
			$relative_path_parts = explode( '/', trim( $relative_path, '/' ) );
			$upload_base_dir_parts = explode( '/', trim( $upload_dir['basedir'], '/' ) );
			$shared_parts = array_intersect( $relative_path_parts, $upload_base_dir_parts );
			$i = 0;
			foreach( $shared_parts as $key => $value ) {
				if( $i == $key ) {
					unset( $relative_path_parts[$key] );
				} else {
					break;
				}
				$i++;
			}
			/**
			 * @var string $sub_dirname Directory path for file that matches postmeta '_wp_attached_file'.
			 */
			$sub_dirname = implode( '/', $relative_path_parts );
			/**
			 *  @var string $dirname Get full path to directory where requestor expects file to reside
			 */
			$dirname = $upload_dir['basedir'] . '/' . $sub_dirname;

			if ( isset( self::$valid_extensions[$extension] ) ) {
				$args = array(
					'request_uri' => $request_uri,
					'dirname'     => $dirname,
					'regenerate'  => $regenerate,
					'image_type'  => false,
					'image_size'  => false,
					'filename'    => false,
					'extension'   => $extension,
					'sub_dirname' => $sub_dirname,
				);
				/*
				 * Look for images like /john-smith-small-headshot.jpg, create if not found and  appropriate
				 *
				 * TODO: (mikes 2012-06-16): I want to restructure this so we can support .../uploaded-file-headshot.jpg
				 * TODO:                     in addition to /uploaded-file-small-headshot.jpg.
				 */
				$image_size_types = sr_get_image_size_types();
				$viable_size_types = array();
				foreach( $image_size_types as $size_type_name => $type_size ) {
					$suffix = sr_dashize( $size_type_name );
					$pattern = "#{$suffix}.{$extension}$#";
					if( preg_match( $pattern, $request_uri ) ) {
						$viable_size_types[] = $size_type_name;
					}
				}
				usort( $viable_size_types, function( $a, $b ) {
					return strlen( $b ) - strlen( $a );
				} );
				$actual_size_type = array_shift( $viable_size_types );
				$args['filename'] = $actual_size_type;
				$args['image_type'] = $image_size_types[$actual_size_type]['image_type'];
				$args['image_size'] = $image_size_types[$actual_size_type]['image_size'];
				$image_type = sr_get_image_type( $image_size_types[$actual_size_type]['image_size'] );
				$args['default_size'] = $image_type['default_size'];

				self::_generate_image_if_not_exists( $args );
			}
			return $status_header;
		}

		/**
		 * @static
		 * @param $args
		 * @return bool
		 *
		 */
		 private static function _generate_image_if_not_exists( $args ) {

			if ( 'default' == $args['image_size']
				/**
				 * Removed this "||" because it wouldn't generate "myphoto-search-headshot.png"
				 * if search was 'default.'  This was added previously because it wouldn't
				 * generate "myphoto-headshot.png" for an image when the image had a default
				 * that was not named 'default'. Removing the || to see if we really need it.
				 */ //|| $args['default_size'] == $args['image_size'] )
			)
				$args['filename'] = $args['image_type'];

			$suffix = sr_dashize( $args['filename'] ). ".{$args['extension']}";
			$suffix_length = strlen( $suffix );

			if ( 0 != substr_compare( $args['request_uri'], $suffix, - $suffix_length ) ) {
				return false;
			}

			$full_file_basename = substr_replace( substr( basename( $args['request_uri'] ), 0, - $suffix_length ), '', -1 ) . ".{$args['extension']}";
			$filepath = "{$args['dirname']}/{$full_file_basename}";

			if ( ! file_exists( $filepath ) ) {
				return false;
			}

			$_wp_attached_file_meta_value = "{$args['sub_dirname']}/{$full_file_basename}";

			$attachment_id = (int) sr_get_attachment_id_by( 'filepath', $_wp_attached_file_meta_value );
			if ( 0 == $attachment_id ) {
				return false;
			}

			/**
			 * @var Sunrise_Image_File $image_file
			 */
			$image_filepath = sr_generate_sized_image_by( 'attachment_id', $attachment_id, $args['image_size'], $args );
			sr_output_image_by( 'filepath', $image_filepath, $args );

			return true;
		}

		/**
		 * @param string $image_size  Valid image size as defined via sr_register_image_type()
		 * @return Sunrise_Image_File
		 */
		function as_sized( $image_size ) {
			$this->image_size = $image_size;
			$sized_image_file = clone $this;
			$sized_image_filepath = $this->get_sized_filepath( $image_size );
			if ( ! file_exists( $sized_image_filepath ) ) {
				$this->generate_size( $image_size );
			}
			$sized_image_file->filepath = $sized_image_filepath;
			return $sized_image_file;
		}

		/**
		 * @return void
		 */
		function delete_sized() {
			$sizes = sr_get_image_type_sizes( $this->image_type );
			foreach( array_keys( $sizes ) as $image_size ) {
				$sized_filepath = $this->get_sized_filepath( $image_size );
				$delete_file = apply_filters( 'sr_delete_sized_image_file',
					file_exists( $sized_filepath ), $sized_filepath, $this->image_type, $image_size, $this );
				if ( $delete_file ) {
					unlink( $sized_filepath );
				}
			}
		}
		/**
		 * @param bool|string $image_size
		 * @return bool|string
		 */
		function get_sized_filepath( $image_size = false ) {
			$sized_filepath = false;
			if ( $image_size ) {
				switch ( $image_size ) {
					case false:
					case 'default':
						$image_type = sr_get_image_type( $this->image_type );
						$image_size = isset( $image_type['default_size'] ) ? $image_type['default_size'] : 'default';
					default:
						$size_type = "{$image_size}-{$this->image_type}";
						if ( ! sr_is_valid_image_size_type( $size_type ) ) {
							$size_type = false;
						}
						break;
				}
				if ( $size_type ) {
					$extension = $this->extension;
					$size_type = sr_dashize( $size_type );
					$sized_filepath = "{$this->dirname}/{$this->filename}-{$size_type}.{$extension}";
				}
			} else {
				// TODO: Make this handle cropped information
				$sized_filepath = $this->filepath;
			}
			return $sized_filepath;
		}
		function get_crop_attributes( $image_size ) {
			/**
			 * @var array $image_meta Get all the custom field values for this attachment
			 */
			$crop_attributes = sr_get_attached_image_attributes( $this, $image_size );
			return apply_filters( 'sr_get_image_file_crop_attributes', $crop_attributes, $this, $image_size );

		}
		/**
		 * @param $image_size
		 * @return bool|mixed|void
		 *
		 * TODO (mikes): Consider renaming to generate_sized()
		 */
		function generate_size( $image_size ) {
			$sized_filepath = false;
			require_once( sr_admin_path( 'includes/image.php' ) );
			$image_size = apply_filters( 'sr_pre_generate_sized_image_file', $image_size, $this );
			if ( ! sr_is_valid_image_type_size( $this->image_type, $image_size ) ) {
				sr_die( '[%s] is not a valid image size for [%s]', $image_size, $this->image_type );
			}
			if ( $this->exists ) {
				$image_crop = new Sunrise_Image_Crop( $this, array(
					'image_size'    => $image_size,
					'attachment_id' => $this->attachment_id,
				));
				$image_crop = apply_filters( 'sr_sized_image_crop', $image_crop );
				$sized_filepath = apply_filters( 'sr_generate_sized_image_file', $this->filepath, $image_crop, $image_crop->image_file );
			}
			return $sized_filepath;
		}
		/**
		 *
		 * TODO (mikes): Make this function private, and test to see if that works
		 *
		 * @static
		 * @param $filepath
		 * @param $image_crop
		 * @param $image_file
		 * @return false|string|WP_Error
		 */
		static function generate_sized_image_file( $filepath, $image_crop, $image_file ) {
			$sized_filepath = $filepath;
			if ( $image_file->exists ) {
				$image_dimensions = apply_filters( 'sr_generated_image_dimensions', array(
					'crop_top'        => $image_crop->top,
					'crop_left'       => $image_crop->left,
					'crop_width'      => $image_crop->width,
					'crop_height'     => $image_crop->height,
					'output_width'    => $image_crop->output_width,
					'output_height'   => $image_crop->output_height,
					'original_width'  => $image_file->width,
					'original_height' => $image_file->height,
				), $image_crop->image_type, $image_crop->image_size, $filepath, $image_crop );
				$sized_filepath = sr_wp_crop_image(
					$filepath,
					$image_dimensions['crop_left'],
					$image_dimensions['crop_top'],
					$image_dimensions['crop_width'],
					$image_dimensions['crop_height'],
					$image_dimensions['output_width'],
					$image_dimensions['output_height'],
					false,                      // false = NOT "ABSOLUTE" - TODO: Figure out what that means and if we need true.
					$image_crop->filepath       // File to output
				);
			}
			return $sized_filepath;
		}
		function __construct( $image_type, $args = array() ) {

			parent::__construct( $args );

			/*
			 * Check if image type is registered
			 */
			if ( ! sr_is_valid_image_type( $image_type ) ) {

				$error_msg = sprintf( __( 'Image type [%s] not registered yet.' ), $image_type );
				if ( WP_DEBUG )
					sr_die( $error_msg );

				$this->last_error = new Exception( $error_msg );

			}
			/*
			 * Set the only must-have property
			 */
			$this->image_type = $image_type;

			$this->do_action( 'image_file_construct', $this );
			return $this;

		}
		function get_size() {
			if ( ! $this->_size ) {
				$image_size = getimagesize( $filepath );
				$this->_size = $image_size['size'];         // TODO: If this correct?!?!
			}
			return $this->_size;
		}

		function set_width( $width ) {
			$this->_width = intval( $width );
		}
		function set_max_width( $max_width ) {
			$this->_max_width = intval( $max_width );
		}
		function set_height( $height ) {
			$this->_height = intval( $height );
		}
		function set_max_height( $max_height ) {
			$this->_max_height = intval( $max_height );
		}
		function get_max_width() {
			if ( ! $this->_max_width ) {
				$this->_set_max_width_height();
			}
			return $this->_max_width;
		}
		function get_max_height() {
			if ( ! $this->_max_height ) {
				$this->_set_max_width_height();
			}
			return $this->_max_height;
		}
		function _set_max_width_height() {
			list( $this->_max_width, $this->_max_height ) = sr_get_max_image_type_dimensions( $this->image_type );
		}
		function get_max_size() {
			$sizes = array_keys( sr_get_image_type_sizes( $this->image_type ) );
			return end( $sizes );
		}
		protected function _initialize_from_getimagesize( $force = false ) {
			if ( ! $this->_imagesize_initialized || $force ) {
				$this->_imagesize_initialized = true;
				if ( ! $this->exists ) {
					$this->generate_size( $this->get_max_size() );
				}
				if ( $this->exists ) {
					$imagesize = getimagesize( $this->filepath );
					if ( ! $imagesize ) {
						if ( WP_DEBUG )
							sr_die(  __( 'Image File not found or invalid within %s: %s.' ), get_class( $this ), $this->filepath );
					} else {
						if ( ! $this->_width || $force )
							$this->_width = $imagesize[0];

						if ( ! $this->_height || $force )
							$this->_height = $imagesize[1];

						if ( ! $this->_hwstring || $force )
							$this->_hwstring = $imagesize[3];

						if ( ! $this->_mime_type || $force )
							$this->_mime_type = $imagesize['mime'];
					}
				}
				$this->_imagesize_initialized = true;
			}
		}
		function set_filepath( $filepath ) {
			parent::set_filepath( $filepath );
			if ( ! empty( $filepath ) && file_exists( $filepath ) )
				$this->_initialize_from_getimagesize( true /* true=>force */);
		}
		protected function _clear_calculated_properties() {
			if ( ! self::$_pause_clear ) {
				parent::_clear_calculated_properties();
				$this->_hwstring = false;
				$this->_width = false;
				$this->_height = false;
			}
		}
		function get_width() {
			if ( ! $this->_width ) {
				$this->_initialize_from_getimagesize( true );
				$this->_width = $this->apply_filters( 'image_file_width', $this->_width );
			}
			return $this->_width;
		}
		function get_height() {
			if ( ! $this->_height ) {
				$this->_initialize_from_getimagesize( true );
				if ( ! $this->_height )
					$this->_height = 0;
				$this->_height = $this->apply_filters( 'image_file_height', $this->_height );
			}
			return $this->_height;
		}
		function get_hwstring() {
			if ( ! $this->_hwstring ) {
				$this->_initialize_from_getimagesize( true );
				$this->_hwstring = $this->apply_filters( 'image_file_hwstring', $this->_hwstring );
			}
			return $this->_hwstring;
		}
		function set_hwstring( $hwstring ) {
			$this->_hwstring = $hwstring;
		}
		function get_dimensions() {
			$this->_initialize_from_getimagesize( true );
			return "{$this->_width}x{$this->_height}";
		}
		function get_exists() {
			return '/' != substr( $this->filepath, -1 ) && file_exists( $this->filepath );
		}
		function __set( $property_name, $value ) {
			switch ( $property_name ) {
				case 'width':
				case 'height':
				case 'max_width':
				case 'max_height':
				case 'hwstring':
					call_user_func( array( &$this, "set_{$property_name}" ), $value );
					break;

				default:
					parent::__set( $property_name, $value );
					break;
			}
		}
		function __get( $property_name ) {
			$value = false;
			switch ( $property_name ) {
				case 'width':
				case 'height':
				case 'max_width':
				case 'max_height':
				case 'hwstring':
				case 'dimensions':
				case 'exists':
					$value = call_user_func( array( &$this, "get_{$property_name}" ) );
					break;

				default:
					$value = parent::__get( $property_name );
					break;
			}
			return $value;
		}
		function __toString() {
			$html = '';
			// Make sure the file exists
			if( file_exists( $this->filepath ) ) {
				// Fetch the alt attribute from the database
				$alt = get_post_meta( $this->attachment_id, '_wp_attachment_image_alt', true );
				// Setup all of the image html attributes
				$image_attributes = array_filter( array(
					'src' => esc_url( $this->url ),
					'width' => esc_attr( $this->width ),
					'height' => esc_attr( $this->height ),
					'alt' => empty( $this->extra['alt'] ) ? esc_attr( $alt ) : esc_attr( $this->extra['alt'] ),
					'class' => empty( $this->extra['class'] ) ? '' : esc_attr( $this->extra['class'] ),
					'itemprop' => empty( $this->extra['itemprop'] ) ? '' : esc_attr( $this->extra['itemprop'] ),
				) );
				// Allow image html attributes to be altered
				$image_attributes = apply_filters( 'sunrise_image_file_html_attributes', $image_attributes, $this );
				// Convert array of html attributes into actual markup
				$atts = array();
				foreach( $image_attributes as $key => $value ) {
					$atts[] = $key . '="' . $value . '"';
				}
				$html = '<img '. join(' ', $atts) .' />';
			}
			// Allow output to be filtered, whether empty or not
			$html = apply_filters( 'sunrise_image_file_html', $html, $this );
			return (string) $html;
		}
	}
	Sunrise_Image_File::on_load();
}
