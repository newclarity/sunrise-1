<?php
/*
Plugin Name: Sunrise Files
Plugin URL: http://docs.getsunrise.org/files/
File Name: sunrise-files.php
Author: The Sunrise Team
Author URL: http://getsunrise.org
Version: 0.0.0
Description: Adds File Management Objects to WordPress
*/

define( 'SUNRISE_FILES_DIR', dirname( __FILE__ ) );
require( SUNRISE_FILES_DIR . '/includes/file-functions.php');
require( SUNRISE_FILES_DIR . '/includes/file-class.php');

sr_load_plugin_files( 'files', array( 'file' ) );

//sr_load_demos( SUNRISE_FILES_DIR );
