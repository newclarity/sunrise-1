<?php
/*
 * test.php - Used to test Sunrise Templates Module
 */
list($dir) = explode('/wp-content/',getcwd());
include_once "{$dir}/wp-load.php";
$templates = new Sunrise_Templates();
$template_files = $templates->query(array(
	'headers' => array( 'Include in Export' => true ),
	'include_posts' => true,
));
if (isset($_GET['print_r'])) {
	header('Content-Type:text/plain');
	print_r($template_files);
} else {
	echo '<h1>Templates and Their Others Pages, for Export</h1><ol>';
	foreach($template_files as $filename => $template) {
		echo "<li>{$filename}:<ul>";
		foreach($template->posts as $post) {
			$title = get_the_title($post->ID);
			echo "<li>{$title}</li>";
		}
		echo "</ul></li>";
	}
	echo '</ol>';
}
