<?php
/*
Plugin Name: Sunrise Templates Module
Plugin URL: http://docs.getsunrise.org/templates/
File Name: sunrise-templates.php
Author: The Sunrise Team
Author URL: http://getsunrise.org
Version: 0.1
Description: Currently supports Page Template Headers. Future versions will support Custom Post Type-specific templates.
Globals Exposed: $sr_templates
Hooks Exposed: sr_templates_query, extra_template_headers
*/
if (!class_exists('Sunrise_Templates')) {
	class Sunrise_Templates {
		var $template_files = array();
		var $non_template_files = array();
		function __construct( $args = array() ) {
			$args = wp_parse_args( array(
				'initialize' => false,
			));
			// TODO: Decide if this approach with initialize() should stay.  Need input from others.
			if ($args['initialize'])
				$this->initialize();
		}
		static function on_load() {
			global $sr_templates;
			add_action( 'init', array( __CLASS__, '_init' ) );
			add_action( 'switch_theme', array( __CLASS__, '_switch_theme' ) );
			$sr_templates = new Sunrise_Templates( $args = array() );
		}
		static function _init() {
		  $templates = get_option('sunrise_persist_templates');
		  if (strlen($templates)==0)
		    update_option('sunrise_persist_templates',1);  // Default to persisting
		}
		static function _switch_theme($theme) {
		  delete_option('Sunrise_Templates');
			global $sr_templates;
			$sr_templates = new Sunrise_Templates( array( 'initialize' => true) );
		}
		// TODO: Decide if this approach with initialize() should stay.  Need input from others.
		function initialize($force = false) {
			static $initialized = false;
			if (!$initialized || $force) {
				$save = false;
				$theme = get_current_theme();
				if (!isset($this->template_files)) {
					$this->template_files = get_option('Sunrise_Templates');
					$this->non_template_files = get_option('sunrise_non_templates');
					if (empty($this->template_files)) {
						$this->template_files = $this->non_template_files = array();
						$save = true;
					}
				}
				$first_time = (empty($this->template_files) ? true : false);
				$themes = get_themes();
				$templates = $themes[$theme]['Template Files'];
				$all_theme_files = array_merge($this->template_files,$this->non_template_files);
				foreach($templates as $template) {
					$file_name = basename($template);
					$file_time = date(DATE_ISO8601,filemtime($template)); //http://en.wikipedia.org/wiki/ISO_8601, i.e. '2005-08-15T15:52:01+0000'
					if ($first_time || !isset($all_theme_files[$file_name]) || $file_time > $all_theme_files[$file_name]['Last Updated']) {
						$headers = get_file_data( $template, array('Template Name'=>'Template Name'), 'template' );
						$this->non_template_files[$file_name] = new stdClass();
						if (empty($headers['Template Name'])) {
							$this->non_template_files[$file_name]->last_updated = $file_time;
						} else {
							$this->template_files[$file_name]->headers = $headers;
							$this->template_files[$file_name]->last_updated = $file_time;
							foreach($this->template_files[$file_name]->headers as $name => $value)
								if (preg_match('#^(yes|true)$#i',$value)) {
									$this->template_files[$file_name]->headers[$name] = true;
								} else if (preg_match('#^(no|false)$#i',$value)) {
									$this->template_files[$file_name]->headers[$name] = false;
								}
							$save = true;
						}
					}
				}
				if ($save && get_option('sunrise_persist_templates')) {
					update_option('Sunrise_Templates',$this);
				}
			}
			$initialized = true;
		}
    function query( $args = array() ) {
	    $args = wp_parse_args( $args, array(
			 'headers' => false,  // Header values can be null for all, or a non-null value for a match
			 'include_posts' => false,
	    ));
	    $results = array();
	    $this->initialize();
	    if (!is_array($args['headers'])) {
		    $results = $this->template_files;
	    } else {
		    foreach($this->template_files as $filename => $template_file) {
					foreach($args['headers'] as $header_name => $header_value) {
						if (isset($template_file->headers[$header_name])) {
							if (null===$header_value || $header_value==$template_file->headers[$header_name])
								$results[$filename] = $template_file;
						}
					}
		    }
	    }
	    if ($args['include_posts']) {
		    add_filter( 'posts_fields', array( &$this, '_posts_fields' ), 10, 2 );
		    add_filter( 'posts_where', array( &$this, '_posts_where' ), 10, 2 );
		    add_filter( 'posts_join', array( &$this, '_posts_join' ), 10, 2 );
		    add_filter( 'posts_orderby', array( &$this, '_posts_orderby' ), 10, 2 );
		    $template_filenames = implode( "','", array_keys( $results ) );
		    $posts_query = new WP_Query( array(
			    'post_type' => 'page',
			    'posts_per_page' => -1,
			    'template_filenames' => "'{$template_filenames}'",
			   ));
		    if (count($posts_query->posts)==0) {
			    $results = array();
		    } else {
			    $template_filename = false;
					foreach($posts_query->posts as $post) {
						if ($template_filename != $post->template_filename) {
							$template_filename = $post->template_filename;
							$results[$template_filename]->posts = array();
						}
						// Remove the non-standard posts property
						unset($post->template_filename);
						$results[$template_filename]->posts[] = $post;
					}
		    }
		    remove_filter( 'posts_orderby', array( &$this, '_posts_orderby' ) );
		    remove_filter( 'posts_join', array( &$this, '_posts_join' ) );
		    remove_filter( 'posts_where', array( &$this, '_posts_where' ) );
		    remove_filter( 'posts_fields', array( &$this, '_posts_fields' ) );
	    }
	    return apply_filters( 'sr_templates_query', $results, $args );
    }
		function _posts_orderby( $orderby, $query ) {
			global $wpdb;
			$orderby = "{$wpdb->postmeta}.meta_value";
			return $orderby;
		}
		function _posts_fields( $fields, $query ) {
			global $wpdb;
			$fields = "{$wpdb->postmeta}.meta_value AS template_filename,{$fields}";
			return $fields;
		}
		function _posts_where( $where, $query ) {
			global $wpdb;
			$where = "{$where} AND {$wpdb->postmeta}.meta_key='_wp_page_template' AND {$wpdb->postmeta}.meta_value IN ({$query->query_vars['template_filenames']})";
			return $where;
		}
		function _posts_join( $join, $query ) {
			global $wpdb;
			$join = "{$join} INNER JOIN {$wpdb->postmeta} ON {$wpdb->posts}.ID={$wpdb->postmeta}.post_id";
			return $join;
		}
	}
	Sunrise_Templates::on_load();

	function sr_get_template_files( $args = array() ) {
		global $sr_templates;
		$sr_templates->initialize();
		return $sr_templates->template_files;
	}
}

