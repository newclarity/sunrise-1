<?php

function register_initialization( $args = array() ) {
	Sunrise_Initializations::register( $args );
}
function add_initializer( $initializer, $args = array() ) {
	Sunrise_Initializations::add( $initializer, $args );
}
