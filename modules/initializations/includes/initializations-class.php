<?php
if (!class_exists('Sunrise_Initializations')) {
	class Sunrise_Initializations {
		static function on_load() {
			global $sr_initializations;
			$sr_initializations = array();
//			add_action( 'init', array( __CLASS__, 'init' ) );
		}
//		static function init() {
//		}
		static function add( $initializer, $args = array() ) {
			$args = wp_parse_args( array(
				'file_changed' => false,
				'priority' => 0,
			) );
			extract( $args );
			if ( $file_changed && sr_file_changed( $file_changed ) ) {
				add_action( 'init', $initializer, $priority );
			}
		}
		static function register( $args = array() ) {
			global $sr_initializations;

			$args = wp_parse_args( $args, array(
				'requirement' => false,
			) );
			$requirement = $args['requirement'];

			// Plugins and Themes
			if ( preg_match( '#^(download|install|activate|delete)_(plugin|theme)$#', $requirement, $match ) ) {
				$args = wp_parse_args( $args, array(
					$match[2] => false,
					'version' => 'latest',
					'directory' => "{$match[2]}s",
					'repository' => 'wordpress.org',
					'local_path' => false,
					'zipped' => true,
				) );

				if ( $args['local_path'] )
					$args['repository'] = 'local';

				$key = $args[ $match[2] ];

			} else if ( preg_match( '#^(add|delete)_(page|post)$#', $requirement, $match ) ) {
				// A Page is really just an 'add_post' for post_type='page'
				$args = wp_parse_args( $args, array(
					'post_type' => $match[2],
					'post_title' => false,
					'post_content' => false,
					'url_path' => false,  // This is set when multiple segements are desired
					'template' => false,
				) );
				$key = $args[ 'post_type' ];

			} else if ( preg_match( '#^(add|delete)_(category|tag|post_tag|taxonomy_term)$#', $requirement, $match ) ) {
				if ( 'tag' == $match[2] )
					$match[2] = 'post_tag';
				$args = wp_parse_args( $args, array(
					'taxonomy' => $match[2],
					'term' => false,
				) );
				$key = "{$args[ 'taxonomy' ]}:{$args[ 'term' ]}";

			} else if ( preg_match( '#^(add|delete)_user$#', $requirement, $match ) ) {
				$args = wp_parse_args( $args, array(
					'user_login' => false,
					'role' => 'subscriber',
					'user_url' => false,
					'user_email' => false,
					'first_name' => false,
					'last_name' => false,
					'use_ssl' => false,
					'user_pass' => false,
				) );
				$key = $args[ 'user_login' ];

			} else if ( preg_match( '#^(add|remove)_menu$#', $requirement, $match ) ) {
				$args = wp_parse_args( $args, array(
					 'title' => false,
					 'location' => 'primary',
					 'items' => array(),
				) );
				$key = "{$args[ 'location' ]}:{$args[ 'title' ]}";

			} else if ( preg_match( '#^(add|remove)_menu_item$#', $requirement, $match ) ) {
				$args = wp_parse_args( $args, array(
					'type' => false,
					'url' => false,
				) );
				$key = $args[ 'url' ];

			} else if ( preg_match( '#^(set|update|delete|remove)_option(s)?$#', $requirement, $match ) ) {
				if ( 'set' == $match[1] )
					$match[1] = 'update';
				if ( 'remove' == $match[1] )
					$match[1] = 'delete';
				if ( !isset( $match[2] ) ) {  // Only "*_option", not "*_options"
					$args = wp_parse_args( $args, array(
						'option' => false,
						'value' => false,
					) );
					$key = $args[ 'option' ];
				} else {
					// This is just a convenience for multiple (update|delete)_option
					$args = wp_parse_args( $args, array(
						'options' => array(),
					) );
					foreach( $args[ 'options' ] as $option => $value ) {
						register_initialization( array(
							'requirement' => "{$match[1]}_option",
							'option' => $option,
							'value' => $value,
						) );
					}
					$key = false; // Bypass adding this one site is was a convenience
				}

			} else {
				switch ( $requirement ) {
					case 'foo':
					case 'bar':
					case 'baz':
				}
				$key = '???';

			}
			if ($key) {
				$name = "{$requirement}:{$key}";
				$sr_initializations[ $name ] = $args;
			}

		}
	}
	Sunrise_Initializations::on_load();

}



