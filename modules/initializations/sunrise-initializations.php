<?php
/*
Plugin Name: Sunrise Initializations
Plugin URL: http://docs.getsunrise.org/initializations/
File Name: sunrise-initializations.php
Author: The Sunrise Team
Author URL: http://getsunrise.org
Version: 0.0.0
Description: Supports registering initializations for plugins, themes and modules.
Globals Exposed: $sr_initializations
Hooks Exposed:
*/

define( 'SUNRISE_INITIALIZATIONS_DIR', dirname( __FILE__ ) );
require( SUNRISE_INITIALIZATIONS_DIR . '/includes/initializations-functions.php');
require( SUNRISE_INITIALIZATIONS_DIR . '/includes/initializations-class.php');

//sr_load_plugin_files( 'initializations', array() );
//sr_load_demos( SUNRISE_INITIALIZATIONS_DIR );
