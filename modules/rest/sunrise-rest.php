<?php
/*
Filename: sunrise-rest.php
Plugin Name: Sunrise REST
Plugin URL: http://getsunrise.com/plugins/urls/
Description: Provides a RESTful API for WordPress and Sunrise
Author: The Sunrise Team
Author URL: http://getsunrise.com/
Version: 0.0.0
Requires: n/a
*/

define( 'SUNRISE_REST_DIR', dirname( __FILE__ ) );
require( SUNRISE_REST_DIR . '/includes/rest-router-class.php' );

//sr_load_plugin_files( 'rest', array( 'whatever' ) );

//sr_load_demos( SUNRISE_REST_DIR );



