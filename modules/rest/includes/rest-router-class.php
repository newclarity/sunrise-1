<?php


/**
 * Routes URLs for Sunrise REST
 *
 * @example:
 *
 *  URL: http://example.com/api/ticket/12345
 *
 */
class Sunrise_Rest_Router {
  static function on_load() {
    add_action( 'template_redirect', array( __CLASS__, 'template_redirect' ), 0 );
  }
  /**
   * Function to test for a URL that starts with /api/ and then looks to see if
   * it can match the rest of URL with a known API, continues on if no match.
   *
   * @return void
   */
  static function template_redirect() {
    if ( preg_match( '#^/api/(.*)(\.)([a-z]+)$#i', $_SERVER['REQUEST_URI'], $match ) ) {
      $request = explode( '/', $match[1] );
      if ( ! $output = apply_filters( 'sr_rest_output', false, $request ) )
				switch ( $request[0] ) {
					case 'post':
						if ( isset( $request[1] ) && is_numeric( $request[1] ) )
							$output = get_post( $request[1] );
						break;
					case 'taxonomies':
						if ( ! isset( $request[1] ) ) {
							$output = get_taxonomies();
						} else {
							$taxonomy = get_taxonomy( $request[1] );
							if ( ! isset( $request[2] ) ) {
								$output = $taxonomy;
							} else {
								switch ( $request[2] ) {
									case 'terms':
										if ( isset( $taxonomy->name ) )
											$output = get_terms( $taxonomy->name );
										break;
								}
							}
						}
						break;
				}
			if ( $output ) {
				$extensions = array(
					'json' => 'application/json',
					'phps' => 'application/vnd.php.serialized',
					'xml'  => 'application/xml',
					'yaml' => 'application/yaml',
					'html' => 'text/html',
					'txt'  => 'text/plain',
					);

				if ( isset( $extensions[end($match)] ) ) {
					$content_type = apply_filters( 'sr_rest_content_type', strtolower( $extensions[end($match)] ), $output );
					header( "Content-Type: {$content_type}" );
					switch ( end($match) ) {
						case 'json':
							$output = json_encode( $output );
							break;

						case 'phps':
							$output = serialize( $output );
							break;

						case 'txt':
							ob_start();
							print_r( $output );
							$output = ob_get_clean();
							break;

						default:
							$output = false;
					}
					if ( $output ) {
						echo $output;
						exit(0);
					}
				}
      }
    }
  }
}
Sunrise_Rest_Router::on_load();
