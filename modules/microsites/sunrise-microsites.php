<?php
/*
Plugin Name: Sunrise Microsites
Plugin URL: http://docs.getsunrise.org/microsites/
File Name: sunrise-microsites.php
Author: The Sunrise Team
Author URL: http://getsunrise.org
Version: 0.0.0
Description: Adds Microsites for Post Types
Globals Exposed:
Hooks Exposed:
*/

define( 'SUNRISE_MICROSITES_DIR', dirname( __FILE__ ) );
require( SUNRISE_MICROSITES_DIR . '/includes/microsite-reorder-child-class.php');
require( SUNRISE_MICROSITES_DIR . '/includes/microsite-functions.php');
require( SUNRISE_MICROSITES_DIR . '/includes/microsite-class.php');
require( SUNRISE_MICROSITES_DIR . '/includes/microsites-class.php');
require( SUNRISE_MICROSITES_DIR . '/includes/microsite-page-class.php');
require( SUNRISE_MICROSITES_DIR . '/includes/microsite-pages-class.php');

sr_load_plugin_files( 'microsites', array( 'microsite-page' ) );
sr_load_demos( SUNRISE_MICROSITES_DIR );
