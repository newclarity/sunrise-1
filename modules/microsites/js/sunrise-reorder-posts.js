/**
 * TODO (mikes 2011-12-14): This needs to be rearchitected as it doesn't really fit into Microsites per se.
 */
jQuery(document).ready(function($) {
	$("#reorder-posts-metabox ul#posts-to-reorder").sortable({
		items:"li",
		cursor:"move",
		opacity:0.6,
		placeholder: "ui-sortable-placeholder",
		update: function(event,ui) {
			var postOrderIds = [];
			$("#reorder-posts-metabox ul#posts-to-reorder li").each(function(i,e) {
				postOrderIds.push($(e).attr("id").replace("reorder-post-bar-",""));
			});
			var ajaxData = {
				action:'reorder_posts_action',
				sub_action:'reorder_posts',
				post_id: $("#post_ID").val(),
				post_type: $("#post_type").val(),
				page_type: $('#post-body-content input[name="microsite_page_type"]').val(),
				post_order_ids:postOrderIds
			};
			$.sunrise.ajaxPost(ajaxData);
		}
	}).disableSelection();

	$('select.visibility-select').change(function(){
		var select = $(this);
		var value = select.val();
		var li = select.closest('li');
		li.toggleClass('not-visible');
		var ajaxData = {
			post_id: $('#post_ID').val(),
			post_type: $("#post_type").val(),
			page_type: $('#post-body-content input[name="microsite_page_type"]').val(),
			page_post_id: $('#post-body-content input[name="microsite_page_post_id"]').val(),
			value: li.attr('data-postID')
		};
		var invisible = li.hasClass('not-visible');
		if( invisible ) {
			ajaxData.action = 'hide_reorder_posts_action';
			ajaxData.sub_action = 'hide_reorder_posts';
		} else {
			ajaxData.action = 'show_reorder_posts_action';
			ajaxData.sub_action = 'show_reorder_posts';
		}
		//console.log( ajaxData );
		$.sunrise.ajaxPost(ajaxData);
	});

});


