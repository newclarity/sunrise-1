/**
 * TODO:Need to add some error handling for AJAX calls.
 */
/*
 * This first set of code defines $.sunrise.microsites and $.sunrise.microsites.pages
 * TODO (mikes):Redo so that (almost?) only $.sunrise.microsites.pages.get(element) actually needs element passed.
 */
jQuery(document).ready(function($) {
	$.sunrise.microsites = {
		getPage:function(page) {
			return $(page).hasClass('microsite-page') ? page :this.pages.get(page);
		},
		getAdminId:function() {
			return $(".microsite-admin").attr("id");
		},
		getPostId:function() {
			return this.getAdminId().replace("microsite-admin-","");
		},
		getPostType:function() {
			return $("#microsite-post-type").val();
		},
		getControlText:function(controlId) {
			return $("#"+controlId).val();
		},
		getAjaxData:function(sub_action,extra) {
			if (undefined==extra)
				extra = {};
			var ajaxData = $.extend({
				action:'microsite_action',
				sub_action:sub_action,
				is_microsite:1,
				post_id:this.getPostId(),
				post_type:this.getPostType(),
			},extra);
			return ajaxData;
		},
		expandAllPages:function() {
			$(".microsite-admin .microsite-page").each(function(i,e) {
				e = $(e);
				if (!e.hasClass("expanded"))
					e.addClass("expanded");
			});
			this.fixupPageExpansion();
		},
		collapseAllPages:function() {
			$(".microsite-admin .microsite-page").each(function(i,e) {
				e = $(e);
				if (e.hasClass("expanded"))
					e.removeClass("expanded");
			});
			this.fixupPageExpansion();
		},
		toggleAllPageExpansion:function() {
			var isExpanded = "expanded"==this.getPageExpansionStatus()
			this.setPageExpansionStatus(isExpanded ? "collapsed" :"expanded");
			$(".microsite-admin .microsite-page").each(function(i,e) {
				e = $(e);
				if (isExpanded) {
					e.removeClass("expanded").addClass("collapsed");
				} else {
					e.removeClass("collapsed").addClass("expanded");
				}
			});
			this.fixupPageExpansion();
		},
		fixupPageExpansion:function() {
			var collapse_label = this.getLocalizedLabel("collapse_pages_text");
			var expand_label = this.getLocalizedLabel("expand_pages_text");
			this.setPageExpansionLabel("expanded"==this.getPageExpansionStatus()?collapse_label:expand_label);
			this.setListHeight();
		},
		getPageExpansionStatus:function() {
			return $("#toggle-expansion-button").hasClass("expanded") ? "expanded" : "collapsed";
		},
		setPageExpansionStatus:function(status) { // This does NOT toggle
			var button = $("#toggle-expansion-button");
			if ("expanded"==status) {
				button.addClass("expanded").removeClass("collapsed");
			} else {
				button.removeClass("expanded").addClass("collapsed");
			}
			return this;
		},
		getPageExpansionLabel:function() {
			return $("#toggle-expansion-label").html();
		},
		setPageExpansionLabel:function(value) {
			$("#toggle-expansion-button").text(value);
		},
		getLocalizedLabel:function(labelName) {
			return $("input#"+labelName).val();
		},
		setListHeight:function() {
			$(".microsite-admin .page-containers").each(function(i,e) {
				var pageList = $(e).find("ul.page-list");
				var additionalMargin = Math.round(parseInt(pageList.find("li.microsite-page").offsetLeft) / 2);
				var newHeight = pageList[0].offsetTop+pageList[0].offsetHeight+additionalMargin;
				$(e).css("height",newHeight+"px");
			});
		},
		updatePageOrder:function() {
			var pageIds = [];
			var doNothing = function(){};
			$("#publish-pages .microsite-page").not(".page-type-main").each(function(i,e) {
				pageIds.push($(e).attr("id").replace("microsite-page-",""));
			});
			$.sunrise.ajaxPost(this.getAjaxData('update_page_order',{'page_ids':pageIds}),doNothing);
			return false;
		},
		addCustomPage:function(onSuccess) {
			$.sunrise.ajaxPost(this.getAjaxData('add_custom_page'),onSuccess);
			return false;
		},
		appendPage:function(page,listStatus) {
			$(".microsite-admin ul#"+listStatus+"-page-list").append(page);
		},
		getVirtualCustomPages:function(){
			return $(".microsite-admin .microsite-page.page-type-custom.no-post");
		},
		hasVirtualCustomPages:function(){
			return 0<this.getVirtualCustomPages().length;
		},
		saveVirtualCustomPages:function(onSuccess){
			this.getVirtualCustomPages().each(function(i,e){
				// Should only ever be one unsaved custom microsite page...
				$.sunrise.microsites.pages.createPage($("#"+e.id),onSuccess);
			});
		}
	};
	$.sunrise.microsites.pages = {
		parent :$.sunrise.microsites,
		get:function(element) {
			var page;
			switch (typeof element) {
				case "string":
					page = $(element);
					break;
				case "object":
					page = undefined == element.jquery ? $(element) :element;
					break;
				default:
					page = false;
					break;
			}
			return !page ? $("") :page.hasClass("microsite-page") ? page :page.parents(".microsite-page");
		},
		getId:function(element) {
			return this.get(element).attr("id");
		},
		getPostId:function(element) {
			return this.getId(element).replace("microsite-page-","");
		},
		hasPost:function(element) {
			return this.get(element).hasClass("has-post");
		},
		getList:function(element) {
			return this.get(element).parents(".page-list");
		},
		getListId:function(element) {
			return this.getList(element).attr("id");
		},
		getStatus:function(element) {
			var regExp = new RegExp("^(publish|hidden)-page-list$","g");
			return regExp.exec(this.getListId(element))[1];
		},
		getTitle:function(element) {
			return $("#"+this.getId(element)+"-edit-title").val();
		},
		getKey:function(element) {
			return $("#"+this.getId(element)+"-key").val();
		},
		getType:function(element) {
			return $("#"+this.getId(element)+"-type").val();
		},
		getTemplateName:function(element) {
			return $("#"+this.getId(element)+"-template-name").val();
		},
		getAjaxData:function(sub_action,page) {
			var ajaxData = jQuery.extend(this.parent.getAjaxData(sub_action),{
				action:'microsite_action',
				page_type:this.getType(page),
				page_key:this.getKey(page),
				page_template_name:this.getTemplateName(page),
				page_title:this.getTitle(page),
				page_status:this.getStatus(page),
				page_post_id:this.hasPost(page)?this.getPostId(page):0
			});
			return ajaxData;
		},
		isExpanded:function(element) {
			return this.get(element).hasClass("expanded");
		},
		expand:function(element,adjustHeight) {
			var page = this.get(element);
			var expanded = false;
			if (! page.hasClass("expanded")) {
				page.addClass("expanded");
				if (undefined == adjustHeight || adjustHeight)
					this.parent.setListHeight();
				expanded = true;
			}
			return expanded;
		},
		collapse:function(element,adjustHeight) {
			var page = this.get(element);
			var collapsed = false;
			if (page.hasClass("expanded")) {
				page.removeClass("expanded");
				if (undefined == adjustHeight || adjustHeight)
					this.parent.setListHeight();
				collapsed = true;
			}
			return collapsed;
		},
		enableTitleEdit:function(element) {
			var page = this.get(element);
			page.find(".page-title").hide();
			page.find(".page-title-edit").css("display","block");  // Not show() because it will set as inline
			page.find(".page-title-input").focus();
		},
		closeTitleEdit:function(element) {
			var pageId = this.getId(element);
			$("#"+pageId+" .page-title-edit").hide();
			$("#"+pageId+" .page-title").delay("slow").show();
		},
		setTitle:function(element,pageTitle) {
			$("#"+this.getId(element)+"-bar .page-title").html(pageTitle);
		},
		setViewLink:function(element,pageViewLink) {
			$("#"+this.getId(element)+"-view-page").attr("href",pageViewLink);
		},
		setControlText:function(control,controlText) {
			$(control).html(controlText);
		},
		updateStatus:function(element) {
			var page = this,wasExpanded = this.isExpanded(element);
			var pageId = this.getId(element);
			if (wasExpanded)
				this.collapse(element,false);
			this.ajaxPost(element,this.getAjaxData("update_page_status",element),function(page) {
				// Assumes the page will be in the correct container update.
				if (wasExpanded)
					page.expand("#"+pageId,true);
				// TODO (mikes):Might be able to optimize this so only called when needed.
				$.sunrise.microsites.updatePageOrder();
			});
		},
		updateTitle:function(element,onSuccess) {
			var pages = $.sunrise.microsites.pages;
			pages.ajaxPost(element,pages.getAjaxData("update_page_title",element),onSuccess);
		},
		togglePageControls:function(element) {
			var page = this.get(element);
			if (page.hasClass("expanded")) {
				page.removeClass("expanded");
			} else {
				page.addClass("expanded");
			}
			this.parent.setListHeight();
		},
		toggleStatusLabel:function(control) {
			this.setControlText(control,this.parent.getControlText(("hidden" == this.getStatus(control) ? "hide" :"publish")+"_control_text"));
		},
		toggleStatus:function(control) {
			var pageStatus = this.getStatus(control);
			this.parent.appendPage(this.get(control),pageStatus == "hidden" ? "publish" :"hidden");
			this.toggleStatusLabel(control);
			this.updateStatus(control);
		},
		replaceWith:function(element,replacement,args) {
			this.get(element).replaceWith(replacement);
			var page = this.get("#"+this.getId(replacement));
			if (undefined != args.expand && args.expand)
				this.expand(page);
			return page;
		},
		ajaxPost:function(element,data,onSuccess) {
			var page = this,wasExpanded = this.isExpanded(element);
			$.post(ajaxurl,data,function(response) {
				if (undefined != response.html) {
					page.replaceWith(element,response.html,{expand:wasExpanded});
				}
				if (undefined != onSuccess) {
					onSuccess(response,data,page);
				}
			});
		},
		createPage:function(element,onSuccess) {
			var pages = $.sunrise.microsites.pages;
			pages.ajaxPost(element,pages.getAjaxData("create_page",element),onSuccess);
		},
		editVirtualPage:function(element,onSuccess) {
			$.sunrise.microsites.pages.createPage(element,function(response,data,page) {
				window.location.href = response.editUrl.replace('&amp;','&');
			});
		}
	}
});
// This second set of code implement the hooks showing how other code could also implement the same hooks.
jQuery(document).ready(function($) {
	$.sunrise.addAction('onMicrositePageAction',function(args) {
		var pages = $.sunrise.microsites.pages;
		if ($(args.element).hasClass('rename-page')) {
			pages.enableTitleEdit(args.element);
			return false;
		} else if ($(args.element).hasClass('toggle-page-status')) {
			pages.toggleStatus(args.element);
			return false;
		} else if ($(args.element).hasClass('view-page')) {
			alert(page.getId(args.element));
			return false;
		} else if ($(args.element).hasClass('edit-page')) {
			pages.editVirtualPage(args.element);
			return false;
		} else if ($(args.element).hasClass('configure-page')) {
			pages.editVirtualPage(args.element);
			return false;
		} else if ($(args.element).hasClass('trash-page')) {
			return false;
		}
	});
});
// This last set of code are the events that respond to user interaction
jQuery(document).ready(function($) {
	//$.sunrise.microsites.expandAllPages();
	$.sunrise.microsites.setListHeight();
	$(".microsite-admin .microsite-page-controls .hashed-link").live("click",function() {
		var $this = $(this);
		if (this.id.match(/(edit-page|configure-page)/)) {
			// TODO (mikes):this if() test should be made generic
			if (!$this.hasClass("disabled")) {
				$this.addClass("disabled");
				$.sunrise.doAction('onMicrositePageAction',{'element':this});
			}
		} else {
			$.sunrise.doAction('onMicrositePageAction',{'element':this});
		}
		// No need to removeClass('disabled') because both edit-page & configure-page redirect to  admin edit screen
		return false;
	});
	$(".microsite-admin .microsite-page-bar .page-title").live("dblclick",function() {
		$.sunrise.microsites.pages.enableTitleEdit(this);
		return false;
	});
	$(".microsite-admin .microsite-page .page-title-edit-controls a.ok").live("click",function(){
		var $this = $(this);
		if (!$this.hasClass("disabled")) {
			$this.addClass("disabled");
			$.sunrise.microsites.pages.updateTitle(this,function() {
				$this.removeClass("disabled");
			});
		}
		return false;
	});
	$(".microsite-admin .microsite-page .page-title-edit-controls a.cancel").live("click",function(){
		$.sunrise.microsites.pages.closeTitleEdit(this);
		return false;
	});
	$(".microsite-admin .page-edit").live("click",function(){
		$.sunrise.microsites.pages.togglePageControls(this);
		return false;
	});
	$(".microsite-admin #publish-page-list").sortable({
		items:"li.movable",
		cursor:"move",
		opacity:0.6,
		connectWith:"#hidden-page-list",
		placeholder:"ui-sortable-placeholder",
		update:function(event,ui) {
			// TODO (mikes):Consider optimization that can enable batching these two
			$.sunrise.microsites.pages.updateStatus(ui.item);
		}
	}).disableSelection();
	$(".microsite-admin #hidden-page-list").sortable({
		items:"li.movable",
		cursor:"move",
		opacity:0.6,
		connectWith:"#publish-page-list",
		placeholder:"ui-sortable-placeholder"
	}).disableSelection();
	$(".microsite-admin #toggle-expansion-button").click(function(){
		$.sunrise.microsites.toggleAllPageExpansion();
		return false;
	});
	$(".microsite-admin #add-custom-page").click(function(){
		var $this = $(this);
		if (!$this.hasClass("disabled")) {
			$this.addClass("disabled");
			var microsites = $.sunrise.microsites;
			var onSuccess = function(response,data) {
				$this.removeClass("disabled");
				microsites.appendPage(response.html,'hidden');
			};
			if (microsites.hasVirtualCustomPages()) {
				microsites.saveVirtualCustomPages(function(){
					microsites.addCustomPage(onSuccess);
				});
			} else {
				microsites.addCustomPage(onSuccess);
			}
		}
		return false;
	});
	/**
	 * TODO (mikes 2011-12-14): Need to move this to wherever the form gets moved to.
 	 */
	$("#reorder-posts-metabox ul#posts-to-reorder").sortable({
		items:"li",
		cursor:"move",
		opacity:0.6,
		placeholder: "ui-sortable-placeholder",
		update: function(event,ui) {
			//saveMicrositePageOrder(event,ui);
		}
	}).disableSelection();

});


