<?php
/**
 * TODO (mikes): Need to reconcile 'title', 'label', and 'page_title' for Microsite Pages and Microsite Page templates.

 */
if ( ! class_exists( 'Sunrise_Microsite_Page' ) ) {
	/**
	 * @property object            $post
	 * @property string            $post_status
	 * @property string            $admin_bar_html
	 * @property string            $title_edit_html
	 * @property string            $labels
	 * @property object            $page_id
	 * @property bool              $is_fixed
	 * @property bool              $has_post
	 * @property string            $slug
	 * @property string            $title
	 * @property string            $fixed_class
	 * @property string            $page_title
	 * @property string            $permalink
	 * @property string            $page_key
	 * @property string            $post_id
	 * @property array             $controls
	 * @property string            $controls_html
	 * @property string            $page_bar_classes
	 * @property string            $page_status
	 * @property string            $link_properties
	 * @property Sunrise_Microsite $parent
	 * @property string            $edit_slug
	 * @property string            $form_name
	 * @property array             $forms
	 */
	class Sunrise_Microsite_Page extends Sunrise_Base {

		var $page_type = false;
		var $label = false;
		var $user_type = false;
		var $page_template_name = false;
		var $is_virtual = false;
		/**
		 * @var SR_DOCS_Post $_post
		 */
		protected $_post = false;
		/**
		 * @var bool|string
		 */
		protected $_page_id = false;
		/**
		 * @var bool|string
		 */
		protected $_page_key = false;
		/**
		 * @var bool|string
		 */
		protected $_page_status = false;
		/**
		 * @var bool|string
		 */
		protected $_slug = false;
		/**
		 * @var bool|string
		 */
		protected $_title = false;
		/**
		 * @var bool
		 */
		protected $_is_fixed = false;
		/**
		 * @var bool|string
		 */
		protected $_form_name = false;
		/**
		 * @var bool|array
		 */
		protected $_forms = false;
		/**
		 * @var bool|array
		 */
		protected $_controls = array();
		/**
		 * @var int
		 */
		protected $_microsite_post_id = false;
		/**
		 * @var bool|string
		 */
		protected $_microsite_post_type = false;

		function __construct ( $args = array() ) {
			$args = sr_parse_args( $args );
			$args = Sunrise::normalize_args(
				$args,
				array(
					'type' => 'page_type',
					'template' => 'page_template_name',
				)
			);
			$args = $this->apply_filters( 'args', $args );
			$this->construct_from_args( $args );
			$this->do_action( 'initialize', $args );
			return $this;
		}

		function get_form_name () {
			if ( ! $this->_form_name ) {
				$prefix =
					$this->page_template_name == $this->page_type
						? $this->page_type :
						"{$this->page_template_name}_{$this->page_type}";
				$this->_form_name = sr_underscorize( "_{$this->parent->post_type}_{$prefix}_microsite_page" );
			}
			return $this->_form_name;
		}

		function get_forms () {
			if ( ! $this->_forms || ! is_object( $this->_forms[0] ) ) {
				if ( is_array( $this->_forms ) ) {
					/**
					 * If we have array of forms properties convert them to an array of form objects.
					 */
					foreach ( $this->_forms as $form_name => $form_array ) {
						/**
						 * See if the form was array( $n => $form_name ) instead of array( $form_name => $form ),
						 *   i.e. array( 'microsite_custom_page' ) vs. array( 'microsite_custom_page' => array( ... ) )
						 * If so, try to look it up.
						 */
						if ( is_numeric( $form_name ) && is_string( $form_array ) ) {
							/**
							 * Remove the numerically indexed form
							 */
							unset( $this->_forms[$form_name] );
							/**
							 * Find the form for the microsite
							 */
							if ( $form = sr_get_form( 'post', 'microsite-page', $form_name = $form_array ) ) {
								/**
								 * Add to list if found
								 */
								$this->_forms[$form_name] = $form;

							}
						} else {
							$this->_forms[$form_name] = sr_register_form( $form_name, $form_array );
						}
					}

				} else if ( isset( $this->extra['form'] ) && is_array( $this->extra['form'] ) ) {
					/**
					 * If we only have one set of forms properties convert them to a form object in a 1 element array.
					 */
					$form_name = $this->form_name;
					$this->_forms = array( $form_name => sr_register_form( $form_name, $this->extra['form'] ) );
					unset( $this->extra['form'] );

				} else if ( isset( $this->extra['fields'] ) && is_array( $this->extra['fields'] ) ) {
					/**
					 * If we only have $this->extra['fields'] then convert to a form object using use defaults for other form properties.
					 */
					$form_name = $this->form_name;
					$this->_forms = array(
						$form_name => sr_register_form(
							$form_name,
							array(
								'form_type' => 'admin_post_form',
								'post_type' => 'microsite-page',
								'title'     => "{$this->label} " . __( "Basic Information", 'sunrise-microsites' ),
								'priority'  => 'high',
								'fields'    => $this->extra['fields'],
							)
						)
					);
					unset( $this->extra['fields'] );

				}
			}
			return $this->_forms;
		}

		function set_parent ( $parent ) {
			$this->_parent = $parent;
		}

		function get_parent () {
			if ( ! $this->_parent ) {

				if ( ! $this->_microsite_post_type ) {
					$this->_microsite_post_type = Sunrise::get_context()->post_type;
				}

				$this->_parent = sr_get_microsite( $this->_microsite_post_type );

				if ( $this->_microsite_post_id ) {
					$this->_parent = get_post( $this->_microsite_post_id );
				}

			}
			return $this->_parent;
		}

		/**
		 * @param array|object $args
		 * @return Sunrise_Microsite_Page
		 * TODO (mikes): This is still probably not a great name, nor probably is the functionality properly located yet...
		 */
		function finalize ( $args ) {
			if ( ! is_object( $this->post ) ) {
				$this->post = isset( $args['post'] ) && $args['post'] ? $args['post'] : @get_post( $args['post_id'] );
			}
			$this->page_template_name = $args['page_template_name'];
			$this->page_type = $args['page_type'];
			if ( empty( $args['microsite_post_type'] ) ) {
				$args['microsite_post_type'] = @get_post( $args['microsite_post_id'] )->post_type;
			}
			$parent = Sunrise_Microsites::get_microsite( $args['microsite_post_type'] );
			if ( $parent ) {
				$parent->post_id = $args['microsite_post_id'];
			}
			$this->parent = $parent;
			$this->do_action( 'finalize', $args );
		}

		function update_page_key ( $page_key = false ) {
			$page_key = $page_key ? $page_key : Sunrise_Microsites::make_page_key(
				$this->page_type,
				$this->page_template_name
			);
			Sunrise_Microsites::update_page_key( $this->post_id, $page_key );
		}

		/**
		 * @return string
		 * TODO (mikes): Implement a more robust approach here.
		 */
		function get_edit_slug () {
			return "post.php?post={$this->post_id}&action=edit";
		}

		function update_post_fields ( $field_values ) {
			/**
			 * @var wpdb $wpdb
			 */
			global $wpdb;
			if ( is_numeric( $this->post_id ) && 0 < $this->post_id ) {
				$wpdb->update( $wpdb->posts, $field_values, array( 'ID' => $this->post_id ) );
				wp_cache_delete( $this->post_id, 'posts' );
				$post_id = $this->post_id;
				/**
				 * NOTE: If we pass $this->post_id to get_post()
				 *  it will throw an error during AJAX calls.
				 */
				$this->post = get_post( $post_id );
			}
		}

		function update_post_field ( $field_name, $value ) {
			/**
			 * @var wpdb $wpdb
			 */
			global $wpdb;
			if ( is_numeric( $this->post_id ) && 0 < $this->post_id ) {
				$wpdb->update(
					$wpdb->posts,
					array(
						$field_name => $value,
					),
					array(
						'ID' => $this->post_id,
					)
				);
				wp_cache_delete( $this->post_id, 'posts' );
			}
		}

		function set_title ( $title ) {
			//			sr_die( 'We really are using $page->title?' );
			$this->_title = $title;
		}

		function get_title () {
			//			sr_die( 'We really are using $page->title?' );
			if ( ! $this->_title ) {
				$this->_title = @$this->_post->post_title;
			}
			return $this->_title;
		}

		function set_post ( $post ) {
			$this->_post = $post;
		}

		function get_post () {
			return $this->_post;
		}

		function set_page_status ( $page_status ) {
			$this->_page_status = $page_status;
		}

		function get_permalink () {
			return is_numeric( $this->post_id ) ? get_post_permalink( $this->post_id ) : '#';
		}

		function get_page_status () {
			if ( ! $this->_page_status ) {
				$this->_page_status = $this->_post ? $this->_post->post_status : false;
			}
			return $this->_page_status;
		}

		/**
		 * Returns the meta_value used to lookup the page in wp_postmeta.
		 *
		 * @example:
		 *         Example values might be
		 *         'main'                        (for the main page, no matter it's template name)
		 *         'twitter', 'blog'             (when $page->page_type == $page->page_template_name)
		 *         'parent_child[publications]'  (when $page->page_type != $page->page_template_name)
		 * @return string
		 */
		function get_page_key () {
			if ( ! $this->_page_key ) {
				$this->_page_key = Sunrise_Microsites::make_page_key( $this->page_type, $this->page_template_name );
			}
			$this->_page_key = $this->apply_filters( 'page_key', $this->_page_key );
			return $this->_page_key;
		}

		function get_microsite () {
			return $this->parent;
		}

		function get_post_status () {
			return $this->_post->post_status;
		}

		function set_page_id ( $page_id ) {
			$this->_page_id = $page_id;
		}

		function get_ID () {
			if ( ! $this->_page_id ) {
				return $this->get_page_id();
			}
			return $this->_page_id;
		}

		function get_page_id () {
			if ( ! $this->_page_id ) {
				if ( ! $this->_post ) {
					sr_trigger_error(
						__( '[%s->%s] not an object when attempting to retrieve [page_id] from Microsite Page [%s]' ),
						get_class( $this ),
						'post',
						$this->label
					);
				}
				$this->_page_id = "microsite-page-{$this->_post->ID}";
			}
			return $this->_page_id;
		}

		function get_post_id () {
			$post_id = false;
			if ( @$this->_post->ID ) {
				$post_id = $this->_post->ID;
			}
			return $post_id;
		}

		function set_post_id ( $post_id ) {
			$this->_post = get_post( $post_id );
		}

		function get_slug () {
			if ( ! $this->_slug ) {
				$this->_slug = $this->_post ? $this->_post->post_name : false;
			}
			return $this->_slug;
		}

		function get_is_fixed () {
			return 'main' == $this->page_type ? true : $this->_is_fixed;
		}

		/**
		 * @return bool|string
		 * TODO (mikes): Consider naming to just 'get_title()'; need to document class first to understand if this is okay.
		 */
		function get_page_title () {
			return $this->_post ? get_the_title( $this->_post->ID ) : $this->label;
		}

		function update_page_title ( $post_title ) {
			$this->update_post_fields(
				array(
					'post_title' => $post_title,
					'post_name'  => $this->generate_unique_post_slug( $post_title ),
				)
			);
		}

		/**
		 * @param string $post_title
		 * @return string
		 */
		function generate_unique_post_slug ( $post_title ) {
			return apply_filters(
				'sr_microsite_page_unique_slug',
				wp_unique_post_slug(
					sanitize_title( $post_title ),
					$this->post_id,
					$this->post_status,
					'microsite-page',
					$this->parent->post_id
				),
				$this
			);
		}

		function set_is_fixed ( $is_fixed ) {
			if ( 'main' == $this->page_type ) {
				sr_trigger_error( 'Cannot set the Main Page to be fixed because it is already fixed' );
			} else {
				$this->_is_fixed = $is_fixed;
			}
		}

		function get_controls () {
			if ( ! is_array( $this->_controls ) ) {
				sr_trigger_error(
					__( 'The controls property of %s must be an array.', 'sunrise-microsites' ),
					$this->description
				);
			}
			$this->_controls = Sunrise_Controls::expand_controls( $this, 'microsite-page-controls', $this->_controls );
			return $this->_controls;
		}

		/**
		 * Allow a subclass to set the controls but don't allow other classes to set controls.
		 *
		 * @param $controls
		 * @return
		 */
		protected function set_controls ( $controls ) {
			return $this->_controls = $controls;
		}

		function get_link_properties () {
			/**
			 * @var Sunrise_Microsite $parent - Note the inline format $x ? $y : $z doesn't work when $x is a virtual property.
			 */
			$parent = $this->parent;
			$parent_id = isset( $parent->post_id ) ? $parent->post_id : $parent->ID;
			$link_properties = array(
				'microsite_post_type' => $parent ? $parent->post_type : false,
				'post_parent'         => $parent_id,
				'post_id'             => $this->post_id,
				'page_status'         => $this->page_status,
				'page_template'       => $this->page_template_name,
				'post_type'           => 'microsite-page',
			);
			$link_properties = $this->apply_filters( 'link_properties', $link_properties );
			return $link_properties;
		}

		function get_controls_html () {
			/**
			 * TODO: Fix this to generate the real control set control html
			 */
			$html = array();
			sr_push_link_properties( $this->link_properties );
			/**
			 * @var Sunrise_Control $control
			 */
			foreach ( $this->controls as $key => $control ) {
				if ( preg_match( '#^(admin_edit_post|view_post|admin_trash_post)$#', $control->link_template ) ) {
					$control->use_hashed_link = ! $this->_post;
				}
				/**
				 * This is for the toggle post status control for 'Publish' and 'Hide'
				 */
				if ( isset( $control->post_status ) ) {
					$control->post_status = $this->page_status;
				}

				$html[] = $control->html;
			}
			sr_pop_link_properties();
			return implode( ' | ', $html );
		}

		function get_dashized_template_name () {
			return sr_dashize( $this->page_template_name );
		}

		function get_dashized_page_type () {
			return sr_dashize( $this->page_type );
		}

		function get_has_post () {
			return false !== $this->_post;
		}

		function get_description () {
			return parent::get_description(
				"template_name={$this->page_template_name}, page_type={$this->page_template_name}"
			);
		}

		function get_page_bar_classes () {
			return implode(
				' ',
				$this->apply_filters(
					'page_bar_classes',
					array(
						'microsite-page',
						'page-type-' . sr_dashize( $this->page_type ),
						'user-type-' . sr_dashize( $this->user_type ),
						'template-' . sr_dashize( $this->page_template_name ),
						$this->is_fixed ? 'fixed' : 'movable',
						$this->has_post ? 'has-post' : 'no-post',
					)
				)
			);
		}

		function get_title_edit_html () {
			$labels = Sunrise_Microsites::get_labels();
			$html = <<<HTML
<div class="page-title-edit-controls">
	<a id="{$this->page_id}-edit-ok" href="#save-page-title" class="button ok">{$labels['ok_button']}</a>
	<a id="{$this->page_id}-edit-cancel" href="#cancel-page-title-edit" class="cancel">{$labels['cancel_link']}</a>
</div>
HTML;
			return $html;
		}

		function get_related_objects_count() {
			$i = 0;
			if ( is_a( $this, 'Sunrise_Parent_Child_Microsite_Page' ) ) {
				$relationship = sr_get_connecting_relationship( $this->parent->post_type, $this->_child_type );
				$relationship = apply_filters( 'sunrise_microsite_related_objects_relationship', $relationship, $this->parent->post_type, $this->_child_type );
				$related_objects = sr_get_related_objects(
					$relationship,
					$this->parent->post_id
				);
				$i = count( $related_objects );
			}
			return $i;
		}

		function get_admin_bar_html () {
			$labels = Sunrise_Microsites::get_labels();
			$related_objects_count = $this->get_related_objects_count();
			$count = $related_objects_count ? "($related_objects_count)" : '';
			$html = <<<HTML
<li id="{$this->page_id}" class="{$this->page_bar_classes}">
	<dl id="{$this->page_id}-bar" class="microsite-page-bar">
		<dt class="microsite-page-handle">
			<span class="page-title">
				{$this->page_title} {$count}
			</span>
			<span class="page-title-edit">
				<span class="edit-fields">
					<input type="text" id="{$this->page_id}-edit-title" name="page-title-edit" class="page-title-input widefat" value="{$this->page_title}" />
				</span>
				{$this->title_edit_html}
			</span>
			<span class="page-controls">
				<span class="user-type">{$this->user_type}</span>
				<a id="{$this->page_id}-edit-link" class="page-edit" href="#expand-page-bar">{$labels['edit_page_link']}</a>
			</span>
		</dt>
		<dd id="{$this->page_id}-controls" class="microsite-page-controls">
			<ul>{$this->controls_html}</ul>
		</dd>
	</dl>
	<input type="hidden" id="{$this->page_id}-key" class="microsite-page-key" value="{$this->page_key}" />
	<input type="hidden" id="{$this->page_id}-type" class="microsite-page-type" value="{$this->page_type}" />
	<input type="hidden" id="{$this->page_id}-template-name" class="microsite-page-template-name" value="{$this->page_template_name}" />
</li>
HTML;
			return $this->apply_filters( 'admin_bar_html', $html );
		}

		function __set ( $property_name, $value ) {
			switch ( $property_name ) {
				case 'post':
				case 'page_id':
				case 'post_id':
				case 'page_status':
				case 'title':
				case 'permalink':
				case 'parent':
					call_user_func( array( &$this, "set_{$property_name}" ), $value );
					break;

				case 'post_status':
				case 'page_key':
				case 'admin_bar_html':
				case 'microsite':
				case 'slug':
				case 'is_fixed':
				case 'has_post':
				case 'fixed_class':
				case 'page_title':
				case 'controls':
				case 'controls_html':
				case 'page_bar_classes':
				case 'link_properties':
				case 'title_edit_html':
				case 'labels':
				case 'edit_slug':
				case 'form_name':
				case 'forms':
					$this->_cannot_set_error( $property_name, $value );
					break;

				default:
					//if ( ! Sunrise::delegate_set( $this->_post, $property_name, $value ) )
					parent::__set( $property_name, $value );
					break;
			}
		}

		function __get ( $property_name ) {
			$value = false;
			switch ( $property_name ) {
				case 'page_status':
				case 'link_properties':
				case 'post':
				case 'permalink':
				case 'title':
				case 'page_id':
				case 'page_key':
				case 'post_id':
				case 'post_status':
				case 'has_post':
				case 'admin_bar_html':
				case 'microsite':
				case 'slug':
				case 'is_fixed':
				case 'fixed_class':
				case 'page_title':
				case 'controls':
				case 'controls_html':
				case 'page_bar_classes':
				case 'title_edit_html':
				case 'labels':
				case 'edit_slug':
				case 'form_name':
				case 'forms':
				case 'parent':
					$value = call_user_func( array( &$this, "get_{$property_name}" ) );
					break;

				default:
					//$value = Sunrise::delegate_get( $this->_post, $property_name );
					//if ( is_null( $value ) )
					$value = parent::__get( $property_name );
					break;
			}
			return $value;
		}
	}
}
