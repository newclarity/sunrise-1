<?php

if ( ! class_exists( 'Sunrise_Microsites' ) ) {

	class Sunrise_Microsites extends Sunrise_Static_Base {

		/**
		 * @var bool
		 */
		static $prefix_permalink = true;
		/**
		 * @var bool|Sunrise_Microsite
		 */
		protected static $_current_microsite = false;
		private static $_labels = array();
		private static $reorder_admin_page = false;

		static function on_load () {
			global $sr_microsites;
			$sr_microsites = array(
				'microsites'     => array(),
				'page_templates' => array(),
			);

			/**
			 * TODO (mikes 2011-12-14): Replace this next line once we have a registration system for classes and objects.
			 */
			self::$reorder_admin_page = new Sunrise_Reorder_Admin_Page();

			self::$_labels = array(
				'ok_button'            => __( 'Ok', 'sunrise-microsites' ),
				'cancel_link'          => __( 'Cancel', 'sunrise-microsites' ),
				'edit_page_link'       => __( 'Edit Page', 'sunrise-microsites' ),
				'view_page_button'     => __( 'View Page', 'sunrise-microsites' ),
				'preview_page_button'  => __( 'Preview Page', 'sunrise-microsites' ),
				'create_page_button'   => __( 'Create New Custom Page', 'sunrise-microsites' ),
				'publish_pages_text'   => __( 'Published Pages', 'sunrise-microsites' ),
				'hidden_pages_text'    => __( 'Hidden Pages', 'sunrise-microsites' ),
				'publish_control_text' => __( 'Publish', 'sunrise-microsites' ),
				'hide_control_text'    => __( 'Hide', 'sunrise-microsites' ),
				'expand_pages_text'    => __( 'Expand All', 'sunrise-microsites' ),
				'collapse_pages_text'  => __( 'Collapse All', 'sunrise-microsites' ),
				'hidden_callout_text'  => __( 'Hidden', 'sunrise-microsites' ),
			);
			$sr_microsite = false;
			sr_add_action( __CLASS__, 'admin_menu' );
			sr_add_action( __CLASS__, 'admin_init' );
			sr_add_action( __CLASS__, 'sr_controls_init' );
			sr_add_action( __CLASS__, 'init' );
			sr_add_filter( __CLASS__, 'sr_get_context' );
			sr_add_filter( __CLASS__, 'sr_urls_init' );
			sr_add_filter( __CLASS__, 'sr_post_types_init' );
			sr_add_filter( __CLASS__, 'parent_file' );
			sr_add_action( __CLASS__, 'adminmenu' );
			sr_add_action( __CLASS__, 'post_type_link', 10, 2 );
			sr_add_action( __CLASS__, 'sr_microsite_page_templates_init', 99 ); // Do after (most) others do it.
			sr_add_action( __CLASS__, 'sr_add_admin_menu_option', 9, 3 ); // Gotta do before Sunrise_Admin_Skins
			sr_add_action( __CLASS__, 'edit_form_advanced' );
			sr_add_filter( __CLASS__, 'sr_get_forms', 10, 2 );
			sr_add_action( __CLASS__, array( 'page_row_actions', 'row_actions' ), 10, 2 );
			sr_add_action( __CLASS__, array( 'post_row_actions', 'row_actions' ), 10, 2 );
			sr_add_filter( __CLASS__, 'template_redirect', 99 );
			sr_add_filter( __CLASS__, 'template_include' );
			sr_add_filter( __CLASS__, 'nocache_headers' );
			sr_add_filter( __CLASS__, 'wp_title' );
			sr_add_filter( __CLASS__, 'wp_head' );

			remove_action( 'wp_head', 'rel_canonical' );

			if ( defined( 'DOING_AJAX' ) ) {
				sr_add_action( __CLASS__, array( 'admin_init', 'microsites_init' ), 9 );
				sr_add_action( __CLASS__, array( 'admin_init', 'assign_microsite' ), 9 );
			} else if ( is_admin() ) {
				sr_add_action( __CLASS__, array( 'admin_menu', 'microsites_init' ), 9 );
				sr_add_action( __CLASS__, array( 'admin_menu', 'assign_microsite' ), 9 );
			} else {
				// 'wp_loaded' is called at the end of /wp-setting.php, before $wp->main( $query_vars )
				sr_add_action( __CLASS__, array( 'wp_loaded', 'microsites_init' ) );
				// 'wp' is called at end of $wp->main( $query_vars ) after $wp->parse_query()
				// 9 = Do before Sunrise Fields init
				sr_add_action( __CLASS__, array( 'wp', 'assign_microsite' ), 9 );
			}
		}

		/**
		 * If within a Microsite and a Related Post Page,
		 * set <link rel="canonical" href="..." /> to proper URL.
		 */
		static function wp_head () {
			$context = sr_get_context();
			if ( $context->is_microsite && ! empty( $context->related_post_id ) ) {
				$link = get_post_permalink( $context->related_post_id );
				echo "<link rel=\"canonical\" href=\"{$link}\" />\n";
			} else {
				rel_canonical();
			}
		}

		static function template_include ( $template ) {
			if ( is_microsite() ) {

				$context = sr_get_context();
				$post_type = get_post_type();
				$microsite_post_type = $context->microsite_post_type;

				$templates = array();
				if ( $context->microsite_page_post_id && $post_type != 'microsite-page' ) {
					$templates[] = "microsite-{$microsite_post_type}-post-{$post_type}.php";
					$templates[] = "microsite-{$microsite_post_type}-{$post_type}.php";
					$templates[] = "microsite-post-{$post_type}.php";
					$templates[] = "microsite-{$post_type}.php";
				} else if ( property_exists(
											$context,
											'microsite_page_template_name'
										) && ! empty( $context->microsite_page_template_name )
				) {
					$microsite_page_template_name = sr_dashize( $context->microsite_page_template_name );
					$templates[] = "microsite-{$microsite_post_type}-page-{$microsite_page_template_name}.php";
					$templates[] = "microsite-{$microsite_post_type}-{$microsite_page_template_name}.php";
					$templates[] = "microsite-page-{$microsite_page_template_name}.php";
					$templates[] = "microsite-{$microsite_page_template_name}.php";
				}
				$templates[] = "microsite-{$microsite_post_type}.php";
				$templates[] = 'microsite.php';

				if ( $microsite_template = get_query_template( 'microsite', $templates ) ) {
					$template = $microsite_template;
				}
			}
			return $template;
		}

		static function wp_title ( $title ) {
			$context = sr_get_context();
			if ( $context->is_microsite ) {
				if ( ! empty( $context->related_post_id ) ) {
					$post_title = get_the_title( $context->related_post_id );
					$parent_title = get_the_title( $context->microsite_post_id );
					/**
					 * TODO (mikes 2012-02-14): Create a template, maybe 'related_html_title'?
					 */
					$title = "{$post_title} | {$parent_title}";
				} else if ( is_post_type_archive() ) {
					$post_type_obj = get_post_type_object( $context->microsite_post_type );
					$title = $post_type_obj->label;
				} else {
					$page_template = self::get_page_template(
						$context->microsite_page_template_name,
						array(
							'post_type' => $context->microsite_post_type,
						)
					);

					if ( empty( $page_template['html_title'] ) ) {
						$page_template['html_title'] = get_the_title( $context->microsite_page_post_id );
					}

					$title = str_replace(
						array(
							'%page_title%',
							'%post_title%',
							'%site_name%',
						),
						array(
							$context->microsite_page_post_title,
							get_the_title( $context->microsite_post_id ),
							get_bloginfo( 'name' ),
						),
						$page_template['html_title']
					);
				}
			}
			return $title;
		}

		static function admin_init () {
			$context = Sunrise::get_context();

			if ( 'microsite-page' == $context->post_type ) {
				sr_add_action( __CLASS__, array( 'admin_action_hide', 'admin_action_toggle_status' ) );
				sr_add_action( __CLASS__, array( 'admin_action_publish', 'admin_action_toggle_status' ) );
				//sr_add_action( __CLASS__, 'all_admin_notices' );  // TODO (mikes 2011-12-29): Implement this.
			}

			if ( $context->is_microsite ) {
				$root_dir = dirname( dirname( __FILE__ ) );
				$style_filepath = '/css/sunrise-microsites.css';
				sr_enqueue_style(
					'sunrise-microsites',
					plugins_url( $style_filepath, dirname( __FILE__ ) ),
					array(
						'file' => "{$root_dir}{$style_filepath}",
					)
				);
				$script_filepath = '/js/sunrise-microsites.js';
				sr_enqueue_script(
					'sunrise-microsites',
					plugins_url( $script_filepath, dirname( __FILE__ ) ),
					array(
						'file'         => "{$root_dir}{$script_filepath}",
						'dependencies' => array( 'jquery', 'jquery-ui-sortable', 'sunrise-core' )
					)
				);
				Sunrise::add_ajax_handler( __CLASS__, 'microsite_action' );
			}
		}

		/**
		 * Adds an undo message
		 *
		 * @return bool
		 * TODO (mikes 2011-12-29): Implement this.
		 */
		//		static function all_admin_notices() {
		//
		//		}

		static function admin_action_toggle_status () {
			$post_id = (int) $_GET['post'];
			$post = get_post( $post_id );

			if ( 'microsite-page' != $post->post_type ) {
				return false;
			}

			if ( empty( $_GET['action'] ) ) {
				return false;
			}

			$action = $_GET['action'];

			if ( ! preg_match( '#^(publish|hide)$#', $action ) ) {
				return false;
			}

			check_admin_referer( "{$action}-{$post->post_type}_{$post_id}" );

			// This was copied from $action=='trash', change for 'hide' if needed
			//			if ( !current_user_can($post_type_object->cap->delete_post, $post_id) )
			//				wp_die( __('You are not allowed to move this item to the Trash.') );

			// This was copied from $action=='trash', I don't think we need it
			//			if ( !$post = wp_get_single_post($post_id, ARRAY_A) )
			//				return $post;

			$status = 'hide' == $action ? 'hidden' : 'publish';

			if ( $status == $post->post_status ) {
				return false;
			}

			do_action( "{$action}_post", $post_id );

			// This was copied from wp_trash_post(), I don't think we need it
			//			add_post_meta($post_id,'_wp_trash_meta_status', $post['post_status']);
			//			add_post_meta($post_id,'_wp_trash_meta_time', time());

			$post->post_status = $status;
			wp_insert_post( $post );

			// This was copied from wp_trash_post(), I don't think we need it
			//			wp_trash_post_comments($post_id);

			$arg = 'hide' == $action ? 'hidden' : 'published';

			do_action( "{$arg}_post", $post_id );

			wp_redirect( add_query_arg( array( $arg => 1, 'ids' => $post_id ), wp_get_referer() ) );

			exit();

		}

		static function is_microsite_editor () {
			global $pagenow;
			return ( 'edit.php' == $pagenow && isset( $_GET['page'] ) && 'microsite' == $_GET['page'] );
		}

		static function nocache_headers ( $headers ) {
			if ( self::is_microsite_editor() ) {
				$headers['Cache-Control'] = "post-check=0, pre-check=0, no-store, {$headers['Cache-Control']}";
			}
			return $headers;
		}

		static function sr_get_forms ( $forms, $args = array() ) {
			if ( 'post' == $args['object_type'] && 'microsite-page' == $args['object_sub_type'] ) {
				/**
				 * @var SR_DOCS_Post $parent_post
				 */
				$parent_post = get_post( $args['object']->post_parent );
				$microsite = sr_get_microsite( $parent_post->post_type );
				/**
				 * TODO (mikes 2011-12-16): This next is really a hack. We need to define the appropriate values
				 * TODO: for this (and all) functions and set up assertions to politely fail when the are not set.
				 */
				if ( ! $microsite->post_id && ! empty( $args['object_id'] ) ) {
					$microsite->post_id = intval( $args['object_id'] );
				}
				$page = $microsite->get_page_by( 'page_key', self::get_page_key( $args['object']->ID ) );
				if ( $page ) {
					$page_forms = $page->forms;
					$forms = array_merge( $forms, $page_forms );
				}
			}
			return $forms;
		}

		static function the_microsite_nav ( $args = array() ) {
			echo self::get_microsite_nav( $args );
		}

		static function get_microsite_nav ( $args = array() ) {

			$args = wp_parse_args(
				$args,
				array(
					'empty_html' => '<div class="microsite-nav-wrap empty"></div>',
					'before_nav' => '<div class="microsite-nav-wrap"><ul>',
					'nav_item'   => '<li class="%1$s">%2$s</li>',
					'after_nav'  => '</ul></div>',
				)
			);

			$args = apply_filters( 'get_microsite_nav_args', $args );

			$microsite = self::get_current_microsite();
			$pages = $microsite->published_pages;
			$last_index = count( $pages ) - 1;
			if ( 0 >= $last_index ) {
				$html = $args['empty_html'];
			} else {
				$context = Sunrise::get_context();
				$html = array( $args['before_nav'] );
				$item_callout = ' first-item';
				for ( $i = 0; $i <= $last_index; $i ++ ) {
					if ( $item_callout && 0 < $i ) {
						$item_callout = false;
					} else if ( $last_index == $i ) {
						$item_callout = ' last-item';
					}
					$page = $pages[$i];
					/**
					 * If this $page is a 'main' page and $context has no microsite page post ID,
					 * OR if the page's post ID equals the $context's microsite page post ID.
					 */
					$current_item = ( 'main' == $page->page_type && ! $context->microsite_page_post_id )
													|| ( $page->post_id == $context->microsite_page_post_id ) ? ' current-menu-item active' : '';
					$html[] = sprintf(
						$args['nav_item'],
						"page-item page-item-{$page->post_id}{$item_callout}{$current_item}",
						"<a href='{$page->permalink}'>{$page->page_title}</a>"
					);
				}
				$html[] = $args['after_nav'];
				$html = implode( "\n", $html );
			}
			return $html;
		}

		static function sr_post_types_init () {
			$post_type_args = apply_filters(
				'sr_register_microsite_page_post_type',
				array(
					'label'              => __( 'Microsite Page' ),
					'labels'             => array(
						'name'          => __( 'Microsite Pages' ),
						'singular_name' => __( 'Microsite Page' ),
					),
					'public'             => false,
					'show_ui'            => false, // Change this is you want: /wp-admin/edit.php?post_type=microsite-page
					'show_in_menu'       => false,
					'query_var'          => 'microsite-page',
					'publicly_queryable' => false,
					'rewrite'            => false,
					'hierarchical'       => true,
					'supports'           => array( 'dummy' ),
				)
			);
			register_post_type( 'microsite-page', $post_type_args );
		}

		/**
		 * Add hidden fields for admin posts forms to display Microsite-specific properties.
		 *
		 * @return void
		 */
		static function edit_form_advanced () {
			$context = Sunrise::get_context();
			if ( $is_page = ( 'microsite-page' == $context->post_type ) ||
											sr_has_microsites( $context->microsite_post_type )
			) {

				$html = <<<HTML
<input type="hidden" name="is_microsite" 						value="1" />
<input type="hidden" name="microsite_post_id" 			value="{$context->microsite_post_id}" />
<input type="hidden" name="microsite_post_type" 		value="{$context->microsite_post_type}" />
HTML;
				if ( $is_page ) {
					$html = "{$html}\n" . <<<HTML
<input type="hidden" name="microsite_page_post_id" 	value="{$context->microsite_page_post_id}" />
<input type="hidden" name="microsite_page_title" 		value="{$context->microsite_page_title}" />
<input type="hidden" name="microsite_page_status" 	value="{$context->microsite_page_status}" />
<input type="hidden" name="microsite_page_key" 			value="{$context->microsite_page_key}" />
<input type="hidden" name="microsite_page_type" 		value="{$context->microsite_page_type}" />
<input type="hidden" name="microsite_page_template_name" value="{$context->microsite_page_template_name}" />
HTML;
				}
				echo "\n{$html}\n";
			}
		}

		static function sr_microsite_page_templates_init () {
			if ( ! sr_has_microsite_page_template( 'custom' ) ) {
				/**
				 * TODO (mikes 2012-02-08) This is page template initialization which should be done in a central place
				 * TODO: Search for "meaningless==45780345" to find related code.
				 */
				sr_register_microsite_page_template(
					'custom',
					array(
						'label'          => __( 'Custom Page', 'sunrise-microsites' ),
						'allow_multiple' => true,
						'html_title'     => '%page_title% | %post_title%',
						'fields'         => array(
							'post_title',
							'post_content',
						)
					)
				);
			}
		}

		static function sr_ajax_update_page_order ( $context ) {
			/**
			 * @var wpdb $wpdb
			 */
			global $wpdb;
			foreach ( $context->page_ids as $order => $page_id ) {
				$wpdb->update(
					$wpdb->posts,
					array( 'menu_order' => $order ),
					array(
						'post_parent' => $context->microsite_post_id,
						'post_type'   => 'microsite-page',
						'ID'          => $page_id,
					)
				);
			}
			return array( 'html' => '' );
		}

		static function _get_custom_page_count ( $post_id ) {
			/**
			 * @var wpdb $wpdb
			 */
			global $wpdb;
			$sql = <<<SQL
SELECT
	COUNT(*) AS custom_page_count
FROM
	{$wpdb->posts} p
	INNER JOIN {$wpdb->postmeta} pm ON pm.post_id=p.ID AND pm.meta_key='_sr_microsite_page_key'
WHERE 1=1
	AND p.post_status IN ('publish','hidden')
	AND 'microsite-page' = p.post_type
	AND pm.meta_value LIKE 'custom[custom]'
	AND p.post_parent = %d
SQL;
			$custom_page_count = $wpdb->get_var( $wpdb->prepare( $sql, $post_id ) );
			return intval( $custom_page_count );
		}

		static function sr_ajax_add_custom_page ( $context ) {
			$new_page_number = self::_get_custom_page_count( $context->microsite_post_id ) + 1;
			$page = self::insert_page(
				array(
					'page_title'         => "Custom Page #{$new_page_number}",
					'page_status'        => 'hidden',
					'page_type'          => 'custom',
					'page_template_name' => 'custom',
					'microsite_post_id'  => $context->microsite_post_id,
				)
			);
			return array( 'html' => $page->admin_bar_html );
		}

		static function sr_ajax_create_page ( $context ) {
			$page = self::instantiate_page( (array) $context );
			sr_push_link_properties( array( 'post_id' => $page->post_id ) );
			$response = array(
				'editUrl' => sr_get_link( 'admin_edit_post' ),
				'viewUrl' => sr_get_link( 'view_post' ),
				'html'    => $page->admin_bar_html,
			);
			sr_pop_link_properties();
			return $response;
		}

		static function sr_ajax_update_page_status ( $context ) {
			$page = self::instantiate_page( (array) $context );
			$page->update_post_fields(
				array(
					'post_status' => $context->microsite_page_status,
				)
			);
			return array( 'html' => $page->admin_bar_html );
		}

		static function sr_ajax_update_page_title ( $context ) {
			$page = self::instantiate_page( (array) $context );
			$page->update_page_title( $context->microsite_page_title );
			return array( 'html' => $page->admin_bar_html );
		}

		/**
		 * Inserts a Microsite Page
		 *
		 * @param object|array $args - Expects these elements: post_title, post_parent, post_status and page_key.
		 * @return int|WP_Error
		 * TODO: Consider if we should do a direct update to the posts table?
		 */
		static function insert_page ( $args ) {
			$error = false;
			if ( empty( $args['page_title'] ) ) {
				$error = 'page_title';
			} elseif ( empty( $args['microsite_post_id'] ) ) {
				$error = 'microsite_post_id';
			} elseif ( empty( $args['page_status'] ) ) {
				$error = 'page_status';
			} elseif ( empty( $args['page_type'] ) ) {
				$error = 'page_type';
			} elseif ( empty( $args['page_template_name'] ) ) {
				$error = 'page_template_name';
			}
			if ( $error ) {
				sr_die(
					__( 'Sunrise_Microsites::insert_page($page) called with an empty value for $args[\'%s\'].' ),
					$error
				);
			}
			$args['post_id'] = wp_insert_post(
				array(
					'post_type'   => 'microsite-page',
					'post_title'  => $args['page_title'],
					'post_parent' => $args['microsite_post_id'],
					'post_status' => $args['page_status'],
				)
			);
			$args['post'] = get_post( $args['post_id'] );
			/**
			 * @var Sunrise_Microsite_Page $page
			 */
			$page = Sunrise_Microsite_Pages::get_instance_for( $args['page_type'], $args );
			$page->update_page_key();

			$page->finalize( $args );
			$page->do_action( 'insert_page', $args );
			return $page;
		}

		static function get_page_key ( $post_id ) {
			return get_post_meta( $post_id, '_sr_microsite_page_key', true );
		}

		static function update_page_key ( $post_id, $page_key ) {
			update_post_meta( $post_id, '_sr_microsite_page_key', $page_key );
		}

		static function make_page_key ( $page_type, $page_template ) {
			return "{$page_type}[{$page_template}]";
		}

		/**
		 * @static
		 * @param      $page_key
		 * @param bool $associative - true returns key/value array, false returns index/value array
		 * @return array
		 *   $associative parameter added to support:
		 *       list($type,$template_name) = self::parse_page_key($key);

		 */
		static function parse_page_key ( $page_key, $associative = true ) {
			$parsed_page_key = array(
				'microsite_page_type'          => false,
				'microsite_page_template_name' => false,
			);
			if ( preg_match( '#^([^[]+)\[([^]]+)\]$#', $page_key, $matches ) ) {
				$parsed_page_key['microsite_page_type'] = $matches[1];
				$parsed_page_key['microsite_page_template_name'] = $matches[2];
			} else if ( $page_key ) { // TODO (mikes): This is just a hack that needs to be revised...
				$parsed_page_key['microsite_page_type'] = $page_key;
				$parsed_page_key['microsite_page_template_name'] = $page_key;
			}
			return $associative ? $parsed_page_key : array_values( $parsed_page_key );
		}

		/**
		 * @static
		 * @param object|array $page_args
		 * @return int|mixed|Sunrise_Microsite_Page|WP_Error
		 */
		static function instantiate_page ( $page_args ) {
			if ( WP_DEBUG && is_object( $page_args ) ) {
				sr_die( __( "%s::instantiate_page must be called with an array parameter, not an object." ), __CLASS__ );
			}

			$args_keys = array(
				'microsite_page_post_id'       => 'post_id',
				'microsite_page_type'          => 'page_type',
				'microsite_page_status'        => 'page_status',
				'microsite_page_template_name' => 'page_template_name',
				'microsite_page_title'         => 'page_title',
			);
			$extract_keys = array_merge(
				array_values( $args_keys ),
				array_keys( $args_keys ),
				array( 'microsite_post_id' )
			);
			$this_page_args = sr_array_extract( (array) $page_args, $extract_keys );
			$this_page_args = sr_array_rename_keys( $this_page_args, $args_keys );

			$this_page_args['microsite_post_type'] = get_post( $this_page_args['microsite_post_id'] )->post_type;
			if ( 0 == intval( $this_page_args['post_id'] ) ) {
				$page = self::insert_page( $this_page_args );
			} else {
				/**
				 * @var Sunrise_Microsite_Page $page
				 */
				$page = Sunrise_Microsite_Pages::get_instance_for( $page_args['page_type'], $this_page_args );
				$page->finalize( $this_page_args );
			}
			/**
			 * TODO (mikes 2012-06-23): Verify if the param shouldn't be $this_page_args?
			 */
			$page->do_action( 'instantiate', $page_args );
			return $page;
		}

		/**
		 * @param string       $link
		 * @param SR_DOCS_Post $post
		 * @return string
		 * TODO (mikes): Change this to use the URLs that are configured using Sunrise URLs rather than having it be hardcoded.
		 * TODO (mikes): Looks for ways to improve performance here
		 */
		static function post_type_link ( $link, $post ) {
			static $links = array();
			/**
			 * See if the post type already has a locally cached link?
			 */
			if ( isset( $links[$post->ID] ) ) {
				$link = $links[$post->ID];
			} else {
				/**
				 * Microsite Pages are relative to their parent post type
				 */
				if ( 'microsite-page' == $post->post_type ) {
					/**
					 * Another microsite page; strip '/microsite-pages/' from it's URL
					 * TODO (mikes): Is there a more elegant want to do this?
					 */
					/**
					 * @var SR_DOCS_Post $parent
					 */
					$parent = get_post( $post->post_parent );
					$link = $parent ? home_url( "{$parent->post_name}/{$post->post_name}/" ) : false;

					//					/**
					//					 * LEAVE THIS COMMENTED OUT SECTION! (mikes)
					//					 * Another way to do the above, this one is probably faster, but less robust?
					//					 */
					//					$microsite_page_slug = get_post_type_object( 'microsite-page')->rewrite['slug'];
					//					$link = str_replace( "/{$microsite_page_slug}/",'/',$link );

				} else {
					/**
					 * Is it is a post type that has a Microsite then set the URLs to be just root-based, i.e.
					 * for a Person post type with be one of:
					 *    http://example.com/john-smith/
					 *    http://example.com/mary-jones/
					 *    http://example.com/manish-patel/
					 *    http://example.com/alicia-garcia/

					 */
					if ( sr_has_microsites( $post->post_type ) ) {
						$link = home_url( "{$post->post_name}/" );
					} /**
					 * This code ensures that links which are children in a parent-child, or reorder-child microsite
					 * link within the context of the current microsite.
					 * For example:
					 *    http://example.com/events/my-event
					 * in the context of this microsite:
					 *    http://example.com/john-smith/
					 * becomes:
					 *    http://example.com/john-smith/events/my-event

					 */
					else if ( is_microsite() && ( $page_type = sr_context_var(
							'microsite_page_type'
						) ) && ( 'reorder_child' == $page_type || 'parent_child' == $page_type )
					) {
						$microsite = sr_get_current_microsite();
						$child_post_type = $microsite->page_templates[sr_context_var(
							'microsite_page_template_name'
						)]['child_type'];
						if ( $child_post_type == $post->post_type ) {
							if ( $microsite_page_post_id = sr_context_var( 'microsite_page_post_id' ) ) {
								$microsite_page = $microsite->get_page_by( 'post_id', $microsite_page_post_id );
							} else {
								/**
								 * This is for post_types that are part of a microsite but for which
								 * we don't have an appropriate microsite_page_post_id', such as in a
								 * sidebar.
								 */
								$microsite_page = $microsite->get_page_by( 'child_type', $post->post_type );
							}
							if ( $microsite_page ) {
								$link = "{$microsite_page->permalink}{$post->post_name}/";
							}
						}
					}
					/**
					 * Locally cache the link for this page load.
					 */
					$links[$post->ID] = $link;
				}
			}
			return $link;
		}

		/**
		 * If the current page's context represents a post type
		 * the set this Microsite's post_id to equal the current post_id
		 * We hook 'wp_loaded' because it run after all the 'init' hooks
		 * has been run.
		 */
		static function microsites_init () {
			do_action( 'sr_microsite_page_templates_init' );
			do_action( 'sr_microsites_init' );
		}

		static function fixup_microsite_page_templates () {
			global $sr_microsites;
			$page_templates = self::$_current_microsite->get_page_templates();
			$sr_microsites['page_templates'] = array_merge( $sr_microsites['page_templates'], $page_templates );
		}

		static function assign_microsite () {
			$context = Sunrise::get_context();
			foreach ( self::get_post_types() as $post_type ) {
				if ( $post_type = $context->microsite_post_type && $microsite = self::get_current_microsite() ) {
					$microsite->post_id = $context->microsite_post_id;
					self::fixup_microsite_page_templates();
					/**
					 * No need for adjacent posts <link>s in the HTML <head> for Microsites
					 */
					remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head' );
					/**
					 * Microsites are like Highlanders, there can be only one (on a given page load.)
					 * So break already.
					 */
					break;
				}
			}
		}

		/**
		 * Returns the link the the Microsite Admin Page for the current post
		 *
		 * @return string
		 */
		static function get_admin_page_link () {
			$context = Sunrise::get_context();
			/**
			 * Leave the trailing hash; it forces Safari to clear the bfcache - weird, huh? (mikes: 2011-12-28)
			 */
			return "edit.php?post={$context->post_id}&post_type={$context->post_type}&page=microsite#";
		}

		/**
		 * ARRRGHHH!!!!
		 * This method is a pair with self::adminmenu(); see if for comments.
		 *
		 * @param string $parent_file
		 * @return string
		 */
		static function parent_file ( $parent_file ) {
			$microsite = self::get_current_microsite();
			if ( $microsite ) {
				ob_start(); // See: ob_get_clean() in self::adminmenu()
				/**
				 * This is needed to fix the admin menu to point to the correct menu option.
				 * TODO (mike): Do these URLs more elegantly
				 */
				if ( $parent_file == "edit.php?post_type=microsite-page" ) {
					global $submenu_file;
					$submenu_file = $parent_file = "edit.php?post_type=" . $microsite->post_type;
				}
			}
			return $parent_file;
		}

		/**
		 * ARRRGHHH!!!!
		 * This hook of craziness keeps WordPress from changing the top level menu slug
		 * from 'edit.php?post_type=%post_type%' to 'admin.php?page=microsite'.
		 * Doh! Don't ask me to explain why WordPress team designed the menu system so
		 * it requires this level of braindeadiness, or why they won't add hooks to
		 * allow us to fix it, they just did.
		 * This method is a pair with self::parent_file()

		 */
		static function adminmenu () {
			$microsite = self::get_current_microsite();
			if ( $microsite ) {
				$html = ob_get_clean(); // See: ob_start() in self::parent_file()
				$parent_slug = $microsite->admin_menu_parent_slug;
				/**
				 * If all other submenu pages are removed besizes one whose URL is the Microsite's admin page link
				 * we end up with an 'admin.php?page=microsite' URL for the parent menu page link instead of having
				 * 'edit.php?post_type=%post_type%' as it should be. Fix it.
				 */
				$html = str_replace( 'admin.php?page=microsite', $parent_slug, $html );
				/**
				 * The add_submenu_page() doesn't allow setting of the URL so when we add the Microsite menu submenu page
				 * we end up with an 'edit.php?post_type=%post_type%&#038;page=microsite' URL, which doesn't contain the
				 * $post_id value we need. Fix it.
				 */
				$html = str_replace( "{$parent_slug}&#038;page=microsite", self::get_admin_page_link(), $html );
				echo $html;
			}
		}

		static function template_redirect () {
			global $wp_query;
			if ( $wp_query->is_404() ) {
				/**
				 * Reset all the microsite context vars to non-microsite condition.
				 */
				Sunrise::set_context_vars( self::get_initial_context() );
			}
			/**
			 * If a Microsite and WordPress-specific Preview, fixup all Microsite local URLs for preview
			 */
			if ( sr_get_context()->is_microsite && isset( $_GET['preview'] ) && 'true' == $_GET['preview'] ) {
				ob_start( array( __CLASS__, 'fixup_microsite_urls_for_preview' ) );
			}
		}

		/**
		 * Add '?preview=true' to any of the Microsite-local URLs for microsites, i.e.
		 *  http://example.com/john-doe/photos/ => http://example.com/john-doe/photos/?preview=true
		 *
		 * @param string $html
		 * @return string
		 */
		static function fixup_microsite_urls_for_preview ( $html ) {
			$regex_permalink = str_replace( '.', '\.', get_post_permalink( sr_get_context()->microsite_post_id ) );
			preg_match_all( $regex = "#href\\s*=\\s*(['\"])({$regex_permalink}.*?)\\1#", $html, $matches, PREG_SET_ORDER );
			foreach ( $matches as $match ) {
				$preview_link = add_query_arg( 'preview', 'true', $match[2] );
				$replace_with = "href={$match[1]}{$preview_link}{$match[1]}";
				$html = str_replace( $match[0], $replace_with, $html );
			}
			return $html;
		}

		/**
		 * Returns an array of all the initial context values for Microsites with all values = false;
		 *
		 * @return array
		 */
		static function get_initial_context () {
			return array(
				'is_microsite'                 => false,
				// True if sr_has_microsites( $post->post_type ) or $post->post_type='microsite-page'
				'sr_ajax_sub_action'           => false,
				// A Sunrise-specific sub_action of a wp_ajax hook call.
				/**
				 * microsite_post is of post_type for the microsite, i.e. person, practice-area, etc.
				 */
				'microsite_post'               => false,
				// Is the stdClass post object for $post_type=='person', etc.
				'microsite_post_id'            => false,
				// Is the $post->ID for $context->microsite_post
				'microsite_post_type'          => false,
				// Is the $post->post_type for $context->microsite_post
				/**
				 * microsite_page is of post_type 'microsite-page' (except 'main' which uses the parent's post)
				 */
				'microsite_page_post'          => false,
				// Is the stdClass post object for $post_type=='microsite-page'
				'microsite_page_post_id'       => false,
				// Is the $post->ID for $context->microsite_page_post
				'microsite_page_type'          => false,
				// Defined in 'microsite-page-type' classes: 'main', parent_child', 'reorder-child', 'twitter', 'blog', 'custom', etc.
				'microsite_page_template_name' => false,
				// Registered with sr_register_microsite_page_template()
				'microsite_page_key'           => false,
				// "{$page_type}[{$page_template_name}]"
				'microsite_page_title'         => false,
				// post_title for the page (mostly, unless it page_template label)
				'microsite_page_status'        => false,
				// post_status for the page (mostly, unless it page_template page_status)
			);
		}

		/**
		 * @static
		 * @param SR_DOCS_Microsite_Context $context
		 * @return SR_DOCS_Microsite_Context
		 */
		static function sr_get_context ( $context ) {
			global $wp_the_query;
			$context = sr_set_object_vars(
				$context,
				self::get_initial_context(),
				false
			); // false = do not overwrite if they already exist

			// TODO (mikes 2012-02-06): See if there are other special cases we need to address.
			if ( $wp_the_query->is_search ) {
				return $context;
			}

			if ( defined(
						 'DOING_AJAX'
					 ) && ( 'microsite_action' == $context->action || 'microsite-page' == $context->object_sub_type )
			) {
				$context->is_microsite = true;
				$context->sr_ajax_sub_action = sr_POST( 'sub_action' );
				$context->microsite_post_id = sr_POST( 'post_id' );
				$context->microsite_post_type = sr_POST( 'post_type' );
				$context->microsite_page_type = sr_POST( 'page_type' );
				$context->microsite_page_key = sr_POST( 'page_key' );
				$context->microsite_page_template_name = sr_POST( 'page_template_name' );
				$context->microsite_page_title = sr_POST( 'page_title' );
				$context->microsite_page_status = sr_POST( 'page_status' );
				$context->microsite_page_post_id = sr_POST( 'page_post_id' );

				$context->microsite_post = get_post( $context->microsite_post_id );

				if ( $context->microsite_page_post_id ) {
					$context->microsite_page_post = get_post( $context->microsite_page_post_id );
				} else {
					$context->microsite_page_post = false;
				}

			} else if ( isset( $context->post_type ) ) {
				$context->sr_ajax_sub_action = false;

				$microsite_post_id = false;
				if ( 'microsite-page' == $context->post_type ) {
					$context->is_microsite = true;
					if ( 'post.php' == $context->pagenow && 'editpost' == $context->action ) {
						// We know it works for this. Let's do each case specifically
						$context->microsite_post_id = sr_REQUEST( 'microsite_post_id' );
						$context->microsite_post_type = sr_REQUEST( 'microsite_post_type' );
						$context->microsite_page_type = sr_REQUEST( 'microsite_page_type' );
						$context->microsite_page_key = sr_REQUEST( 'microsite_page_key' );
						$context->microsite_page_template_name = sr_REQUEST( 'microsite_page_template_name' );
						$context->microsite_page_title = sr_REQUEST( 'microsite_page_title' );
						$context->microsite_page_status = sr_REQUEST( 'microsite_page_status' );
						$context->microsite_page_post_id = sr_REQUEST( 'microsite_page_post_id' );
						$context->microsite_post = @get_post( $context->microsite_post_id );
						$context->microsite_page_post = @get_post( $context->microsite_page_post_id );
						$context->post_parent = $context->microsite_post_id;
					} else {

						if ( ! empty( $context->microsite_post_id ) ) {
							$microsite_post_id = $context->microsite_post_id;
							$context->microsite_post = @get_post( $microsite_post_id );
						} else if ( ! empty( $context->post ) ) {
							$microsite_post_id = @$context->post->post_parent;
							$context->microsite_post = @get_post( $microsite_post_id );
						} else if ( ! empty( $context->post_id ) ) {
							$context->microsite_post = @get_post( $context->post_id );
							$microsite_post_id = $context->microsite_post->post_parent;
						}

						$context->post_parent = @$microsite_post_id;

						$page_key = self::get_page_key( $context->post_id );
						list( $page_type, $page_template_name ) = self::parse_page_key( $page_key, false );

						$context->microsite_page_key = $page_key;
						$context->microsite_page_type = $page_type;
						$context->microsite_page_template_name = $page_template_name;
						$context->microsite_page_post = @$context->post;
						$context->microsite_page_title = @$context->post->post_title;
						$context->microsite_page_status = @$context->post->post_status;
						$context->microsite_page_post_id = @$context->post_id;
					}

				} else if ( self::has_microsites( $context->post_type ) ) {

					$context->is_microsite = true;
					$context->microsite_page_key = false;
					$context->microsite_page_type = false;
					$context->microsite_page_template_name = false;
					$context->microsite_page_title = false;
					$context->microsite_page_status = false;
					$context->microsite_page_post_id = false;

					if ( ! empty( $context->microsite_post_id ) ) {
						$microsite_post_id = $context->microsite_post_id;
					} else if ( ! empty( $context->post_id ) ) {
						$microsite_post_id = @$context->post_id;
					} else if ( ! empty( $context->post ) ) {
						$microsite_post_id = @$context->post->ID;
					}
					$context->microsite_post = get_post( $microsite_post_id );
					$context->parent = false;
					$context->post_parent = false;

					$context->microsite_page_type = 'main';
					$main_page_template = self::get_main_page_template( $context->post_type );
					$context->microsite_page_template_name = $main_page_template['page_template_name'];
					$context->microsite_page_post_title = $main_page_template['label'];

				} else if ( ! empty( $context->microsite_page_post_id ) ) {
					/**
					 * Matched a related post at the third level URL i.e. 'todays-news-item' in:
					 *     example.com/dr-phil/articles/social-behavior-of-bonobos/
					 *     example.com/oprah/movies/the-color-purple/

					 */
					$microsite_page_post = @get_post( $context->microsite_page_post_id );
					$context->microsite_page_post = @$microsite_page_post;
					$context->microsite_page_type = @$microsite_page_post->post_type;
					$context->microsite_page_title = @$microsite_page_post->post_title;
					$context->microsite_page_status = @$microsite_page_post->post_status;

					$microsite_page_key = @self::get_page_key( $context->microsite_page_post_id );
					list( $microsite_page_type, $microsite_page_template_name ) = @self::parse_page_key(
						$microsite_page_key,
						false
					);

					$context->microsite_page_key = @$microsite_page_type;
					$context->microsite_page_template_name = @$microsite_page_template_name;

					$microsite_post_id = @$context->microsite_post_id;
					$context->microsite_post = @get_post( $microsite_post_id );
				}
				if ( $microsite_post_id ) {
					$context->microsite_post_id = $microsite_post_id;
					$context->microsite_post_type = @$context->microsite_post->post_type;
				}
			}
			return $context;
		}

		/**
		 * @static
		 * @return Sunrise_Microsite
		 */
		static function get_current_microsite () {
			if ( ! self::$_current_microsite ) {
				/**
				 * It will be 0 if we've already tested and found it isn't valid.
				 * This so that it bypasses the other code below.
				 */
				if ( 0 === self::$_current_microsite ) {
					return false;
				}

				/**
				 * @var SR_DOCS_Microsite_Context $context
				 */
				$context = Sunrise::get_context();

				self::$_current_microsite = $context->is_microsite ? self::get_microsite( $context->microsite_post_type ) : 0;

			}
			return self::$_current_microsite;
		}

		/**
		 * Enables the Microsite editor to add an admin menu option (a.k.a.
		 * a 'submenu page' in WordPress-speak) to an admin menu section
		 * (a.k.a. a 'menu page' in WordPress-speak) that links to the
		 * current microsite editor when editing microsite page posts
		 * or the main post for a microsite.
		 *
		 * @param book   $add_option
		 * @param array  $submenu_value
		 * @param string $option
		 * @return bool
		 */
		static function sr_add_admin_menu_option ( $add_option, $submenu_value, $option ) {
			if ( ! $add_option && 'microsite' == $option &&
					 'microsite-page' == @Sunrise::get_context()->post_type &&
																preg_match( '#&amp;page=microsite$#', $submenu_value[2] )
			) {
				$add_option = true;
			}
			return $add_option;
		}

		static function admin_menu () {
			/**
			 * @var Sunrise_Microsite $microsite
			 */
			if ( $microsite = self::get_current_microsite() ) {
				add_submenu_page(
					$microsite->admin_menu_parent_slug,
					$microsite->admin_page_title,
					$microsite->admin_menu_title,
					$microsite->required_capability,
					'microsite',
					array( &$microsite, 'admin_page' )
				);
				/**
				 * If we are editing a microsite-page add an admin menu
				 * option linking us to the Microsite editor.

				 */
				if ( self::_is_editing_microsite_page() ) {
					self::_add_editor_menu_option( $microsite );
				}
			}
		}

		/**
		 * @param Sunrise_Microsite $microsite
		 * @return void
		 * TODO (mikes): This does not currently work, and since the sponsor doesn't want it
		 * TODO: we'll wait to fix it for when someone who is paying does want it.
		 * TODO: Hopefully by then the WordPress core team will make it a little less impossible.
		 */
		static private function _add_page_menu_option ( $microsite ) {
			$context = Sunrise::get_context();
			$args['post'] = $context->post;
			$page = Sunrise_Microsite_Pages::get_instance_for( $context->microsite_page_type, $args );
			add_submenu_page(
				$microsite->editor_slug,
				__( 'Edit Custom Page', 'sunrise-microsites' ),
				__( 'Edit Custom Page for ', 'sunrise-microsites' ) . $microsite->title,
				$capability = 'edit_posts',
				$page->edit_slug
			);
		}

		static private function _is_editing_microsite_page () {
			return 'microsite-page' == @Sunrise::get_context()->post_type;
		}

		/**
		 * Adds an admin menu option that links to the Microsite editor.
		 *
		 * @param Sunrise_Microsite $microsite
		 * @return void
		 */
		static private function _add_editor_menu_option ( $microsite ) {
			global $wp_post_types;
			$post_type_meta = $wp_post_types[$microsite->post_type];
			global $menu;
			$slug = false;
			foreach ( $menu as $index => $menu_page ) {
				if ( $menu_page[0] === $post_type_meta->label ) {
					$slug = $menu_page[2];
					break;
				}
			}
			if ( $slug ) {
				$context = Sunrise::get_context();
				global $submenu;
				global $submenu_file; // Setting this makes gives the link to the microsite in the menu the highlight for "current" menu option
				$submenu_file = 'microsite-page' == $context->post_type ? $slug : $microsite->editor_slug;
				$submenu_options = array_keys( $submenu[$slug] );
				$index = end( $submenu_options );
				$submenu[$slug][$index][2] = $microsite->editor_slug;
			}
		}

		/**
		 * @param bool $key
		 * @return array
		 * TODO (mikes): Consider moving labels to Sunrise_Static_Base
		 */
		static function get_labels ( $key = false ) {
			return $key ? self::$_labels[$key] : self::$_labels;
		}

		static function sr_controls_init () {
			/**
			 * Register a control set for the 'microsites' module.
			 * These will be valid with the microsites module and/or for any module
			 * that explicitly requests a 'microsites' control.
			 */
			sr_register_control_set(
				'microsite-page-controls',
				array(
					'controls' => array(
						'toggle_page_status' => array(
							'type'         => 'toggle_post_status',
							'control_set'  => 'microsite-page-controls',
							'controls'     => array( 'publish_page', 'hide_page' ),
							'toggle_index' => array( 'publish' => 'hide_page', 'hidden' => 'publish_page' ),
						),
						'edit_page'          => array(
							'text'          => __( 'Edit' ),
							'link_template' => 'admin_edit_post',
						),
						'publish_page'       => array(
							'text' => self::get_labels( 'publish_control_text' ),
							'link' => '#publish-page',
						),
						'hide_page'          => array(
							'text' => self::get_labels( 'hide_control_text' ),
							'link' => '#hide-page',
						),
						'rename_page'        => array(
							'text' => __( 'Rename' ),
							'link' => '#rename-page',
						),
						'view_page'          => array(
							'text'          => __( 'View' ),
							'link_template' => 'view_post',
						),
						'reorder_pages'      => array(
							'text'          => __( 'Modify Ordering' ),
							'link_template' => 'admin_edit_post',
						),
						'filter_pages'       => array(
							'text'          => __( 'Edit in Module' ),
							'link_template' => 'admin_filter_posts',
						),
						'configure_page'     => array(
							'text'          => __( 'Configure' ),
							'link_template' => 'admin_edit_post',
						),
						'trash_page'         => array(
							'text'          => __( 'Trash' ),
							'link_template' => 'admin_trash_post',
						),
					),
				)
			);

		}

		static function init () {

			//			sr_die( 'Why do we need to remove rel_canonical?' );
			remove_action( 'wp_head', 'rel_canonical' );

			/**
			 * TODO: Can we make this call to register_post_status() less tedious?
			 * TODO: Maybe an sr_register_post_status()?
			 */
			$hidden_count_label = 'Hidden <span class="count">(%s)</span>';
			register_post_status(
				'hidden',
				array(
					'label'                     => _x( 'Hidden', 'sunrise-microsites' ),
					'label_count'               => _n_noop( $hidden_count_label, $hidden_count_label ),
					'capability_type'           => 'post',
					'internal'                  => false,
					'public'                    => true,
					'private'                   => false,
					'protected'                 => false,
					'hierarchical'              => false,
					'exclude_from_search'       => true,
					'show_in_admin_all'         => true,
					'publicly_queryable'        => true,
					'show_in_admin_all_list'    => true,
					'show_in_admin_status_list' => true,
				)
			);

		}

		static function sr_urls_init () {
			/**
			 * Register the URL path for posts displayed in the context of a microsite parent-child page.
			 * For example, a news item might have a URL like:
			 *    http://example.com/john-smith/news/new-partner-joins/

			 */
			sr_register_url_path(
				"%microsite_post_name%/%microsite_page_name%/%postname%",
				array(
					'@match' => array( __CLASS__, 'match_microsite_page_post_name' ),
				)
			);

			/**
			 * Register the URL path for microsite pages.
			 * For example, a news page for a person microsite might have a URL like:
			 *    http://example.com/john-smith/news/

			 */
			sr_register_url_path(
				"%microsite_post_name%/%microsite_page_name%",
				array(
					'@match' => array( __CLASS__, 'match_microsite_page_name' ),
				)
			);

			/**
			 * Register the URL path for posts of microsite-registered post types.
			 * For example, a person might have a URL like:
			 *    http://example.com/john-smith/

			 */
			sr_register_url_path(
				"%microsite_post_name%",
				array(
					'@match' => array( __CLASS__, 'match_microsite_post_name' ),
				)
			);
			/**
			 * Register the URL path for the post type of microsite-registered post types.
			 * For example, your 'archive' page for a microsite type might look like this:
			 *    http://example.com/people/

			 */
			// TODO: Micah / Mike - Go through microsite code with post type archives in mind (2012-06-20)
			/*sr_register_url_path(
				"%microsite_post_type%", array(
					'@match' => array( __CLASS__, 'match_microsite_post_type' ),
				)
			);*/

		}

		private static function _is_page_previewable () {
			return isset( $_GET['preview'] ) && 'true' == $_GET['preview'] && current_user_can( 'edit_posts' );
		}

		private static function _microsite_page_post_sql ( $reciprocal = false ) {
			global $wpdb;
			if ( $reciprocal ) {
				$fields = array( 'ms.ID' => 'related_id', 'rp.ID' => 'object_id' );
			} else {
				$fields = array( 'ms.ID' => 'object_id', 'rp.ID' => 'related_id' );
			}
			/**
			 * Using ID<>0 here as a convenient "no-op"
			 */
			$post_status = self::_is_page_previewable() ? "ID<>0" : "post_status='publish'";
			$sql = <<<SQL
SELECT DISTINCT
	r.relationship_name,
	rp.ID AS related_post_id,
	rp.post_parent AS related_parent_id,
	rp.post_name AS related_post_name,
	rp.post_type AS related_post_type,
	rp.post_status AS related_post_status,
	mspm.meta_value AS microsite_page_key,
	ms.ID AS microsite_post_id,
	ms.post_name AS microsite_post_name,
	ms.post_type AS microsite_post_type,
	ms.post_status AS microsite_post_status,
	msp.ID AS microsite_page_post_id,
	msp.post_name AS microsite_page_post_name,
	msp.post_type AS microsite_page_post_type,
	msp.post_status AS microsite_page_post_status,
	msp.post_title AS microsite_page_post_title
FROM
	{$wpdb->posts} msp
	INNER JOIN {$wpdb->postmeta} mspm ON msp.ID=mspm.post_id AND mspm.meta_key='_sr_microsite_page_key'
	INNER JOIN {$wpdb->posts} ms ON ms.ID=msp.post_parent
	INNER JOIN {$wpdb->prefix}relationships r ON r.{$fields['ms.ID']}=ms.ID
	INNER JOIN {$wpdb->posts} rp ON rp.ID=r.{$fields['rp.ID']}
WHERE 1=1
	AND ms.{$post_status}
	AND msp.{$post_status}
	AND rp.{$post_status}
	AND ms.post_name='%s'
	AND msp.post_type='microsite-page'
	AND msp.post_name='%s'
	AND rp.post_name='%s'
SQL;
			return $sql;
		}

		static function match_microsite_page_post_name ( $args, &$query_vars ) {
			/**
			 * @var wpdb $wpdb
			 */
			global $wpdb;
			$matched = false;
			foreach ( array( true, false ) as $reciprocal ) {
				$sql = $wpdb->prepare(
					self::_microsite_page_post_sql( $reciprocal ),
					$args['grandparent'],
					$args['parent'],
					$args['this']
				);
				$matches = $wpdb->get_results( $sql, OBJECT );
				$matched = 0 < count( $matches ) && sr_has_microsites( $matches[0]->microsite_post_type );
				if ( $matched ) {
					global $sr_relationships;
					$matched = 0;
					$error_info = array();
					foreach ( $matches as $match ) {
						if ( isset( $sr_relationships[$match->relationship_name] ) ) {
							$relationship = $sr_relationships[$match->relationship_name];
							if ( 'post' == $relationship->object_type && (
									( $reciprocal && $match->microsite_post_type == $relationship->related_sub_type && $match->related_post_type == $relationship->object_sub_type )
									||
									( ! $reciprocal && $match->microsite_post_type == $relationship->object_sub_type && $match->related_post_type == $relationship->related_sub_type )
								) && 'post' == $relationship->related_type
							) {
								$matched ++;
								if ( 1 == $matched ) {
									$this_match = $match;
								}
								$error_info[] = __( 'Relationship: ', 'sunrise-microsites' ) . "[{$match->relationship_name}], " .
																__( 'Microsite Post: ', 'sunrise-microsites' ) . "[{$match->microsite_post_name}], " .
																__( 'Related Post: ', 'sunrise-microsites' ) . "[{$match->related_post_name}].";
							}
						}
					}
					if ( 1 == $matched ) {
						break;
					} else if ( 1 < $matched ) {
						$error_msg = __( 'More than one relationship matched for the following: %s' );
						sr_trigger_error( $error_msg, implode( "\n", $error_info ) );
					}
				}
			}
			if ( $matched = 0 < $matched ) {
				$this_match = (array) $this_match;
				$this_match['is_microsite'] = true;
				Sunrise::set_context_vars(
					array_merge(
						$this_match,
						self::parse_page_key( $this_match['microsite_page_key'] )
					)
				);
				$query_vars = self::_compose_query_vars( $args, $this_match['related_post_type'] );
			}
			return $matched;
		}

		static function match_microsite_post_type ( $args, &$query_vars ) {
			$match = false;
			$post_types = get_post_types();
			foreach ( $post_types as $post_type ) {
				$post_type_obj = get_post_type_object( $post_type );
				if ( $post_type_obj->has_archive && $post_type_obj->rewrite['slug'] == $args['this'] ) {
					$microsite = sr_get_microsite( $post_type );
					if ( $microsite && ! isset( $args['child'] ) ) {
						Sunrise::set_context_vars(
							array(
								'is_microsite'        => true,
								'microsite_post_type' => $post_type,
							)
						);
						$query_vars = array(
							'post_type'           => $post_type,
							'microsite_post_type' => $post_type,
						);
						$match = true;
					}
				}
			}
			return (boolean) $match;
		}

		/**
		 * @static
		 * @param $args
		 * @param $query_vars
		 * @return bool
		 * TODO (mikes): SQL query does NOT contemplate when there are duplicate values for post_type in post_name field.
		 */
		static function match_microsite_post_name ( $args, &$query_vars ) {
			/**
			 * @var wpdb $wpdb
			 */
			global $wpdb;
			$matched = false;
			$and_post_status = self::_is_page_previewable() ? '' : "AND post_status='publish'";
			$sql = <<<SQL
SELECT
	ID AS microsite_post_id,
	post_type AS microsite_post_type,
	post_name AS microsite_post_name
FROM
	{$wpdb->posts}
WHERE 1=1
	AND post_name='%s'
	{$and_post_status}
LIMIT 1
SQL;
			$sql = $wpdb->prepare( $sql, $args['this'] );
			$matches = $wpdb->get_results( $sql, OBJECT );
			$matched = 0 < count( $matches ) && sr_has_microsites( $matches[0]->microsite_post_type );

			if ( $matched ) {
				$obj = (array) $matches[0];
				$obj['is_microsite'] = true;
				//                Sunrise::set_context_vars( array_merge( $obj,
				//                   self::parse_page_key( @$obj['microsite_page_key'] )
				//                ));
				$query_vars = self::_compose_query_vars( $args, $matches[0]->microsite_post_type );
			}

			return $matched;
		}

		static function match_microsite_page_name ( $args, &$query_vars ) {
			global $wpdb;
			$matched = false;
			/**
			 * Using ID<>0 here as a convenient "no-op"
			 */
			$post_status = self::_is_page_previewable() ? "ID<>0" : "post_status='publish'";
			$sql = <<<SQL
SELECT
	ms.ID AS microsite_post_id,
	ms.post_name AS microsite_post_name,
	ms.post_type AS microsite_post_type,
	mspm.meta_value AS microsite_page_key,
	msp.ID AS microsite_page_post_id,
	msp.post_name AS microsite_page_post_name,
	msp.post_type AS microsite_page_post_type,
	msp.post_status AS microsite_page_post_status,
	msp.post_title AS microsite_page_post_title
FROM
	{$wpdb->posts} msp
	INNER JOIN {$wpdb->postmeta} mspm ON msp.ID=mspm.post_id AND mspm.meta_key='_sr_microsite_page_key'
	INNER JOIN {$wpdb->posts} ms ON ms.ID=msp.post_parent
WHERE 1=1
	AND ms.post_name='%s'
	AND ms.{$post_status}
	AND msp.post_type='microsite-page'
	AND msp.post_name='%s'
	AND msp.{$post_status}
SQL;
			$sql = $wpdb->prepare( $sql, $args['parent'], $args['this'] );
			$matches = $wpdb->get_results( $sql );
			$matched = 0 < count( $matches );

			if ( $matched ) {
				$obj = (array) $matches[0];
				$obj['is_microsite'] = true;
				Sunrise::set_context_vars(
					array_merge(
						$obj,
						self::parse_page_key( @$obj['microsite_page_key'] )
					)
				);
				$query_vars = self::_compose_query_vars( $args, 'microsite-page' );
				$query_vars['post_parent'] = $matches[0]->microsite_post_id;
			}

			return $matched;
		}

		private static function _compose_query_vars ( $args, $post_type ) {
			$new_query_vars = array(
				'page'      => '',
				'post_type' => $post_type,
				'name'      => $args['this'],
			);
			$post_type_object = get_post_type_object( $post_type );
			if ( isset( $post_type_object->rewrite ) ) {
				$rewrite = $post_type_object->rewrite;
			}
			if ( isset( $rewrite['slug'] ) ) {
				$new_query_vars[$rewrite['slug']] = $args['this'];
			}
			return $new_query_vars;
		}

		/**
		 * This function is used to modify the row actions for a post in a WordPress admin list of posts.
		 * This code is pretty much a hack, but doesn't matter too much because it is isolated and because
		 * the code for action links in the WordPress admin list of posts is a hack.
		 *
		 * @param $actions
		 * @param $post
		 * @return array
		 */
		static function row_actions ( $actions, $post ) {
			if ( sr_has_microsites( $post->post_type ) ) {
				$post_type_label = sr_get_post_type_field( $post->post_type, 'labels' )->singular_name;
				$new_actions = array(
					'microsite' => self::_make_post_action_link(
							$post->ID,
							array(
								'page'       => 'microsite',
								'css_class'  => 'submitmicrosite',
								'title_text' => sprintf( __( 'Edit the Microsite for this %s' ), $post_type_label ),
								'link_text'  => __( 'Microsite' ),
								'nonce'      => false,
							)
						),
				);
				$actions = array_merge( $new_actions, $actions );
			} else if ( 'microsite-page' == $post->post_type ) {
				$post_type_label = sr_get_post_type_field( $post->post_type, 'labels' )->singular_name;
				/**
				 * This section adds a "Hide" or "Publish" link
				 */
				if ( $post->post_status == 'publish' ) {
					$action = array(
						'action'     => 'hide',
						'css_class'  => 'submithide',
						'title_text' => __( 'Hide this %s from visitors of the external website' ),
						'link_text'  => __( 'Hide' ),
					);
				} else {
					$action = array(
						'action'     => 'publish',
						'css_class'  => 'submitunhide',
						'title_text' => __( 'Publish this %s for visitors of the external website' ),
						'link_text'  => __( 'Publish' ),
					);
				}
				$action['title_text'] = sprintf( $action['title_text'], $post_type_label );
				$actions[$action['action']] = self::_make_post_action_link( $post->ID, $action );
			}
			return $actions;
		}

		private static function _make_post_action_link ( $post_id, $args ) {
			global $wpdb;
			if ( ! $post_type = sr_get_post_object_field( 'post_type', $post_id ) ) {
				return false;
			}

			/**
			 * @var SR_DOCS_Post_Type_Object $post_type_object
			 */
			$post_type_object = get_post_type_object( $post_type );
			if ( ! $post_type_object ) {
				return false;
			}

			$action_template = '<a class="%s" href="%s" title="%s">%s</a>';
			$title_text = esc_attr( $args['title_text'] );
			$action_link = admin_url( sprintf( "{$post_type_object->_edit_link}&amp;post_type={$post_type}", $post_id ) );

			if ( isset( $args['action'] ) ) {

				$action_link = add_query_arg( 'action', $args['action'], $action_link );
				if ( ! isset( $args['nonce'] ) || $args['nonce'] ) {
					$action_link = wp_nonce_url( $action_link, "{$args['action']}-{$post_type}_{$post_id}" );
				}
			} else if ( isset( $args['page'] ) ) {
				$action_link = preg_replace( '#/post.php#', '/edit.php', $action_link );

				$action_link = add_query_arg( 'page', $args['page'], $action_link );

				/**
				 * The trailing hash ('#') *appears* to start Safari and maybe other browsers
				 * from maintaining the original page in the bfcache on the back button vs.
				 * the page as it was updated by user's drag-and-drop.
				 */
				if ( 'microsite' == $args['page'] ) {
					$action_link .= '#';
				}

			}

			return sprintf( $action_template, $args['css_class'], $action_link, $title_text, $args['link_text'] );

		}

		static function get_post_types () {
			global $sr_microsites;
			$post_types = array();
			if ( isset( $sr_microsites['microsites'] ) && is_array( $sr_microsites['microsites'] ) ) {
				$post_types = array_keys( $sr_microsites['microsites'] );
			}
			return $post_types;
		}

		static function has_microsites ( $post_type ) {
			global $sr_microsites;
			return isset( $sr_microsites['microsites'][$post_type] );
		}

		/**
		 * @param string $post_type
		 * @return bool|Sunrise_Microsite
		 */
		static function get_microsite ( $post_type ) {
			global $sr_microsites;
			if ( isset( $sr_microsites['microsites'][$post_type] ) ) {
				return $sr_microsites['microsites'][$post_type];
			}
			return false;
		}

		static function register ( $post_type, $args = array() ) {
			global $sr_microsites;
			if ( isset( $sr_microsites['microsites'][$post_type] ) ) {
				sr_die( __( 'Microsite for post type [%s] already registered.', $post_type ) );
			}

			$microsite = new Sunrise_Microsite( $post_type, $args );
			$sr_microsites['microsites'][$post_type] = & $microsite;

		}

		/**
		 * @static
		 * @param $page_template_name
		 * @param $args
		 * @return void
		 * TODO: This really should move to a Sunrise_Microsite_Page_Templates class
		 */
		static function register_page_template ( $page_template_name, $args ) {
			global $sr_microsites;
			if ( isset( $sr_microsites['page_templates'][$page_template_name] ) ) {
				sr_die( __( 'Microsite page template [%s] already registered.' ), $page_template_name );
			}

			if ( ! isset( $args['html_title'] ) ) {
				$args['html_title'] = '%page_title% | %post_title%';
			}

			if ( ! isset( $args['page_type'] ) ) {
				$args['page_type'] = $page_template_name;
			}

			if ( ! isset( $args['label'] ) ) {
				$args['label'] = ucwords( $page_template_name );
			}

			$sr_microsites['page_templates'][$page_template_name] = $args;

		}

		/**
		 * @return array
		 */
		static function get_page_templates () {
			global $sr_microsites;
			return $sr_microsites['page_templates'];
		}

		static function get_instance_for ( $page_type, $args = array() ) {
			/**
			 * @var Sunrise_Microsite_Page $page
			 */
			$page = Sunrise::get_instance_for( 'microsites/microsite-page', $page_type, $args );
			$page->page_type = $page_type;
			return $page;
		}

		/**
		 * @param string     $page_template_name
		 * @param null|array $args
		 * @return array
		 */
		static function has_page_template ( $page_template_name, $args = array() ) {
			global $sr_microsites;
			$args = sr_parse_args(
				$args,
				array(
					'post_type' => false,
				)
			);
			$has_page_template = isset( $sr_microsites['page_templates'][$page_template_name] );
			if ( ! $has_page_template && $args['post_type'] ) {
				$has_page_template = isset( $sr_microsites['page_templates'][$args['post_type']]->page_templates[$page_template_name] );
			}
			return $has_page_template;
		}

		/**
		 * @param string     $page_template_name
		 * @param null|array $args
		 * @return array
		 */
		static function get_page_template ( $page_template_name, $args = array() ) {
			global $sr_microsites;
			$page_template = false;
			$args = sr_parse_args(
				$args,
				array(
					'post_type' => false,
				)
			);
			$error = false;

			$page_templates = $sr_microsites['page_templates'];
			if ( ! isset( $page_templates[$page_template_name] ) ) {
				if ( ! ( $error = ( false === $args['post_type'] ) ) ) {
					/**
					 * TODO (mikes 2012-01-30): Add error handling to explain need for $args['post_type'] when false === $args['post_type']
					 */
					$page_templates = $sr_microsites['microsites'][$args['post_type']]->page_templates;
					$error = ! isset( $page_templates[$page_template_name] );
				}
			}
			if ( $error ) {
				sr_trigger_error( __( 'Microsite page template [%s] not registered.' ), $page_template_name );
			} else {
				$page_template = $page_templates[$page_template_name];
			}
			return $page_template;
		}

		/**
		 * @param string $post_type
		 * @return array
		 */
		static function get_main_page_template ( $post_type ) {
			$page_templates = sr_get_microsite( $post_type )->page_templates;
			return reset( $page_templates );
		}

		/**
		 * Get the permalink for a given Post within the context of the current microsite.
		 * TODO: Explain argument 'microsite_page_post_id'
		 *
		 * @param $id - Post object or Post ID
		 * @param $args
		 * @return bool|String
		 */
		static function get_permalink( $id, $args = array() ) {

			$post = null;

			if ( is_object($id) ) {
				$post = $id;
			} else {
				$post = get_post($id);
			}

			if ( empty($post->ID) ) {
				return false;
			}

			$permalink = get_permalink( $post->ID );

			$microsite = self::get_current_microsite();
			if ( $microsite ) {

				if ( isset($args['microsite_page_post_id']) && $args['microsite_page_post_id'] ) {
					$microsite_page = $microsite->get_page_by( 'post_id', $args['microsite_page_post_id'] );
				} else {
					/**
					 * This is for post_types that are part of a microsite but for which
					 * we don't have an appropriate microsite_page_post_id', such as in a
					 * sidebar.
					 */
					$microsite_page = $microsite->get_page_by( 'child_type', $post->post_type );
				}

				if ( $microsite_page ) {
					$permalink = "{$microsite_page->permalink}{$post->post_name}/";
				}
			}

			return $permalink;
		}
	}

	Sunrise_Microsites::on_load();
}

/**
 * This class is ONLY for enabling PhpStorm to provide auto-sense
 */
class SR_DOCS_Microsite_Context extends SR_DOCS_Context {

	var $is_microsite;
}
