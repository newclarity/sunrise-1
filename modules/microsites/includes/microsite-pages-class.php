<?php

if ( !class_exists( 'Sunrise_Microsite_Pages' ) ) {
	class Sunrise_Microsite_Pages extends Sunrise_Static_Base {
		/**
		 * @param string $page_type
		 * @return bool
		 */
		static function is_valid_type( $page_type ) {
			return Sunrise::is_valid_type( 'microsites/microsite-page', $page_type );
		}

		/**
		 * @return array
		 */
		static function get_valid_types() {
			return Sunrise::get_valid_types( 'microsites/microsite-page' );
		}

		/**
		 * @param string $page_type
		 * @return string
		 */
		static function get_class_for( $page_type ) {
			return Sunrise::get_class_for( 'microsites/microsite-page', $page_type );
		}

		/**
		 * @param string $page_type
		 * @param array $args
		 * @return Sunrise_Microsite_Page
		 */
		static function get_instance_for( $page_type, $args = array() ) {
			return Sunrise::get_instance_for( 'microsites/microsite-page', $page_type, $args );
		}
	}
}

