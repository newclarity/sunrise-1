<?php

if ( ! class_exists( 'Sunrise_Microsite' ) ) {
	/**
	 * @property string $title
	 * @property array $pages
	 * @property array $published_pages
	 * @property array $hidden_pages
	 * @property array $posts
	 * @property array $post
	 * @property array $page_templates
	 * @property string $admin_menu_parent_slug
	 * @property string $admin_page_title
	 * @property string $admin_menu_title
	 * @property string $sub_title
	 * @property string $trashed_message
	 * @property string $page_admin_bars_html
	 * @property string $permalink
	 * @property string $screen_icon
	 * @property string $main_page
	 * @property string $admin_id
	 * @property string $labels
	 * @property string $hidden_help
	 * @property string $publish_help
	 * @property string $admin_class
	 * @property string $page_expansion_label
	 * @property string $editor_slug
	 * @property string $hidden_callout_html
	 * @property string $view_page_button_text
	 * @property string $view_page_link
	 *
	 */
	class Sunrise_Microsite extends Sunrise_Base {
		var $post_type = false;
		var $post_id = false;
		var $label = false;
		var $singular_label = false;

		var $required_capability = false;

		/**
		 * @var bool|array
		 */
		protected $_pages = false;
		/**
		 * @var bool|array
		 */
		protected $_posts = false;
		/**
		 * @var bool|object
		 */
		protected $_post = false;
		/**
		 * @var bool|array
		 */
		protected $_indexed_posts = false;
		/**
		 * @var Sunrise_Microsite_Page
		 */
		protected $_main_page = false;
		/**
		 * @var bool|array
		 */
		protected $_page_templates = false;
		/**
		 * @var bool|string
		 */
		protected $_publish_help = false;
		/**
		 * @var bool|string
		 */
		protected $_hidden_help = false;

		function __construct( $post_type, $args ) {
			$args = wp_parse_args(
				$args, array(
					'publish_help' => __(
						'Pages here are published. Drag to the <em>"Hidden Pages"</em> box to hide them on the Microsite. You can reorder by dragging up or down. %microsite_label% is required and cannot be hidden or moved.',
						'sunrise-microsites'
					),
					'hidden_help' => __(
						'Pages here are hidden. Drag to the <em>"Published Pages"</em> box to enable them on the Microsite. Create additional custom pages by clicking "<em>Create New Custom Page</em>" above.',
						'sunrise-microsites'
					),
				)
			);

			$args['post_type'] = $post_type;

			$this->args = Sunrise::normalize_args(
				$args, array(
					'templates' => 'page_templates',
				)
			);

			/**		We are not going to do this by default
			 *
			 *		 $this->args['page_templates'][] = 'custom';
			 */

			/*
			 * Grab all the args that are properties or _properties and set them.
			 * Put everything into $args and everything else in $extra.
			 */
			$this->construct_from_args( $this->args );

			$this->required_capability = 'edit_posts';

			$this->do_action( 'initialize', $this->args );

		}

		/**
		 * @param string $selector
		 * @param mixed $data
		 * @param array $args
		 * @return bool|Sunrise_Microsite_Page
		 */
		function get_page_by( $selector, $data, $args = array() ) {
			$found_page = false;
			switch ( $selector ) {
				case 'page_key':
					foreach ( $this->published_pages as $page_name => $page ) {
						if ( $data == $page->page_key ) {
							$found_page = $page;
							break;
						}
					}
					break;

				case 'post_id':
				case 'microsite_page_post_id':
					foreach ( $this->published_pages as $page_name => $page ) {
						if ( $data == $page->post->ID ) {
							$found_page = $page;
							break;
						}
					}
					break;

				case 'child_type':
					foreach ( $this->published_pages as $page_name => $page ) {
						if ( is_a( $page, 'Sunrise_Parent_Child_Microsite_Page' ) && $data == $page->child_type ) {
							$found_page = $page;
							break;
						}
					}
			}
			if ( ! $found_page )
				foreach ( $this->hidden_pages as $page_name => $page ) {
					if ( $data == $page->page_key ) {
						$found_page = $page;
						break;
					}
				}
			return $found_page;
		}

		/**
		 * Used for error handling to identify the current object.
		 *
		 * @return string
		 */
		function get_description() {
			return parent::get_description( "post_type={$this->post_type}, title={$this->title}" );
		}

		private static function get_screen_icon() {
			ob_start();
			screen_icon();
			$screen_icon = ob_get_contents();
			ob_end_clean();
			return $screen_icon;
		}

		/**
		 * @return string
		 *
		 * TODO: Move logic over
		 */
		function get_trashed_message() {
			return '';
		}

		function get_publish_help() {
			return str_replace( '%microsite_label%', $this->main_page->label, $this->_publish_help );
		}

		function get_hidden_help() {
			return str_replace( '%microsite_label%', $this->main_page->label, $this->_hidden_help );
		}

		/**
		 *
		 * @return string
		 *
		 * TODO: Make this configurable. Dion didn't like "Add/Remove Pages" but others might.
		 */
		function get_sub_title() {
			return $this->title;
		}

		function get_admin_id() {
			return "microsite-admin-{$this->post_id}";
		}

		function get_posts() {
			if ( ! $this->_posts )
				$this->_load_posts();
			return $this->_posts;
		}

		function get_post() {
			if ( ! $this->_post ) {
				$this->_post = get_post( $this->post_id );
				$main_page_template = sr_get_microsite_main_page_template( $this->post_type );
				$this->_post->page_key = Sunrise_Microsites::make_page_key( 'main', $main_page_template['page_template_name'] );
			}
			return $this->_post;
		}

		protected function _load_posts() {
			/**
			 * @var wpdb $wpdb
			 */
			global $wpdb;
			/**
			 * TODO (mikes 2011-12-14): Verify the bug that duplicates non-multiple pages is fixed and
			 * TODO: to make sure there are no more posts with post_name='xxxxx-2' thus to remove
			 * TODO: the last line of WHERE.
			 */
			$sql = <<<SQL
SELECT
	p.ID,
	pm.meta_value AS page_key
FROM
	{$wpdb->prefix}posts p
	INNER JOIN {$wpdb->prefix}postmeta pm ON pm.post_id=p.ID AND pm.meta_key='_sr_microsite_page_key'
WHERE 1=1
	AND p.post_type='microsite-page'
	AND p.post_parent=%d
	AND p.post_status IN ('publish','hidden')
	AND (p.post_name NOT REGEXP '^.+-[0-9]+$' OR pm.meta_value = 'custom[custom]')
SQL;
			$posts = $wpdb->get_results( $sql = $wpdb->prepare( $sql, $this->post_id ) );
			$page_posts = array( $this->post_id => $this->post );
			/**
			 * @var SR_DOCS_Post $post
			 */
			foreach ( $posts as $post )
			{
				$page_posts[$post->ID] = $post;
			}

			/**
			 * We query a second time using the standard WordPress query so we can get $posts
			 * loaded how WordPress normally loads posts. Now we suppress the filters on this
			 * because we don't want filters screwing these up, we only want the ones we've
			 * already identified.
			 */
			$query = new WP_Query(array(
				'suppress_filters' => true,
				'post__in' => array_keys( $page_posts ),
				'post_type' => 'microsite-page',
				'post_status' => 'all',
				'posts_per_page' => - 1,
				'order' => 'ASC',
				'orderby' => 'menu_order ID', // Note this is import to get Custom Pages to display in order of creation
			));
			array_unshift( $query->posts, $this->post );
			$this->_posts = $this->_indexed_posts = array();
			foreach ( $query->posts as $post ) {
				$post->page_key = @$page_posts[$post->ID]->page_key;
				$parsed_page_key = Sunrise_Microsites::parse_page_key( $post->page_key );
				$post->page_type = $parsed_page_key['microsite_page_type'];
				$post->page_template_name = $parsed_page_key['microsite_page_template_name'];
				$this->_posts[$post->ID] = $post;
				if ( isset($page_posts[$post->ID]) ) {
					$page_post = $page_posts[$post->ID];
					if ( ! isset($this->_indexed_posts[$page_post->page_key]) )
						$this->_indexed_posts[$page_post->page_key] = array();
					$this->_indexed_posts[$page_post->page_key][] = &$this->_posts[$post->ID];
				}
			}
			return $this->_posts;
		}

		/**
		 * @return string
		 *
		 * TODO: Move logic over
		 */
		function get_page_admin_bars_html() {
			$bars = array(
				'publish' => array(),
				'hidden' => array(),
			);
			/**
			 * @var Sunrise_Microsite_Page $page
			 */
			//$bars = array();
			foreach ( $this->pages as $page_status => $status_pages ) {
				foreach ( $status_pages as $page_index => $page ) {
					if ( 'main' == $page->page_type ) {
						$bars['publish'][] = $page->admin_bar_html;
					} else if ( preg_match( '#^(publish|hidden)$#', $page_status ) ) {
						$bars[$page_status][] = $page->admin_bar_html;
					} else if ( 'auto-draft' == $page_status ) {
						// ignore this
					} else {
						// ignore this
						//	sr_trigger_error( __( 'Unexpected Page Status [%s] while processing Microsite Pages' ), $page_status );
					}
				}
			}
			foreach ( $bars as $page_status => $status_bars )
			{
				$bars[$page_status] = implode( "\n", $status_bars );
			}

			if ( ! isset($bars['hidden']) )
				$bars['hidden'] = '';

			return $bars;
		}

		function get_permalink() {
			if ( ! $this->post_id )
				sr_die( __( 'Microsite post ID not set when attempting to access permalink.' ) );
			return get_post_permalink( $this->post_id );
		}

		function get_admin_class() {
			return "microsite-admin expanded";
		}

		/**
		 * @return array
		 *
		 * TODO (mikes): Reconsider this; would it be better done with more generic label functionality?
		 */
		function get_page_expansion_label() {
			$label = Sunrise_Microsites::get_labels( 'expand_pages_text' );
			return $label;
		}

		function get_hidden_callout_html() {
			$hidden_callout_html = false;
			$microsite = Sunrise_Microsites::get_current_microsite();
			$labels = Sunrise_Microsites::get_labels();
			if ( 'draft' == $microsite->post->post_status )
				$hidden_callout_html = " &ndash; <span class=\"hidden-callout\">{$labels['hidden_callout_text']}</span>";
			return $hidden_callout_html;
		}

		function get_view_page_button_text() {
			$microsite = Sunrise_Microsites::get_current_microsite();
			$labels = Sunrise_Microsites::get_labels();
			return 'draft' == $microsite->post->post_status ? $labels['preview_page_button'] : $labels['view_page_button'];
		}

		function get_view_page_link() {
			$microsite = Sunrise_Microsites::get_current_microsite();
			$labels = Sunrise_Microsites::get_labels();
			return 'draft' == $microsite->post->post_status ? "{$this->permalink}?preview=true" : $this->permalink;
		}

		function admin_page() {
			$microsite = Sunrise_Microsites::get_current_microsite();
			$labels = Sunrise_Microsites::get_labels();
			// TODO: Micah 2012-05-23 - Add custom page template dropdown
			// var_dump( Sunrise_Microsites::get_page_templates() );
			$html = <<<HTML
<div class="wrap">
	<div id="{$microsite->admin_id}" class="{$this->admin_class}">
		{$this->screen_icon}
		<h2>{$this->admin_page_title}{$this->hidden_callout_html}</h2>
		{$this->trashed_message}
		<h3>
			{$this->sub_title}
			<a href="{$this->view_page_link}" class="button">{$this->view_page_button_text}</a>
			<a href="#toggle-expansion" id="toggle-expansion-button" class="button collapsed">{$this->page_expansion_label}</a>
		</h3>
		<div id="add-new-button">
			<a href="#add-custom-page" id="add-custom-page" class="button-primary">{$labels['create_page_button']}</a>
		</div>
		<div id="publish-pages" class="page-containers postbox">
			<h3 class="side-title hndle">{$labels['publish_pages_text']}</h3>
			<p class="help">{$this->publish_help}</p>
			<ul id="publish-page-list" class="page-list ui-sortable">
				{$this->page_admin_bars_html['publish']}
			</ul>
		</div>
		<div id="hidden-pages" class="page-containers postbox">
			<h3 class="side-title hndle">{$labels['hidden_pages_text']}</h3>
			<p class="help">{$this->hidden_help}</p>
			<ul id="hidden-page-list" class="page-list ui-sortable">
				{$this->page_admin_bars_html['hidden']}
			</ul>
		</div>
	</div>
	<input type="hidden" id="microsite-post-type" value="{$microsite->post_type}" />
	<input type="hidden" id="publish_control_text" value="{$labels['publish_control_text']}" />
	<input type="hidden" id="hide_control_text" value="{$labels['hide_control_text']}" />
	<input type="hidden" id="collapse_pages_text" value="{$labels['collapse_pages_text']}" />
	<input type="hidden" id="expand_pages_text" value="{$labels['expand_pages_text']}" />

</div>
HTML;
			echo $html;
		}

		function get_published_pages() {
			return $this->pages['publish'];
		}

		function get_hidden_pages() {
			return $this->pages['hidden'];
		}

		function get_pages() {
			if ( ! $this->_pages ) {
				$this->_pages = array(
					'hidden' => array(),
					'publish' => array(),
				);
				$page_templates = $this->page_templates;
				$main_page_template = array_shift( $page_templates );
				$custom_page_template = $page_templates['custom'];
				if ( 'main' != $main_page_template['page_type'] )
					sr_die( __( 'The first page template of a [%s] microsite must have a page_type [main]' ), $this->post_type );
				/**
				 * @var SR_DOCS_Post $post
				 */
				foreach ( $this->posts as $post ) {
					if ( ! isset($this->_pages[$post->post_status]) )
						$this->_pages[$post->post_status] = array();
					if ( 'main' == $post->page_type ) {
						$page_args = $main_page_template;
					} else if ( 'custom' == $post->page_type ) {
						$page_args = $custom_page_template;
						unset($page_templates['custom']);
					} else if ( isset($page_templates[$post->page_template_name]) ) {
						$page_args = $page_templates[$post->page_template_name];
					} else {
                        //$page_args = array();
                        /**
                         * If a template isn't registered, don't do anything with it just because there is data
                         * in the database.  We don't want phantom entries showing when we intentionally disabled a
                         * microsite template.
                         */
                        continue;
					}
					$page_args['parent'] = &$this;
					$page_args['post'] = &$post;
					/**
					 * Microsite 'main' pages should always appear published even when the underlying post is not.
					 */
					$page_status = 'main' == $post->page_type ? 'publish' : $post->post_status;
					$page_args['page_status'] = $page_status;
					$page_type = isset($page_args['page_type']) ? $page_args['page_type'] : 'main';
					$microsite_page = Sunrise_Microsites::get_instance_for( $page_type, $page_args );
					$this->_pages[$page_status][] = $microsite_page;
					if ( isset($page_templates[$post->page_template_name]) ) {
						unset($page_templates[$post->page_template_name]);
					}
				}
				$counter = 1;
				foreach ( $page_templates as $page_template_name => $page_template ) {
					$page_template['parent'] = &$this;
					/**
					 * Set the $page_id to have a leading 0 for microsite pages that do not yet have a associated $post in the
					 * database to ensure they will differ in the HTML from this pages that use the $post->ID.	So the $page_ids
					 * will be '01', '02', etc.
					 */
					$page_template['page_id'] = "microsite-page-vp{$counter}"; // vp = virtual page
					$page_template['page_status'] = 'main' == $page_template['page_type'] ? 'publish' : 'hidden';
					/**
					 * @var Sunrise_Microsite_Page $microsite_page
					 */
					$microsite_page = Sunrise_Microsites::get_instance_for( $page_template['page_type'], $page_template );
					$microsite_page->is_virtual = true;
					$this->_pages[$microsite_page->page_status][] = $microsite_page;
					$counter ++;
				}
			}
			return $this->_pages;
		}

		/**
		 * @return string
		 *
		 * TOOD (mikes): Need to determine why using $context is appropriate here,
		 * or if it even is?
		 */
		function get_title() {
			if ( empty($this->post) || empty($this->post->ID) ) {
				$post_id = @Sunrise::get_context()->microsite_post_id;
			} else {
				$post_id = $this->post->ID;
			}
			return get_the_title( $post_id );
		}

		function get_editor_slug() {
			$url = admin_url( "edit.php?post={$this->post_id}&amp;post_type={$this->post_type}&amp;page=microsite" );
			return $url;
		}

		function get_admin_menu_title() {
			return __( 'Microsite Editor: ' ) . $this->title;
		}

		function get_admin_page_title() {
			return __( 'Microsite Editor: ' ) . $this->title;
		}

		function get_admin_menu_parent_slug() {
			return "edit.php?post_type={$this->post_type}";
		}

		/**
		 * Returns the first Microsite page
		 *
		 * @return Sunrise_Microsite_Page
		 */
		function get_main_page() {
			$keys = array_keys( $this->pages['publish'] );
			if ( 0 < count( $keys ) && isset($this->pages['publish'][$keys[0]]) ) {
				$main_page = $this->pages['publish'][$keys[0]];
			} else {
				$main_page = Sunrise_Microsites::get_instance_for( 'main' );
			}
			return $main_page;
		}

		/**
		 * There is no way to update the page templates in a microsite after the fact without this function.
		 * Still requires changing the $sr_microsites['page_templates'][$template] beforehand.
		 *
		 * @param $template
		 */
		function update_page_templates( $template ) {
			global $sr_microsites;
			$this->_page_templates[$template] = $sr_microsites['page_templates'][$template];
		}

		function get_page_templates() {
			$new_page_templates = $this->_page_templates;
			$main_page = array_shift( $new_page_templates );
			if ( is_string( $main_page ) || (! is_array( $main_page ) || ! isset($main_page['_processed'])) ) {
				$new_page_templates = array();
				foreach ( $this->_page_templates as $page_template_name => $page_template ) {
					$args = array( 'post_type' => $this->post_type );
					if ( is_string( $page_template ) ) {
						$page_template_name = $page_template;
						$page_template = Sunrise_Microsites::get_page_template( $page_template, $args );
						if ( ! $page_template ) {
							sr_trigger_error(
								__(
									'The page template %s specified during Microsite %s registration is not a registered page template',
									'sunrise-microsites'
								), $page_template_name, $this->post_type
							);
							continue;
						}
					} else {
						/**
						 * If we have a shared page template too, merge in it's values
						 */
						if ( Sunrise_Microsites::has_page_template( $page_template_name, $args ) ) {
							$shared_page_template = Sunrise_Microsites::get_page_template( $page_template_name, $args );
							if ( $shared_page_template )
								$page_template = array_merge( $shared_page_template, $page_template );
						}
					}
					if ( ! isset($page_template['page_type']) )
						sr_trigger_error(
							__( 'Page type not specified for page template [%s]', 'sunrise-microsites' ), $page_template_name
						);

					/**
					 * TODO (mikes 2012-02-08) This is page template initialization which should be done in a central place
					 * TODO: Search for "meaningless==45780345" to find related code.
					 */
					$new_page_templates[$page_template_name] = wp_parse_args(
						$page_template, array(
							'page_template_name' => $page_template_name,
							'default_status' => 'main' == $page_template['page_type'] ? 'publish' : 'hidden',
							'html_title' => '%page_title% | %post_title%',
							'user_type' => 'unspecified',
							'allow_multiple' => false,
							'label' => ucwords( sr_spacize( $page_template_name ) ),
							'_processed' => true,
						)
					);
				}

				$this->_page_templates = $new_page_templates;
			}
			return apply_filters( 'sr_microsites_filter_page_templates', $this->_page_templates, $this );
		}

		function __set( $property_name, $value ) {
			switch ( $property_name ) {
				case '_temp_':
					call_user_func( array( &$this, "set_{$property_name}" ), $value );
					break;

				case 'pages':
				case 'posts':
				case 'post':
				case 'page_templates':
				case 'main_page':
				case 'title':
				case 'sub_title':
				case 'admin_class':
				case 'admin_menu_title':
				case 'admin_page_title':
				case 'admin_menu_parent_slug':
				case 'admin_menu_editor_title':
				case 'page_admin_bars_html':
				case 'trashed_message':
				case 'permalink':
				case 'screen_icon':
				case 'publish_help':
				case 'hidden_help':
				case 'admin_id':
				case 'labels':
				case 'page_expansion_label':
				case 'editor_slug':
				case 'published_pages':
				case 'hidden_pages':
				case 'hidden_callout_html':
				case 'view_page_button_text':
				case 'view_page_link':
					$this->_cannot_set_error( $property_name, $value );
					break;

				default:
					parent::__set( $property_name, $value );
					break;
			}
		}

		function __get( $property_name ) {
			$value = false;
			switch ( $property_name ) {
				case 'pages':
				case 'posts':
				case 'post':
				case 'published_pages':
				case 'hidden_pages':
				case 'page_templates':
				case 'main_page':
				case 'title':
				case 'sub_title':
				case 'admin_class':
				case 'admin_menu_title':
				case 'admin_page_title':
				case 'admin_menu_parent_slug':
				case 'admin_menu_editor_title':
				case 'page_admin_bars_html':
				case 'trashed_message':
				case 'permalink':
				case 'screen_icon':
				case 'publish_help':
				case 'hidden_help':
				case 'admin_id':
				case 'labels':
				case 'page_expansion_label':
				case 'editor_slug':
				case 'hidden_callout_html':
				case 'view_page_button_text':
				case 'view_page_link':
					$value = call_user_func( array( &$this, "get_{$property_name}" ) );
					break;

				default:
					$value = parent::__get( $property_name, $value );
					break;
			}
			return $value;
		}

	}
}
