<?php
/**
 * TODO (mikes 2011-12-14): Change base class to be Sunrise_Admin_Page once that Admin Pages module is implemented.
 */
if (!class_exists('Sunrise_Reorder_Admin_Page')) {

	class Sunrise_Reorder_Admin_Page extends Sunrise_Base {
		function __construct() {
			sr_add_action( $this, 'admin_init', 11 ); 			// Do after 10, after it's added
		}
		function is_this() {
			$value = false;
			$context = sr_get_context();
			$ajax_actions = array(
				'reorder_posts_action',
				'show_reorder_posts_action',
				'hide_reorder_posts_action',
			);
			if( 'reorder_child' == $context->microsite_page_type )
				$value = true;
			else if( in_array( $context->action, $ajax_actions ) )
				$value = true;
			return $value;
		}
		function admin_init() {
			if ( self::is_this() ) {
				sr_add_action( $this, 'dbx_post_advanced', 25 );   // 25 means run after all the 10s.
				sr_add_action( $this, 'add_meta_boxes', 25, 2 );   // 25 means run after all the 10s.
				sr_remove_action( 'Sunrise_Post_Object', 'add_meta_boxes' );
				$root_dir = dirname( dirname( __FILE__ ) );
				$script_filepath = '/js/sunrise-reorder-posts.js';
				sr_enqueue_script( 'sunrise-reorder-posts', plugins_url( $script_filepath, dirname( __FILE__ ) ), array(
					'file' => "{$root_dir}{$script_filepath}",
					'dependencies' => array( 'jquery', 'jquery-ui-sortable', 'sunrise-core' )
				));
				Sunrise::add_ajax_handler( __CLASS__, 'reorder_posts_action' );
				Sunrise::add_ajax_handler( __CLASS__, 'show_reorder_posts_action' );
				Sunrise::add_ajax_handler( __CLASS__, 'hide_reorder_posts_action' );
				// Force a single column layout
				add_filter( 'get_user_option_screen_layout_microsite-page', array( $this, 'layout_columns' ) );
				add_filter( 'admin_body_class', array( $this, 'admin_body_class' ) );
			}
		}

		function admin_body_class( $classes ) {
			$classes .= ' microsite-reorder-child';
			return $classes;
		}
		function layout_columns( $cols ) {
			return 1;
		}
		function add_meta_boxes( $post_type, $post ) {
			sr_remove_all_metaboxes();
			sr_clear_post_type_supports( 'microsite-page' );
			$microsite = sr_get_current_microsite();
			$context = sr_get_context();
			remove_meta_box( 'submitdiv', 'microsite-page', 'side' );
			add_meta_box(
				'reorder-posts-metabox',
				"Reorder {$microsite->title} {$context->microsite_page_title}",
				array( &$this, 'reorder_meta_box' ),
				'microsite-page',
				'normal',
				'high'
			);
			global $form_extra;

		}
		function dbx_post_advanced() {
			ob_start();
		}
		function reorder_meta_box( $post, $metabox ) {
			$view_button = '<a href="' . get_post_permalink( $post->ID ) . '" class="button">View Page</a>';
			$html =  ob_get_clean();
			echo str_replace( '</h2>', "{$view_button}</h2>", $html );
			$help_text = __( '%s can be rearranged into a desired order. By default, they appear alphabetically by headline. Drag boxes up or down to change the ordering. Individual case studies can be modified in the <em>"%s"</em> module by clicking the linked title of the %s.', 'sunrise-microsites' );
			$labels = self::get_labels();
			$help_text = sprintf( $help_text,$labels['plural_child'], $labels['plural_child'], $labels['singular_child'] );
			$context = sr_get_context();
			$post_type = $context->microsite_post_type;
			$page = self::get_page();
			if ( WP_DEBUG && ! isset( $page->relationship_name ) )
				sr_die( __( "Reorder Posts requires the relationship to be specified when registering the page template or in the microsite." ), $page->page_template_name );
			$related_posts = sr_get_related_objects( $page->relationship_name, $context->microsite_post_id );
			$post_order = '';
			$post_bars = $this->get_post_bars_html($post,$related_posts);
			$metabox = <<<METABOX
<div id="reorder-{$post_type}-posts" class="reorder-posts">
<input id="post-order" type="hidden" name="post_order" value="{$post_order}" />
<input id="parent-id" type="hidden" name="microsite_post_id" value="{$context->microsite_post_id}" />
<div class="help-text">$help_text</div>
{$post_bars}
<div class="clear"></div>
</div>
METABOX;
			echo $metabox;
		}
		function get_post_bars_html( $post, $posts_to_order ) {
			$bars = array();
			if ( count( $posts_to_order ) ) {
				$posts_to_order = self::order_posts( $post->ID, $posts_to_order );
				$bars[] = '<ul id="posts-to-reorder">';
				foreach( $posts_to_order as $post ) {
					$bars[] = $this->get_post_bar_html( $post );
				}
				$bars[] = '</ul>';
			} else {
				$labels = self::get_labels();
				$child_type = self::get_child_type();
				/**
				 * TODO (mikes 2011-12-14): Implement the hyperlink below using Sunrise_Controls.
				 */
				$bars[] = <<<MESSAGE
<div class="message">
	<p>There are no {$labels['plural_child']} associated with this {$labels['singular_microsite']}.</p>
	<p>You may associate {$labels['plural_child']} with a {$labels['singular_microsite']} in the {$labels['plural_child']} module by clicking  <a href="/wp-admin/edit.php?post_type={$child_type}" target="_blank">here</a>.</p>
</div>
MESSAGE;
			}
			return implode("\n",$bars);
		}
		/**
		 * Return an ordered list of posts.
		 *
		 * The case studies are tied to the microsite page post ID, not the microsite post ID.
		 *
		 * @static
		 * @param $post_id
		 * @param $posts_to_order
		 * @return array
		 */
		static function order_posts( $post_id, $posts_to_order ) {
			$ordered_posts = array_flip( self::get_post_order( $post_id ) );
			foreach( $posts_to_order as $index => $post ) {
				if( array_key_exists( $post->ID, $ordered_posts ) ) {
					$ordered_posts[$post->ID] = $post;
					unset( $posts_to_order[$index] );
				}
			}
			foreach( $posts_to_order as $post ) {
				$append = apply_filters( 'sr_microsite_reorder_child_append', true );
				if( $append ) {
					$ordered_posts[$post->ID] = $post;
				} else {
					$reverse = array_reverse( $ordered_posts, true );
					$reverse[$post->ID] = $post;
					$ordered_posts = array_reverse( $reverse, true );
				}
			}
			foreach( $ordered_posts as $post_id => $value ) {
				/**
				 * Test for orphan posts.
				 */
				if ( is_numeric( $value ) ) {
					$post = get_post( $ordered_posts[$post_id] );
					if ( is_null( $post ) ) {
						/**
						 * If the post is an orphan, delete it.
						 */
						unset( $ordered_posts[$post_id] );
					} else {
						/**
						 * If the post is not an orphan, assign it.
						 * Micah disabled this as it causes issues when trying to fetch a smaller selection of posts.
						 */
						//$ordered_posts[$post_id] = $post;
						unset( $ordered_posts[$post_id] );
					}
				}
			}
			return $ordered_posts;
		}
		function get_post_bar_html($post) {
			$permalink = admin_url( "post.php?post={$post->ID}&action=edit" );
			$post_title = get_the_title( $post->ID );
			$hidden = self::sr_get_microsite_reorder_child_hidden_post_ids();
			$invisible = in_array( $post->ID, $hidden ) ? ' selected="selected"': false;
			$visible = $invisible ? false: ' selected="selected"';
			$classes = array( 'reorder-post-bar' );
			if( $invisible ) {
				$classes[] = 'not-visible';
			}
			$classes = implode(' ', $classes);
			$html =<<<HTML
<li id="reorder-post-bar-{$post->ID}" class="{$classes}" data-postID="{$post->ID}">
		<select class="visibility-select">
			<option{$visible}>Visible</option>
			<option{$invisible}>Not Visible</option>
		</select>
		<span class="post-title"><a href="{$permalink}">{$post_title}</a></span>
</li>
HTML;
			$html = apply_filters( 'sr_microsite_reorder_post_bar_html', $html, $post );
			return $this->apply_filters( 'post_bar_html', $html, $post );
		}
		protected static function get_page() {
			static $page = false;
			if ( ! $page ) {
				$context = sr_get_context();
				$microsite = sr_get_current_microsite();
				$page = $microsite->get_page_by( 'page_key', $context->microsite_page_key );
			}
			return $page;
		}
		protected static function get_child_type() {
			$page = self::get_page();
			return $page->child_type;
		}
		protected static function get_labels() {
			static $labels = false;
			if ( ! $labels ) {
				$microsite = sr_get_current_microsite();
				$page = self::get_page();
				$labels = array(
					'plural_child' => get_post_type_object( $page->child_type )->label,
					'singular_child' => get_post_type_object( $page->child_type )->labels->singular_name,
					'plural_microsite' => get_post_type_object( $microsite->post_type )->label,
					'singular_microsite' => get_post_type_object( $microsite->post_type )->labels->singular_name,
				);
			}
			return $labels;
		}
		static function get_post_order( $post_id ) {
			$post_order_ids = get_post_meta( $post_id, '_sr_post_order_ids', true );
			return empty( $post_order_ids ) ? array() : explode( ',', $post_order_ids );
		}

		static function update_post_order( $post_id, $post_order_ids ) {
			if ( is_array( $post_order_ids ) )
				$post_order_ids = implode( ',', array_map( 'intval', $post_order_ids ) );
			update_post_meta( $post_id, '_sr_post_order_ids', $post_order_ids );
			return $post_order_ids;
		}

		static function sr_ajax_reorder_posts( $context ) {
			$post_order_ids = self::update_post_order( $context->post_id, $context->post_order_ids );
			return array( 'html' => $post_order_ids );
		}

		static function sr_ajax_show_reorder_posts( $context ) {
			delete_post_meta( $context->microsite_post_id, '_sr_hidden_post_id', $context->value );
			return;
		}

		static function sr_ajax_hide_reorder_posts( $context ) {
			add_post_meta( $context->microsite_post_id, '_sr_hidden_post_id', $context->value );
			return;
		}

		static function sr_get_microsite_reorder_child_hidden_post_ids(){
			return get_post_meta( sr_get_context()->microsite_page_post_id, '_sr_hidden_post_id' );
		}

	}
}




