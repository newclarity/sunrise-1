<?php

if( ! function_exists( 'sr_get_microsite_post_types' ) ) {
	function sr_get_microsite_post_types() {
		return Sunrise_Microsites::get_post_types();
	}
}

if ( ! function_exists( 'sr_the_microsite_nav' ) ) {
	function sr_the_microsite_nav( $args = array() ) {
		Sunrise_Microsites::the_microsite_nav( $args );
	}
}
if ( ! function_exists( 'sr_register_microsite' ) ) {
	function sr_register_microsite( $post_type, $args = array() ) {
		Sunrise_Microsites::register( $post_type, $args );
	}
}
if ( ! function_exists( 'sr_register_microsite_page_template' ) ) {
	function sr_register_microsite_page_template( $page_template_name, $args = array() ) {
		Sunrise_Microsites::register_page_template( $page_template_name, $args );
	}
}
if ( ! function_exists( 'sr_has_microsites' ) ) {
	function sr_has_microsites( $post_type ) {
		return Sunrise_Microsites::has_microsites( $post_type );
	}
}
if ( ! function_exists( 'sr_get_microsite' ) ) {
	function sr_get_microsite( $post_type ) {
		return Sunrise_Microsites::get_microsite( $post_type );
	}
}
if ( ! function_exists( 'sr_get_current_microsite' ) ) {
	/**
	 * @return bool|Sunrise_Microsite
	 * TODO (mikes 2011-12-14): Added assertion handling for sr_get_context()->microsite_post_type.
	 */
	function sr_get_current_microsite() {
		return Sunrise_Microsites::get_microsite( sr_get_context()->microsite_post_type );
	}
}

if ( ! function_exists( 'sr_has_microsite_page_template' ) ) {
	function sr_has_microsite_page_template( $page_template_name, $args = array() ) {
		return Sunrise_Microsites::has_page_template( $page_template_name, $args );
	}
}
if ( ! function_exists( 'sr_get_microsite_page_template' ) ) {
	function sr_get_microsite_page_template( $page_template_name, $args = array() ) {
		return Sunrise_Microsites::get_page_template( $page_template_name, $args );
	}
}
if ( ! function_exists( 'sr_get_microsite_main_page_template' ) ) {
	function sr_get_microsite_main_page_template( $post_type ) {
		return Sunrise_Microsites::get_main_page_template( $post_type );
	}
}

if ( ! function_exists( 'sr_get_microsite_reorder_child_hidden_post_ids' ) ) {
	function sr_get_microsite_reorder_child_hidden_post_ids() {
		return Sunrise_Reorder_Admin_Page::sr_get_microsite_reorder_child_hidden_post_ids();
	}
}

if( ! function_exists( 'sr_get_microsite_permalink' ) ) {
	function sr_get_microsite_permalink( $id, $args = array() ) {
		return Sunrise_Microsites::get_permalink( $id, $args );
	}
}