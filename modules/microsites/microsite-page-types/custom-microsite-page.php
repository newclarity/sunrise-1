<?php

if ( ! class_exists( 'Sunrise_Custom_Microsite_Page' ) ) {
	class Sunrise_Custom_Microsite_Page extends Sunrise_Microsite_Page {
		function do_initialize( $args ) {
			$this->user_type = 'custom';
			$this->set_controls( Sunrise::expand_args( array_merge(
				array( 'edit_page', 'rename_page', 'trash_page', 'toggle_page_status', 'view_page'	),
				$this->_controls  // This must be $this->_controls, not $this->controls
			)));
		}
	}
}

