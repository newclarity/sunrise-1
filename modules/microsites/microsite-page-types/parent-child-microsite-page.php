<?php

if ( ! class_exists( 'Sunrise_Parent_Child_Microsite_Page' ) ) {
	/**
	 * @property string $child_type
	 */
	class Sunrise_Parent_Child_Microsite_Page extends Sunrise_Microsite_Page {
		protected $_child_type = false;

		function do_initialize( $args ) {
			$this->user_type = 'database';
			$this->set_controls( Sunrise::expand_args( array_merge(
				array( 'filter_pages', 'rename_page', 'toggle_page_status', 'view_page'	),
				$this->_controls  // This must be $this->_controls, not $this->controls
			)));
		}
		function do_insert_page( $args ) {
			if ( ! empty( $args['child_type'] ) ) {
				$this->child_type = $args['child_type'];
			}

			if ( ! $this->child_type ) {
				$page_template = sr_get_microsite_page_template( $this->page_template_name, array(
					'post-type' => $args['post']->post_type,
				));
				$this->child_type = $page_template['child_type'];
			}
			$this->update_child_type();
		}
		function update_child_type() {
			update_post_meta( $this->post_id, '_sr_microsite_page_child_type', $this->child_type );
		}
		/**
		 * @return string
		 *
		 * TODO (mikes 2011-12-14): Should this be name 'load_', or something else?
		 */
		function load_child_type() {
			return get_post_meta( $this->post_id, '_sr_microsite_page_child_type' );
		}
		function do_instantiate( $page_args ) {
			/**
			 * TODO (mikes): This has got to be fixed. ONLY works is the page_templates are shared/global page templates.
			 */
			$page_template = sr_get_microsite_page_template( $page_args['page_template_name'], array(
				'post_type' => $page_args['microsite_post_type'],
		 	));
			$this->page_type = $page_template['page_type'];
			$this->child_type = $page_template['child_type'];
			/**
			 * TODO (mikes 2012-06-23): This next line was previously using $page_template['page_title'] instead of
			 * TODO:                    $page_template['label'] but the former wasn't being set, and I couldn't tell
			 * TODO:                    the difference between title and label. Once we document Microsites maybe we
			 * TODO:                    can resolve what this should be.
			 */

			$this->title = $page_template['label'];
			$this->label = $page_template['label'];
		}
		function get_child_type() {
			if ( ! $this->_child_type && $this->_post )
				$this->_child_type = $this->load_child_type();
			return $this->_child_type;
		}
		function set_child_type( $child_type ) {
			$this->_child_type = $child_type;
		}
		function filter_link_properties( $link_properties ) {
			$link_properties['child_type'] = $this->child_type;
			return $link_properties;
		}
		function filter_page_bar_classes( $classes ) {
			$classes[] = 'child-type-' . sr_dashize( $this->child_type );
			return $this->apply_filters( 'parent_child_page_bar_classes', $classes );
		}
//		function filter_admin_bar_html( $html ) {
//			$query = new WP_Query( array(
//
//			));
//			$child_post_count = 10;
//			$element = sprintf( '<span class="child-post-count">(%s)</span>', $child_post_count );
//			return preg_replace( '#(<span class="user-type">)#', "{$element}$1", $html );
//		}

		function __set( $property_name, $value ) {
			switch ( $property_name ) {
				case 'child_type':
					call_user_func( array( &$this, "set_{$property_name}" ), $value );
					break;

//				case 'foo':
//					$this->_cannot_set_error( $property_name, $value );
//					break;

				default:
					parent::__set( $property_name, $value );
					break;
			}
		}

		function __get( $property_name ) {
			$value = false;
			switch ( $property_name ) {
				case 'child_type':
					$value = call_user_func( array( &$this, "get_{$property_name}" ) );
					break;

				default:
					$value = parent::__get( $property_name );
					break;
			}
			return $value;
		}
	}
}

