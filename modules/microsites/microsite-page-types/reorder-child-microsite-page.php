<?php

require_once(dirname(__FILE__) . '/parent-child-microsite-page.php'); // Make sure it has been defined first.

if ( ! class_exists( 'Sunrise_Reorder_Child_Microsite_Page' ) ) {
	class Sunrise_Reorder_Child_Microsite_Page extends Sunrise_Parent_Child_Microsite_Page {
		public $relationship_name;

		function filter_args( $args ) {
			return Sunrise::renormalize_args( $args, array(
				'relationship' => 'relationship_name',
			));
		}
		function do_initialize( $args ) {
			parent::do_initialize( $args );
			$this->set_controls( sr_array_insert_key(
				$this->_controls,  // This must be $this->_controls, not $this->controls
				'reorder_pages',
				sr_get_control_set_control( 'microsite-page-controls', 'reorder_pages', array( 'parent' => &$this ) ),
				'rename_page',
				'before'
			 ));
		}
// TODO (mikes): Uncomment when this is actually needed.
// TODO: Verify that having two "page-type-..." classes won't cause problems.
//		function filter_page_bar_classes( $classes ) {
//			$classes[] = 'page-type-parent-child';
//			return $classes;
//		}

	}
}

