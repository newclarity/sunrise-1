<?php

if ( ! class_exists( 'Sunrise_Main_Microsite_Page' ) ) {
	class Sunrise_Main_Microsite_Page extends Sunrise_Microsite_Page {

		function get_page_title() {
			$title = $this->_load_page_title();
			return empty( $title ) ? $this->label : $title;
		}
		function update_page_title( $post_title ) {
			update_post_meta( $this->post_id, '_sr_microsite_page_title', $post_title );
		}
		protected function _load_page_title() {
			return get_post_meta( $this->post_id, '_sr_microsite_page_title', true );
		}
		function do_initialize( $args ) {
			$this->user_type = 'form';
			$this->set_controls( Sunrise::expand_args( array_merge(
				array( 'edit_page', 'rename_page', 'view_page'	),
				$this->_controls  // This must be $this->_controls, not $this->controls
			)));
		}
	}
}

