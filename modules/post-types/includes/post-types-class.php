<?php

if (!class_exists('Sunrise_Post_Types')) {
	class Sunrise_Post_Types {
		static function on_load() {
			/**
			 * Use 'init' rather than 'sunrise_init' because taxonomies and post_types effect admin_menus
			 */
			sr_add_action( __CLASS__, 'init' );
		}
		static function init() {
			do_action( 'sr_post_types_init' );
			do_action( 'sr_taxonomies_init' );		// TODO (mike): Move this to a future "Sunrise Taxonomies"
		}
	}
	Sunrise_Post_Types::on_load();
}
