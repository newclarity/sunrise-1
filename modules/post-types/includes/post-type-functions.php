<?php

/**
 * Clear the supports array from a given post type thus enabling it's admin page to be built up from scratch.
 *
 * @param $post_type
 * @return void
 *
 */
function sr_clear_post_type_supports( $post_type ) {
	global $_wp_post_type_features;
	$_wp_post_type_features[$post_type] = array(); // Clear post type support.
}

/*
 * Allow for setting labels using "label/%label_property%" syntax
 *
 */
function sr_get_post_type_labels( $object ) {

	if ( isset( $object->labels ) ) {
		if ( is_array( $object->labels ) )
			$labels = $object->labels;
		else
			$labels = array( $object->labels );
	} else {
		$labels = array();
	}

	// Unravel the simplified label syntax of: "label/%label_property%"
	$_to_unset = array();
	foreach( (array) $object as $name => $value ) {
		if ( is_string($value) && $name[0]=='l' && preg_match( '#^labels/(.+)$#', $name, $match ) ) {
			$labels[$match[1]] = $value;
			$_to_unset[] = $name;
		}
	}
	$labels['_to_unset'] = $_to_unset; // Give us ability to remove these after the post type object is built.

	// Make sure singular name is set
	if ( ! isset( $labels['singular_name'] ) )
		$labels['singular_name'] = false;

	// If singular_name is not set
	if ( ! $labels['singular_name'] ) {
		// Grab singular label if set
		$labels['singular_name'] = isset( $object->singular_label ) ? $object->singular_label : false;
		// If still not set and last
		if ( ! $labels['singular_name'] ) {
			if ( isset( $labels['name'] ) && 's' == substr( $labels['name'], -1, 1 ) ) {
				// If the plural name was set and ends with 's', strip the 's'
				$labels['singular_name'] = substr( $labels['name'], 0, -1 );
			} else if ( isset( $object->label ) && 's' == substr( $object->label, -1, 1 ) ) {
				// If the label was set and ends with 's', strip the 's'
				$labels['singular_name'] = substr( $object->label, 0, -1 );
			}
		}
	}

	// Grab to a local variable for convenience
	$singular_name = $labels['singular_name'];

	// Put in the object so we
	$object->labels = $labels;
	$labels = (array) _get_custom_object_labels( $object, array( 'name' => false ) );

	// If $singular_name not previously set and it now ends with an 's', lop off the 's'
	if ( ! $singular_name && substr( $labels['singular_name'], -1, 1 ) == 's' )
		$labels['singular_name'] = substr( $labels['singular_name'], 0, -1 );

	$plural_name = $labels['name'];
	$singular_name = $labels['singular_name'];

	if ( ! isset( $labels['name'] ) )
		$labels['name'] = _x( $plural_name, "{$object->name} post type general name" );

	if ( ! isset( $labels['singular_name'] ) )
		$labels['singular_name'] = _x( $singular_name, "{$object->name} post type singular name" );

	if ( ! isset( $labels['add_new'] ) )
		$labels['add_new'] = _x( 'Add New', "{$object->name} post" );

	if ( ! isset( $labels['add_new_item'] ) )
		$labels['add_new_item'] = __( "Add New {$singular_name}" );

	if ( ! isset( $labels['edit_item'] ) )
		$labels['edit_item'] = __( "Edit {$singular_name}" );

	if ( ! isset( $labels['new_item'] ) )
		$labels['new_item'] = __( "New {$singular_name}" );

	if ( ! isset( $labels['view_item'] ) )
		$labels['view_item'] = __( "View {$singular_name}" );

	if ( ! isset( $labels['search_items'] ) )
		$labels['search_items'] = __( "Search {$plural_name}" );

	if ( ! isset( $labels['not_found'] ) )
		$labels['not_found'] = __( "No {$plural_name} found." );

	if ( ! isset( $labels['not_found_in_trash'] ) )
		$labels['not_found_in_trash'] = __("No {$plural_name} found in Trash.");

	if ( ! isset( $labels['parent_item_colon'] ) )
		$labels['parent_item_colon'] =  $object->hierarchical ? __("Parent {$singular_name}:") : null;

	return $labels;
}
/*
 * Adds hooks pre and return value hooks to register_post_type()
 *
 * See: http://core.trac.wordpress.org/ticket/17447
 *
 */
function sr_register_post_type( $post_type, $args ) {

	$args['name'] = $post_type = sanitize_key( $post_type );

  $args = apply_filters( 'pre_sr_register_post_type', $args, $post_type );

  $args['labels'] = (array)sr_get_post_type_labels( (object)$args, $args );

	// Remove the "label/%label_property%" features
	foreach( $args['labels']['_to_unset'] as $feature_name )
		unset( $args[ $feature_name ] );
	unset( $args['labels']['_to_unset'] );

	// Make sure we have a rewrite array
	if ( ! isset( $args['rewrite'] ) ) {
		$args['rewrite'] = array();
	}
	// Make sure the slug if plural, if possible
	if ( ! isset( $args['rewrite']['slug'] ) && 's' != substr( $post_type, -1, 1 ) ) {
		$args['rewrite']['slug'] = "{$post_type}s";
	}

	// Allow easier disable of "supports", i.e. just accept 'false'
	if ( isset( $args['supports'] ) && ! $args['supports'] ) {
		$args['supports'] = array( false );  // This will clear all supports values
	}

	$args = register_post_type( $post_type, $args );

 	return apply_filters( 'sr_register_post_type', $args, $post_type );
}
/**
 * Convert Post Fields
 *
 * ONLY TESTED with FROM 'repeating_fieldset' TO 'post'.  Was developed for Haslaw to move 'Office Locations'
 * from repeating fieldsets to a post type.
 *
 * @param int $from_post_id The ID of the post to convert from
 * @param array $args The array of information about conversions, see example.
 * @author Mike Schinkel
 * @since 2012-08-24
 * @example:
 *
 *	sr_convert_post_fields_data( $contact_page->ID, array(
 *		'from_type' 			=> 'repeating_fieldset',
 *		'from_field_name'	=> 'office_locations',
 *		'from_post_type' 	=> 'page',
 *		'to_type' 				=> 'post',
 *		'to_post_type' 		=> 'office',
 *		'to_post_title' 	=> 'location',
 *		'fields' => array(
 *			'fax' 						=> 'fax_number',
 *			'google_maps_url' => 'google_maps_url',
 *			'phone' 					=> 'telephone',
 *			'street' 					=> 'street_address',
 *			'city_state_zip' 	=> array( 'regex' => '#^([^,]+),\s*([A-Z]{2})\s*([0-9]{5})$#', 'fields' => array(
 *				1 => 'address_locality', 	// City
 *				2 => 'address_region', 		// State
 *				3 => 'postal_code', 			// Zip
 *			)),
 *	)));
 *	}
 *
 */
function sr_convert_post_fields_data( $from_post_id, $args = array() ) {
	$args = wp_parse_args( $args, array(
		'from_type' 			=> 'repeating_fieldset',
		'from_field_name' => false,
		'from_post_type' 	=> 'page',
		'to_type' 				=> 'post',		// vs. 'repeating_fieldset'
		'to_field_name' 	=> false,
		'to_post_type' 		=> 'post',
		'to_post_title' => 'post_title',
	));
	switch ( $args['from_type'] ) {
		case 'post':
			$from_data[] = array( get_post( $from_post_id = $from_post_ids ) );
			break;
		case 'repeating_fieldset':
			$from_data = sr_get_the_field( $args['from_field_name'], array(
				'post_id' => $from_post_id,
				'object_type' => 'post',
				'object_sub_type' => $args['from_post_type'],
			));
			break;
		default;
			$from_data = null;
			break;
	}
	$insert_new_posts = $args['from_type'] == 'repeating_fieldset' && $args['to_type'] == 'post';
	if ( ! $insert_new_posts ) {
		sr_die( 'The function has yet to be tested with any options beside repeating_fieldset => post.' );
	}
	foreach( $from_data as $index => $post_data ) {
		if ( $insert_new_posts ) {
			$to_post_id = wp_insert_post( array(
				'post_title' => $post_data[$args['to_post_title']],
				'post_type' => $args['to_post_type'],
				'post_status' => 'publish',
			));
		}
		$matches = array();
		foreach( $args['fields'] as $from_field_name => $to_field_name ) {
			$value = $post_data[$from_field_name];
			if ( ! is_array( $to_field_name ) ) {
				$to_field_name = array( 'fields' => array( $to_field_name ) );
				$matches = array( $value );
			} else {
				preg_match( $to_field_name['regex'], $value, $matches );
			}
			foreach( $to_field_name['fields'] as $ordinal => $to_sub_field_name ) {
				$field = sr_get_field_object( $to_sub_field_name, 'post', $args['to_post_type'], array( 'post_id' => $to_post_id, ));
				$field->post_id = $to_post_id;
				$field->update_value( $matches[$ordinal] );
			}
		}
	}
}

/**
 * Retrieves a single 'page' post based on
 *
 * @param $page_type
 * @return mixed|void
 * @author Mike Schinkel
 * @since 2012-08-24
 *
 * TODO (mikes 2012-08-24): Redo this using Sunrise Fields functions vs. in SQL.
 */
function sr_get_pages_by_type( $page_type ) {
	global $wpdb;
	$post_statuses = apply_filters( 'sr_get_pages_by_type_statuses', array( 'publish', 'draft', 'pending', 'private' ) );
	$post_statuses = "'" . implode( "','", $post_statuses ) . "'";
	$sql =<<<SQL
SELECT
	p.ID
FROM
	{$wpdb->prefix}posts p
	INNER JOIN {$wpdb->prefix}postmeta pm ON p.ID=pm.post_id AND pm.meta_key='_page_type'
WHERE 1=1
	AND p.post_type='page'
	AND pm.meta_value='{$page_type}'
	AND p.post_status IN ({$post_statuses})
SQL;
	$page_ids = $wpdb->get_col( apply_filters( 'sr_get_pages_by_type_sql', $sql, $page_type ) );
	$pages = array();
	if ( ! empty( $page_ids ) ) {
		$pages = array_map( 'get_page', $page_ids );
	}
	return apply_filters( 'sr_get_pages_by_type', $pages, $page_ids, $page_type );
}
