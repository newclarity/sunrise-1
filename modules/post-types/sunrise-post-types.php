<?php
/*
Plugin Name: Sunrise Post Types
Plugin URL: http://docs.getsunrise.org/post-types/
File Name: sunrise-post-types.php
Author: The Sunrise Team
Author URL: http://getsunrise.org
Version: 0.0.0
Description: Enhanced WordPress' (Custom) Post Types
*/

define( 'SUNRISE_POST_TYPES_DIR', dirname( __FILE__ ) );
require( SUNRISE_POST_TYPES_DIR . '/includes/post-type-functions.php');
require( SUNRISE_POST_TYPES_DIR . '/includes/post-types-class.php');

//sr_load_plugin_files( 'post-types', array( ) );
//sr_load_demos( SUNRISE_POST_TYPES_DIR );
