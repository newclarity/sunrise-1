<?php
/*
Plugin Name: Sunrise Images
Plugin URL: http://docs.getsunrise.org/images/
File Name: sunrise-images.php
Author: The Sunrise Team
Author URL: http://getsunrise.org
Version: 0.0.0
Description: Adds Image Management for Sunrise to WordPress
TODO: Consider integrating with http://punypng.com/api
*/

define( 'SUNRISE_IMAGES_DIR', dirname( __FILE__ ) );
require( SUNRISE_IMAGES_DIR . '/includes/image-type-functions.php');
require( SUNRISE_IMAGES_DIR . '/includes/image-functions.php');
require( SUNRISE_IMAGES_DIR . '/includes/image-file-functions.php');
require( SUNRISE_IMAGES_DIR . '/includes/image-crop-class.php');
require( SUNRISE_IMAGES_DIR . '/includes/images-class.php');

//sr_load_plugin_files( 'images', array( 'image' ) );

//sr_load_demos( SUNRISE_IMAGES_DIR );
