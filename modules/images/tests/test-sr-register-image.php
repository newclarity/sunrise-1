<?php

$image_type = sr_register_image_type( $name = 'test_image_type_1', '150x200' );
$data = array(
  'function_call' => "sr_register_image_type( 'test_image_type_1', '150x200' )",
  'data' => $image_type,
);
/*
  Array (
    [image_type] => test_image_type_1
    [default] => default
    [aspect_ratio] => 150:200
    [sizes] => array(
        [default] => array(
            [dimensions] => 150x200
            [height] => 200
            [width] => 150
            [image_size_type] => default-test_image_type_1
            [image_type_size] => test-image-type-1/default
            [image_type] => test_image_type_1
            [image_size] => default
          )
      )
  )
 */
sr_assert_is_array( $image_type, $data );
sr_assert_has_element_value( $image_type, 'image_type', $name, $data );
sr_assert_has_element_value( $image_type, 'default', 'default', $data );
sr_assert_has_element_value( $image_type, 'aspect_ratio', '150:200', $data );
sr_assert_has_element( $image_type, 'sizes', $data );
sr_assert_has_element_count( $image_type, 'sizes', 1, $data );
sr_assert_has_element_key( $image_type, 'sizes', 'default', $data );
if ( isset( $image_type['sizes']['default'] ) ) {
	$default_data = $data;
	$default_data['data'] = $default = $image_type['sizes']['default'];
	$default_data['value_name'] = "\$value['sizes']['default']";
	sr_assert_has_element_value( $default, 'dimensions', 			'150x200', $default_data );
	sr_assert_has_element_value( $default, 'height', 					'200', $default_data );
	sr_assert_has_element_value( $default, 'width', 					'150', $default_data );
	sr_assert_has_element_value( $default, 'image_size_type', 'default-test_image_type_1', $default_data );
	sr_assert_has_element_value( $default, 'image_type_size', 'test-image-type-1/default', $default_data );
	sr_assert_has_element_value( $default, 'image_type', 			'test_image_type_1', $default_data );
	sr_assert_has_element_value( $default, 'image_size', 			'default', $default_data );
}



//sr_register_image_type( 'test_image_type_2', array( 'size' => '150x200' ) );
//
//sr_register_image_type( 'test_image_type_3', array(
//  'sizes' => array(
//    'default' => '150x200'
//  )
//));
//
//sr_register_image_type( 'test_image_type_4', array(
//  'default' => 'sidebar',
//  'sizes' => array(
//    'search'   => '50x50',
//    'sidebar'  => '120x180',
//    'featured' => '200x300',
//  )
//));

