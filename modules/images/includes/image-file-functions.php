<?php

function sr_the_image( $image_type_size, $args = array() ) {
	$args['image_type_size'] = $image_type_size;
	$args['output'] = 'html';
	echo sr_get_image_file( $args );
}

function sr_the_sized_image_file( $image_size, $args = array() ) {
	echo sr_get_sized_image_file( $image_size, $args );
}

function sr_the_image_file( $args = array() ) {
	echo sr_get_image_file( $args );
}

function sr_get_sized_image_file( $image_size, $args = array() ) {
	$args = sr_parse_args( $args );
	$args['image_size'] = $image_size;
	return sr_get_image_file( $args );
}

function sr_get_image_file( $args = array() ) {
	$args = sr_parse_args( $args, array(
		'attachment_id' => false,
		'image_type' => false,
		'image_size' => false,
		'image_type_size' => false,
		'output' => 'object',
		'class' => false,
		'alt' => false,
        'itemprop' => false,
	));
	if ( $args['image_type_size'] ) {
		list( $args['image_type'], $args['image_size'] ) = explode( '/', "{$args['image_type_size']}/default" );
	}
	if ( empty( $args['attachment_id'] ) ){
		sr_die( __( 'You must pass an attachment_id in the $args parameter of sr_get_image_file()' ) );
	}

	if ( false === $args['image_type'] ) {
		$args['image_type'] = sr_get_attached_image_type(  $args['attachment_id'] );
	}

	$image_file = new Sunrise_Image_File( $args['image_type'] );
	$image_file->initialize_from( 'attachment_id', $args['attachment_id'] );

	if ( $args['image_size'] ) {
		$image_file = $image_file->as_sized( $args['image_size'] );
	}

	switch ( $args['output'] ) {
		case 'html':
			if( ! empty( $args['class'] ) ) {
				$image_file->extra['class'] = $args['class'];
			}
			if( ! empty( $args['alt'] ) ) {
				$image_file->extra['alt'] = $args['alt'];
			}
            if( ! empty( $args['itemprop'] ) ) {
                $image_file->extra['itemprop'] = $args['itemprop'];
            }
			$value = $image_file->__toString();
			break;

		case 'permalink':
		case 'url':
			$value = $image_file->url;
			break;

		case 'object':
			$value = $image_file;
			break;

		default:
			$value = false;

	}
	return $value;

}
