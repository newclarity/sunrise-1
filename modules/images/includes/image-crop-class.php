<?php
if ( ! class_exists( 'Sunrise_Image_Crop' ) ) {
	/**
	 *  @property int $top
	 *  @property int $left
	 *  @property int $bottom
	 *  @property int $right
	 *  @property int $height
	 *  @property int $width
	 *  @property int $dimensions
	 *  @property int $output_height
	 *  @property int $output_width
	 *  @property int $output_dimensions
	 *  @property int $output_aspect_ratio
	 *  @property int $original_aspect_ratio
	 *  @property int $image_size
	 *  @property int $image_type
	 *  @property float $aspect_ratio
	 *  @property bool $has_crop
	 *  @property int $x1
	 *  @property int $x2
	 *  @property int $y1
	 *  @property int $y2
	 */
	class Sunrise_Image_Crop extends Sunrise_Base {

		/**
		 * @var bool|Sunrise_Image_File $image_file
		 */
		public $image_file       = false;

		protected $_top          = false;
		protected $_left         = false;
		protected $_width        = false;
		protected $_height       = false;
		protected $_has_crop     = false;

		protected $_image_type_size = false;

		protected $_inside_fixup = false;

		/**
		 * @param Sunrise_Image_File $image_file
		 * @param array $args
		 */
		function __construct( $image_file, $args = array() ) {
			$args = sr_parse_args( $args, array(
	            'image_size' => 'fullsize',
			) );
		    $this->image_file = $image_file;
		    $this->image_size = $args['image_size'];

			if ( isset( $args['attachment_id'] ) ) {
				$from = 'attachment_id';
				$data = $args['attachment_id'];

			} else if ( isset( $args['selection'] ) ) {
				$from = 'selection';
				$data = $args['selection'];

			} else {
				$from = 'defaults';
				$data = false;
			}
			$this->initialize_from( $from, $data );

			do_action( 'sr_image_crop', $this );

		}
		function as_selection() {
			return array(
				'x1' => $this->x1,
				'y1' => $this->y1,
				'x2' => $this->x2,
				'y2' => $this->y2,
			);
		}
		function _fixup_horizontal_dimensions() {
			if ( ! $this->_inside_fixup ) {
				$this->_inside_fixup = true;
				$aspect_ratio = $this->aspect_ratio;
				$output_aspect_ratio = $this->output_aspect_ratio;
				if ( round( $aspect_ratio, 2 ) > round( $output_aspect_ratio, 2 ) ) {
					$prior_width = $this->_width;
					$this->_width = intval( $this->_height * $output_aspect_ratio );
					$margins = $prior_width - $this->_width;
					if ( 1 == $margins % 2 ) {
						/**
						 * $margins was odd so make the left size 1px smaller
						 */
						$margins--;
					}
					$this->_left += intval( $margins / 2 );
				}
				$this->_inside_fixup = false;
			}
		}
		function _fixup_vertical_dimensions() {
			if ( ! $this->_inside_fixup ) {
				$this->_inside_fixup = true;
				$aspect_ratio = $this->aspect_ratio;
				$output_aspect_ratio = $this->output_aspect_ratio;
				if ( round( $aspect_ratio, 2 ) < round( $output_aspect_ratio, 2 ) ) {
					$prior_height = $this->_height;
					$this->_height = intval( $this->_width / $output_aspect_ratio );
					$margins = $prior_height - $this->_height;
					if ( 1 == $margins % 2 ) {
						/**
						 * $margins was odd so make the top size 1px smaller
						 */
						$margins--;
					}
					$this->_top += intval( $margins / 2 );
				}
				$this->_inside_fixup = false;
			}
		}
		function get_image_size() {
			return $this->_image_type_size['image_size'];
		}
		function get_image_type() {
			return $this->_image_type_size['image_type'];
		}
		function get_x1() {
			return $this->left;
		}
		function get_x2() {
			return $this->left + $this->width;
		}
		function get_y1() {
			return $this->top;
		}
		function get_y2() {
			return $this->top + $this->height;
		}
		function get_top() {
			$this->_fixup_vertical_dimensions();
			return $this->_top;
		}
		function get_left() {
			$this->_fixup_horizontal_dimensions();
			return $this->_left;
		}
		function get_width() {
			$this->_fixup_horizontal_dimensions();
			return $this->_width;
		}
		function get_height() {
			$this->_fixup_vertical_dimensions();
			return $this->_height;
		}
		function get_bottom() {
			$bottom = $this->image_file->height - ( $this->top + $this->height );
			return $bottom;
		}
		function get_right() {
			$right = $this->image_file->width - ( $this->left + $this->width );
			return $right;
		}
		function get_image_size_aspect_ratio() {
			$height = floatval( $this->_height );
			return 0 == $height ? 0 : floatval( $this->_width ) / $height;
		}
		function get_aspect_ratio() {
			$height = floatval( $this->_height );
			return 0 == $height ? 0 : floatval( $this->_width ) / $height;
		}
		function get_output_dimensions() {
			return "{$this->output_width}x{$this->output_height}";
		}
		function get_output_aspect_ratio() {
			return floatval( $this->output_width ) / floatval( $this->output_height );
		}
		function get_original_aspect_ratio() {
			return floatval( $this->image_file->width ) / floatval( $this->image_file->height );
		}
		function get_output_width() {
			return $this->_image_type_size['width'];
		}
		function get_output_height() {
			return $this->_image_type_size['height'];
		}
		function get_filepath() {
			$filepath = $this->image_file->get_sized_filepath( $this->image_size );
			return $filepath;
		}
		function set_image_size( $image_size ) {
			if ( ! $this->_image_type_size ) {
				$this->_image_type_size = sr_get_image_type_size( $this->image_file->image_type, $image_size );
			}
		}
		function set_x1( $x1 ) {
			$this->_left = intval( $x1 );
		}
		function set_x2( $x2 ) {
			$this->_width = $x2 - $this->left;
		}
		function set_y1( $y1 ) {
			$this->_top = intval( $y1 );
		}
		function set_y2( $y2 ) {
			$this->_height = $y2 - $this->top;
		}
		function set_top( $top ) {
			$this->_top = intval( $top );
		}
		function set_left( $left ) {
			$this->_left = intval( $left );
		}
		function set_width( $width ) {
			$this->_width = intval( $width );
		}
		function set_height( $height ) {
			$this->_height = intval( $height );
		}
		function set_bottom( $bottom ) {
			$this->_height = intval( $this->image_file->height - $this->top - $bottom );
		}
		function set_right( $right ) {
			$this->_width = intval( $this->image_file->width - $this->left - $right );
		}
		function set_output_dimensions( $dimensions ) {
			if ( ! preg_match( '#^(\d+)x(\d+)$#', $dimensions, $match ) ) {
				sr_die( __( 'The output dimensions were not correct formatted; expected [WxH], got: %s.' ), $dimensions );
			}
			$this->output_width =  intval( $match[1] );
			$this->output_height = intval( $match[2] );
		}
		function initialize_from( $from, $data = false ) {
			$image_file = $this->image_file;
			switch ( $from ) {
				/**
				 * 100 is a random picky default.
				 * TODO (mikes): Fix the reason why we have to test for empty height and width
				 */
				case 'defaults':
					$this->top =     0;
					$this->left =    0;
					$this->height =  $image_file->height;
					$this->width =   $image_file->width;
					break;

				case 'selection':
					$this->x1    = $data['x1'];
					$this->y1     = $data['y1'];
					$this->height  = $data['height'];
					$this->width   = $data['width'];
					break;

				case 'attachment_id':
					$this->load_crop( $data );
					if ( ! $this->has_crop )
						$this->initialize_from( 'defaults' );
					else
						$this->fixup_crop();
					break;
			}
		}
		/**
		 * This is called from $this->initialize_from('attachment_id', $data) method to resolve issues when
		 * the height or width saved for the crop is larger than the image.  (There seems to be a bug that
		 * adds 1 to the crop; not sure if that's our code or the cropper's code, or a rounding issue, but
		 * we can't address that today. Maybe tomorrow, after all, tomorrow is another day.)
		 */
		function fixup_crop() {
			if ( $this->height > $this->image_file->height ) {
				$this->width = intval( $this->width * $this->image_file->height / $this->height );
				$this->height = $this->image_file->height;
			}
			if ( $this->width > $this->image_file->width ) {
				$this->height = intval( $this->height * $this->image_file->width / $this->width );
				$this->width = $this->image_file->width;
			}
		}
		function load_crop( $data ) {
			$meta = get_post_custom( $data );
			$count = 0;
			foreach( array( 'top', 'left', 'height', 'width' ) as $dimension ) {
				$meta_key = "_sr_attachment_crop[{$dimension}]";
				if ( isset( $meta[$meta_key] ) ) {
					$count++;
					call_user_func( array( &$this, "set_{$dimension}" ), $meta[$meta_key][0] );
				}
			}
			$this->_has_crop = ( 4 == $count );
		}
		function get_has_crop() {
			return $this->_has_crop;
		}
		function update( $attachment_id ) {
			update_post_meta( $attachment_id, "_sr_attachment_crop[top]",   $this->top );
			update_post_meta( $attachment_id, "_sr_attachment_crop[left]",  $this->left );
			update_post_meta( $attachment_id, "_sr_attachment_crop[height]",$this->height );
			update_post_meta( $attachment_id, "_sr_attachment_crop[width]", $this->width );
		}
		function delete( $attachment_id ) {
			foreach( array( 'top', 'left', 'height', 'width' ) as $dimension ) {
				$meta_key = "_sr_attachment_crop[{$dimension}]";
				delete_post_meta( $attachment_id, $meta_key );
			}
		}
		function __set( $property_name, $value ) {
			switch ( $property_name ) {
				case 'image_size':
				case 'top':
				case 'left':
				case 'bottom':
				case 'right':
				case 'width':
				case 'height':
				case 'output_width':
				case 'output_height':
				case 'output_dimensions':
				case 'x1':
				case 'x2':
				case 'y1':
				case 'y2':
					call_user_func( array( &$this, "set_{$property_name}" ), $value );
					break;

				case 'aspect_ratio':
				case 'output_aspect_ratio':
				case 'original_aspect_ratio':
				case 'image_type':
					$this->_cannot_set_error( 'aspect_ratio', $value );
					break;

				default:
					parent::__set( $property_name, $value );
					break;
			}
		}
		function __get( $property_name ) {
			$value = false;
			switch ( $property_name ) {
				case 'top':
				case 'left':
				case 'bottom':
				case 'right':
				case 'width':
				case 'height':
				case 'aspect_ratio':
				case 'output_width':
				case 'output_height':
				case 'output_dimensions':
				case 'output_aspect_ratio':
				case 'original_aspect_ratio':
				case 'x1':
				case 'x2':
				case 'y1':
				case 'y2':
				case 'filepath':
				case 'image_size':
				case 'image_type':
				case 'has_crop':
					$value = call_user_func( array( &$this, "get_{$property_name}" ) );
					break;

				default:
					$value = parent::__get( $property_name );
					break;
			}
			return $value;
		}
	}
}

