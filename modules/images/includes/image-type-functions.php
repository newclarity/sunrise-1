<?php
/**
 * TODO (mikes 2011-12-16): Create a Sunrise_Image_Type class and port this code.
 */

if ( ! function_exists( 'sr_is_valid_image_dimensions' ) ) {
	/**
	 * @param string $dimensions
	 * @return bool
	 */
	function sr_is_valid_image_dimensions( $dimensions ) {
		$digit = "[1-9][0-9]{1,3}";
		return 0 != preg_match( "#^{$digit}x{$digit}$#", $dimensions );
	}
}

if ( ! function_exists( 'sr_register_image_type' ) ) {
	function sr_register_image_type( $image_type, $args = array() ) {
		global $sr_image_types, $sr_image_size_types, $sr_image_type_sizes;
		/*
		 * Make sure the global array is not empty.
		 */
		if ( ! isset($sr_image_types) )
			$sr_image_types = array();

		/*
		 * Make sure this image type has not previously been registered.
		 * However, if marked as $args['overridable'] then this we'll just
		 * return in the case the image type has already been defined. This
		 * allows an application plugin to define an image type but to have
		 * another plugin or theme define it if they define it with a higher
		 * priority hook than the Application Plugin.
		 */
		if ( isset( $sr_image_types[$image_type] ) && isset( $args['overridable'] ) && $args['overridable'] )
			return $args; // TODO (mikes 2012-06-14): Restructure so it doesn't bail in the middle of the function

		/*
		 * Check if 'image_type' argument was defined
		 */
		if ( WP_DEBUG ) {
			if ( isset( $sr_image_types[$image_type] ) )
				sr_die( "Image Type [{$image_type}] has already been registered." );

			if ( ! is_string( $image_type ) || empty($image_type) ) {
				sr_die( __( "'image_type' [%s] not a string or not specified when registering an image type." ), $image_type );
			}

			if ( ! (is_array( $args ) || (is_string( $args ) && sr_is_valid_image_dimensions( $args ))) ) {
				sr_die(
					__( '$args are not specified as an array or a string of image dimensions when registering image type [%s].' ),
					$image_type
				);
			}
		}

		if ( is_string( $args ) ) {
			$args = array( 'sizes' => array( 'default' => $args ) );
		}

		$args = Sunrise::normalize_args(
			$args, array(
				'default' => 'default_size',
			)
		);

		/*
		 * Set $args['image_type']
		 */
		$args['image_type'] = $image_type;

		/*
		 * If we have a single 'size' $arg convert to a single element 'sizes' array with a 'default' key.
		 */
		if ( ! isset($args['sizes']) ) {

			if ( WP_DEBUG && (! isset($args['size']) || ! sr_is_valid_image_dimensions( $args['size'] )) )
				sr_die( __( "You must specify either 'size' or sizes' when registering Image Type [%s]." ), $image_type );

			if ( is_string( $args['size'] ) ) {
				$args['sizes'] = array( 'default' => $args['size'] );

			} else if ( is_array( $args['size'] ) ) {
				$size = reset( $args['size'] );
				if ( WP_DEBUG && (! sr_is_valid_image_dimensions( $size )) ) ;
				sr_die(
					__(
						"If you specify 'size' as an array for when registering Image Type [%s] it's element much be a valid image dimension."
					), $image_type
				);

				$args['sizes'] = $args['size'];
			}

			unset($args['size']);

		}

		/*
		 * Check if 'sizes' argument was defined correctly
		 */
		if ( WP_DEBUG ) {
			if ( ! isset($args['sizes']) || count( $args['sizes'] ) == 0 )
				sr_die( __( "'sizes' not specified when registering Image Type [%s]." ), $image_type );
			else if ( ! is_array( $args['sizes'] ) )
				sr_die( __( "'sizes' argument is not an array when registering Image Type [%s]." ), $image_type );
		}

		if ( empty($args['default_size']) ) {
			/**
			 * If we have no 'default_size', set to the key of the first size.
			 */
			$size_keys = array_keys( $args['sizes'] );
			$args['default_size'] = reset( $size_keys );
		} else if ( WP_DEBUG ) {
			if ( ! is_string( $args['default_size'] ) ) {
				sr_die( __( "The 'default_size' you specify when registering Image Type [%s] must be a string." ), $image_type
				);
			} else {
				$default_size = $args['default_size'];
				if ( ! isset($args['sizes'][$default_size]) )
					sr_die(
						__(
							"The 'default_size' [%s] you specified when registering Image Type [%s] must be a key into the 'sizes' array."
						), $default_size, $image_type
					);
			}
		}

		/*
		 * If we didn't get an 'aspect_ratio' assign it from the last value in the sizes array.
		 */
		if ( empty($args['aspect_ratio']) ) {
			$args['aspect_ratio'] = str_replace( 'x', ':', end( $args['sizes'] ) );
		}

		/*
		 * If we didn't get an 'view_size' make it be the first size specified.
		 */
		if ( empty($args['view_size']) ) {
			$size_keys = array_keys( $args['sizes'] );
			$args['view_size'] = reset( $size_keys );
		}

		/*
		 * Dimension can be '*', 0-100%, 100px or 100
		 */
		$dimension_pattern = '#^(\*|([1-9][0-9]?|100)%|[1-9][0-9]{0,3}(px)?)$#';
		$dimensions_pattern = '#^(\*|([1-9][0-9]?|100)%|[1-9][0-9]{0,3}(px)?)x(\*|([1-9][0-9]?|100)%|[1-9][0-9]{0,3}(px)?)$#';

		reset( $args['sizes'] );
		foreach ( $args['sizes'] as $size_name => $size ) {
			/*
			 * Implement the shorthand width allows us to define $size as a string instead of an array
			 */
			if ( is_string( $size ) )
				$size = array( 'dimensions' => $size );

			/*
			 * Make sure we have an array of sizes here
			 */
			if ( WP_DEBUG && ! is_array( $size ) )
				sr_die( "The value for size [{$size_name}] for Image Type [{$image_type}] must be specified as an array." );

			/*
			 * Check to see if either height and width not set.
			 * If one of them not set and dimensions not set in the right format, error out.
			 * Dimensions are expressed in "{$width}x{height}" format.
			 */
			if ( isset($size['height']) && isset($size['width']) ) {
				if ( ! preg_match( $dimension_pattern, $size['height'] ) || ! preg_match( $dimension_pattern, $size['width'] ) )
					sr_die(
						"The dimensions for Image Type [{$image_type}] were specified as [{$size['height']}x{$size['width']}] are not valid; they should be in the format \"{\$width}x{\$height}\" where either dimension can be an asterisk ['*'], in percentage ['%'] or in pixel."
					);

				$size['dimensions'] = "{$size['width']}x{$size['height']}";

			} else if ( isset($size['dimensions']) && is_string( $size['dimensions'] ) ) {

				if ( ! preg_match( $dimensions_pattern, $size['dimensions'] ) )
					sr_die(
						"The dimensions for Image Type [{$image_type}] were specified as [{$size['dimensions']}] are not valid; they should be in the format \"{\$width}x{\$height}\" where either dimension can be an asterisk ['*'], in percentage ['%'] or in pixel."
					);

				$size['dimensions'] = str_replace( 'px', '', $size['dimensions']
				); // Remove 'px' to keep explode('x', ...) work properly

				list($size['width'], $size['height']) = explode( 'x', $size['dimensions'] );
			}

			if ( is_numeric( $size['width'] ) )
				$size['width'] = intval( $size['width'] );

			if ( is_numeric( $size['height'] ) )
				$size['height'] = intval( $size['height'] );

			/**
			 * TODO (mikes 2011-12-16): We should sr_dashize( $size['image_size_type'] ) and fix all breakage.
			 */
			$size['image_size_type'] = $size_type = "{$size_name}-{$args['image_type']}";
			$size['image_type_size'] = $type_size = sr_dashize( "{$args['image_type']}/{$size_name}" );
			$size['image_type'] = $image_type;
			$size['image_size'] = $size_name;

			$args['sizes'][$size_name] = $size;
			$sr_image_size_types[$size_type] = &$args['sizes'][$size_name];
			$sr_image_type_sizes[$type_size] = &$args['sizes'][$size_name];

			//			add_image_size( "{$size_name}-{$args['image_type']}", $size['width'], $size['height'], isset( $size['crop'] ) );
		}

		$sr_image_types[$image_type] = $args;

		return $args;
	}
}

if ( ! function_exists( 'sr_update_attached_image_type' ) ) {
    function sr_update_attached_image_type( $attachment_id, $image_type ) {
        update_post_meta( $attachment_id, '_sr_attachment_image_type', $image_type );
    }
}

if ( ! function_exists( 'sr_get_attached_image_type' ) ) {
    function sr_get_attached_image_type( $attachment_id ) {
        return get_post_meta( $attachment_id, '_sr_attachment_image_type', true );
    }
}

if( ! function_exists( 'sr_is_valid_image_type' ) ) {
    function sr_is_valid_image_type( $image_type ) {
        global $sr_image_types;
        return isset($sr_image_types[$image_type]) || 'original' == $image_type;
    }
}

if( ! function_exists( 'sr_is_valid_image_size_type' ) ) {
    function sr_is_valid_image_size_type( $size_type ) {
        global $sr_image_size_types;
        return isset($sr_image_size_types[$size_type]);
    }
}

if( ! function_exists( 'sr_is_valid_image_type_size' ) ) {
    function sr_is_valid_image_type_size( $image_type, $image_size ) {
        global $sr_image_types;
        if ( 'default' == $image_size ) {
            $is_valid = true;
        } else {
            $is_valid = false;
            if ( isset($sr_image_types[$image_type]) ) {
                $sizes = $sr_image_types[$image_type]['sizes'];
                $is_valid = isset($sizes[$image_size]);
            }
        }
        return $is_valid;
    }
}

if( ! function_exists( 'sr_get_image_type_sizes' ) ) {
    function sr_get_image_type_sizes( $image_type ) {
        global $sr_image_types;
        return $sr_image_types[$image_type]['sizes'];
    }
}

if( ! function_exists( 'sr_get_image_type_size' ) ) {
    function sr_get_image_type_size( $image_type, $image_size ) {
        global $sr_image_types;
        if ( 'fullsize' == $image_size ) {

            $type_sizes = $sr_image_types[$image_type]['sizes'];
            $type_size = isset($type_sizes['fullsize']) ? $type_sizes['fullsize'] : $type_size = end( $type_sizes );

        } else if ( 'default' == $image_size ) {

            $type_sizes = $sr_image_types[$image_type]['sizes'];
            $type_size = isset($type_sizes['default']) ? $type_sizes['default'] : $type_size = end( $type_sizes );

        } else {

            $type_size = $sr_image_types[$image_type]['sizes'][$image_size];
            if ( WP_DEBUG ) {

                foreach ( array( 'dimensions', 'width', 'height', 'image_size_type', 'image_type', 'image_size' ) as $key )
                {
                    if ( empty($type_size[$key]) )
                        sr_die(
                            __( "The attribute [%s] not set for Image Type/Size [%s][%s]." ), $key, $image_type, $image_size
                        );
                }

                if ( false === strpos( $type_size['dimensions'], 'x' ) )
                    sr_die(
                        __( "The attribute [dimensions] not correct formatted; expected [WxH], got: %s." ), $type_size['dimensions']
                    );

                unset($prefix);

            }

        }
        return $type_size;
    }
}

if( ! function_exists( 'sr_get_image_size_type' ) ) {
    function sr_get_image_size_type( $size_type ) {
        global $sr_image_size_types;
        return sr_is_valid_image_size_type( $size_type ) ? $sr_image_size_types[$size_type] : false;
    }
}

if( ! function_exists( 'sr_get_image_types' ) ) {
    function sr_get_image_types() {
        global $sr_image_types;
        return $sr_image_types;
    }
}

if( ! function_exists( 'sr_get_image_type' ) ) {
    function sr_get_image_type( $image_type ) {
        global $sr_image_types;
        return isset( $sr_image_types[$image_type] ) ? $sr_image_types[$image_type] : false;
    }
}

if( ! function_exists( 'sr_get_image_size_types' ) ) {
    function sr_get_image_size_types() {
        global $sr_image_size_types;
        return $sr_image_size_types;
    }
}

/**
 * Get maximum dimensions of all sizes of given image type
 * @param string $image_type A value previously registered using sr_register_image_type()
 * @return array array($max_width, $max_height)
 */
if( ! function_exists( 'sr_get_max_image_type_dimensions' ) ) {
    function sr_get_max_image_type_dimensions( $image_type ) {
        static $max_dimensions = array();
        global $sr_image_types;
        if ( ! isset($max_dimensions[$image_type]) ) {
            /*
             * Get max width and height for all sizes
             */
            $max_width = $max_height = 0;
        }
        foreach ( $sr_image_types[$image_type]['sizes'] as $size_args ) {
            $width = $size_args['width'];
            $height = $size_args['height'];
            /*
             * Get only when they're in pixels
             */
            if ( ('*' !== $width) && (false === strpos( $width, '%' )) )
                $max_width = intval( $width ) > $max_width ? intval( $width ) : $max_width;

            if ( ('*' !== $height) && (false === strpos( $height, '%' )) )
                $max_height = intval( $height ) > $max_height ? intval( $height ) : $max_height;
        }
        $max_dimensions[$image_type] = array( $max_width, $max_height );
        return $max_dimensions[$image_type];
    }
}

if( ! function_exists( 'sr_generate_image_from' ) ) {
    function sr_generate_image_from( $existing_filepath, $new_filepath, $args = array() ) {
        //echo "Resizing from [{$existing_filepath}] to [{$new_filepath}] ...<br/>";

        $image_resource = sr_create_image_from( $existing_filepath );

        $args['filepath'] = $new_filepath;

        sr_output_image_by( 'resource', $image_resource, $args );

        imagedestroy( $image_resource );
    }
}

/**
 * @param $image_type
 * @param array|string $args output=string, numeric or array.
 * @return bool
 */
if( ! function_exists( 'sr_get_image_type_aspect_ratio' ) ) {
    function sr_get_image_type_aspect_ratio( $image_type, $args = array() ) {
        $args = sr_parse_args( $args, 'output=string' /* 'string', 'array', 'numeric' */ );

        $aspect_ratio = false;
        $image_type = sr_get_image_type( $image_type );
        if ( isset($image_type['aspect_ratio']) ) {
            $aspect_ratio = $image_type['aspect_ratio'];
            if ( 'string' != $args['output'] ) {
                list($ratio_width, $ratio_height) = array_map( 'floatval', explode( ':', "{$aspect_ratio}:" ) );
                $ratio_value = floatval( $ratio_width / $ratio_height );
                if ( 'array' == $args['output'] ) {
                    $aspect_ratio = array(
                        'ratio_string' => $aspect_ratio,
                        'ratio_width' => $ratio_width,
                        'ratio_height' => $ratio_height,
                        'ratio_value' => $ratio_value,
                    );
                } else if ( 'numeric' == $args['output'] ) {
                    $aspect_ratio = $ratio_value;
                }
            }
        }
        return $aspect_ratio;
    }
}