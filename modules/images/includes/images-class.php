<?php

class Sunrise_Images {
	static function on_load() {
		/**
		 * TODO (mikes): Rework priority order before launching beta.
		 */
		sr_add_action( __CLASS__, 'sunrise_init', 30 );
	}
	static function sunrise_init() {
		do_action( 'sr_images_init' );
	}
}
Sunrise_Images::on_load();
