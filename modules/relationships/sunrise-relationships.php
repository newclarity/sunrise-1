<?php
/*
Plugin Name: Sunrise Relationships
Plugin URL: http://docs.getsunrise.org/relationships/
File Name: sunrise-relationships.php
Author: The Sunrise Team
Author URL: http://getsunrise.org
Version: 0.0.0
Description: Adds Object Relationships to WordPress
*/

define( 'SUNRISE_RELATIONSHIPS_DIR', dirname( __FILE__ ) );
require( SUNRISE_RELATIONSHIPS_DIR . '/includes/relationship-functions.php');
require( SUNRISE_RELATIONSHIPS_DIR . '/includes/relationships-class.php');

//sr_load_plugin_files( 'relationships', array( ) );
//sr_load_demos( SUNRISE_RELATIONSHIPS_DIR );
