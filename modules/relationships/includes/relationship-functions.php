<?php

if ( ! function_exists( 'register_relationship' ) ) {
	function register_relationship( $relationship_name, $args = array() ) {
		return sr_register_relationship( $relationship_name, $args );
	}
}
function sr_register_relationship( $relationship_name, $args = array() ){
	return Sunrise_Relationships::register( $relationship_name, $args );
}
if ( ! function_exists( 'create_relationship' ) ) {
	function create_relationship( $relationship_name, $object_id, $related_id, $args = array() ) {
		return sr_create_relationship( $relationship_name, $object_id, $related_id, $args );
	}
}
function sr_create_relationship( $relationship_name, $object_id, $related_id, $args = array() ) {
	$args = wp_parse_args( $args, array(
		'object_id'           => $object_id,
		'related_id'          => $related_id,
		'relationship_order'  => 0,
	));
	return Sunrise_Relationships::create( $relationship_name, $args );
}
if ( ! function_exists( 'delete_relationship' ) ) {
	function delete_relationship( $relationship_name, $object_id, $related_id, $args = array() ) {
		return sr_delete_relationship( $relationship_name, $object_id, $related_id, $args );
	}
}
function sr_delete_relationship( $relationship_name, $object_id, $related_id, $args = array() ) {
	$args = wp_parse_args( $args, array(
		'object_id'  => $object_id,
		'related_id' => $related_id
	));
	return Sunrise_Relationships::delete( $relationship_name, $args );
}
if ( ! function_exists( 'delete_relationships' ) ) {
	function delete_relationships( $relationship_name, $object_id, $args = array() ) {
		return sr_delete_relationships( $relationship_name, $object_id, $args );
	}
}
function sr_delete_relationships( $relationship_name, $object_id, $args = array() ){
	$args = wp_parse_args( $args, array(
		'object_id'  => $object_id,
	));
	return Sunrise_Relationships::delete( $relationship_name, $args );
}
if ( ! function_exists( 'delete_relationships_except' ) ) {
	function delete_relationships_except( $relationship_name, $object_id, $args = array() ) {
		return sr_delete_relationships_except( $relationship_name, $object_id, $args );
	}
}
function sr_delete_relationships_except( $relationship_name, $object_id, $except_related, $args = array()  ){
	$args = wp_parse_args( $args, array(
		'object_id'       => $object_id,
		'except_related'  => $except_related,
	));
	return Sunrise_Relationships::delete( $relationship_name, $args );
}
if ( ! function_exists( 'update_relationships' ) ) {
	function update_relationships( $relationship_name, $object_id, $related_objects, $args = array() ) {
		return sr_update_relationships( $relationship_name, $object_id, $related_objects, $args );
	}
}
function sr_update_relationships( $relationship_name, $object_id, $related_objects, $args = array() ){
	return Sunrise_Relationships::update( $relationship_name, $object_id, $related_objects, $args );
}
if ( ! function_exists( 'relationship_exists' ) ) {
	function relationship_exists( $relationship_name, $object_id, $related_id, $args = array() ) {
		return sr_relationship_exists( $relationship_name, $object_id, $related_id, $args );
	}
}
function sr_relationship_exists( $relationship_name, $object_id, $related_id, $args = array() ) {
	$args = wp_parse_args( $args, array(
		'object_id'   => $object_id,
		'related_id'  => $related_id,
	));
	return Sunrise_Relationships::exists( $relationship_name, $args );
}
if ( ! function_exists( 'get_related_objects' ) ) {
	function get_related_objects( $relationship_name, $object_id, $args = array() ) {
		return sr_get_related_objects( $relationship_name, $object_id, $args );
	}
}
function sr_get_related_objects( $relationship_name, $object_id, $args = array() ){
	return Sunrise_Relationships::get_related( $relationship_name, $object_id, $args );
}
if ( ! function_exists( 'has_relationship' ) ) {
	function has_relationship( $relationship_name, $args = array() ) {
		return sr_has_relationship( $relationship_name, $args );
	}
}
function sr_has_relationship( $relationship_name, $args = array() ){
	return Sunrise_Relationships::has_relationship( $relationship_name, $args );
}
if ( ! function_exists( 'is_reciprocal' ) ) {
	function is_reciprocal( $relationship_name, $args = array() ) {
		return sr_is_reciprocal( $relationship_name, $args );
	}
}
function sr_is_reciprocal( $relationship_name, $args = array() ){
	return Sunrise_Relationships::is_reciprocal( $relationship_name, $args );
}
if ( ! function_exists( 'get_relationship' ) ) {
	function get_relationship( $relationship_name, $args = array() ) {
		return sr_get_relationship( $relationship_name, $args );
	}
}
function sr_get_relationship( $relationship_name, $args = array() ){
	return Sunrise_Relationships::get_relationship( $relationship_name, $args );
}

function sr_get_connecting_relationship( $post_type_a, $post_type_b ) {
	return Sunrise_Relationships::get_connecting_relationship( $post_type_a, $post_type_b );
}

function sr_get_ids_having_connected( $post_type, $relationship_name ) {

	return Sunrise_Relationships::get_ids_having_connected( $post_type, $relationship_name );
}

/**
 * @param string $post_type
 *
 * @return array
 */
function sr_get_relationships_for_post_type( $post_type ) {
  global $sr_relationships;
  $relationships = array();
  foreach( $sr_relationships as $relationship_name => $relationship ) {
    if ( 'post' !== $relationship->object_type )
      continue;
    $relationship->relationship_name = $relationship_name;
    if ( $post_type == $relationship->object_sub_type ) {
      $relationships[$relationship->related_sub_type] = $relationship;
    } else if ( $relationship->is_reciprocal && $post_type == $relationship->related_sub_type )  {
      $relationships[$relationship->object_sub_type] = $relationship;
    }
  }
  return count( $relationships ) ? $relationships : false;
}
