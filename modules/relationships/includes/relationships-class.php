<?php

if ( ! class_exists( 'Sunrise_Relationships' ) ) {

	/**
	 * Class Sunrise_Relationships
	 */
	class Sunrise_Relationships extends Sunrise_Static_Base {

		static function on_load () {

			register_activation_hook( __FILE__, array( __CLASS__, 'activate' ) );

			/**
			 * Do before Sunrise Fields
			 */
			if ( is_admin() || defined( 'DOING_AJAX' ) ) {
				sr_add_action( __CLASS__, array( 'admin_init', 'relationships_init' ), 8 );
			} else {
				sr_add_action( __CLASS__, array( 'wp_loaded', 'relationships_init' ), 8 );
			}
		}

		static function relationships_init () {
			do_action( 'sr_relationships_init' );
		}

		static function activate () {
			global $wpdb;
			$db_charset = DB_CHARSET;
			$db_collate = DB_COLLATE ? ' COLLATE ' . DB_COLLATE : '';
			$sql = <<<SQL
CREATE TABLE IF NOT EXISTS {$wpdb->prefix}relationships (
	relationship_id BIGINT(20) NOT NULL AUTO_INCREMENT,
	object_id BIGINT(20),
	related_id BIGINT(20),
	term_taxonomy_id BIGINT(20),
	related_order BIGINT(20),
	relationship_name VARCHAR(32),
	flag VARCHAR(55),
	PRIMARY KEY (relationship_id)
) DEFAULT CHARACTER SET {$db_charset}{$db_collate};
SQL;
			require_once( sr_admin_path( 'includes/upgrade.php' ) );
			dbDelta( $sql );
		}

		/**
		 * Register a new relationship
		 *
		 * @param string $relationship_name
		 * @param array  $args
		 */
		static function register ( $relationship_name, $args = array() ) {
			global $sr_relationships;
			$args = wp_parse_args(
				$args,
				array(
					'object_type'   => 'post',
					'related_type'  => 'post',
					'is_reciprocal' => true,
				)
			);
			$args = self::__fixup_args( $args );
			$args = wp_parse_args(
				$args,
				array(
					'object_sub_type'  => 'post',
					'related_sub_type' => 'post',
				)
			);
			$sr_relationships[$relationship_name] = (object) $args;
		}

		/**
		 * Delete all related objects for a specific relationship name / object id combination.
		 *
		 * @param string $relationship_name
		 * @param int    $object_id
		 * @param array  $args
		 * @return int
		 */
		static function delete ( $relationship_name, $object_id, $args = array() ) {
			global $wpdb;
			$args = wp_parse_args(
				$args,
				array(
					'object_id' => $object_id,
				)
			);
			/**
			 * Determine what is going to be deleted and store so we can run action hook after deletion
			 */
			$select_sql = 'SELECT * ' . self::__build_sql_from_where( $relationship_name, $args );
			$to_delete = $wpdb->get_results( $select_sql );
			/**
			 * Actually delete relationships
			 */
			$delete_sql = 'DELETE ' . self::__build_sql_from_where( $relationship_name, $args );
			$affected = $wpdb->query( $delete_sql );
			/**
			 * Run action hook for each deleted relationship after the fact
			 */
			if ( $to_delete && is_array( $to_delete ) ) {
				foreach ( $to_delete as $deleted ) {
					do_action( 'sr_delete_relationship', $relationship_name, $object_id, $deleted->related_id );
				}
			}
			return $affected;
		}

		/**
		 * Create a relationship between two objects
		 *
		 * @param string $relationship_name
		 * @param array  $args
		 * @return int
		 */
		static function create ( $relationship_name, $args = array() ) {
			global $wpdb;
			$args = wp_parse_args(
				$args,
				array(
					'object_id'     => false,
					'related_id'    => false,
					'related_order' => 0,
				)
			);
			if ( WP_DEBUG ) {
				if ( ! $args['object_id'] ) {
					sr_die( 'object_id must be passed as in $args to Sunrise_Relationship::create($name,$args)' );
				}
				if ( ! $args['related_id'] ) {
					sr_die( 'related_id must be passed as in $args to Sunrise_Relationship::create($name,$args)' );
				}
			}
			$created = false;
			if ( ! self::exists( $relationship_name, $args ) ) {
				$sql = "INSERT INTO {$wpdb->prefix}relationships (relationship_name,object_id,related_id,related_order) VALUES ('%s',%d,%d,%d)";
				$sql = $wpdb->prepare(
					$sql,
					$relationship_name,
					$args['object_id'],
					$args['related_id'],
					$args['related_order']
				);
				$created = $wpdb->query( $sql );
				do_action( 'sr_create_relationship', $relationship_name, $args['object_id'], $args['related_id'] );
			}
			return $created;
		}

		/**
		 * Update an existing relationship
		 *
		 * @var wpdb     $wpdb
		 * @param string $relationship_name
		 * @param int    $object_id
		 * @param array  $related_objects
		 * @param array  $args
		 */
		static function update ( $relationship_name, $object_id, $related_objects, $args = array() ) {

			global $wpdb;

			//Set defaults
			$flag = isset( $args['flag'] ) ? $args['flag'] : '';
			$post_status = isset( $args['post_status'] ) ? $args['post_status'] : 'publish';

			// Eliminate duplicates
			$related_objects = array_unique( array_values( $related_objects ) );

			//Get rid of any related posts that we are not using now
			self::delete(
				$relationship_name,
				$object_id,
				array(
					'except_related' => $related_objects,
					'flag'           => $flag,
				)
			);

			// Delete database duplicates
			self::delete_database_duplicates( $object_id );
			/*
			 * Get currently related objects so we can update them.
			 */
			$currently_related_objects = self::get_related(
				$relationship_name,
				$object_id,
				array(
					'flag'        => $flag,
					'post_status' => $post_status,
				)
			);
			/*
			 * Make object_ids the keys and make the order be the values.
			 */
			$related_objects = array_flip( array_filter( $related_objects ) );
			/*
			 * Get prior objects so we can update them.
			 */
			if ( count( $currently_related_objects ) ) {
				/**
				 * Reference for SQL query:
				 * http://www.karlrixon.co.uk/articles/sql/update-multiple-rows-with-different-values-and-a-single-sql-query/
				 */
				$sql = array( "UPDATE {$wpdb->prefix}relationships SET related_order = CASE related_id" );
				foreach ( $currently_related_objects as $related_object ) {
					if ( isset( $related_objects[$related_object->ID] ) ) {
						$sql[] = $wpdb->prepare(
							" WHEN %d THEN %d",
							$related_object->ID,
							$related_objects[$related_object->ID] + 1
						);
						unset( $related_objects[$related_object->ID] ); // Remove them so we don't add them below.
					}
				}
				/*
				 * TODO: Change this so we can support multiple related records for the same related object for the same named relationship.
				 * TODO: Will likely require the passed in related objects to contain their relationship_id.
				 */
				$sql[] = ' ELSE related_order END' . self::__build_sql_where(
						$relationship_name,
						array( 'object_id' => $object_id, 'flag' => $flag )
					);
				$sql = implode( '', $sql );
				$wpdb->query( $sql );
			}

			// Now add any newly related records using a long INSERT.
			if ( count( $related_objects ) ) {
				$records = array();
				foreach ( $related_objects as $related_id => $related_order ) {
					$records[] = $wpdb->prepare(
						"(%d,%d,%d,'%s', %s)",
						$object_id,
						$related_id,
						$related_order + 1,
						$relationship_name,
						$flag
					);
					do_action( 'sr_create_relationship', $relationship_name, $object_id, $related_id );
				}
				$records = implode( ',', $records );
				$sql = "INSERT {$wpdb->prefix}relationships (object_id,related_id,related_order,relationship_name, flag) VALUES {$records};";
				$wpdb->query( $sql );
			}
		}

		/**
		 * Delete duplicate relationships in the database by object_id
		 *
		 * @var wpdb  $wpdb
		 * @param int $object_id
		 */
		static function delete_database_duplicates ( $object_id ) {

			global $wpdb;
			$sql = <<<SQL
SELECT a.relationship_id
FROM {$wpdb->prefix}relationships a
INNER JOIN {$wpdb->prefix}relationships b ON a.object_id = b.object_id
    AND a.related_id = b.related_id
    AND a.relationship_name = b.relationship_name
WHERE a.object_id = %d AND a.relationship_id <> b.relationship_id
SQL;
			$duplicates = $wpdb->get_results( $wpdb->prepare( $sql, absint( $object_id ) ), OBJECT_K );
			if ( $duplicates ) {
				$duplicates_list = join( ', ', array_keys( $duplicates ) );
				$wpdb->query( "DELETE FROM {$wpdb->prefix}relationships WHERE relationship_id IN ( {$duplicates_list} )" );
			}
		}

		/**
		 * Check if a relationship exists
		 *
		 * @param string $relationship_name
		 * @param array  $args
		 * @return bool|string
		 */
		static function exists ( $relationship_name, $args = array() ) {
			global $wpdb;
			$exists = false;
			if ( self::has_relationship( $relationship_name ) ) {
				if ( ! self::is_reciprocal( $relationship_name ) ) {
					$field = 'related_id';
				} else {
					// Swap object_id and related_id, see; http://tech.petegraham.co.uk/2007/03/29/php-swap-variables-one-liner/
					list( $args['related_id'], $args['object_id'] ) = array( $args['object_id'], $args['related_id'] );
					$field = 'object_id';
				}
				$sql = "SELECT {$field}" . self::__build_sql_from_where( $relationship_name, $args );
				$exists = $wpdb->get_var( $sql );
			}
			return $exists;
		}

		/**
		 * Get related objects
		 *
		 * @param string $relationship_name
		 * @param int    $object_id
		 * @param array  $args
		 * @return array
		 */
		static function get_related ( $relationship_name, $object_id, $args = array() ) {
			global $wpdb, $sr_relationships;
			$args['post_status'] = isset( $args['post_status'] ) ? (array) $args['post_status'] : array( 'publish' );
			// Setup array to be returned
			$related = array();
			// Check if relationship has been defined
			if ( self::has_relationship( $relationship_name ) ) {
				// Fetch relationship
				$relationship = $sr_relationships[$relationship_name];
				// Get type of object_id // TODO: This will eventually detect object type and sub type
				$post_type = get_post_type( $object_id );
				// Fetch any flags
				$args['flag'] = isset( $args['flag'] ) ? $args['flag'] : false;
				// Are we after reciprocal results?
				if ( self::is_reciprocal( $relationship_name ) && $post_type == $relationship->related_sub_type ) {
					$args['reciprocal'] = true;
					$args['related_id'] = $object_id;
					//unset( $args['object_id'] );
					$sql = "SELECT DISTINCT object_id AS ID, relationship_id, object_id, related_id, related_order, flag, 1 AS reciprocal";
					$sql .= self::__build_sql_from_where( $relationship_name, $args );
					$posts = $wpdb->get_results( $sql );
					$order_by = isset( $args['order_by'] ) ? $args['order_by'] : 'ID';
				} else {
					$args['object_id'] = $object_id;
					$sql = "SELECT DISTINCT related_id AS ID, relationship_id, object_id, related_id, related_order, flag, 0 AS reciprocal";
					$sql .= self::__build_sql_from_where( $relationship_name, $args );
					$posts = $wpdb->get_results( $sql );
					// NOTE: SQL sorts by related order, so unless user wants something else, that is what they will get
					$order_by = isset( $args['orderby'] ) ? $args['orderby'] : false;
					$order_by = ( ! $order_by && isset( $args['order_by'] ) ) ? $args['order_by'] : false;
				}
				// Fetch post object
				$meta = isset( $args['meta'] ) ? (array) $args['meta'] : array();
				$post__not_in = ( isset( $args['post__not_in'] ) && is_array(
						$args['post__not_in']
					) ) ? $args['post__not_in'] : array();
				if ( $posts ) {
					foreach ( $posts as $post ) {
						// Get post as an array
						$current_post = (array) get_post( $post->ID );
						// Check for post status
						if ( ! isset( $current_post['post_status'] ) || ! in_array(
								@$current_post['post_status'],
								$args['post_status']
							)
						) {
							continue;
						}
						// Exclude posts
						if ( in_array( $post->ID, $post__not_in ) ) {
							continue;
						}
						// Add any post meta (allows ordering by post meta)
						$meta_pairs = array();
						if ( $meta ) {
							foreach ( $meta as $key ) {
								$meta_pairs[$key] = get_post_meta( $post->ID, $key, true );
							}
							// Return post as object, making sure not to overwrite standard post data
							$related[] = (object) wp_parse_args( $current_post, $meta_pairs );
						} else {
							$related[] = (object) $current_post;
						}
					}
				}
				// Check if we need to reorder the results
				if ( $related && $order_by ) {
					// Custom sort
					$order = ( isset( $args['order'] ) && $args['order'] == 'DESC' ) ? 'DESC' : 'ASC';
					// Create array for sorting
					$sorter = array();
					// Create array for final results
					$return = array();
					// Make sure pointer is in the right place
					reset( $related );
					// Put only relevant data into the sorting array
					foreach ( $related as $k => $v ) {
						$sorter[$k] = $v->$order_by;
					}
					// Sort data using appropriate method
					natcasesort( $sorter );
					if ( $order == 'DESC' ) {
						$sorter = array_reverse( $sorter, true );
					}
					// Place data in the return array in the correct order
					foreach ( $sorter as $k => $v ) {
						$return[$k] = $related[$k];
					}
					// Return properly sorted data
					$related = $return;
				}
			} else { // Relationship not defined
				if ( WP_DEBUG ) {
					trigger_error( 'Relationship "' . $relationship_name . '" has not been registered', E_USER_WARNING );
				}
			}
			return $related;
		}

		/**
		 * Check if a relationship exists
		 *
		 * @param string $relationship_name
		 * @return bool
		 */
		static function has_relationship ( $relationship_name ) {
			global $sr_relationships;
			return isset( $sr_relationships[$relationship_name] );
		}

		/**
		 * Check if a relationship is reciprocal
		 *
		 * @param string $relationship_name
		 * @return bool
		 */
		static function is_reciprocal ( $relationship_name ) {
			if ( ! self::has_relationship( $relationship_name ) ) {
				return false;
			}
			global $sr_relationships;
			return $sr_relationships[$relationship_name]->is_reciprocal;
		}

		/**
		 * Get the details on a relationship.  Returns false if relationship doesn't exist.
		 *
		 * @param string $relationship_name
		 * @return bool|array
		 */
		static function get_relationship ( $relationship_name ) {
			if ( ! self::has_relationship( $relationship_name ) ) {
				return false;
			}
			global $sr_relationships;
			return $sr_relationships[$relationship_name];
		}

		/**
		 * Get relationship, connecting given post types.
		 *
		 * @param string $post_type_a
		 * @param string $post_type_b
		 * @return bool|string
		 */
		static function get_connecting_relationship ( $post_type_a, $post_type_b ) {

			global $sr_relationships;

			foreach ( $sr_relationships as $name => $data ) {

				if ( $data->object_sub_type == $post_type_a && $data->related_sub_type == $post_type_b ) {
					return $name;
				}

				if ( $data->object_sub_type == $post_type_b && $data->related_sub_type == $post_type_a ) {
					return $name;
				}
			}

			return false;
		}

		/**
		 * Get IDs that have connection of specific relationship.
		 *
		 * @param string $post_type
		 * @param string $relationship_name
		 * @return array
		 */
		static function get_ids_having_connected ( $post_type, $relationship_name ) {

			global $wpdb, $sr_relationships;

			if ( empty( $sr_relationships[$relationship_name] ) ) {
				return array();
			}

			$relationship = $sr_relationships[$relationship_name];

			if ( $relationship->object_sub_type == $post_type ) {
				$column = 'object_id';
			} elseif ( $relationship->related_sub_type == $post_type ) {
				$column = 'related_id';
			} else {
				return array();
			}

			return $wpdb->get_col(
				$wpdb->prepare(
					"SELECT {$column} FROM {$wpdb->prefix}relationships WHERE relationship_name = %s;",
					$relationship_name
				)
			);
		}

		/**
		 * Allows a user to use shorthand.  Allow things like 'post_type'=>'product' instead of 'object_sub_type'=>'product'
		 * and 'related_post_type'=>'order_item' instead of 'related_sub_type'=>'order_item'
		 *
		 * @param array $args
		 * @return mixed
		 */
		private static function __fixup_args ( $args ) {
			foreach ( array( 'object', 'related' ) as $prefix ) {
				if ( ! isset( $args["{$prefix}_sub_type"] ) ) {
					$sub_type_name = ( 'related' == $prefix ? "{$prefix}_" : '' ) . "{$args["{$prefix}_type"]}_type";
					if ( isset( $args[$sub_type_name] ) ) {
						$args["{$prefix}_sub_type"] = $args[$sub_type_name];
						unset( $args[$sub_type_name] );
					}
				}
			}
			return $args;
		}

		/**
		 * @param stirng $relationship_name
		 * @param array  $args
		 * @return string
		 */
		private static function __build_sql_from_where ( $relationship_name, $args ) {
			global $wpdb;
			$join = isset( $args['where'] ) ? " JOIN {$wpdb->postmeta} ON object_id = post_id" : '';
			return " FROM {$wpdb->prefix}relationships{$join}" . self::__build_sql_where( $relationship_name, $args );
		}

		/**
		 * @param string $relationship_name
		 * @param array  $args
		 * @return string
		 */
		private static function __build_sql_where ( $relationship_name, $args ) {
			global $wpdb;
			$args = wp_parse_args(
				$args,
				array(
					'object_id'      => false,
					'related_id'     => false,
					'related_order'  => 0,
					'related'        => array(),
					'except_related' => array(),
					'reciprocal'     => false,
					'flag'           => false,
					'where'          => false,
				)
			);
			if ( WP_DEBUG ) {
				// TODO: Get 'whatever' from call stack.
				if ( ! $args['reciprocal'] && ! $args['object_id'] ) {
					sr_die( 'object_id must be passed as in $args to Sunrise_Relationship::whatever($name,$args)' );
				}
				if ( $args['reciprocal'] && ! $args['related_id'] ) {
					sr_die(
						'related_id must be passed as in $args (when reciprocal=>true) to Sunrise_Relationship::whatever($name,$args)'
					);
				}
			}
			$and_object_id = $args['object_id'] ? $wpdb->prepare( ' AND object_id = %d', $args['object_id'] ) : '';
			$and_related_id = $args['related_id'] ? $wpdb->prepare( ' AND related_id = %d', $args['related_id'] ) : '';
			$and_except_related = '';
			if ( is_array( $args['except_related'] ) && count( $args['except_related'] ) ) {
				$exceptions = implode( ',', array_map( 'absint', $args['except_related'] ) );
				$and_except_related = " AND related_id NOT IN ({$exceptions})";
			}
			$and_flag = $args['flag'] ? $wpdb->prepare( ' AND flag = %s', $args['flag'] ) : '';
			$order = ' ORDER BY related_order ASC';
			$where = isset( $args['where'] ) ? $args['where'] : false;
			$sql = " WHERE relationship_name='%s' {$and_object_id}{$and_related_id}{$and_flag}{$and_except_related}{$where}{$order}";
			$sql = $wpdb->prepare( $sql, $relationship_name );
			return $sql;
		}
	}

	Sunrise_Relationships::on_load();

}

