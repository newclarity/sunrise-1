<?php
if (!class_exists('Sunrise_Toggle_Post_Status_Control')) {
	/**
	 * @property Sunrise_Control $status_control
	 * @property Sunrise_Control $controls
	 */
	class Sunrise_Toggle_Post_Status_Control extends Sunrise_Control {
		var $control_set_name = false;
		var $post_status = false;           // To be set prior to calling get_status_control()
		var $toggle_index = false;   // To be set prior to calling get_status_control()

		protected $_status_control = false;
		protected $_controls = false;
		/**
		 * @return Sunrise_Control
		 */
		function get_status_control() {
			if ( WP_DEBUG ) {
				if ( ! $this->post_status ) {
					sr_trigger_error( __( '%s must be given a valid post_status.', 'sunrise-controls' ), $this->description );
				}
				if ( WP_DEBUG && ! is_array( $this->toggle_index ) )
					sr_trigger_error( __( '%s must be given a \'toggle_index\' array.', 'sunrise-controls' ), $this->description );
				if ( WP_DEBUG && count( $this->toggle_index ) != count( $this->_controls ) )
					sr_trigger_error( __( 'The \'toggle_index\' array for %s must match in number the number of elements in the \'controls\' array.', 'sunrise-controls' ), $this->name );
			}

			if ( ! $this->_status_control ) {
				$control_index = $this->toggle_index[$this->post_status];
				$this->_status_control= $this->controls[$control_index];
			}
			return $this->_status_control;
		}
		function filter_args( $args ) {
			return Sunrise::renormalize_args( $args, array(
				'control_set' => 'control_set_name',
				'statuses' => 'valid_post_statuses',
			));
		}
		function get_description() {
			return parent::get_description( "control_name={$this->control_name}" );
		}
		function get_controls() {
			if ( WP_DEBUG && ! sr_has_control_set( $this->control_set_name )) {
				$message = __( '%s must be given a valid \'control_set\'. Valid control sets include: %s.', 'sunrise-controls' );
				sr_trigger_error( $message, $this->description, implode( ', ', array_keys( sr_get_control_sets() ) ) );
			}
			if ( WP_DEBUG && ! $this->_controls || ! is_array( $this->_controls ) || 0 == count( $this->_controls ) ) {
				$message = __( '%s must be given a non-empty array of control names from the control set of [%s]. Valid control names include: %s', 'sunrise-controls' );
				sr_trigger_error( $message, $this->description, $this->control_set_name, implode( ', ', array_keys( sr_get_control_set( $this->control_set_name ) ) ) );
			}
			$this->_controls = Sunrise_Controls::expand_controls( $this, $this->control_set_name, $this->_controls	);
			return $this->_controls;
		}
		function get_text() {
			$control = $this->get_status_control();
			return $control->text;
		}
		function get_control_link() {
			$control = $this->get_status_control();
			return $control->control_link;
		}
		function __set( $property_name, $value ) {
			switch ( $property_name ) {
				case '_temp_':
					call_user_func( array( &$this, "set_{$property_name}" ), $value );
					break;

				case 'controls':
				case 'status_control':
					$this->_cannot_set_error( $property_name, $value );
					break;

				default:
					parent::__set( $property_name, $value );
					break;
			}
		}
		function __get( $property_name ) {
			$value = false;
			switch ( $property_name ) {

				case 'controls':
				case 'status_control':
					$value = call_user_func( array( &$this, "get_{$property_name}" ) );
					break;

				default:
					$value = parent::__get( $property_name, $value );
					break;
			}
			return $value;
		}

	}
}

