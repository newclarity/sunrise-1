<?php
/*
Plugin Name: Sunrise Controls
Plugin URL: http://docs.getsunrise.org/controls/
File Name: sunrise-controls.php
Author: The Sunrise Team
Author URL: http://getsunrise.org
Version: 0.0.0
Description: Adds Control (Links, Buttons) Support for Sunrise/WordPress
*/

define( 'SUNRISE_CONTROLS_DIR', dirname( __FILE__ ) );
require( SUNRISE_CONTROLS_DIR . '/includes/link-functions.php');
require( SUNRISE_CONTROLS_DIR . '/includes/control-functions.php');
require( SUNRISE_CONTROLS_DIR . '/includes/control-class.php');
require( SUNRISE_CONTROLS_DIR . '/includes/controls-class.php');

sr_load_plugin_files( 'controls', array( 'control' ) );

//sr_load_demos( SUNRISE_CONTROLS_DIR );
