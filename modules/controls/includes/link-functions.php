<?php
global $sr_link_property_stack;
$sr_link_property_stack = array( 0 => array(
	'site_url' 		=> site_url(),
	'admin_path' 	=> 'wp-admin',
));

/**
 * TODO (mikes): Need to add more link properties as needs for them are discovered
 */
global $sr_link_templates;
$sr_link_templates = array(
	'admin_edit_post'     => '%site_url%/%admin_path%/post.php?post=%post_id%&amp;action=edit',
	'view_post'           => 'permalink:%post_id%',
	'admin_trash_post'    => 'trash:%post_id%',
	'admin_filter_posts'  => '%site_url%/%admin_path%/edit.php?post_type=%child_type%&amp;%microsite_post_type%=%post_parent%',
);
function sr_push_link_properties( $link_properties ) {
	global $sr_link_property_stack;
	array_push( $sr_link_property_stack, array_merge( sr_top_link_properties(), $link_properties ) );
}
function sr_top_link_properties() {
	global $sr_link_property_stack;
	$link_property_index = end( array_keys( $sr_link_property_stack ) );
	return $sr_link_property_stack[$link_property_index];
}
function sr_pop_link_properties() {
	global $sr_link_property_stack;
	return array_pop( $sr_link_property_stack );
}
function sr_add_link_template( $template_name, $link_template ) {
	global $sr_link_templates;
	$sr_link_templates[$template_name] = $link_template;
}
function sr_remove_link_template( $template_name ) {
	global $sr_link_templates;
	unset( $sr_link_templates[$template_name] );
}
function sr_has_link_template( $template_name ) {
	global $sr_link_templates;
	return isset( $sr_link_templates[$template_name] );
}
function sr_get_link_templates() {
	global $sr_link_templates;
	return $sr_link_templates;
}
function sr_get_link_template( $template_name ) {
	$link_templates = sr_get_link_templates();
	return isset( $link_templates[$template_name] ) ? $link_templates[$template_name] : $template_name;
}
/**
 * Returns a link based on a link template, which can either be looked up
 * in $sr_link_templates or just passed through.
 *
 * @param string $link_template
 * @param bool|array $link_properties
 * @return string
 */
function sr_get_link( $link_template, $link_properties = false ) {
	if ( $link_properties )
		sr_push_link_properties( $link_properties );
	$link = sr_get_link_template( $link_template );
	if ( $link ) {
		$top_link_properties = sr_top_link_properties();
		if ( preg_match( '#(permalink|trash):(.*)$#', $link, $match ) ) {
			//$post_id = intval( $top_link_properties[trim($match[2],'%')] );  //Generic, but do we need it?
			$post = get_post( $top_link_properties['post_id'] );
			switch ( $match[1] ) {
				case 'permalink':
					$link = is_object( $post ) ? get_post_permalink( $post ) : 'permalink';
					break;
				case 'trash':
					$link = is_object( $post ) ? get_delete_post_link( $post ) : 'trash';
					break;
			}
		} else {
			/**
			 * TODO: Find a word for $placeholder more consistent with WordPress' permalink terminology
			 */
			foreach( sr_top_link_properties() as $placeholder => $value ) {
				$link = str_replace( "%{$placeholder}%", $value, $link );
			}
		}
		$link_template = $link;

		if ( $link_properties )
			sr_pop_link_properties();
	}
	return $link_template;
}
