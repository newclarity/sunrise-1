<?php

if (!class_exists('Sunrise_Controls')) {
	/**
	 *
	 */
	class Sunrise_Controls extends Sunrise_Static_Base {
		static function on_load() {
			add_action( 'sunrise_init', array( __CLASS__, 'sunrise_init' ), 10 );	// Priority order is important
		}
		static function sunrise_init() {
			do_action( 'sr_controls_init' );
		}
		/**
		 * @param string $container_id
		 * @param array|Sunrise_Control $controls
		 * @param string|array $args
		 * @return string
		 */
		 static function as_html( $container_id, $controls, $args = array() ) {
			$args = sr_parse_args( $args, array(
				'container' => 'span',
				'scope'     => 'item',
				'class'     => false,
				'clear'     => true,
			));
			$class = $args['class'] ? "{$args['class']} " : '';

			$controls_html = array( "<{$args['container']} id=\"{$container_id}-controls\" class=\"{$class}{$args['scope']}-controls controls\">" );
			if ( is_array( $controls ) ) {
				foreach( $controls as $control ) {
					$controls_html[] = $control->html;
				}
		 } else if ( is_a( $controls, 'Sunrise_Control' ) ) {
				$controls_html[] = $controls->html;
			}

			$controls_html[] = "</{$args['container']}>";
			if ( $args['clear'] )
				$controls_html[] = '<div class="clear"></div>';
			return implode( '', $controls_html );
		}
		static function get_instance_of( $control_type, $args = array() ) {
			return Sunrise::get_instance_for( 'controls/control', $control_type, $args );
		}
		static function expand_controls( &$object, $control_set_name, $controls ) {
			if ( is_array( $controls ) && count( $controls ) && ! is_a( reset( $controls ), 'Sunrise_Control' ) ) {
				$new_controls = array();
				foreach( $controls as $control_name => $control_args ) {
					if ( is_object( $control_args ) ) {
						$new_controls[$control_name] = $control_args;
					} else {
						if ( is_numeric( $control_name ) && is_string( $control_args ) ) {
							$control_name = $control_args;
							$control_args = array();
						}
						$control_args['parent'] = &$object;
						$new_controls[$control_name] = sr_get_control_set_control( $control_set_name, $control_name, $control_args );
					}
				}
				$controls = $new_controls;
			}
			return $controls;
		}
		static function normalize_args( $args ) {
			return Sunrise::normalize_args( $args, array(
				'type'  => 'control_type',
				'shape' => 'control_shape',
				'scope' => 'control_scope',
				'link'  => 'control_link',
			));
		}

//		/**
//		 * @static
//		 * @param $controls
//		 * @return array
//		 *
//		 * TODO: Isn't this similar to something in Sunrise_Base or Sunrise_Static_Base?
//		 */
//		static function initialize( $controls ) {
//			$new_controls = array();
//			foreach( $controls as $name => $control ) {
//				if ( is_object( $control ) ) {
//					$new_controls[$name] = $control;
//				} else if ( sr_has_control( $name ) ) {
//					$new_controls[$name] = sr_clone_control( $name );
//				} else if ( is_array( $control ) ) {
//					$new_controls[$name] = sr_register_control( $name, $control );
//				} else if ( is_string( $name ) ) {
//					sr_die( "The control [{$name}] has not been defined." );
//				} else  {
//					sr_die( "Unknown control added to Sunrise_Repeating_Field->initialize()." );
//				}
//			}
//			return $new_controls;
//		}
	}
	Sunrise_Controls::on_load();
}
