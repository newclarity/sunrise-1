<?php
if (!class_exists('Sunrise_Control')) {
	/**
	 * @property string $text
	 * @property string $html
	 * @property string $control_id
	 * @property string $control_class
	 * @property string $control_link
	 * @property string $use_hashed_link
	 * @property string $nonce
	 * @property string $input_id
	 * @property string $nonce_input_html
	 */
	class Sunrise_Control extends Sunrise_Base {
		static $link_templates = false;

		var $control_name   = false;
		var $control_type   = false;
		var $control_shape  = false;   // 'link' or 'button'
		var $control_scope  = false;   // 'list' or 'item'
		/**
		 * @var bool|Sunrise_Base
		 */
		var $link_template      = false;
		var $visible            = true;

		protected $_text         = false;
		protected $_nonce        = false;
		protected $_control_link = false;
		/**
		 * @var bool
		 */
		protected $_use_hashed_link = false;

		function __construct( $args = array() ) {

			$args = $this->apply_filters( 'args', $args );

			$args = Sunrise_Controls::normalize_args( $args );

			$this->construct_from_args( $args );

			if ( ! $this->control_shape )
				$this->control_shape = 'link';

			if ( ! $this->control_scope )
				$this->control_scope = 'list';

			$this->do_action( 'initialize', $args );

		}
		function set_text( $text ) {
			$this->_text = $text;
		}
		function get_text() {
			return $this->_text;
		}
		function set_control_link( $control_link ) {
			$this->_control_link = $control_link;
		}
		function set_use_hashed_link( $use_hashed_link ) {
			$this->_use_hashed_link = $use_hashed_link;
			$this->_control_link = false;
		}
		/**
		 * @return bool
		 * TODO (mikes): Decide it this use_hashed_link makes sense. It does not feel right.
		 */
		function get_use_hashed_link() {
			return $this->_use_hashed_link;
		}
		function get_control_link() {
			if ( ! $this->_control_link ) {
				if ( $this->use_hashed_link )
					$this->_control_link = sr_dashize( "#{$this->control_name}" );
				else
					$this->_control_link = sr_get_link( $this->link_template );
			}
			return $this->_control_link;
		}
		/**
		 *
		 * @return bool|string
		 *
		 * TODO (mikes): The control_id should not be cached into a local $this->_control_id property.
		 * TODO: Caching causes wrong control IDs to be generated for Repeating Fields Indent/Outdent controls.
		 */
		function get_control_id() {
			if ( WP_DEBUG && ! method_exists( $this->parent, 'get_ID' ) )
				sr_die( __( '%s must have a parent with an [ID] property.' ), $this->description );
			return $this->apply_filters( 'control_id', sr_dashize( $this->control_name ), array(
				'control' => $this,
			));
		}
		function get_description() {
			return parent::get_description( array(
				'name'  => $this->control_name,
				'type'  => $this->control_type,
				'shape' => $this->control_shape,
				'scope' => $this->control_scope,
				'link'  => $this->control_link,
			), $this->parent );
		}
		function get_nonce() {
			if ( ! $this->_nonce )
				$this->_nonce = wp_create_nonce( $this->input_id );
			return $this->_nonce;
		}
		function get_input_id() {
			return "{$this->control_id}-nonce";
		}
		function get_nonce_input_html() {
			$html = <<<HTML
<input type="hidden" id="{$this->input_id}" value="{$this->nonce}" />
HTML;
			return $html;
		}
		function get_control_class() {
			$hidden = $this->visible ? '' : ' hidden';
			$hashed_link = $this->use_hashed_link || '#' == $this->control_link[0] ? ' hashed-link' : '';
			$class = sr_dashize( "{$this->control_name} {$this->control_shape} {$this->control_type}{$hidden}{$hashed_link}" );
			return $this->apply_filters( 'control_class', $class );
		}
		function get_html() {
			$html = <<<HTML
<a href="{$this->control_link}" id="{$this->control_id}" class="{$this->control_class}">{$this->text}{$this->nonce_input_html}</a>
HTML;
			return $html;
		}
		function __set( $property_name, $value ) {
			switch ( $property_name ) {

				case 'text':
				case 'use_hashed_link':
					call_user_func( array( &$this, "set_{$property_name}" ), $value );
					break;

				case 'html':
				case 'control_id':
				case 'control_link':
				case 'control_class':
				case 'nonce':
				case 'input_id':
				case 'nonce_input_html':
				case 'link':
					$this->_cannot_set_error( $property_name, $value );
					break;

				default:
					parent::__set( $property_name, $value );
					break;
			}
			return $value;
		}
		function __get( $property_name ) {
			$value = false;
			switch ( $property_name ) {
				case 'html':
				case 'text':
				case 'control_link':
				case 'control_id':
				case 'control_class':
				case 'use_hashed_link':
				case 'nonce':
				case 'input_id':
				case 'nonce_input_html':
					$value = call_user_func( array( &$this, "get_{$property_name}" ) );
					break;

				case '_dummy_':
					$this->_cannot_get_error( $property_name );
					break;

				default:
					$value = parent::__get( $property_name, $value );
					break;
			}
			return $value;
		}
	}
}

