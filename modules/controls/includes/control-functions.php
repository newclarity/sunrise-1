<?php

/**
 * Registers a 'control set' for a Sunrise module.
 * These controls will be scoped to the named module and are expected to be used by that
 * module but are accessibe to any other module that wants to access them.
 *
 * @param string $control_set_name - Names like 'microsite-page-controls' and 'repeating-field-controls', etc.
 * @param array $args - Array containing sub-arrays for 'defaults' and 'controls'
 * @return void
 */
function sr_register_control_set( $control_set_name, $args = array() ) {
	global $sr_control_sets;

	if ( ! isset( $sr_control_sets ) )
		$sr_control_sets = array();

	if ( ! isset( $args['defaults'] ) )
		$args['defaults'] = array();

	$args['defaults'] = array_merge( array(
		'control_type'  => 'hyperlink',
		'control_shape' => 'link',
		'control_scope' => 'item',
	), $args['defaults'] );

	if ( ! isset( $args['controls'] ) )
		$args['controls'] = array();

	$sr_control_sets[$control_set_name] = &$args;

}
function sr_has_control_set( $control_set_name ) {
	global $sr_control_sets;
	return isset( $sr_control_sets[$control_set_name] );
}
function sr_get_control_sets() {
	global $sr_control_sets;
	return $sr_control_sets;
}
/**
 * @param string $control_set_name
 * @return bool|array
 */
function sr_get_control_set( $control_set_name ) {
	global $sr_control_sets;
	$control_set = false;
	if ( isset( $sr_control_sets[$control_set_name] ) )
		$control_set = $sr_control_sets[$control_set_name];
	return $control_set;
}
/**
 * @param string $control_set_name
 * @param string $control_name
 * @param array $args
 * @return bool|Sunrise_Control
 */
function sr_get_control_set_control( $control_set_name, $control_name, $args = array() ) {
	$control = false;
	$control_set = sr_get_control_set( $control_set_name );
	if ( ! $control_set )
		sr_die( __( 'The control set [%s] has not been registered.' ), $control_set_name );

	if ( ! isset( $control_set['controls'][$control_name] ) )
		sr_die( __( 'Invalid control. There is no control named [%s] in the control set [%s].' ), $control_name, $control_set_name );

	$control = $control_set['controls'][$control_name];
	if ( is_object( $control ) ) {
		// TODO: This logic path may not be tested...
		$control->construct_from_args( $args );
	} else {
		$defaults = Sunrise_Controls::normalize_args( $control_set['defaults'] );
		$control = Sunrise_Controls::normalize_args( $control );
		$args = Sunrise_Controls::normalize_args( $args );
		/**
		 * @var Sunrise_Control $control
		 */
		$control = sr_array_merge( $control_set['defaults'], $control );
		$control = sr_array_merge( $control, $args );
		$control['control_name'] = $control_name;
		$control = Sunrise_Controls::get_instance_of( $control['control_type'], $control );
	}
	return $control;
}
/**
 * Registers a Control, i.e. a link or a button
 *
 * @param $name
 * @param $args
 * @return Sunrise_Control
 */
function sr_register_control( $name, $args ) {
	global $sr_controls;
	$args['control_name'] = $name;

	$args = Sunrise::normalize_args( $args, array(
		'type' => 'control_type',
	));

//	$control = Sunrise::get_instance_for( $args['control_type'], $args );
	$control = new Sunrise_Control( $args );
	$sr_controls[$name] = $control;

	return $control;
}
/**
 * Returns a control given it's name.
 *
 * @param $name
 * @return bool
 */
function sr_get_control( $name ) {
	global $sr_controls;
	$control = false;
	if ( isset( $sr_controls[$name] ) )
		$control = $sr_controls[$name];
	return $control;
}
/**
 * Returns true is a control with a given name exists.
 *
 * @param $name
 * @return bool
 */
function sr_has_control( $name ) {
	global $sr_controls;
	return isset( $sr_controls[$name] );
}
/**
 * Returns a control given it's name.
 *
 * @param $name
 * @return bool
 */
function sr_clone_control( $name ) {
	if ( $control = sr_get_control( $name ) ) {
		$control = clone $control;
//		$vars = get_class_vars( get_class( $control ) );
//		foreach( $vars as $var_name => $var_value ) {
//			$vars[$var_name] = $control->{$var_name};
//		}
//		Sunrise_Control::initialize( $control, $vars );
	}
	return $control;
}
