<?php
/*
Plugin Name: Sunrise Themes
Plugin URL: http://docs.getsunrise.org/themes/
File Name: sunrise-themes.php
Author: The Sunrise Team
Author URL: http://getsunrise.org
Version: 0.0.1
Description: Adds custom theming functionality to the WordPress
*/

define( 'SUNRISE_THEMES_DIR', dirname( __FILE__ ) );
require( SUNRISE_THEMES_DIR . '/includes/theme-functions.php');
require( SUNRISE_THEMES_DIR . '/includes/sidebars-class.php');