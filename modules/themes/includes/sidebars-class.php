<?php

if ( ! class_exists( 'Sunrise_Sidebars' ) ) {

	class Sunrise_Sidebars {

		private static $count = 0;
		static $sidebars = array();
		static $widgets = array();
		static $sidebar_widgets = array( 'array_version' => 3 );
		static $widget_options = array();
		static $instance_id = false;
		static $instance_number = false;

		static function on_load () {
			sr_add_filter( __CLASS__, array( 'get_sidebar', 'the_sidebar' ) );
			if ( defined( 'SUNRISE_ENABLE_ALTERNATE_WIDGET_SYSTEM' ) && SUNRISE_ENABLE_ALTERNATE_WIDGET_SYSTEM ) {
				sr_add_filter( __CLASS__, 'wp_loaded' );
				sr_add_action( __CLASS__, 'dynamic_sidebar' );
			}
		}

		/**
		 * Sets up the 'pre_option_widget_{$base_id}' action filter so that we can pass in our
		 * own options array for each specific widget instance.  This filter allows us to set
		 * the options prior to WordPress querying the database.
		 * This action hook takes the requested widget instance and does three things:
		 *  1 - Extracts the base id and uses it to call the pre_option_widget_{$base_id} filter
		 *  2 - Sets the instance number to be used as reference in the applied filter
		 *  3 - Sets the instance id to be used as reference in the applied filter
		 *
		 * @static
		 * @param $instance
		 */
		static function dynamic_sidebar ( $instance ) {
			self::$instance_id = $instance['id'];
			self::$instance_number = $number = $instance['params'][0]['number'];
			$base_id = preg_replace( "#-{$number}$#", '', $instance['id'] );
			sr_add_filter( __CLASS__, array( 'pre_option_widget_' . $base_id, 'pre_option_instance' ) );
		}

		/**
		 * Passes in our own options array for the current widget instance.
		 *
		 * @static
		 * @param $instance
		 * @return array
		 */
		static function pre_option_instance ( $instance ) {
			if ( self::$instance_number && self::$instance_id ) {
				$instance = array( self::$instance_number => self::$widget_options[self::$instance_id] );
				self::$instance_id = false;
				self::$instance_number = false;
				remove_filter( current_filter(), array( __CLASS__, current_filter() ) );
			}
			return $instance;
		}

		/**
		 * Actually overrides the global WordPress variables that control which sidebars and
		 * widgets are loaded.
		 *
		 * @static
		 */
		static function wp_loaded () {
			global $wp_registered_widgets, $wp_registered_sidebars, $_wp_sidebars_widgets;
			$wp_registered_sidebars = self::$sidebars;
			$_wp_sidebars_widgets = self::$sidebar_widgets;
			$wp_registered_widgets = array();
			foreach ( self::$widgets as $widgets ) {
				foreach ( $widgets as $widget ) {
					$wp_registered_widgets[$widget['id']] = $widget;
				}
			}
		}

		/**
		 * Registers a Sunrise sidebar
		 *
		 * @static
		 * @param       $sidebar_name
		 * @param array $args
		 */
		static function register_sidebar ( $sidebar_name, $args = array() ) {
			self::$sidebars[$sidebar_name] = $args;
		}

		/**
		 * Registers a Sunrise widget.  This should never be called directly.
		 *
		 * @static
		 * @param       $widget_class
		 * @param array $args
		 * @return string
		 */
		static function register_widget ( $widget_class, $args = array() ) {
			$widget_instance = new $widget_class();
			$id_base = $widget_instance->id_base;
			self::$widgets[$id_base] = isset( self::$widgets[$id_base] ) ? self::$widgets[$id_base] : array();
			$number = count( self::$widgets[$id_base] ) + 1;
			$id = $id_base . '-' . $number;
			$widget = array(
				'id'       => $id,
				'name'     => $widget_instance->name,
				'callback' => array( $widget_instance, 'display_callback' ),
				'params'   => array(
					array(
						'number' => $number,
					),
				),
			);
			self::$widget_options[$id] = $args;
			$widget = array_merge( $widget, $widget_instance->widget_options );
			do_action( 'wp_register_sidebar_widget', $widget );
			self::$widgets[$id_base][] = $widget;
			return $id;
		}

		/**
		 * Registers a specific instance of a Sunrise widget to a Sunrise sidebar.
		 *
		 * @static
		 * @param       $sidebar_name
		 * @param       $widget_class
		 * @param array $args
		 */
		static function register_sidebar_widget ( $sidebar_name, $widget_class, $args = array() ) {
			self::$sidebar_widgets[$sidebar_name][] = self::register_widget( $widget_class, $args );
		}

		/**
		 * Unregister a specific widget from a specific sidebar
		 *
		 * @static
		 * @param string $sidebar_name The name of the sidebar in which the widget displays
		 * @param string $id           The HTML id of the output widget
		 */
		static function unregister_sidebar_widget ( $sidebar_name, $id ) {
			$sidebars = & self::$sidebar_widgets;
			if ( isset( $sidebars[$sidebar_name] ) ) {
				$key = array_search( $id, $sidebars[$sidebar_name] );
				if ( $key !== false ) {
					unset( $sidebars[$sidebar_name][$key] );
				}
			}
		}

		/**
		 * Allows sidebar.php to serve as the default template for all sidebars.
		 * More specific sidebar files will override sidebar.php.  By default, WordPress doesn't
		 * allow sidebar.php to be loaded more than once.  Since we are using it as a default
		 * for potentially multiple sidebars on a single page, this function is setup to load
		 * sidebar.php again as needed.
		 *
		 * @static
		 * @param bool $sidebar_name
		 */
		static function the_sidebar ( $sidebar_name = false ) {
			global $sr_current_sidebar;

			$template = false;
			if ( isset( $sidebar_name ) ) {
				if ( ! $template = locate_template( array( "sidebar-{$sidebar_name}.php" ) ) ) {
					self::$count ++;
				}
			} else {
				$sidebar_name = apply_filters( 'sr_default_sidebar', 'primary' );
			}

			$sr_current_sidebar = apply_filters( 'sr_current_sidebar', $sidebar_name );

			/**
			 * The get_sidebar() function forces require_once().  We need to detect if
			 * sidebar.php has already been loaded or not.  If so, we need to load it again.
			 */
			if ( ! $template && self::$count > 0 ) {
				locate_template( 'sidebar.php', true, false );
			}
		}

	}

	Sunrise_Sidebars::on_load();

}