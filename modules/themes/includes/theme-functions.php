<?php

function sr_register_sidebar( $sidebar_name, $args = array() ) {
	Sunrise_Sidebars::register_sidebar( $sidebar_name, $args );
}

function sr_register_sidebar_widget( $sidebar_name, $widget_class, $args = array() ) {
	Sunrise_Sidebars::register_sidebar_widget( $sidebar_name, $widget_class, $args );
}

function sr_unregister_sidebar_widget( $sidebar_name, $id ) {
	Sunrise_Sidebars::unregister_sidebar_widget( $sidebar_name, $id );
}