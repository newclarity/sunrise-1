<?php  // http://wp31.dev/wp-content/plugins/sunrise-urls/test/test.php

return;

define( 'WP_DEBUG', false );

$sr_root_path = getcwd() . "/../../../..";
require( "{$sr_root_path}/sr-core/test/support.php" );

Sunrise_Urls::$fallback = false;
register_url_path( '%category_name%/%name%' );
register_url_path( '%pagename%' );

$sr_test['has_errors'] = false;

test_url_path( '/' );

// TODO: Need to make sure these pages are in the database
test_url_path( '/parent/child/grandchild/' );
test_url_path( '/parent/child/grandchild', array(
	'match' => array(
		'_new_path' => '/parent/child/grandchild/',
		'page' => '',
		'pagename' => 'parent/child/grandchild',
		),
));

if ( $sr_test['has_errors'] ) {
	echo "\nTests Failed";
} else {
	echo 'ok';
}

function test_url_path( $url_path, $args = array() ) {
	global $wp;

	$args = wp_parse_args( $args, array(
		'match'     => false,
	));
	extract( $args );

	$debug = defined( 'WP_DEBUG' ) && WP_DEBUG;
	$_SERVER['REQUEST_URI'] = $url_path;

	if ( $debug )
		echo "PATH: {$url_path}\n\n";

	if ( ! $match ) {
		$wp = new WP();
		$wp->parse_request();
		$wp_qv = $wp->query_vars;
		if ( $debug ) {
			echo "===[WordPress]========\n";
			query_vars_dump( $wp_qv );
			echo "\n";
		}
	}
	$wp = new Sunrise_Urls_WP();
	$wp->parse_request();
	$sr_qv = $wp->query_vars;
	if ( $debug ) {
		echo "===[Sunrise URLs]========\n";
		query_vars_dump( $sr_qv );
		echo "\n";
	}

	if ( ! $match ) {
		dump_delta( 'Missing from Sunrise:', $wp_qv, $sr_qv, false );
		dump_delta( 'Missing from WordPress:', $sr_qv, $wp_qv, false );
	}

	if ( is_array( $match ) ) {
		dump_delta( 'Mismatch - Expected vs. Actual:', $match, $sr_qv, true );
		dump_delta( 'Mismatch - Actual vs. Expected:', $sr_qv, $match, true );
	}
}
function dump_delta( $description, $qv1, $qv2, $must_match ) {
	$delta = array_merge( array_diff( $qv1, $qv2), array_diff_key( $qv1, $qv2 ) );
	if ( count( $delta ) ) {
		global $sr_test;
		echo "===[{$description}]========\n";
		query_vars_dump( $delta );
		echo "\n";
		if ( $must_match ) {
			$sr_test['has_errors'] = true;
		}
	}
}
function query_vars_dump( $query_vars ) {
	foreach( $query_vars as $name => $value ) {
		echo "[{$name}] => {$value}\n";
	}
}
