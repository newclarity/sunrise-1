<?php

if ( ! class_exists( 'Sunrise_Url_Node' ) ) {
	/**
	 * @property bool|callback $get_list
	 * @property bool|callback $validate
	 * @property array         $query_vars
	 * @property bool          $trailing_slash
	 */
	class Sunrise_Url_Node extends Sunrise_Base {

		var $matched_pattern = false; // The pattern used to match $this->matched_segment
		var $matched_segment = ''; // The path that matched the $this->pattern
		var $parent_node = false; // The parent node that was matched
		var $child_segments = array(); // The child path segments that are potential at this level
		var $multi_segment = false; // Is this a multi-segment (multi-backslash) node such as for pages with subpages?
		var $inherit = false; // Inherit query vars from prior path segments?
		var $regex = false; // regex that allows for matching
		var $match = false; // Callable for matching URL segment

		/**
		 * @var bool|void
		 */
		private $_trailing_slash = null; // Will this node have a trailing slash? Really only matters on last segment
		/**
		 * @var bool|callback
		 */
		private $_query_vars = array(); // The list of query vars

		function __construct ( $query_vars = array() ) {
			$this->query_vars = $query_vars;
		}

		static function _locate_callable ( $callable ) {
			if ( ! is_callable( $callable ) && false !== strpos( $callable, '::' ) ) {
				foreach ( array( 'Sunrise_Url_Helpers' ) as $class ) {
					if ( method_exists( $class, $callable ) ) {
						$callable = array( $class, $callable );
						break;
					}
				}
			}
			return $callable;
		}

		function match_path_segment ( $path_segment ) {
			$matched = false;
			if ( $this->match && is_callable( $this->match ) ) {
				$query_vars = $this->query_vars;
				if ( call_user_func_array( $this->match, array( $this->_get_args( $path_segment ), &$query_vars ) ) ) {
					$matched = true;
					$this->query_vars = $query_vars;
				}
			} else if ( $this->regex ) {
				if ( preg_match( "#^{$this->regex}$#", $path_segment ) ) {
					$matched = true;
				}
			}
			return $matched;
		}

		function _expand_query_vars ( $path_segment = false, $args = false ) {
			if ( ! $args ) {
				$args = $this->_get_args( $path_segment );
			}
			foreach ( $this->query_vars as $var_name => $var_value ) {
				foreach ( $args as $arg_name => $arg_value ) {
					if ( strpos( $var_value, "%{$arg_name}%" ) !== false ) {
						$this->_query_vars[$var_name] = str_replace( "%{$arg_name}%", $arg_value, $var_value );
					}
				}
			}
			return;
		}

		function _get_args ( $path_segment = false ) {
			$index = Sunrise_Urls::$index;
			$path_segments = Sunrise_Urls::$path_segments;
			if ( ! $path_segment ) {
				$path_segment = $path_segments[$index];
			}

			$args = array( 'this' => $path_segment );

			/**
			 * Remove the path segment to test (which could be a multi-segment) and replace with a single dummy segment
			 */
			$path_segments = implode( '/', $path_segments );
			$path_segments = trim( str_replace( "/{$path_segment}/", '/~/', "/{$path_segments}/" ), '/' );
			$path_segments = explode( '/', $path_segments );

			// Now add the other args, as appropriate
			if ( 0 < $index ) {
				$args['parent'] = $path_segments[$index - 1];
			}
			if ( 1 < $index ) {
				$args['grandparent'] = $path_segments[$index - 2];
			}
			if ( 2 < $index ) {
				$args['greatgrandparent'] = $path_segments[$index - 3];
			}
			if ( $index + 1 < count( $path_segments ) ) {
				$args['child'] = $path_segments[$index + 1];
			}
			if ( $index + 2 < count( $path_segments ) ) {
				$args['grandchild'] = $path_segments[$index + 2];
			}
			if ( $index + 3 < count( $path_segments ) ) {
				$args['greatgrandchild'] = $path_segments[$index + 3];
			}

			$args['ordered'] = $args['offset'] = array();
			for ( $segment = 0; $segment < count( $path_segments ); $segment ++ ) {
				$offset = $segment - $index;
				$args['ordered'][$segment] = $args['offset'][$offset] = $segment == $index ? $path_segment : $path_segments[$segment];
			}

			return $args;
		}

		function __get ( $name ) {
			$value = null;
			switch ( $name ) {
				case 'query_vars':
					$value = & $this->_query_vars;
					break;
				case 'get_list':
				case 'validate':
					$property = "_{$name}";
					$value = $this->$property;
					$is_immediate_func = $value[0] == '%';
					$is_delayed_func = $is_immediate_func ? false : ! empty( $value );
					if ( $is_immediate_func || $is_delayed_func ) {
						$callable = self::_locate_callable( $value );
						if ( $is_delayed_func ) {
							$value = $callable;
						} else if ( $is_immediate_func ) {
							$value = call_user_func( $callable, $this->_get_args() );
						}
					}
					break;
				case 'trailing_slash':
					if ( is_null( $this->_trailing_slash ) ) {
						$last_segment = Sunrise_Urls::$path_segments[count( Sunrise_Urls::$path_segments ) - 1];
						$this->_trailing_slash = strpos( $last_segment, '.' ) === false;
					}
					$value = $this->_trailing_slash;
					break;
			}
			return $value;
		}

		function __set ( $name, $value ) {
			switch ( $name ) {
				case 'query_vars':
					/*
					 * Convert query vars that are prefixed with '@' into properties of the Query Vars object
					 * This is a bit unorthodox, allowing other properties to be set by assigning one property
					 * specially formatted array, but this makes specification very easy and this class it
					 * very purpose built; it is not meant for reuse in other areas.
					 */
					foreach ( $value as $attribute_name => $property_value ) {
						if ( $attribute_name[0] == '@' ) {
							$property_name = substr( $attribute_name, 1 ); // Strip off the '@'
							if ( property_exists( $this, $property_name ) ) {
								$this->$property_name = $property_value;
							} else if ( property_exists( $this, "_{$property_name}" ) ) {
								$property_name = "_{$property_name}";
								$this->$property_name = $property_value;
							} else {
								sr_die(
									"Attempting to set a property named '{$property_name}' and " . __CLASS__ . ' does not contain that property.'
								);
							}
							// Remove it from the $query_vars array
							unset( $value[$attribute_name] );
						}
					}
					// Assign the remaining real query vars to the query_vars property.
					$this->_query_vars = $value;
					break;
			}
		}
	}
}
