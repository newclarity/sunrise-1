<?php

if ( ! class_exists( 'Sunrise_Url_Helpers' ) ) {
	class Sunrise_Url_Helpers {
		static function match_category_slug( $args ) {
			$term = get_term_by('slug', $args['this'], 'category' );
			return $term !== false;
		}
		static function match_post_in_category( $args, &$query_vars ) {
			global $wpdb;
			$query = new WP_Query( array( 'name' => $args['this'] ) );
			$matched = false;
			if ( $query->post_count == 1 ) {
				$category_slug = $args['parent'];
				$term = get_term_by('slug', $category_slug, 'category' );
				$sql = "SELECT ID FROM {$wpdb->posts} p INNER JOIN {$wpdb->term_relationships} tr ON p.ID = tr.object_id INNER JOIN {$wpdb->term_taxonomy} tt ON tt.term_taxonomy_id = tr.term_taxonomy_id WHERE tt.term_id = %d AND p.ID = %d";
				$matched = $query->post->ID == $wpdb->get_var( $wpdb->prepare( $sql, $term->term_id, $query->post->ID ) );
			}
			return $matched;
		}
		static function get_post_type_by_slug( $args ) {
			$post_type_archive_slugs = array_flip( self::get_post_type_archive_slug_list( $args ) );
			if ( ! isset($post_type_archive_slugs[ $args['this'] ]) )
				return false;
			else
				return $post_type_archive_slugs[ $args['this'] ];
		}
		static function get_page_list( $args ) { //TODO: Make this work with child pages and private posts or other ones that should match
			global $wpdb;
			static $parents = array();
			$parent_path = $args['parent'];
			if ( ! isset( $parents[$parent_path] ) ) {
				$parent_id = 0;
				if ( ! empty( $parent_path ) ) {
					$parts = explode( '/', $parent_path );
					foreach( $parts AS $index => $part ) {
						$sql = "SELECT ID FROM {$wpdb->posts} WHERE post_name='%s' AND post_parent=%d AND post_type='page' AND post_status='publish'";
						$parent_id = $wpdb->get_var( $wpdb->prepare( $sql, $part, $parent_id ) );
					}
				}
				$parents[ $parent_path ] = $parent_id;
			}
			// TODO: We will cache these after the logic is robust
			$sql = "SELECT post_name FROM {$wpdb->posts} WHERE post_parent={$parents[$parent_path]} AND post_type='page' AND post_status='publish'";
			$page_names = $wpdb->get_col($sql);
			return $page_names;
		}
		static function get_post_type_archive_slug_list( $args ) {
			global $wp_post_types;
			static $post_type_archive_slugs;
//			if ( ! isset( $post_type_archive_slugs ) ) {
				$post_type_archive_slugs = array();
				foreach( array_keys( $wp_post_types ) as $post_type) {
					$slug = get_post_type_archive_link( $post_type );   // TODO: This is missing because of wp31
					if ( $slug ) {
						$slug = end( explode( '/', trim( $slug, '/' ) ) );
						$post_type_archive_slugs[$post_type] = $slug;
					}
				}
//			}
			return $post_type_archive_slugs;
		}
		static function match_page_slug( $args, &$query_vars ) { //TODO: Make this work with child pages and private posts or other ones that should match
			$page = get_page_by_path( $args['this'] );
			return ( is_object( $page ) );
		}
		static function match_post_type_slug( $args, &$query_vars ) {
			$post_type_archive_slugs = array_flip( self::get_post_type_archive_slug_list( $args ) );
			return isset( $post_type_archive_slugs[ $args['this'] ] );
		}
		static function match_post( $args, &$query_vars ) {
			global $wp_post_types;
			$matched = false;
			$post_type_slug = $args['parent'];
			if ( $post_type_slug = self::get_post_type_by_slug($post_type_slug)) {
				global $wpdb;
				$post_type_object = $wp_post_types[$post_type_slug];
				$sql = "SELECT COUNT(*) AS match_count FROM {$wpdb->posts} WHERE post_parent=0 AND post_type='{$post_type_slug}' AND post_name='%s' AND post_status='publish'";
				$sql = $wpdb->prepare($sql,$post_name);
				$match = $wpdb->get_var($sql);
				$matched = ($match>0);
			}
			return $matched;
		}
	}
}
