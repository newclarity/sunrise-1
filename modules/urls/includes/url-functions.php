<?php
/*
 * Globals: $sr_urls: Array with keys 'root', 'paths', 'query_vars', 'expansion_vars', 'suffixes'
 *
 * TODO: Add support for Expansion Vars and URL suffixes
 */


function sr_register_query_var( $var, $args = array() ) {
	global $sr_urls;

	if ( false !== strpos( $var, '%' ) )
		$var = str_replace( '%', '', $var );

	$defaults = array();

	switch ( $var ) {
		case 'name':
			$defaults = array(
				'@inherit'      => true,          // WordPress does not (typically?) have any other query vars for posts
				'name'          => '%this%',
				//'page'          => '',          // This is what WordPress has for a /year/mon/day/post URL

			);
			break;

		case 'category_name':
			$defaults = array(
				'@validate'       => 'is_category_slug',
				'category_name'   => '%this%',
			);
			break;

		case 'pagename':
			$defaults = array(
				'@validate'       => 'is_page_slug',
				'@multi_segment'  => true,
				'page'            => '',          // This is match WordPress' behavior
				'pagename'        => '%this%',
			);
			break;

		case 'post_type_slug':
			$defaults = array(
				'@match'     => 'match_post_type_slug',
				'post_type'  => '%post_type_slug%',
			);
			break;

		case 'year':
			$defaults = array(
				'@regex'       => '([0-9]{4})',
				'year'          => '%this%',
				'post_type'     => 'post',
			);
			break;
	}

	if ( count( $args ) == 0 )
		$args = $defaults;
	else
		$args = wp_parse_args( $args, $defaults );

	$var = "%{$var}%";
	$qv = &$sr_urls['query_vars'];
	$qv[$var] = isset( $qv[$var] ) ? array_merge( $qv[$var], $args ) : $args;

	return $args;
}
function sr_register_url_path( $path, $query_vars = array() ) {
	global $sr_urls;

	// Register the query vars for this path.
	__sr_register_query_vars_from_path( $path );

	// Split URL path on '/' into 'path segments'
	$path_segments = explode( '/', trim( $path, '/' ) );

	// Create a local reference to the anchoring 'root' URL
	$root = &$sr_urls['root'];

	// For each path segment
	$parent_path = array();
	foreach( $path_segments as $path_segment ) {

		$parent_path[] = $path_segment; // Grab this for defining parent path of subnodes

		// Grab the query predefined for this path segment, assuming it is full a query_var
		$this_path_segment_query_vars = isset( $sr_urls['query_vars'][$path_segment] ) ? $sr_urls['query_vars'][$path_segment] : array();

		// If this post segment for this URL route does not already has some query vars defined
		if ( ! isset ( $root->child_segments[$path_segment] ) ) {
			// Set them.
			$root->child_segments[$path_segment] = new Sunrise_Url_Node( $this_path_segment_query_vars );

		}

		// Set the child to be the root and continue down the URL path tree.
		$root = &$root->child_segments[$path_segment];

	}

	// For the last path segment, merge the path segment's default query vars with the ones passed to this function taking the passed ones as prioprity.
	$query_vars = array_merge( $this_path_segment_query_vars, $query_vars );

	// Now look for the default query vars for this path, merge them with the ones passed to this function taking the passed ones as priority.
	$root->query_vars = __sr_register_url_path( $path, $query_vars );

	// Finally set the query_vars for this path as well as for this segment.
	$sr_urls['paths'][$path] = $root->query_vars;
}
function __sr_register_url_path( $path, $query_vars = array() ) {

	$defaults = array();

	switch ( $path ) {

		case '%category_name%/%name%':
			$defaults = array(
				'@match'     => 'match_post_in_category',
				'@inherit'   => true,
			);
			break;

		case '%post_type_slug%/%name%':
			$defaults = array(
				'@match'        => 'match_post',
				'@get_list'     => 'get_post_name_list',
			);
			break;

		case '%year%':
			$defaults = array(
				'@regex'      => '[0..9]{4}',
				'year'          => '%this%',
				'post_type'     => 'post',
			);
			break;

		case '%year%/%monthnum%':
			$defaults = array(
				'@regex'      => '(0[1..9]|[1][0..2])',
				'monthnum'      => '%this%',
			);
			break;

		case '%year%/%monthnum%/%day%':
			$defaults = array(
				'@match'  => 'match_day',
				'day'     => '%this%',
			);
			break;

		case '%year%/%monthnum%/%day%/%name%':
			$defaults = array(
				'@match'     => 'match_post_for_date',
				'post_name'  => '%this%',
			);
			break;

		case '%pagename%':
			$defaults = array(
				'@multi_segment'=> true,
				'@match'        => 'match_page',
		  );
			break;

		case 'robots.txt':
			$defaults = array(
				'robots'        => 1,
			);
			break;

	}

	if ( count( $query_vars ) == 0 )
		$query_vars = $defaults;
	else
		$query_vars = array_merge( $defaults, $query_vars );

	return $query_vars;
}
function __sr_register_query_vars_from_path( $path ) {
	global $sr_urls;
	preg_match_all( '#%([^%]+)%#', $path, $matches, PREG_SET_ORDER );
	foreach( $matches as $match ) {
		if ( ! isset( $sr_urls[$match[0]] ) ) // Only register if not already registered. Why do twice?
			sr_register_query_var( $match[0] );
	}
}
function sr_register_expansion_var( $var, $args = array() ) {
	global $sr_urls;
	$sr_urls['expansion_vars'][$var] = $args;
}
function sr_register_url_suffix( $suffix, $args = array() ) {
	global $sr_urls;
	$sr_urls['suffixes'][$suffix] = $args;
}


//function sr_register_query_var( $var, $match = false ) {
//	global $sr_urls;
//
//	if ( false !== strpos( $var, '%' ) )
//		$var = str_replace( '%', '', $var );
//
//	if ( ! $match ) {
//		switch ( $var ) {
//			case 'category_name':
//				$match = 'match_category_name';
//				break;
//
//			case 'pagename':
//				$match = array (
//					'match'         => 'match_pagename',
//					'multi_segment' => true,
//					);
//				break;
//
//			case 'post_type_slug':
//				$match = 'match_post_type_slug';
//				break;
//
//			case 'year':
//				$defaults = array(
//					'match'     => '([0-9]{4})',
//					'post_type' => 'post',
//				);
//				break;
//		}
//	}
//	if ( is_callable( array( 'Sunrise_Url_Helpers', $match ) ) )
//		$match = array( 'Sunrise_Url_Helpers', $match );
//
//	if ( is_callable( $match ) )
//		$match = array( 'match' => $match );
//
//	$args = 0 == count( $args ) ? $defaults : wp_parse_args( $args, $defaults );
//
//	$var = "%{$var}%";
//	$qv = &$sr_urls['query_vars'];
//	$qv[$var] = isset( $qv[$var] ) ? array_merge( $qv[$var], $args ) : $args;
//
//	return $args;
//}
//
//
