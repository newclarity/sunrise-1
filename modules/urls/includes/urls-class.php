<?php

if ( ! class_exists( 'Sunrise_Urls' ) ) {
	class Sunrise_Urls {
		/*
		 * If self::$fallback
		 *   ===true then Sunrise_Urls_WP->parse_request() will fallback to call WP->parse_request()
		 *   ===false then Sunrise_Urls_WP->parse_request() will issue a 404 is sr_parse_request returns false.
		 */
		static $fallback = true;
		/*
		 * self::$path_segments - Holds the exploded path segments from $_SERVER['REQUEST_URI'] during and after inspection
		 */
		static $path_segments = false;
		/*
		 * self::$index - Hold the index of the path segment during inspection, starting with 0.
		 * Reset to false at end of Sunrise_Urls::sr_parse_request().
		 */
		static $index = false;

		static $result = false;

		static function on_load() {
			sr_add_filter( __CLASS__, 'sr_parse_request', 10, 2 );
			/**
			 * TODO: Verify if this should be 0. Why? Run before all other initializations?
			 */
			sr_add_action( __CLASS__, 'init', 0 );
		}

		static function init() {
			global $sr_urls;
			$sr_urls = array(
				'root' => new Sunrise_Url_Node(),
				'paths' => array(),
				'query_vars' => array(),
				'expansion_vars' => array(),
				'suffixes' => array(),
			);
		}
		static function sr_parse_request( $do_default, $extra_query_vars = '' ) {
			global $wp;
			if ( ! is_array( $wp->query_vars ) )
				$wp->query_vars = array();

			global $sr_urls;
			$matched = $must_match = false;

			if ( self::$path_segments ) {
				self::$index = self::$path_segments = false;
			}

			$path = esc_url( $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );
			$path = str_replace( esc_url( $_SERVER['HTTP_HOST'] ), '', $path );
			list( $path, $query ) = explode( '?', "{$path}?" );

			if ( !empty( $query ) ) {
				$query = preg_replace('/&#038;/', '&', $query);
				parse_str( $query, $query );
			}

			/**
			 *  TODO (mikes): This is a quick hack to enable search to work.
			 *  TODO (mikes): Need to update Sunrise URLs to support architecture
			 */
			if ( isset( $query['s'] ) || isset( $query['p'] ) ) {

				$matched = false;

			} else if ( empty( $path ) ||  $path == '/' ) {

				$wp->query_vars = array(); // Root is blank;
				$matched = true;

			} else {

				self::$path_segments = explode( '/', trim( $path, '/' ) );

				$node = $sr_urls['root'];
				for( self::$index = 0; self::$index < count( self::$path_segments ); self::$index++  ) {
					$this_path_segment = self::$path_segments[self::$index];
					$node = self::_match_path_segment( $node );
					if ( ! $node ) {
						$matched = false;
						break;
					} else {
						$matched = $must_match = true;
						if ( $node->inherit )
							$wp->query_vars = array_merge( $wp->query_vars, $node->query_vars );
						else
							$wp->query_vars = $node->query_vars;
						$wp->query_vars[preg_replace( '#%([^%]+)%#', '$1', $node->matched_pattern )] = $node->matched_segment;
					}
				}
			}
			if ( $matched ) {
				if ( substr( $path, -1, 1 ) != '/' && $node->trailing_slash ) {
					$new_path = "{$path}/" . ( strlen( $query ) == 0 ? '' : "?{$query}" );
					if ( ! defined( 'SUNRISE_UNIT_TEST' ) ) {
						wp_safe_redirect( home_url() . $new_path, 301 );
					} else {
						$wp->query_vars['_new_path'] = $new_path;
						return $matched;
					}
					exit;
				}
			} else if ( $must_match ) {
				$wp->query_vars = array( 'error' => '404' );
			}
			do_action('parse_request', $wp);  // Mirror default WordPress

			if ( $matched ) {
				remove_action('template_redirect','redirect_canonical');  // TODO: Fix canonical redirects
			}
			return $matched;
		}
		/**
		 * @param Sunrise_Url_Node $parent_node
		 * @return bool|Sunrise_Url_Node
		 */
		static function _match_path_segment( $parent_node ) {
			$matched = false;
			$path_segment = self::$path_segments[self::$index];

			/**
			 * @var Sunrise_Url_Node $child_node
			 */
			foreach( $parent_node->child_segments as $match_segment => $child_node ) {

				if ( ! preg_match( '#%#', $match_segment ) ) { // Is it a literal? (vs a %var%)

					if ( $path_segment == $match_segment ) {
						$matched = true;
						break;
					}

				} else //TODO: Make work for partial segments, i.e. /x-%foo%-y/
				if ( preg_match( '#^%.+%$#', $match_segment, $the_match ) ) {

					if ( $child_node->match_path_segment( $path_segment ) ) {
						$matched = true;
						break;
					}

				}
			}

			// To this point $matched is boolean. If $matched==true then we convert to an object of class Sunrise_Url_Node to be the new $root.
			if ( $matched ) {

				$matched = &$child_node;

				if ( ! $child_node->multi_segment ) {

					$child_node->_expand_query_vars( $path_segment );

				} else {

					// This is for multi-segment matches; scan through to see if their is a match. Start with longest first.
					$start_count = $segment_count = count( self::$path_segments );
					while ( self::$index < $segment_count ) {
						$path_segment = implode( '/', array_slice( self::$path_segments, self::$index, $segment_count ) );
						if ( $child_node->match_path_segment( $path_segment ) ) {
							$child_node->_expand_query_vars( $path_segment );
							break;
						}
						$segment_count--;
					}
					if ( self::$index == $segment_count ) {
						$matched = false;
					} else if ( $start_count == $segment_count ) {
						self::$index += $start_count;                 // All remaining segments were used.
					} else {
						self::$index += $segment_count - 1;           // Decrement by one so when outer function's loop increments it will be in line.
					}

				}
				$matched->matched_pattern = $match_segment;
				$matched->parent_node = &$parent_node;
				$matched->matched_segment = $path_segment;
			}

			return $matched;

		}
	}
	Sunrise_Urls::on_load();
}
