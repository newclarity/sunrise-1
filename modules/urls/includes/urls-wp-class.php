<?php
/*
Include File: Yes
Filename: urls-wp-class.php
Description: Extends the WP class in WordPress and assigns an instance to the global $wp variable.
Notes: This is needed because WordPress does not (yet?) have a hook for $wp->parse_request() as proposed in trac ticket #XXXXX
*/

if ( !class_exists( 'Sunrise_Urls_WP' ) ) {
	class Sunrise_Urls_WP extends WP {
		static function on_load() {
			// 'setup_theme' is 1st hook run after WP is created.
			add_action('setup_theme',array(__CLASS__,'setup_theme'));
			add_filter('template_include',array(__CLASS__,'template_include'));
		}
		static function setup_theme() {
			global $wp;
			$wp = new Sunrise_Urls_WP();  // Replace the global $wp
		}
		static function template_include( $template ) {
			if ( defined( 'SUNRISE_URLS_DEBUG' ) ) {
				//TODO: Come up with a better way to handle this
				$result = Sunrise_Urls::$result;
				echo "<div id=\"sunrise-urls-result\">Sunrise URLs Result: {$result}</div>";
			}
			return $template;
		}
		function parse_request( $extra_query_vars = '' ) {
			do_action( 'sr_urls_init' );
			if ( apply_filters( 'sr_parse_request', false, $extra_query_vars ) ) {
				Sunrise_Urls::$result = 'routed';
			} else if ( isset( $this->query_vars['error'] ) ) {
				Sunrise_Urls::$result = 'failed';
			} else {
				Sunrise_Urls::$result = 'fallback';
				if ( Sunrise_Urls::$fallback ) {
					parent::parse_request($extra_query_vars); // Delegate to WP class
				} else {
					sr_die( 'Sunrise URL routing failed.' );
				}
			}
		}
	}
	Sunrise_Urls_WP::on_load();
}
