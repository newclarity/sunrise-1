<?php
/*
Filename: sunrise-urls.php
Plugin Name: Sunrise URLs
Plugin URL: http://getsunrise.com/plugins/urls/
Description: Robust and Flexible URL Routing replacement for WordPress' URL rewrite system of regular expression matching.
Author: The Sunrise Team
Author URL: http://getsunrise.com/
Version: 0.0.3
Requires: n/a

TODO (mikes 2011-11-05): Sunrise URLs is 1st generation and really needs a rearchitecture to make it:
TODO: 1.) easier to use,
TODO: 2.) more flexible,
TODO: 3.) easier to maintain, and to
TODO: 4.) follow standards learned when building Sunrise Fields and Sunrise Microsites.

TODO: So.... Don't do a lot of work with this architecture, as it WILL change.

TODO (mikes): OPTIMIZATION: Test paths from leaf node to root node instead of root to leaf and allow
TODO: short circuit of parent path testing since testing at the root may be 100% sufficient to fully
TODO: validate URL. See Microsite path of /microsite-post/microsite-page/related-post/ for use-case.

*/
/**
 * TODO (mike): Determine if we can avoid loading (any of?) this if in the admin?
 */
// Answer this: http://wordpress.stackexchange.com/questions/12074/multiple-taxonomies-on-same-permalink-rule
// Answer this: http://wordpress.stackexchange.com/questions/12843/priority-for-categories-tags-taxonomies-posts-pages-and-custom-posts-with-sam

define( 'SUNRISE_URLS_DIR', dirname( __FILE__ ) );
require( SUNRISE_URLS_DIR . '/includes/url-functions.php' );
require( SUNRISE_URLS_DIR . '/includes/urls-class.php' );
require( SUNRISE_URLS_DIR . '/includes/urls-wp-class.php' );
require( SUNRISE_URLS_DIR . '/includes/url-node-class.php' );
require( SUNRISE_URLS_DIR . '/includes/url-helpers-class.php' );

//sr_load_plugin_files( 'urls', array( 'whatever' ) );

sr_load_demos( SUNRISE_URLS_DIR );