<?php
/*
Plugin Name: Sunrise Query
Plugin URL: http://docs.getsunrise.org/query/
Author: The Sunrise Team
Author URL: http://getsunrise.org
Version: 0.1
Description: Extends WP_Query() both in features (for posts) and scope (for users, taxonomy terms, etc.)
Globals Exposed:
Hooks Exposed:
*/
if (!class_exists('Sunrise_Query')) {
	class Sunrise_Query extends WP_Query { // TODO: Decide is we can be uber-query if we extend it
		function __construct( $args = array() ) {

			$args = wp_parse_args( $args, array(
				'object_type' => 'post',  // Also 'user', 'taxonomy', 'taxterm', 'blog', 'site', 'value'
			) );
			switch ( $args[ 'object_type' ] ) {
				case 'post':
					parent::__construct( $args );
					break;
				default:
					break;
			}
		}
	}
}
